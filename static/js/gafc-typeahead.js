normalize = function (s) {
    // return regular expresion
    var r=s.toLowerCase();
    //r = r.replace(new RegExp(/\s/g),"");
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/æ/g),"ae");
    r = r.replace(new RegExp(/ç/g),"c");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/ñ/g),"n");                
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/œ/g),"oe");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");
    r = r.replace(new RegExp(/[ýÿ]/g),"y");
    //r = r.replace(new RegExp(/\W/g),"");
    return r;
}

$(function() {

    $('.gafc-typeahead-search').each(function() {
    
        const blood = new Bloodhound({
          initialize: false,
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('normalized'),
          queryTokenizer: function(q) { return Bloodhound.tokenizers.whitespace(normalize(q)); },
          identify: function(obj) { return obj.value; },
          prefetch: {url: window.location.protocol+'//'+window.location.host+$(this).attr('typeahead-source'), 
                     cache: false,
                     ttl: 60000, // 1 min caching
                     }
        });
        
        
        const par = $(this).parents('.input-group')
        const valinput = par.find('.gafc-typeahead-value')
        const type = $(this)
    
        $(this).typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        },
        {
            display: 'display',
            source: blood,
            limit: 15
        }).keydown(function(key) {
            if(key.originalEvent.key.length == 1) {
                valinput.val('')
                $(this).removeClass('is-valid').addClass('is-invalid')
                
                if($(this)[0].hasAttribute('typeahead-callback-invalid')) {
                    eval($(this).attr('typeahead-callback-invalid'))
                }
            }
            if(key.keyCode == 13) {
                  key.preventDefault();
                  return false;
                }
        }).bind('typeahead:select', function(ev, obj) {
            valinput.val(obj.value)
            $(this).removeClass('is-invalid').addClass('is-valid')
            
            if($(this)[0].hasAttribute('typeahead-callback')) {
                eval($(this).attr('typeahead-callback'))
            }
        });
        
        const promise = blood.initialize();
        
        promise
        .done(function() {
            if(valinput.val() != '' && valinput.val() != undefined) {
                obj = blood.get([valinput.val()])
                if(obj.length > 0) {
                    type.typeahead('val', obj[0]['display'])
                    type.removeClass('is-invalid').addClass('is-valid')
                }
            }
        
         })
        .fail(function() {  });
        
        
        
    });
    
    $('.gafc-typeahead-clean').click(function() {
        par = $(this).parents('.input-group')
        valinput = par.find('.gafc-typeahead-value')
        valinput.val('')
        valahead = par.find('.gafc-typeahead-search')
        valahead.typeahead('val','')
        valahead.removeClass('is-valid').removeClass('is-invalid')
        
        if(valahead[0].hasAttribute('typeahead-callback-invalid')) {
                eval(valahead.attr('typeahead-callback-invalid'))
            }
    });
});