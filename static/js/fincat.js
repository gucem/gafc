var allowed_fincat = {
{% for c in g_categories %}
{{ c.id }}: [
    {% for f in c.allowed_fincat.all %}
    {{ f.id }},
    {% endfor %}
],
{% endfor %}
}

var default_fincat = {
{% for c in g_categories %}
{{ c.id }}: '{{ c.default_fincat.id }}',
{% endfor %}
}

function displayTree(elm) {
    elm.removeClass('d-none')
    
    if(elm[0].hasAttribute('toplevel')) {
        displayTree($('optgroup[ref='+elm.attr('toplevel')+']'))
    }
}

function filterAccounts(category, account) {
    cid = category.val()
    account.find('option').addClass('d-none')
    account.find('optgroup').addClass('d-none')
    
    if(cid in allowed_fincat) {
        allowed = allowed_fincat[cid]
        
        allowed.forEach(function(v) {
            displayTree(account.find('option[value='+v+']').removeClass('d-none').parent())
        });
    }
    
    account.find('option[value=""]').removeClass('d-none')
    account.val(default_fincat[category.val()])
}

$(function() {
    
    $('select[data-linked-account]').each(function() {
        select = $(this).parents('body').find('select[name='+$(this).attr('data-linked-account')+']')
        ref = 1
        
        tree = []
        
        select.children('optgroup').each(function() {
            pre = $(this).attr('label').split(' ')[0]
            lvl = pre.length/3
            
            tree[lvl] = ref
            
            $(this).attr('level', lvl)
            $(this).attr('ref', ref++)
            
            if(lvl > 0) {
                $(this).attr('toplevel', tree[lvl-1])
            }
        });
        
        filterAccounts($(this), select)
        
        $(this).change(function() {
            select = $(this).parents('body').find('select[name='+$(this).attr('data-linked-account')+']')
            filterAccounts($(this), select)
        })
    });
    
    $('select[data-initial-force]').each(function() {
        $(this).val($(this).attr('data-initial-force'))
    });

});
