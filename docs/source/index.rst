.. GAFC documentation master file, created by
   sphinx-quickstart on Sat Nov 12 19:33:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GAFC's documentation!
================================

Link to source code: https://gitlab.com/gucem/gafc

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   gafc.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
