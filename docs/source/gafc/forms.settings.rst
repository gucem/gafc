settings
========

.. automodule:: gafc.forms.settings
    :show-inheritance:
    :members:
    :undoc-members: