views
=====

GAFC views

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   views.callback
   views.categories
   views.common
   views.dashboard
   views.equipments
   views.events
   views.financials
   views.handlers
   views.home
   views.jsondata
   views.logins
   views.mailings
   views.permissions
   views.persons
   views.settings
   
