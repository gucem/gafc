forms
=====

.. toctree::
   :maxdepth: 2

   forms.category
   forms.common
   forms.equipments
   forms.events
   forms.financials
   forms.logins
   forms.mailings
   forms.permissions
   forms.persons
   forms.settings
