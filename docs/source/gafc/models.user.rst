user
====

.. automodule:: gafc.models.user
    :show-inheritance:
    :members:
    :undoc-members:
    