events
======

.. automodule:: gafc.models.events
    :show-inheritance:
    :members:
    :undoc-members:
    