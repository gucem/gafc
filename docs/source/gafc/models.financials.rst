financials
==========

.. automodule:: gafc.models.financials
    :show-inheritance:
    :members:
    :undoc-members:
    