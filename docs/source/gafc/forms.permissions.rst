permissions
===========

.. automodule:: gafc.forms.permissions
    :show-inheritance:
    :members:
    :undoc-members: