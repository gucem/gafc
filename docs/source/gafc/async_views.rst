asynchroneous views
===================

bankaccount
-----------

.. automodule:: gafc.async_views.bankaccount
    :show-inheritance:
    :members:
    :undoc-members:
    
event
-----

.. automodule:: gafc.async_views.event
    :show-inheritance:
    :members:
    :undoc-members: