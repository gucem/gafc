persons
=======

.. automodule:: gafc.models.persons
    :show-inheritance:
    :members:
    :undoc-members:
    