# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Formation ULR router
"""



from django.urls import path
from . import views

urlpatterns = [
    path('valuations', views.ValorisationAttributionView.as_view(), name='formation_table'),
    path('accounts', views.FormationAccountView.as_view(), name='formation_accounts'),
    path('meetings', views.MeetingAccountView.as_view(), name='formation_meetings'),
    path('rules', views.ValorisationRuleView.as_view(), name='valorisation_rules'),
    path('rules/del/<int:del_id>', views.ValorisationRuleView.as_view(), name='valorisation_rules_del'),
    path('rules/mod/<int:mod_id>', views.ValorisationRuleView.as_view(), name='valorisation_rules_mod'),
    path('person/<int:pid>', views.ValorisationPersonView.as_view(), name='valorisation_person'),
    
]
 