# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Formation Models definition
"""


from django.db import models
import gafc.models as gm
import datetime as dt

# Create your models here.

class ValorisationRule(models.Model):
    title           = models.CharField(max_length=100, verbose_name="Denomination")
    amount          = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Montant")
    validity        = models.PositiveIntegerField(null=True, verbose_name="Validité (mois)")
    staffing_min    = models.DecimalField(decimal_places=2, max_digits=10, null=True, default=None, verbose_name="Encadrement min")
    staffing_max    = models.DecimalField(decimal_places=2, max_digits=10, null=True, default=None, verbose_name="Encadrement max")
    category_to     = models.ForeignKey(gm.Category, on_delete=models.RESTRICT, null=True, default=None, verbose_name="Payable à")
    fincat_to       = models.ForeignKey(gm.FinancialCategory, on_delete=models.CASCADE, null=True, default=None, verbose_name="Payable dans")
    valid_diploma   = models.BooleanField(default=False, verbose_name="Uniquement pour diplome valide")
    exclusive_group = models.PositiveIntegerField(null=True, default=None, verbose_name="Groupe d'exclusion")
    
    def __str__(self):
        return self.title
    
class Valorisation(models.Model):
    title           = models.CharField(max_length=100, verbose_name="Dénomination")
    amount          = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Valorisation")
    expiry          = models.DateField(null=True, verbose_name="Expiration")
    person          = models.ForeignKey(gm.Person, on_delete=models.CASCADE, verbose_name="Bénéficiaire")
    category_from   = models.ForeignKey(gm.Category, related_name="valo_from", on_delete=models.CASCADE, verbose_name="Payé par")
    category_to     = models.ForeignKey(gm.Category, related_name="valo_to", on_delete=models.RESTRICT, null=True, default=None, verbose_name="Payable à")
    fincat_to       = models.ForeignKey(gm.FinancialCategory, on_delete=models.CASCADE, null=True, default=None, verbose_name="Payable dans")
    season          = models.ForeignKey(gm.Season, null=True, on_delete=models.SET_NULL, verbose_name="Saison")
    rule            = models.ForeignKey(ValorisationRule, null=True, default=None, on_delete=models.SET_NULL, verbose_name='Règle')
    
    def updateFromRule(self):
        if self.rule is None:
            raise ValueError('Can\'t update from rule: rule is None')
            
        self.title = self.rule.title
        self.amount = self.rule.amount
        self.expiry = dt.date.today()+dt.timedelta(days=30*self.rule.validity)
        self.category_to = self.rule.category_to
        self.fincat_to = self.rule.fincat_to
        
    @property
    def total_used(self):
        total = 0.
        
        for ll in ValorisationPaymentLink.objects.filter(valorisation=self):
            total -= float(ll.amount)
        
        return total
    
    @property
    def is_expired(self):
        if self.expiry is None:
            return False
        
        return self.expiry < dt.date.today()
    
class ValorisationPaymentLink(models.Model):
    valorisation    = models.ForeignKey(Valorisation, on_delete=models.CASCADE)
    amount          = models.DecimalField(decimal_places=2, max_digits=10)
    date            = models.DateTimeField()
    payment         = models.OneToOneField(gm.Payment, null=True, default=None, on_delete=models.SET_NULL)
    ipayment        = models.OneToOneField(gm.InternalPayment, null=True, default=None, on_delete=models.SET_NULL)
    
    def save(self, *args, **kwargs):
        
        if self.payment is not None and self.ipayment is not None:
            raise ValueError('Payment and internal payment can\'t be defined together')
            
        if self.payment is not None:
                
            if self.valorisation.category_to is not None:
                raise ValueError('Payment must be an internal payment')
            
            if self.payment.category != self.valorisation.category_from:
                raise ValueError('Payment don\'t originate from the valorisation associated category')
            
            self.amount = self.payment.amount
            self.date   = self.payment.pay_done
            
        elif self.ipayment:
            if self.ipayment.category_d != self.valorisation.category_from:
                raise ValueError('Internal payment don\'t originate from the valorisation associated category')
                
            if self.valorisation.category_to is not None:
                if self.valorisation.category_to != self.ipayment.category_c:
                    raise ValueError('Internal payment id not done toward the dedicated category')
                    
                if self.valorisation.fincat_to != self.ipayment.financial_c:
                    raise ValueError('Internal payment id not done toward the dedicated financial category')
        
            self.amount = -self.ipayment.amount
            self.date   = self.ipayment.pay_done
            
        if self.amount > 0.:
            raise ValueError('Payment must be an outcoming payment')
        
        if self.valorisation.expiry is not None and self.date.date() > self.valorisation.expiry:
            raise ValueError('Valorisation is expired')
            
        super().save(*args, **kwargs)
    
    
    

class Diploma(models.Model):
    person          = models.ForeignKey(gm.Person, on_delete=models.CASCADE, verbose_name="Adherent")
    title           = models.CharField(max_length=100, verbose_name="Diplome")
    obtain_date     = models.DateField(verbose_name="Date obtention")
    renewal_before  = models.DateField(null=True, verbose_name="À renouveller avant")
    renewal_count   = models.PositiveIntegerField(default=0, verbose_name="Renouvellement")
