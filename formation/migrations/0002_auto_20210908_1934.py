# Generated by Django 3.1.13 on 2021-09-08 19:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formation', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='valorisationrule',
            name='deleted',
        ),
        migrations.AlterField(
            model_name='valorisationrule',
            name='valid_diploma',
            field=models.BooleanField(default=False, verbose_name='Uniquement pour diplome valide'),
        ),
        migrations.AlterField(
            model_name='valorisationrule',
            name='validity',
            field=models.PositiveIntegerField(null=True, verbose_name='Validité (mois)'),
        ),
    ]
