# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Formation views
"""



from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce
from django.contrib import messages

import datetime as dt
from decimal import Decimal

from gucem import settings

import gafc.models as gm
from gafc.views.common import GafcPermView, GafcFormView

from .models import Valorisation, ValorisationRule, Diploma
from .forms import ValorisationAttributeForm, PaymentValorisationForm, DiplomaForm, ValorisationRuleForm


class FormationAccountView(GafcPermView):
    """
    Display Valorisation Account available
    """
    
    specific_perms = ['formation.view_valorisation',]
    """
    Need formation.view_valorisation permission
    """
    
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """

        valos = Valorisation.objects.select_related('person').annotate(used=Coalesce(Sum('valorisationpaymentlink__amount'), Decimal(0))).filter(expiry__gte=dt.date.today()).order_by('person__last_name')
        
        vals = dict()
        
        for v in valos:
            if not v.person.id in vals:
                vals[v.person.id] = {'person': v.person, 'given': 0., 'used': 0.}
                
            vals[v.person.id]['given'] += float(v.amount)
            vals[v.person.id]['used'] += float(v.used)
        
        return render(request, 'formation/allaccounts.html', locals())


class  MeetingAccountView(GafcPermView):
    """
    Display Meeting Account available
    """
    
    specific_perms = ['formation.view_valorisation',]
    """
    Need formation.view_valorisation permission
    """
    
    def get(self, request):
        """
        Display the staff contibution for all categories
        
        :param request: Request object
        """
        season = gm.Season.objects.get(pk=request.session['season_id'])
        
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_reunion']:
                allowed_types += [k, ]
        
        events = gm.Event.objects.filter(season=season, eventstate__state='VA', eventtype__in=allowed_types).exclude(eventstate__state='CN').order_by('-begin')
        participations = gm.Participation.objects.select_related('event','event__category','person').filter(event__in = events, event_done=True)
        
        staffing = dict()
        cats = gm.Category.objects.all()
        
        for p in participations:
            if not p.person.id in staffing:
                staffing[p.person.id] = dict()
                staffing[p.person.id]['total'] = {'time': 0., 'time_sup': 0., 'person': p.person }
                
                for c in cats:
                    staffing[p.person.id][c.id] = {'time': 0., 'time_sup': 0., 'category':c}
            
            staffing[p.person.id][p.event.category.id]['time'] += p.event.real_duration
            staffing[p.person.id]['total']['time'] += p.event.real_duration
            
            if p.role != '0PA':
                staffing[p.person.id][p.event.category.id]['time_sup'] += p.event.real_duration
                staffing[p.person.id]['total']['time_sup'] += p.event.real_duration
        
        return render(request, 'formation/table_reunion.html', locals())


class ValorisationAttributionView(GafcFormView):
    """
    Attribute valorisation according to the rules
    """
    
    specific_perms = ['formation.add_valorisation']
    """
    Need formation.add_valorisation permission
    """
    
    def setup_forms(self, request):
        """
        Setup the form 
        
        :param request: Request object
        """
        self.season = gm.Season.objects.get(pk=request.session['season_id'])
        self.add_form(ValorisationAttributeForm, self.season)
        
    def get(self, request):
        """
        Setup the form 
        
        :param request: Request object
        """
        
        season = self.season
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_formation']:
                allowed_types += [k, ]
        
        events = gm.Event.objects.filter(season=season, eventstate__state='VA', eventtype__in=allowed_types).exclude(eventstate__state='CN').order_by('-begin')
        staff_participations = gm.Participation.objects.select_related('event','event__category','person').filter(event__in = events, event_done=True).exclude(role='0PA').exclude(role='3PR')
        valos = Valorisation.objects.select_related('person','rule').filter(season=season)
        
        staffing = dict()
        rules = ValorisationRule.objects.all()
        
        cats = gm.Category.objects.all()
        
        for v in valos:
            if not v.person.id in staffing:
                staffing[v.person.id] = dict()
                staffing[v.person.id]['total'] = {'time': 0., 'person': v.person, 'category':None, 'rules':{}, 'valo':[], 'exclude_grp':[], 'exclude_id': [], 'has_diploma': Diploma.objects.filter(person=v.person).aggregate(count=Count('id'))['count']}
                
                for c in cats:
                    staffing[v.person.id][c.id] = {'time': 0., 'category':c}
                    
            staffing[v.person.id]['total']['valo'] += [v,]
            
            if v.rule is not None:
                staffing[v.person.id]['total']['exclude_id'] += [v.rule.id,]
                if v.rule.exclusive_group is not None:
                    staffing[v.person.id]['total']['exclude_grp'] += [v.rule.exclusive_group, ]
        
        for p in staff_participations:
            if not p.person.id in staffing:
                staffing[p.person.id] = dict()
                staffing[p.person.id]['total'] = {'time': 0., 'person': p.person, 'category':None, 'rules':{}, 'valo':[],'exclude_grp':[], 'exclude_id': [], 'has_diploma': Diploma.objects.filter(person=p.person).aggregate(count=Count('id'))['count']}
                
                for c in cats:
                    staffing[p.person.id][c.id] = {'time': 0., 'category':c}
            
            if p.role == '2SU':
                staffing[p.person.id][p.event.category.id]['time'] += p.event.duration
                staffing[p.person.id]['total']['time'] += p.event.duration
            elif p.role == '1CO':
                staffing[p.person.id][p.event.category.id]['time'] += p.event.duration/2.
                staffing[p.person.id]['total']['time'] += p.event.duration/2.
                
        for (k,s) in staffing.items():
            M = 0
            C = None
            
            for (kk,c) in s.items():
                if kk == 'total':
                    continue
                
                if c['time'] > M:
                    M = c['time']
                    C = c['category']
                    
            s['total']['category'] = C
            
            for r in rules:
                if r.exclusive_group in s['total']['exclude_grp']:
                    continue
                
                if r.id in s['total']['exclude_id']:
                    continue
                
                if (r.staffing_min is None or s['total']['time'] >= r.staffing_min) and (r.staffing_max is None or s['total']['time'] < r.staffing_max):
                    if not r.valid_diploma or s['total']['has_diploma'] > 0:
                        if r.exclusive_group not in s['total']['rules']:
                            s['total']['rules'][r.exclusive_group] = []
                        
                        s['total']['rules'][r.exclusive_group] += [r,]
        
        staffing=dict(sorted(staffing.items(), key=lambda s: s[1]['total']['time'] + (0 if len(s[1]['total']['rules']) == 0 and len(s[1]['total']['valo']) == 0 else 1000), reverse=True))
        avail_persons = gm.Person.objects.all()
        avail_rules = ValorisationRule.objects.all()
        
        return render(request, 'formation/table.html', locals())


class ValorisationPersonView(GafcPermView):
    """
    Person valorisation page
    """
    
    specific_perms = ['formation.view_valorisation','gafc.view_person']
    

    def post(self, request, pid):
        """
        POST method
        
        :param request: Request object
        :param pid person id
        """
        
        what = request.POST['whattodo'] if 'whattodo' in request.POST else None
        season = gm.Season.objects.get(pk=request.session['season_id'])
        
        P = get_object_or_404(gm.Person, pk=pid)
        valos = [(v, PaymentValorisationForm(v,P) if v.total_used < v.amount and not v.is_expired else None) for v in Valorisation.objects.filter(person=P)]
        
        if request.user.has_perm('formation.add_diploma'):
            form_d = DiplomaForm(request.POST if what == 'diploma_save' else None)
            
            if what == 'diploma_save' and form_d.is_valid():
                d = form_d.save(commit=False)
                d.person=P
                d.save()
                return redirect('valorisation_person', pid=pid)
        
        if what == 'valorisation_add_payment' and 'valo_id' in request.POST:
            val = get_object_or_404(Valorisation, id=request.POST['valo_id'], person=P)
            form = PaymentValorisationForm(val, request.user, request.POST)
            
            if form.is_valid():
                try:
                    form.save()
                    return redirect('valorisation_person', pid=pid)
                except ValueError as e:
                    error_txt = str(e)
            else:
                for i in range(len(valos)):
                    v = valos[i]
                    if v[0].id == val.id:
                        valos[i] = (val, form)
                        
            return redirect('valorisation_person', pid=pid)
                        
        diploma = Diploma.objects.filter(person=P)
        
        return render(request, 'formation/person.html', locals())
    
    def get(self, request, pid):
        return self.post(request, pid)
    
    
class ValorisationRuleView(GafcFormView):
    """
    Manage Rules
    """
    
    specific_perms = ['formation.view_valorisationrule',]
    
    def setup_forms(self, request, del_id=None, mod_id=None):
        """
        Setup the forms
        
        :param request: Request object
        :param del_id: ValorisationRule object to be deleted
        :param mod_id: ValorisationRule object to be modified
        """
        
        if request.user.has_perm('formation.change_valorisationrule'):
         
            inst = None
            if mod_id is not None:
                inst = get_object_or_404(ValorisationRule, pk=mod_id)
                
            self.add_form(ValorisationRuleForm, instance=inst)
            
            
    def form_valorisation_rule_save(self,request, del_id=None, mod_id=None ):
        """
        return and redirect to normal view
        
        :param request: Request object
        :param del_id: ValorisationRule object to be deleted
        :param mod_id: ValorisationRule object to be modified
        """
        
        self.forms['valorisation_rule_save'].save()
        return redirect('valorisation_rules')
            
            
    def get(self, request, del_id=None, mod_id=None):
        """
        GET method
        
        :param request: Request object
        :param del_id: ValorisationRule object to be deleted
        :param mod_id: ValorisationRule object to be modified
        """
        
        if del_id is not None:
            if request.user.has_perm('formation.delete_valorisationrule'):
                get_object_or_404(ValorisationRule,pk=del_id).delete()
                messages.success(request, "Suppression effecutée")
                return redirect('valorisation_rules')            
            else:
                messages.error(request, "Vous ne pouvez pas supprimer de règles")
                
        rules = ValorisationRule.objects.order_by('staffing_min','title')
    
        return render(request, 'formation/rules.html', locals())
