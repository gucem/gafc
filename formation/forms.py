# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Formation forms
"""


from django import forms
from django.db import transaction
from django.core.exceptions import ValidationError

from crispy_forms.layout import Hidden

import datetime as dt

from gafc.forms import MyDateInput, FinancialCategoryWidget, PersonTypeaheadWidget
from gafc.forms.common import GafcForm, GafcModelForm
from gafc.templatetags import gafc_tags
import gafc.models as gm

from .models import ValorisationRule, Diploma, ValorisationPaymentLink, Valorisation


class ValorisationAttributeForm(GafcForm):
    """
    Attribute the valorisation to person
    """
    
    rule        = forms.ModelChoiceField(ValorisationRule.objects.all())
    person      = forms.ModelChoiceField(gm.Person.objects.all(), widget=PersonTypeaheadWidget())
    category    = forms.ModelChoiceField(gm.Category.objects.all())
    
    what = "add_valo"
    
    def __init__(self, season, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.season = season
    
    
    def save(self, commit=True):
        """
        Commit the changes
        
        :param commit: Commit the changes
        """
        
        o = Valorisation(rule=self.cleaned_data['rule'],
                         person=self.cleaned_data['person'],
                         category_from=self.cleaned_data['category'],
                         season=self.season)
        o.updateFromRule()
        
        if commit:
            o.save()
            
        return o

class ValorisationRuleForm(GafcModelForm):
    """
    Add/edit valorisation rule
    """
    
    what = "valorisation_rule_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['staffing_min'].required=False
        self.fields['staffing_min'].help_text = "Pas de minimum si non spéficié"
        
        self.fields['staffing_max'].required=False
        self.fields['staffing_max'].help_text = "Pas de maximum si non spéficié. Valeur non-incluse."
        
        self.fields['category_to'].required=False
        self.fields['category_to'].help_text = "Restreindre le paiement à une catégorie"
        self.fields['category_to'].widget.attrs.update({'data-linked-account': 'fincat_to'})
        
        self.fields['fincat_to'].widget = FinancialCategoryWidget()
        if self.instance is not None:
            if self.instance.fincat_to is not None:
                self.fields['fincat_to'].widget.attrs.update({'data-initial-force': self.instance.fincat_to.pk })
            else:
                self.fields['fincat_to'].widget.attrs.update({'data-initial-force': '' })

        
        self.fields['fincat_to'].required=False
        self.fields['valid_diploma'].required=False
        self.fields['exclusive_group'].required=False
        self.fields['exclusive_group'].help_text = "Les valo du même groupe sont mutuellement exclusives."
        
    class Meta:
        model = ValorisationRule
        fields = ('title', 'amount','validity','exclusive_group','staffing_min','staffing_max','category_to','fincat_to','valid_diploma')

class PaymentValorisationForm(GafcForm):
    """
    Add a payment linked to a valorisation given
    """
    
    what = "valorisation_add_payment"
    
    amount=forms.DecimalField()
    season      = forms.ModelChoiceField(queryset=gm.Season.objects.all(), label="Saison")
    fincat_from = forms.ModelChoiceField(queryset=gm.FinancialCategory.objects.all(), label="Imputation débitée")
    to          = forms.ChoiceField(label="Payer à")
    fincat_to   = forms.ModelChoiceField(queryset=gm.FinancialCategory.objects.all(), required=False, label="Imputation créditée")
    method      = forms.ChoiceField( choices=[  ('CA', gafc_tags.payMethod('CA')),
                                                ('CH', gafc_tags.payMethod('CH')),
                                                ('BC', gafc_tags.payMethod('BC')),
                                                ('IN', gafc_tags.payMethod('IN')),
                                                ('VM', gafc_tags.payMethod('VM')),
                                                ('BO', gafc_tags.payMethod('BO')),
                                                ('NO', gafc_tags.payMethod('NO'))], label="Méthode")

    def __init__(self, valorisation, user, *args, **kwargs):
        """
        Initialize the form
        
        :param valorisation: Valorisation rule linked
        :param user: Logged-in user
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.valorisation=valorisation
        self.user = user
        
        super().__init__(*args, **kwargs)
        
        self.fields['season'].initial = gm.Season.objects.get(is_default=True)
        
        self.fields['fincat_from'].widget = FinancialCategoryWidget(self.valorisation.category_from)  
        self.fields['fincat_from'].initial = None if self.valorisation.category_from.default_fincat is None else self.valorisation.category_from.default_fincat
        
        avail = float(self.valorisation.amount) - self.valorisation.total_used
        self.fields['amount'] = forms.DecimalField(min_value=0., max_value=avail, initial=avail, decimal_places=2, label="Montant")
        
        if self.valorisation.category_to is None:
            self.fields['to'].choices=[(-1, '%s %s'%(self.valorisation.person.first_name, self.valorisation.person.last_name)), *[(c.id, c.name) for c in gm.Category.objects.all()]]
            self.fields['fincat_to'].widget = FinancialCategoryWidget()
        else:
            self.fields['to'].choices=[(self.valorisation.category_to.id, self.valorisation.category_to.name),]
            
            if self.valorisation.fincat_to is None:
                self.fields['fincat_to'].widget = FinancialCategoryWidget(self.valorisation.category_from)
            else:
                self.fields['fincat_to'].widget = FinancialCategoryWidget()
                self.fields['fincat_to'].choices = [(self.valorisation.fincat_to.id, self.valorisation.fincat_to.name),]
                self.fields['fincat_to'].widget.attrs.update({'data-initial-force': self.valorisation.fincat_to.id})
                
        if 'fincat_to' in self.data:
            self.fields['fincat_to'].widget.attrs.update({'data-initial-force': self.data['fincat_to']})
                
        self.fields['to'].widget.attrs.update({'data-linked-account': 'fincat_to'})
        
        self.helper.add_input(Hidden('valo_id', self.valorisation.id))
        
    def clean_to(self):
        """
        Check category restrictions
        """
        v = self.cleaned_data['to']
        if v == '-1':
            return None
        else:
            try:
                return gm.Category.objects.get(pk=v)
            except gm.Category.DoesNotExist:
                raise ValidationError('La catégorie sélectionnée n\'existe pas.')        
        
    def clean(self):
        super().clean()
        
        if 'to' in self.cleaned_data and 'fincat_to' in self.cleaned_data and self.cleaned_data['to'] is not None and self.cleaned_data['fincat_to'] is None:
            self.add_error('fincat_to', 'Une imputation doit être définie pour un transfert interne!')
        
        
    def save(self):
        """
        Save the payment
        """
        
        encoder = self.user
        
        if self.is_valid():
            with transaction.atomic():
                if self.cleaned_data['to'] is None:
                    p = gm.Payment.objects.create(amount=-self.cleaned_data['amount'],
                                                  method=self.cleaned_data['method'],
                                                  person=self.valorisation.person,
                                                  encoder=encoder,
                                                  category = self.valorisation.category_from,
                                                  season=self.cleaned_data['season'],
                                                  comment= "%s %s %s"%(self.valorisation.title, self.valorisation.person.first_name, self.valorisation.person.last_name),
                                                  pay_done = dt.datetime.now(),
                                                  financial = self.cleaned_data['fincat_from'])
                    
                    ValorisationPaymentLink.objects.create(valorisation=self.valorisation, payment=p)
                else:
                    p = gm.InternalPayment.objects.create(amount=self.cleaned_data['amount'],
                                                  method=self.cleaned_data['method'],
                                                  person=encoder,
                                                  category_d = self.valorisation.category_from,
                                                  financial_d = self.cleaned_data['fincat_from'],
                                                  category_c = self.cleaned_data['to'],
                                                  financial_c = self.cleaned_data['fincat_to'],
                                                  comment = "%s %s %s"%(self.valorisation.title, self.valorisation.person.first_name, self.valorisation.person.last_name),
                                                  season=self.cleaned_data['season'])
                    
                    ValorisationPaymentLink.objects.create(valorisation=self.valorisation, ipayment=p)

class DiplomaForm(GafcModelForm):
    """
    Add/Edit Diploma
    """
    
    what = "diploma_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['obtain_date'].widget = MyDateInput()
        self.fields['renewal_before'].widget = MyDateInput()
        self.fields['renewal_before'].required = False
        
    class Meta:
        model = Diploma
        fields = ('title', 'obtain_date', 'renewal_before')
