# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Views for Key module
"""

from django.shortcuts import render
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404

from gafc.views.common import GafcFormView

from .forms import KeyForm, KeyGroupForm, KeyLoanForm, KeyLoanFinishForm 
from .models import Key, KeyGroup, KeyLoan

class KeyHomeView(GafcFormView):
    """
    Home view of key managenemt
    """
    
    specific_perms=['keys.view_key',]
    """
    Need keys.view_key permission
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """
        
        if request.user.has_perm('keys.change_key'):
            self.add_form(KeyForm)
            self.add_form(KeyGroupForm)
            
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
    
        groups = KeyGroup.objects.prefetch_related(Prefetch('keyloan_set', queryset=KeyLoan.objects.filter(date_out=None)))
        keys = Key.objects.all()
        
        return render(request, 'keys/home.html', locals())
        
    
class KeyView(GafcFormView):
    """
    Edit Key object
    """
    
    specific_perms = ['keys.change_key',]
    """
    Need keys.change_key permission
    """
    
    def setup_forms(self, request, kid):
        """
        Setup the forms
        
        :param request: Request object
        :param kid: Key id
        """
        
        self.key = get_object_or_404(Key, pk=kid)
        self.add_form(KeyForm, instance=self.key)
            
    def get(self, request, kid):
        """
        GET method
        
        :param request: Request object
        :param kid: Key id
        """
        key = self.key
        return render(request, 'keys/key.html', locals())


class KeyGroupView(GafcFormView):
    """
    Display KeyGroup
    """
    
    specific_perms = ['keys.change_key',]
    
    def setup_forms(self, request, gid):
        """
        Setup the forms
        
        :param request: Request object
        :param gid: KeyGroup id
        """
        
        self.group = get_object_or_404(KeyGroup, pk=gid)
        self.add_form(KeyGroupForm, instance=self.group)
        self.add_form(KeyLoanForm, self.group)
        self.add_form(KeyLoanFinishForm)

    def get(self, request, gid):
        """
        GET method
        
        :param request: Request object
        :param gid: KeyGroup id
        """
        group = self.group
        return render(request, 'keys/group.html', locals())

