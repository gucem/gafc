# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Keys Models
"""

from django.db import models
import gafc.models as gm


class Key(models.Model):
    name            = models.CharField(max_length=100, verbose_name="Nom")
    description     = models.TextField(verbose_name="Description")
    
    def __str__(self):
        return self.name
    
class KeyGroup(models.Model):
    name            = models.CharField(max_length=100, verbose_name="Nom")
    description     = models.TextField(verbose_name="Description")
    keys            = models.ManyToManyField(Key, verbose_name="Clés")
    
    def __str__(self):
        return self.name
    
class KeyLoan(models.Model):
    person          = models.ForeignKey(gm.Person, verbose_name="Adhérent", on_delete=models.CASCADE)
    keygroup        = models.ForeignKey(KeyGroup, verbose_name="Trousseau", on_delete=models.CASCADE)
    date_in         = models.DateField(auto_now_add=True, verbose_name="Date d'attribution")
    date_out        = models.DateField(null=True, default=None, verbose_name="Date de restitution")

