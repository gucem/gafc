# Generated by Django 3.2.13 on 2022-06-26 13:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Key',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('description', models.TextField(verbose_name='Description')),
            ],
        ),
        migrations.CreateModel(
            name='KeyGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nom')),
                ('description', models.TextField(verbose_name='Description')),
                ('keys', models.ManyToManyField(to='keys.Key', verbose_name='Clés')),
            ],
        ),
        migrations.CreateModel(
            name='KeyLoan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_in', models.DateField(auto_now_add=True, verbose_name="Date d'attribution")),
                ('date_out', models.DateField(default=None, null=True, verbose_name='Date de restitution')),
                ('keygroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='keys.keygroup', verbose_name='Trousseau')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Adhérent')),
            ],
        ),
    ]
