# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Keys Forms
"""

from django import forms

from gafc.forms import GafcModelForm, PersonTypeaheadWidget, GafcForm
from .models import Key, KeyGroup, KeyLoan
import datetime as dt


class KeyForm(GafcModelForm):
    """
    Add/Edit a key
    """
    
    what = "key_form_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['description'].widget.attrs.update({'rows': 3})
        
    class Meta:
        model = Key
        fields = ('name', 'description')
        

class KeyGroupForm(GafcModelForm):
    """
    Add/Edit a key group
    """
    
    what = "keygroup_form_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['description'].widget.attrs.update({'rows': 3})
        
    class Meta:
        model = KeyGroup
        fields = ('name', 'description', 'keys')
        
class KeyLoanForm(GafcModelForm):
    """
    Add/Edit a key loan
    """
    
    what = "keyloan_form_save"
    
    def __init__(self, keygroup, *args, **kwargs):
        """
        Initialize the form
        
        :param keygroup: KeyGroup to add the Loan
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.keygroup = keygroup
        
        self.fields['person'].widget = PersonTypeaheadWidget()
        
    class Meta:
        model = KeyLoan
        fields= ('person',)
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes to DB
        """
        
        obj = super().save(commit=False)
        obj.keygroup = self.keygroup
        
        if commit:
            obj.save()
            
        return obj
    
class KeyLoanFinishForm(GafcForm):
    """
    Delete a key loan
    """
    
    what = "keyloan_finish_form_save"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    loan = forms.ModelChoiceField(KeyLoan.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes to DB
        """
        
        obj = self.cleaned_data['loan']
        obj.date_out = dt.date.today()
        
        if commit:
            obj.save()
            
        return obj

