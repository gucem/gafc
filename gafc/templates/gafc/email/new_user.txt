{% load gafc_tags %}Bonjour {{ person.first_name }} {{ person.last_name }},

Vous avez créé un compte sur la plateforme d'inscription aux sorties du GUCEM. Voici le lien pour activer votre compte et créer un mot de passe:

{% abs_url 'new_lost_password' person.lostpasswordrequest.id person.lostpasswordrequest.token %}

Si vous êtes licencié dans un autre club FFCAM, merci de transmettre une copie de votre licence lors de votre première inscription à une sortie pour qu'elle soit prise en compte.

Si vous n'êtes pas à l'origine de la demande, veuillez simplement ignorer cet email. Le compte sera automatiquement supprimé au bout de 24h.

Bonne journée!

- Le GUCEM

