# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Automatic tasks
"""


from celery import shared_task

import gucem.settings as gsets
from .models import SympaMailingList, Person, License, Federation
import datetime as dt
from . import ffcam_utils
from django.db import transaction

from django.template.loader import render_to_string
from django.core.mail import send_mail

@shared_task(name='gafc.tasks.sympa_sync')
def sympa_sync():
    print("Synchronize sympa mailing lists")
    
    with ffcam_utils.SympaConnexxion(
                            gsets.GAFC_SYMPA_USER,
                            gsets.GAFC_SYMPA_PASSWD,
                            gsets.GAFC_SYMPA_SERVER) as conn:
    
        for ml in SympaMailingList.objects.all():
            ml.remotesync(conn)
    
    
@shared_task(name='gafc.tasks.ffcam_sync')
def ffcam_sync():
    """
    FFCAM auto synchronization
    """
    
    memberfile = gsets.GAFC_TEMPFOLDER+'/member_list.csv'
    
    with ffcam_utils.FFCAMExtranetConnexxion(gsets.GAFC_EXTRANET_USER, gsets.GAFC_EXTRANET_PASSWD) as conn:
        conn.save_memberfile(memberfile)
    
    pdat, ldat, mdat = ffcam_utils.extranet_process_memberfile(memberfile)
    
    n_pers = 0
    n_lic  = 0
    
    mailings = { ml.name: [] for ml in SympaMailingList.objects.exclude(code=None).exclude(code='') }
    fede = Federation.objects.get(acronym=gsets.GAFC_EXTRANET_ACROFEDE)
    
    tod = dt.date.today()
    enddate = dt.date(tod.year+1 if tod.month >= 9 else tod.year,9,30) # Next 30 september
    
    with transaction.atomic():
        for i in pdat.index:
            
            if not ffcam_utils.HtmlPortalConnexxion.validateEmail(pdat.loc[i].email):
                continue
            
            try:
                p = Person.objects.get(email=pdat.loc[i].email)
            except Person.DoesNotExist:
                l = License.objects.filter(number=ldat.loc[i].number) # Maybe a licence with same number already exists
                if len(l) == 0: # If not, create new user
                    pers_data = pdat.loc[i].to_dict()
                    p = Person.objects.create(**pers_data, is_superuser=False)
                    p.set_unusable_password()
                    n_pers += 1
                    
                    # Send welcome mail
                    
                    if not gsets.DEBUG:
                        send_mail(
                            'Votre compte intranet GUCEM',
                            render_to_string('gafc/email/new_adherent.txt', {'person': p,}),
                            'info@gucem.fr',
                            [ p.email ],
                            fail_silently=True,
                        )
                    
                    # If needed, subscribe to mailing lists
                    for k in mailings:
                        if mdat.loc[i][k]:
                            mailings[k] += [pdat.loc[i].email,]
                             
                else:
                    p = l[0].person
                    
            (l, cr) = License.objects.update_or_create(person=p, federation=fede, **ldat.loc[i].to_dict(), defaults={'date_end': enddate})
            
            if cr:
                n_lic += 1
                
            # Update CACI
            p.caci = pdat.loc[i].caci
            p.save()
    
    with ffcam_utils.SympaConnexxion(gsets.GAFC_SYMPA_USER, gsets.GAFC_SYMPA_PASSWD, gsets.GAFC_SYMPA_SERVER) as conn:
    
        for k, v in mailings.items():
            conn.sympa_bulk_subscribe(v, k)
            mlobj = SympaMailingList.objects.get(pk=k)
            mlobj.subscribers.add(*Person.objects.filter(email__in=v))
            
    print('%i nouveaux adhérents, %i licences, Mailing lists: %s'%(n_pers, n_lic, ', '.join('%s : %i'%(k,len(v)) for k,v in mailings.items())))
    
    