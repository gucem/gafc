# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Bankaccount receivers
"""



import json
from asgiref.sync import sync_to_async, async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from channels_presence.models import Room, Presence
from channels_presence.decorators import touch_presence, remove_presence
from django.db.models.signals import post_save, post_delete
from django.db.models import Subquery, OuterRef
from django.dispatch import receiver
from ..models import *
from ..templatetags import gafc_tags
import sys


@receiver(post_save, sender=AccountOperation)
def on_accountoperation_saved(sender, **kwargs):
    
    # Don't send newly-created operations
    if kwargs['created']:
        return
    
    layer = get_channel_layer()
    op = kwargs['instance']
    
    if op.cashing is None:
        async_to_sync(layer.group_send)('bankaccount', {
            'type': 'moveOut',
            'opid': op.pk
        })        
    else:
        async_to_sync(layer.group_send)('bankaccount', {
            'type': 'moveIn',
            'opid': op.pk,
            'caid': op.cashing_id
        })


def get_cashings(cids):
    return [ c for c in Cashing.objects.annotate(total_p=Subquery(Payment.objects.filter(cashing=OuterRef('pk')).values('cashing_id').annotate(total=Sum('amount')).values('total')), total_op=Subquery(AccountOperation.objects.filter(cashing=OuterRef('pk')).values('cashing_id').annotate(total=Sum('amount')).values('total'))).exclude(date_done=None).filter(pk__in=cids) ]
    
class live_bankaccount(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_name = 'bankaccount'
        
        await self.accept()        

        # Join room group
        await sync_to_async(Room.objects.add)(
            self.group_name,
            self.channel_name,
            self.scope['user']
        )
        
    async def receive(self, text_data):
        
        data = json.loads(text_data)
        
        try:
            if data['type'] == 'heartbeat':
                await self.send(text_data=text_data)
                
            upcash = []
            
            if data['type'] == 'moveIn':
                oper = await sync_to_async(AccountOperation.objects.select_related('cashing').get)(pk=data['opid'])
                
                if oper.cashing is not None:
                    upcash += [ oper.cashing.pk, ]
                
                oper.cashing = await sync_to_async(Cashing.objects.get)(pk=data['caid'])
                
                if oper.cashing is not None:
                    upcash += [ oper.cashing.pk, ]
                    
                await sync_to_async(oper.save)()
                
            if data['type'] == 'moveOut':
                oper = await sync_to_async(AccountOperation.objects.select_related('cashing').get)(pk=data['opid'])
                
                if oper.cashing is not None:
                    upcash += [ oper.cashing.pk, ]
                    
                oper.cashing = None
                await sync_to_async(oper.save)()
                
                
            cashs = await sync_to_async(get_cashings)(upcash)
            for cash in cashs:
                color = 'danger'
                if cash.total_op is not None:
                    if cash.total_op == cash.total_p:
                        color = 'success'
                    else:
                        color = 'warning'
                elif cash.total_p == 0:
                    color = 'success'
                        
                await self.channel_layer.group_send(self.group_name,
                                                    {'type': 'cashing_color',
                                                     'cid': cash.pk,
                                                     'color': color})
                
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            await self.error({'msg': '%i: %s'%(exc_tb.tb_lineno, str(e))})

    async def disconnect(self, close_code):            
        await sync_to_async(Room.objects.remove)(self.group_name, self.channel_name)
        
    async def error(self, err):
        await self.send(text_data=json.dumps({
                'type': 'error',
                'msg': err['msg']
            }))
    
    async def moveIn(self, move):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(move))
    
    async def moveOut(self, move):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(move))
        
    async def cashing_color(self, elms):
        await self.send(text_data=json.dumps(elms))
        


