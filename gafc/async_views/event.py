# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Event Weksocket receivers
"""



import json
from asgiref.sync import sync_to_async, async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from channels_presence.models import Room, Presence
from channels_presence.decorators import touch_presence, remove_presence
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from ..models import *
from ..templatetags import gafc_tags
import sys
import gucem.settings as stg
from django.db import transaction

@receiver(post_save, sender=Participation)
def on_participation_saved(sender, **kwargs):
    layer = get_channel_layer()
    
    p = kwargs['instance']
    
    data, meta, extra = ParticipationToDict(p)
    
    if kwargs['created']:        
        async_to_sync(layer.group_send)('event_%i'%(p.event.id), {'type': 'add_participation', **data})
    
    async_to_sync(layer.group_send)('event_%i'%(p.event.id), {
        'type': 'update_meta',
        'data': meta
    })

def ParticipationToDict(p):
    return ({
            'id': p.id,
            'idp': p.person.id,
            'name': "%s %s"%(p.person.first_name, p.person.last_name.upper()),
            'email': p.person.email,
            'phone': p.person.phone,
            #'address': p.person.address.strip().replace('\n','<br />'),
            #'birthdate': p.person.birthdate.strftime('%d/%m/%Y'),
            'emg_contact': p.person.emg_contact,
            'emg_phone': p.person.emg_phone,
            'licenses': '<br />'.join(['%s %s'%(l.federation.acronym, l.number) for l in p.valid_license]) if p.has_valid_license else "Aucune licence valide trouvée",
            'valid_license': p.has_valid_license,
            'club_license': p.has_club_license,
            'caci': p.person.caci if p.person.caci is not None else "Aucun certificat trouvé",
            'caci_valid': p.person.caci_valid(p.event.begin),
#            'caci_valid': p.person.caci_tol_days > 0,
            'badge_class': p.badge_class,
            'pay_done': p.pay_done,
            'comment_count': len(p.person_comments),
            'comments': [{'level': c.level, 'text': '<br />'.join(c.text.split('\n')), 'who': "%s %s"%(c.who.first_name, c.who.last_name), 'who_id': c.who.id, 'date': c.date.strftime('%d/%m/%Y')} for c in p.person_comments]
        },[ {'ref': p.id, 'meta': 'waiting', 'value': p.waiting},
            {'ref': p.id, 'meta': 'cancelled', 'value': p.cancelled},
            {'ref': p.id, 'meta': 'done', 'value': p.event_done},
            {'ref': p.id, 'meta': 'role', 'value': p.role, 'avail_role': ', '.join([ gafc_tags.eventRole(r) for r in p.available_roles ]), 'role_ok': p.is_role_valid},],
          [ {'ref': p.id, 'meta': str(m.eventMeta_id), 'value': ((m.data == "True") if m.eventMeta.meta.datatype == "BO" else m.data) } for m in p.participationmetadata_set.all() ])
        
@receiver(post_delete, sender=Participation)
def on_participation_deleted(sender, **kwargs):
    layer = get_channel_layer()
    
    p = kwargs['instance']
    
    async_to_sync(layer.group_send)('event_%i'%(p.event.id), {
        'type': 'delete_participation',
        'id': p.id,
        'idp': p.person.id
    })

@receiver(post_save, sender=EventMetaData)
def on_eventMetaData_saved(sender, **kwargs):
    emd = kwargs['instance']
    layer = get_channel_layer()
    async_to_sync(layer.group_send)('event_%i'%(emd.event.id), {
        'type': 'add_eventMetaData',
        'id': emd.id,
        'name': emd.meta.name,
        'datatype': emd.meta.datatype,
        'cost': float(emd.cost),
        'description': emd.description,
        'choices': emd.availables,
        'availables': emd.available_choices
    })
    
@receiver(post_delete, sender=EventMetaData)
def on_eventMetaData_deleted(sender, **kwargs):
    emd = kwargs['instance']
    layer = get_channel_layer()
    async_to_sync(layer.group_send)('event_%i'%(emd.event.id), {
        'type': 'delete_eventMetaData',
        'id': emd.id
    })

@receiver(post_save, sender=ParticipationMetaData)
def on_participationMetaData_saved(sender, **kwargs):
    layer = get_channel_layer()
    
    m = kwargs['instance']
    v = (m.data == "True") if m.eventMeta.meta.datatype == "BO" else m.data
    
    async_to_sync(layer.group_send)('event_%i'%(m.participation.event.id), {
        'type': 'update_meta',
        'data': [{'ref': m.participation_id, 'meta': m.eventMeta_id, 'value': v},]
    })

async def sync_to_async_iterable(sync_iterable):
    sync_iterator = await iter_async(sync_iterable)
    while True:
        try:
            yield await next_async(sync_iterator)
        except StopAsyncIteration:
            return
        

def add_eventMetaData(event_id, data):
     with transaction.atomic():
        meta,cr = EventMeta.objects.get_or_create(name=data['name'], datatype=data['datatype'])
        cost=0.
        try:
            cost = float(data['cost'])
        except:
            pass
        
        metadata, cr = EventMetaData.objects.update_or_create(meta=meta, event_id=event_id, defaults={'cost':cost, 'description': data['description'], 'availables':data['choices']})

def change_eventMetaData(event_id, data):
        metadata = EventMetaData.objects.get(pk=data['id'])
        metadata.cost = data['cost']
        metadata.description = data['description']
        metadata.availables = data['choices']
        
        metadata.save()
        
def add_MetaModel(event_id, name):
    model, cr = MetaModel.objects.get_or_create(name=name)
    
    # delete old eventmeta fields
    model.eventmetamodel_set.all().delete()
    
    # Add current fields
    for f in EventMetaData.objects.filter(event_id=event_id):
        EventMetaModel.objects.create(meta=f.meta,
                                      model=model,
                                      cost=f.cost,
                                      description=f.description,
                                      availables=f.availables)
        
        
def load_MetaModel(event_id, name):
    model = MetaModel.objects.get(name=name)
    
    # create or update fields from model
    for f in model.eventmetamodel_set.all():
        EventMetaData.objects.update_or_create(event_id=event_id, meta=f.meta,
                                               defaults={'cost': f.cost,
                                                         'description': f.description,
                                                         'availables': f.availables})

class live_event(AsyncWebsocketConsumer):
        
    async def check_perms(self, perm):
        return await sync_to_async(self.scope['user'].perms.__getitem__)(perm)
    
    async def connect(self):
        
        self.event_id = self.scope['url_route']['kwargs']['event_id']
        self.event_group_name = 'event_%s' % self.event_id
        
        await self.accept()
        
        try:
            event = await sync_to_async(Event.objects.get)(pk=self.event_id)
            self.scope['user'].event_for_perms = event
            
            if await sync_to_async(event.a_current_state)() == 'VA':
                await self.close()
                return
            
        except Event.DoesNotExist:
            await self.close()
            return
        
        if not isinstance(self.scope['user'], Person) or not await self.check_perms('view_event'):
            await self.close()
            return
        

        # Join room group
        await sync_to_async(Room.objects.add)(
            self.event_group_name,
            self.channel_name,
            self.scope['user']
        )
        
        room = await sync_to_async(Room.objects.get)(channel_name=self.event_group_name)
        users = await sync_to_async(list)(room.get_users().values())
        
        # Send already-connected users to newly connected
        for u in users:
            if u['id'] != self.scope['user'].id:
                await self.send(text_data=json.dumps({
                    'type': 'connect',
                    'id': u['id'],
                    'name': '%s %s'%(u['first_name'], u['last_name'])
                }))
        
        await self.channel_layer.group_send(
            self.event_group_name,
            {
                'type': 'event_connect_notify',
                'id': self.scope['user'].id,
                'name': '%s %s'%(self.scope['user'].first_name, self.scope['user'].last_name)
            }
        )
            
        metas = await sync_to_async(list)(EventMetaData.objects.select_related('meta').filter(event=event))
        
        for emd in metas:
            await self.send(text_data=json.dumps({
                        'type': 'add_eventMetaData',
                        'id': emd.id,
                        'name': emd.meta.name,
                        'datatype': emd.meta.datatype,
                        'cost': float(emd.cost),
                        'description': emd.description,
                        'choices': emd.availables,
                        'availables': emd.available_choices
                    }))
        
        parts = await sync_to_async(list)(Participation.objects.select_related('person').prefetch_related('participationmetadata_set','participationmetadata_set__eventMeta','participationmetadata_set__eventMeta__meta','person__license_set','person__license_set__federation').filter(event=event))
        
        bulk = []
        
        for pa in parts:
            part, meta, extra = await sync_to_async(ParticipationToDict)(pa)
            
            if not await self.check_perms('view_person'):
                hide = {'email': '***', 'phone': '***', 'address': '***', 'birthdate': '***', 'emg_contact': '***', 'emg_phone': '***'} 
        
                for h in hide:
                    if h in part:
                        part[h] = hide[h]
        
            if not await self.check_perms('view_personcomment'):
                cs = []
                for c in part['comments']:
                    if not c['who_id'] == self.scope['user'].id:
                        cs += [c,]
                
                for c in cs:
                    part['comments'].remove(c)
                
            bulk += [{'type': 'add_participation', **part},{
                'type': 'update_meta',
                'data': meta + extra
            },]
            
#            await self.send(text_data=json.dumps({'type': 'add_participation', **part}))
#            await self.send( text_data=json.dumps({
#                'type': 'update_meta',
#                'data': meta + extra
#            }))
        
        await self.send(text_data=json.dumps({'type': 'bulk', 'data': bulk}))

    async def receive(self, text_data):
        await sync_to_async(Presence.objects.touch)(self.channel_name)
        
        data = json.loads(text_data)
       # print(data)
        
        try:
            if data['type'] == 'heartbeat':
                await self.send(text_data=text_data)
            
            elif data['type'] == 'update_meta':
                if not await self.check_perms('change_participation'):
                    raise Exception("Vous n'avez pas l'autorisation de modifier les inscriptions")
                
                if data['meta'] in ('done', 'role', 'waiting', 'cancelled'):
                    try:
                        p = await sync_to_async(Participation.objects.get)(pk=data['ref'])
                        if data['meta'] == 'done':
                            p.event_done = data['value']
                        elif data['meta'] == 'role' and data['value'] in ('3PR', '2SU', '1CO', '0PA'):
                            p.role = data['value']
                        elif data['meta'] == 'waiting':
                            p.waiting = data['value']
                        elif data['meta'] == 'cancelled':
                            p.cancelled = data['value']
                            
                        await sync_to_async(p.save)()
                    except Participation.DoesNotExist:
                        pass
                else:
                    try:
                        m = await sync_to_async(EventMeta.objects.get)(eventmetadata__pk=data['meta'])
                        
                        if m.datatype == 'IN':
                            try:
                                val_st = '%i'%(int(data['value']))
                            except:
                                val_st = 0
                        elif m.datatype == 'FL':
                            try:
                                val_st = '%.3f'%(float(data['value']))
                            except Exception as e:
                                val_st = 0.
                        else:
                            val_st = data['value']
                            
                            if m.datatype == 'UN':
                                md = await sync_to_async(EventMetaData.objects.get)(pk=data['meta'])
                                if not await sync_to_async(md.choice_valid)(data['value']):
                                    val_st = ''
                        
                        await sync_to_async(ParticipationMetaData.objects.update_or_create)(eventMeta_id=data['meta'], participation_id=data['ref'], defaults={'data': "%s"%(val_st)})
                    except EventMetaData.DoesNotExist:
                        pass
                    
            elif data['type'] == 'add_participant' and 'id' in data:
                if not await self.check_perms('add_participation'):
                    raise Exception("Vous n'avez pas l'autorisation d'ajouter des inscriptions")
                
                await sync_to_async(Participation.objects.create)(person_id=data['id'], event_id=self.event_id, role="0PA")
                
            elif data['type'] == 'delete_participation' and 'ref' in data:
                if not await self.check_perms('delete_participation'):
                    raise Exception("Vous n'avez pas l'autorisation de supprimer des inscriptions")
                try:
                    ev = await sync_to_async(Event.objects.get)(pk=self.event_id)
                    
                    if await sync_to_async(ev.a_current_state)() == 'OP':
                        await sync_to_async((await sync_to_async(Participation.objects.get)(pk=data['ref'], event_id=self.event_id)).delete)()
                    else: 
                        await self.error({'msg': 'Mettre l\'état sur "Ouvert aux inscriptions" pour supprimer un participant'})
                except Participation.DoesNotExist:
                    pass
                
            elif data['type'] == 'delete_eventMetaData' and 'id' in data:
                if not await self.check_perms('change_event'):
                    raise Exception("Vous n'avez pas l'autorisation de modifier des événements")
                
                try:
                    await sync_to_async((await sync_to_async(EventMetaData.objects.get)(pk=data['id'], event_id=self.event_id)).delete)()
                except EventMetaData.DoesNotExist:
                    pass
                
            elif data['type'] == 'add_eventMetaData':
                if not await self.check_perms('change_event'):
                    raise Exception("Vous n'avez pas l'autorisation de modifier des événements")
                
                for t in ('name', 'datatype', 'cost', 'description', 'choices'):
                    if t not in data:
                        raise ValueError('Missing data: %s'%t)
                        
                await sync_to_async(add_eventMetaData)(self.event_id, data)
                
            elif data['type'] == 'change_eventMetaData':
                if not await self.check_perms('change_event'):
                    raise Exception("Vous n'avez pas l'autorisation de modifier des événements")
                
                for t in ('id', 'cost', 'description', 'choices'):
                    if t not in data:
                        raise ValueError('Missing data: %s'%t)
                        
                await sync_to_async(change_eventMetaData)(self.event_id, data)
                
            elif data['type'] == 'add_MetaModel' and 'name' in data:                
                await sync_to_async(add_MetaModel)(self.event_id, data['name'])
                
            elif data['type'] == 'load_MetaModel' and 'name' in data:
                await sync_to_async(load_MetaModel)(self.event_id, data['name'])                
                
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            await self.error({'msg': '%i: %s'%(exc_tb.tb_lineno, str(e))})

    async def disconnect(self, close_code):
        await self.channel_layer.group_send(
            self.event_group_name,
            {
                'type': 'event_disconnect_notify',
                'id': self.scope['user'].id,
                'name': '%s %s'%(self.scope['user'].first_name, self.scope['user'].last_name)
            }
        )
            
        await sync_to_async(Room.objects.remove)(self.event_group_name, self.channel_name)

    
    async def event_connect_notify(self, msg):
        await self.send(text_data=json.dumps({
                'type': 'connect',
                'id': msg['id'],
                'name': msg['name']
            }))
        
    async def event_disconnect_notify(self, msg):
        await self.send(text_data=json.dumps({
                'type': 'disconnect',
                'id': msg['id'],
                'name': msg['name']
            }))
        
    async def error(self, err):
        await self.send(text_data=json.dumps({
                'type': 'error',
                'msg': err['msg']
            }))
        
    async def update_meta(self, metas):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(metas))
        
    async def add_participation(self, metas):
        # Send back changes to all WebSockets
        
        if not await self.check_perms('view_person'):
            hide = {'email': '***', 'phone': '***', 'address': '***', 'birthdate': '***', 'emg_contact': '***', 'emg_phone': '***'} 
        
            for h in hide:
                if h in metas:
                    metas[h] = hide[h]
        
        if not await self.check_perms('view_personcomment'):
            cs = []
            for c in metas['comments']:
                if not c['who_id'] == self.scope['user'].id:
                    cs += [c,]
            
            for c in cs:
                metas['comments'].remove(c)
        
        await self.send(text_data=json.dumps(metas))
        
    async def delete_participation(self, metas):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(metas))
        
    async def add_eventMetaData(self, metas):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(metas))
        
    async def delete_eventMetaData(self, metas):
        # Send back changes to all WebSockets
        await self.send(text_data=json.dumps(metas))
    
