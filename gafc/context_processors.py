# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Context processor
"""

from django.contrib import messages
from django.utils.html import mark_safe

import datetime as dt
from .forms import SeasonChoiceForm
from .models import Season, Category
import gucem.settings as settings
import os, time

def get_context(request):
    s_form = SeasonChoiceForm()
    try:
        s_form.fields['season'].initial = Season.objects.get(is_default=True).id
    except:
        pass
    
    if 'season_id' in request.session:
        s_form.fields['season'].initial = request.session['season_id']
    
    s_form.fields['season'].widget.attrs.update({'onChange':mark_safe("$(this).parents('form').submit()"), 'class':'form-control'})
    
    curr_season = Season.objects.get(pk=s_form.fields['season'].initial)
    if not curr_season.is_default and request.user.has_perm('gafc.staff_person'):
        messages.warning(request, "La saison sélectionnée n'est pas la saison en cours.")
    
    license_warn = False
            
        
    return {'g_season_form': s_form,
            'g_seasons': Season.objects.all(),
            'g_warn_licenses': license_warn,
            'g_categories': Category.objects.prefetch_related('allowed_fincat','default_fincat').all(),
            'g_payment_methods': ['CA','CH', 'BC','IN','VM','BO']}


