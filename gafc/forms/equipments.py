# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""

Forms related to equipments management

"""

from django import forms

from django.db import transaction

import unicodedata, os
import datetime as dt

from gucem import settings

from .common import GafcModelForm, GafcForm, GafcFormException, MyDateInput, set_field_html_name, EventField, normalize_name
from ..models import Equipment, EquipmentGroup, EquipmentMeta, EquipmentMetaData, EquipmentTracking, Category, EquipmentPicture

class EquipmentFilterForm(GafcForm):
    what = "equipment_filter"
    categories = forms.MultipleChoiceField(
        widget  = forms.CheckboxSelectMultiple,
        required = False,
        label="Catégories"
        )
    is_scrap = forms.BooleanField(required=False, label="Voir les équipements au rebus")
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['categories'].choices = [( c.id, c.name ) for c in Category.objects.all()]

        
class EquipmentForm(GafcModelForm):
    """
    Add/edit equipment
    """
    
    what="save_equipment"
    
    description = forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Description')
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.fields['date_service'].required=False
        self.fields['date_eol'].required=False
        
        self.fields['date_buy'].widget = MyDateInput()
        self.fields['date_service'].widget = MyDateInput()
        self.fields['date_eol'].widget = MyDateInput()
        
        self.fields['belong_to'].choices = [(t['obj'].id if t['obj'] is not None else None, t['label'] if t['obj'] is not None else '---')for t in EquipmentGroup.tree2list(EquipmentGroup.getTree())]
        self.fields['reserve'].help_text = "Affiche dans le calendrier des réservations, et vérifie la disponibilité avant l'ajout sur un événement"
        
        if 'instance' in kwargs:
            self.fields['name'].disabled=True
            self.fields['reference'].disabled=True
            self.fields['date_buy'].disabled=True
        
    class Meta:
        model = Equipment
        fields = ('name', 'reference', 'description', 'reserve', 'category', 'belong_to', 'date_buy', 'date_service', 'date_eol')
        
class EquipmentGroupForm(GafcModelForm):
    """
    Add/Edit equipment group
    """
    
    what = "save_eqGroup"
    
    description = forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Description')
    parent      = forms.ChoiceField(choices=[], required=False)
    class Meta:
        model = EquipmentGroup
        fields = ('name', 'description', 'parent')
        
    def __init__(self,*args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['parent'].choices = [(t['obj'].id if t['obj'] is not None else None, t['label'] if t['obj'] is not None else '---') for t in EquipmentGroup.tree2list(EquipmentGroup.getTree())]
        
    def clean_parent(self):
        """
        Transform parent to EquipmentGroup object
        """
        if self.cleaned_data['parent'] == '': return None
        return EquipmentGroup.objects.get(pk=self.cleaned_data['parent'])
    
    def clean(self):
        """
        Check no loop in the tree
        """
        
        if self.instance is not None:
            p = self.cleaned_data['parent']
            ids = [self.instance.id,]
            
            while p is not None and p.id not in ids:
                ids += [p.id,]
                p = p.parent
            
            if p is not None:
                self.add_error('parent',"Boucle détectée dans l'arbre!")
        
class EquipmentMetaForm(GafcModelForm):
    """
    Add/Edit an equipment meta field
    """
    what = "add_meta"
    
    class Meta:
        model = EquipmentMeta
        fields = ('name','datatype','mandatory')
        
    def __init__(self, equipment, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment to add a metadata
        :param \\*args: Extra argument passed to parent
        :param \\*\\*kwargs: Extra argument passed to parent
        """
        self.__equipment = equipment
        super().__init__(*args, **kwargs)
        
    def save(self, commit=True):
        """
        Commit the changes
        
        :param commit: Save change to DB
        """
        
        em = super().save(commit=False)
        em.equipment = self.__equipment
        
        if commit:
            em.save()
            
        return em
        
class EquipmentMetaUpdate(GafcModelForm):
    """
    Edit/delete equipment meta
    """
    
    what = "edit_meta"
    
    delete      = forms.BooleanField(label="Supprimer", required=False)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['mandatory'].required=False
        
        self.fields['name'].widget.attrs.update({'class':'form-control'})
        self.fields['mandatory'].widget.attrs.update({'class':'checkboxinput'})
        self.fields['delete'].widget.attrs.update({'class':'checkboxinput'})
        
    class Meta:
        model = EquipmentMeta
        fields = ('name','mandatory')
        
    def save(self):
        """
        Perform the changes
        """
        
        em = super().save(commit=False)
        
        if self.cleaned_data['delete']:
            em.delete()
        else:
            em.save()
        
class EquipmentMetaUpdateFormset():
    """
    Formset with all Meta
    """
    
    what = "update_meta"
    
    def __init__(self, equipment, data, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment to which the meta belong to
        """
        
        self.equipment  = equipment
        self.meta       = []
        self.prefix     = kwargs['prefix'] if 'prefix' in kwargs  else 'form'
        self.values     = data if data is not None else dict()
        self.forms      = []
        
        meta_ids = EquipmentMeta.objects.filter(equipment=self.equipment).values(('id'))
        
        for d in meta_ids:
            if '%s-%i-delete'%(self.prefix, d['id']) in self.values:
                EquipmentMeta.objects.filter(pk=d['id']).delete()
        
        metas = EquipmentMeta.objects.filter(equipment=self.equipment)
        
        for m in metas:
            
            if data is not None:
                vals = {'name': '', 'mandatory': False, 'delete': False}
                for key,val in vals.items():
                    if '%s-%i-%s'%(self.prefix, m.id, key) in self.values:
                        vals[key] = self.values['%s-%i-%s'%(self.prefix, m.id, key)]
            
                form = EquipmentMetaUpdate(vals, instance=m)
            else:
                form = EquipmentMetaUpdate(instance=m)
            
            for key, field in form.fields.items():
                set_field_html_name(field, '%s-%i'%(self.prefix, m.id))
            
            self.forms += [form,]
            
    def save(self):
        """
        Perform the changes
        """
        for f in self.forms:
            f.save()
            
    def is_valid(self):
        """
        Check the validity of all sub-forms
        """
        for f in self.forms:
            if not f.is_valid():
                print(f.errors)
                return False
        return True
        
        
class EquipmentTrackingForm(GafcForm):
    """
    Add Tracking to equipment, with metadata associated
    """
    what = "add_tracking"
    
    comment     = forms.CharField(widget=forms.Textarea(attrs={'cols':15, 'rows':2}),label='Commentaire')
    trackingtype= forms.ChoiceField(choices=EquipmentTracking.trackingtype.field.choices, label='Type')
    event       = EventField(False, label="Événement", required=False)
    
    def __init__(self, user, equipment=None, instance=None, required=False, event=None, *args, addtlt='', **kwargs):
        """
        Initialize the form
        
        :param user: User that perform the changes
        :param equipment: Equipment to which the new tracking will be related to
        :param instance: To edit a tracking instance
        :param required: Mark the required fields as mandatory.
        :param event: Event to which the tracking will be 
        :param \\*args: Extra arguments passed to parent
        :param addtlt: Position of equiment in equipment tree
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        if 'hide_event' in kwargs:
            hide_event = kwargs['hide_event']
            del kwargs['hide_event']
        else:
            hide_event = event is not None
            
        self.user = user
        self.event = event
        self.addtlt = addtlt
        
        super().__init__(*args, **kwargs)
        self.instance = instance
        
        if self.instance is not None:
            self.equipment = self.instance.equipment
            self.fields['event'].initial = self.instance.event
        else:
            self.equipment = equipment
            
        if hide_event:
            del self.fields['event']
            
        
        self.fields['comment'].required=False
        metas = EquipmentMeta.objects.filter(equipment=self.equipment)
        
        if self.instance is not None:
            self.fields['comment'].initial = self.instance.comment
            self.fields['trackingtype'].initial = self.instance.trackingtype
        
        for m in metas:
            last_v = None
            
            try:
                last_v = EquipmentMetaData.objects.filter(meta=m).exclude(data=None).order_by('-tracking__date').first()
            except Exception as e:
                print(e)
            
            nfkd_form = unicodedata.normalize('NFKD', m.name)
            name = nfkd_form.encode('ASCII', 'ignore').lower().decode().replace(' ','_')
            if m.datatype == 'ST':
                self.fields[name] = forms.CharField(max_length=200, required=m.mandatory if required else False, label=m.name)
            elif m.datatype == 'BO':
                self.fields[name] = forms.BooleanField(required=m.mandatory if required else False, label=m.name)
            elif m.datatype == 'IN':
                self.fields[name] = forms.IntegerField(required=m.mandatory if required else False, label=m.name)
            elif m.datatype == 'FL':
                self.fields[name] = forms.DecimalField(required=m.mandatory if required else False, label=m.name)
            
            self.fields[name].help_text= 'Dernière valeure: %s (%s)'%(last_v.data, last_v.tracking.date.strftime('%d/%m/%Y')) if last_v is not None and last_v.data is not None else ''
            
            if self.instance is not None:
                try:
                    currVal = EquipmentMetaData.objects.get(meta=m, tracking=self.instance)
                    self.fields[name].initial = currVal.data
                except EquipmentMetaData.DoesNotExist:
                    pass
                    
            
                
        for (k,v) in self.fields.items():
            v.widget.attrs.update({'class':'form-control rounded-left'})
                
    def save(self):
        """
        Save the changes
        """
        
        try:
            
            metas = EquipmentMeta.objects.filter(equipment=self.equipment)
            event = None
            
            if self.event is not None:
                event = self.event
            else:
                event = self.cleaned_data['event'] if 'event' in self.cleaned_data else None
            
            with transaction.atomic():
                
                if self.instance is None:
                    tracking = EquipmentTracking.objects.create(equipment=self.equipment,
                                                            event=event,
                                                            comment=self.cleaned_data['comment'],
                                                            date=event.begin if event else dt.date.today(),
                                                            person=self.user,
                                                            trackingtype=self.cleaned_data['trackingtype'] if 'trackingtype' in self.cleaned_data else 'US')
                else:
                    tracking = self.instance
                    tracking.event = event
                    tracking.comment = self.cleaned_data['comment']
                    tracking.date = event.begin if event else dt.date.today()
                    tracking.person = self.user
                    tracking.trackingtype = self.cleaned_data['trackingtype'] if 'trackingtype' in self.cleaned_data else 'US'
                    tracking.save()
                
                for m in metas:           
                    name = normalize_name(m.name)
                    
                    print(name, self.cleaned_data[name])
                    
                    metadata,cr = EquipmentMetaData.objects.update_or_create(tracking=tracking, meta=m, defaults={'data': self.cleaned_data[name] if name in self.cleaned_data else None})
        except Exception as e:
            raise GafcFormException(e)

class EquipmentDeriveForm(GafcForm):
    """
    Create a derived equipment from scrap one
    """
    what = "new_derived_equipment"
    
    name = forms.CharField(label="Nom du nouvel équipement", help_text="Ne peut être changé par après.")
    
    def __init__(self, eq, *args, **kwargs):
        """
        Initialize the form
        
        :param eq: Equipment to be derived
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        if eq.date_scrap is None:
            raise GafcFormException("Seul les équipements au rebus peuvent être dérivés")
        
        self.__base = eq
        
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Perform the changes
        """
        if self.is_valid():
            from copy import deepcopy
            eq = deepcopy(self.__base)
            eq.pk = None
            eq.date_scrap = None
            eq.date_service = dt.date.today()
            eq.parent = self.__base
            eq.name = self.cleaned_data['name']
            
            eq.save()
            
            for md in self.__base.equipmentmeta_set.all():
                md.pk = None
                md.equipment = eq
                md.save()
            return eq
        return None
                
class EquipmentTrackingFormset():
    """
    Multi-forms tracking
    """
    
    what = "add_tracking_formset"
    
    def __init__(self, person, event=None, *args, data=dict(), **kwargs):
        """
        Initialize the form
        
        :param person: User that save the tracking
        :param event: Event object
        """
        
        self.event=event
        self.person=person
        self.equipments = []
        self.forms = []
        self.prefix = kwargs['prefix'] if 'prefix' in kwargs  else 'form'
        provided_values = data is not None and len(data) > 0
        self.values = data
        self.idx=0
        
        meqs = EquipmentTracking.objects.filter(event=self.event).order_by('equipment__belong_to', 'equipment__name')
        
        for e in meqs:
            self.equipments += [e,]
        
        for e in self.equipments:
            vals = dict()
            vals['comment'] = e.comment
            vals['trackingtype'] = 'US'
            
            # Default values
            for m in e.equipment.equipmentmeta_set.all():
                name=normalize_name(m.name)
                if m.datatype == 'BO':
                    vals[name] = False
                else:
                    vals[name] = ''
                
            # Values from database
            for md in e.equipmentmetadata_set.all():
                name = normalize_name(md.meta.name)
                if md.meta.datatype == 'BO':
                    vals[name] = True if md.data else False
                else:
                    vals[name] = md.data
                    
            if provided_values:
                for key in vals:
                    if '%s-%i-%s'%(self.prefix, e.id, key) in self.values:
                        vals[key] = self.values['%s-%i-%s'%(self.prefix, e.id, key)]
                        
            print(vals, )
            
            form = EquipmentTrackingForm(self.person, e.equipment, e, False, self.event, vals)
            del form.fields['trackingtype']
            for key, field in form.fields.items():
                set_field_html_name(field, '%s-%i'%(self.prefix, e.id))
            self.forms += [ form ]
    def save(self):
        """
        Save all the subforms
        """
        for f in self.forms:
            f.save()
            
    def is_valid(self):
        """
        Check that all subforms are valid
        """
        for f in self.forms:
            if not f.is_valid():
                return False
        return True
    
class EquipmentTrackingFormCheck(GafcForm):
    """
    Display and perform tracking of all equipments below the tree
    """
    
    what = "add_equipment_tracking_check"
    
    def __init__(self,tree,person, data=None, *args, **kwargs):
        """
        Initialize the form
        
        :param tree: Tree element root
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        self.person = person
        self.tree = tree
        self.equipments = []
        self.forms = dict()
        self.prefix = kwargs['prefix'] if 'prefix' in kwargs  else 'form'
        provided_values = data is not None
        self.values = data if provided_values else dict()
        self.idx=0
        
        lst = EquipmentGroup.tree2list(self.tree,withEquipments=True)
        
        treelt = ''
        
        for e in lst:
            if e['type'] == 'equipment':
                if e['obj'].date_scrap is None:
                    self.equipments += [(treelt, e['obj']),]
            else:
                treelt = e['label']
        
        for tlt, e in self.equipments:
            vals = dict()
            vals['comment'] = ''
            vals['trackingtype'] = self.values['trackingtype'] if 'trackingtype' in self.values else 'US'
            
            # Default values
            for m in e.equipmentmeta_set.all():
                name=normalize_name(m.name)
                if m.datatype == 'BO':
                    vals[name] = False
                else:
                    vals[name] = ''
                    
            if provided_values:
                for key in vals:
                    if '%s-%i-%s'%(self.prefix, e.id, key) in self.values:
                        vals[key] = self.values['%s-%i-%s'%(self.prefix, e.id, key)]
            
            form = EquipmentTrackingForm(person, e, None, False, None, vals, addtlt=tlt, hide_event=True);
            del form.fields['trackingtype']
            for key, field in form.fields.items():
                set_field_html_name(field, '%s-%i'%(self.prefix, e.id))
            self.forms[e.id] = form
            
    def save(self):
        print(self.values.getlist('equipment_id'), self.values)
        for eid, f in self.forms.items():
            if 'equipment_id' in self.values and '%i'%eid in self.values.getlist('equipment_id'):
                print(f"saveing {eid}")
                f.save()
            
    def is_valid(self):
        for eid, f in self.forms.items():
            if 'equipment_id' in self.values and '%i'%eid in self.values.getlist('equipment_id') and not f.is_valid():
                return False
        return True

class EquipmentPictureForm(GafcForm):
    """
    Upload a picture related to equipment
    """
    
    what = "equipment_addfile"
    submit_txt = "Télécharger"
    
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False, 'accept':'image/jpeg,image/png'}), label="Ajouter photo")
        
    def __init__(self, equipment, request, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment belonging to the image
        :param request: Request object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.__equipment = equipment
        self.__request = request
        self.helper.label_class='col-12'
        self.helper.field_class='col-12'
        self.fields['file'].validators = (self.validate_is_image,)
        
    def validate_is_image(self, file):
        """
        Image validator
        
        :param file: File name
        """
        import magic
        
        valid_mime_types = ['image/jpeg', 'image/png']
        file_mime_type = magic.from_buffer(file.read(2048), mime=True)
        if file_mime_type not in valid_mime_types:
            raise forms.ValidationError('Unsupported file type.')
            
        if file.size > 1024 * 1024:
            raise forms.ValidationError('File size limit is 1Mb')  
        
    def save(self):
        """
        Save the file and add entry to database
        """
        with transaction.atomic():
            cf = EquipmentPicture.objects.create(filename=self.__request.FILES['file'].name, equipment=self.__equipment)
            os.makedirs(cf.disk_fullpath, exist_ok=True)
            
            with open(cf.disk_fullfilepath, 'wb+') as destination:
                for chunk in self.__request.FILES['file'].chunks():
                    destination.write(chunk)
                    
                    
class DeleteEquipmentPictureForm(GafcForm):
    """
    Delete a picture linked to equipment
    """
    
    what = "delete_equipment_picture"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    pic = forms.ModelChoiceField(EquipmentPicture.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, equipment, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment linked to picture
        :param \\*args: Extra argument passed to parent
        :param \\*\\*kwargs: Extra argument passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.fields['pic'].queryset = equipment.equipmentpicture_set.all()
        
    def save(self):
        """
        Commit the changes and delete the file on disk
        """
        
        with transaction.atomic():
            cf = self.cleaned_data['pic']
            
            filename = settings.GAFC_FILEFOLDER+'/equipments/%s'%(cf.disk_filename)
            if os.path.isfile(filename):
                os.unlink(filename)
                
            cf.delete()
            
class ScrapEquipmentForm(GafcForm):
    """
    Delete a picture linked to equipment
    """
    
    what = "scrap_equipment"
    submit_txt = "Mise au rebus"
    submit_class = "btn-danger"
    
    def __init__(self, equipment, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment linked to picture
        :param \\*args: Extra argument passed to parent
        :param \\*\\*kwargs: Extra argument passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.__equipment = equipment
        
    def save(self, commit=True):
        """
        Commit the changes and delete the file on disk
        """
        
        self.__equipment.date_scrap = dt.date.today()
        
        if commit:
            self.__equipment.save()
            
        return self.__equipment
        
class DeleteEquipmentGroupForm(GafcForm):
    """
    Delete a picture linked to equipment
    """
    
    what = "delete_equipment_group"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    def __init__(self, equipmentgrp, *args, **kwargs):
        """
        Initialize the form
        
        :param equipment: Equipment linked to picture
        :param \\*args: Extra argument passed to parent
        :param \\*\\*kwargs: Extra argument passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.__equipmentgrp = equipmentgrp
        
    def save(self):
        """
        Commit the group
        """
        
        try:
            self.__equipmentgrp.delete()
        except Exception as e:
            raise GafcFormException(e)
        
        
        
        
        
        
        
        
        
        
        
        
        