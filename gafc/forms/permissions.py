# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Permissions management forms
"""

from django import forms

from django.contrib import auth

from django.db import transaction

from .common import GafcForm, GafcFormException

from ..models import StaffEventPermission, Person
from ..permissions import GAFC_PERMISSIONS, GAFC_PERMISSIONS_DICT


class GroupForm(GafcForm):
    """
    Permission group management form
    """
    
    what = "change_group"
    
    name = forms.CharField(max_length=150, label="Nom")
    permissions = forms.MultipleChoiceField(choices=GAFC_PERMISSIONS, label="Permissions", required=False) 
    
    def __init__(self, instance=None, *args, **kwargs):
        """
        Initialize the form
        
        :param instance: auth.Group instance
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
            
        super().__init__(*args, **kwargs)
        
        self.instance = instance
        
        if self.instance is not None:
            self.fields['name'].initial = self.instance.name
            self.fields['permissions'].initial = ["%s.%s"%(val.content_type.app_label, val.codename) for val in self.instance.permissions.all()]
        
        self.fields['permissions'].widget.attrs.update(size=len(GAFC_PERMISSIONS_DICT)*1.2)
        
    def save(self):
        """
        Commit the changes
        """
        
        if self.is_valid():
            if self.instance is not None:
                grp = self.instance
            else:
                grp = auth.models.Group()
                
            with transaction.atomic():
                grp.name = self.cleaned_data['name']
                grp.save()
                                
                grp.permissions.set( [ auth.models.Permission.objects.get(codename=p.split('.')[1], content_type__app_label=p.split('.')[0]) for p in self.cleaned_data['permissions'] ])
                
                                        
class EventStaffPermissionForm(GafcForm):
    """
    Set Event-specific permissions for staff
    """
    
    what = "change_event_staff_permissions"
    permissions = forms.MultipleChoiceField(choices=GAFC_PERMISSIONS, label="Permissions", required=False)
    instance = None
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['permissions'].initial = ["%s.%s"%(val.perm.content_type.app_label, val.perm.codename) for val in StaffEventPermission.objects.select_related('perm','perm__content_type').all()]
        self.fields['permissions'].widget.attrs.update(size=25)
        
    def save(self):
        """
        Commit the changes
        """
        
        if self.is_valid():
                
            with transaction.atomic():
                StaffEventPermission.objects.all().delete()
                
                for p in self.cleaned_data['permissions'] :
                    StaffEventPermission.objects.create(perm=auth.models.Permission.objects.get(codename=p.split('.')[1], content_type__app_label=p.split('.')[0]))
  

class AddSuperUserForm(GafcForm):
    """
    Set the superuser flag to the user
    """
    
    what = "add_superuser"
    
    person_obj = forms.ModelChoiceField(Person.objects.all(), empty_label = None, widget=forms.HiddenInput())
    
    def save(self, commit=True):
        """
        Set the flag
        
        :param commit: Perform the changes
        
        :return The user object
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            superU = self.cleaned_data['person_obj']
            superU.is_superuser = True
            
            if commit:
                superU.save()
            
            return superU
            
        except Exception as e:
            raise GafcFormException(e)
            
class DelSuperUserForm(GafcForm):
    """
    Set the superuser flag to the user
    """
    
    what = "del_superuser"
    
    submit_txt = "X"
    submit_class = "btn-danger p-1"
    
    person_obj = forms.ModelChoiceField(Person.objects.all(), empty_label = None, widget=forms.HiddenInput())
    
    def save(self, commit=True):
        """
        Set the flag
        
        :param commit: Perform the changes
        
        :return The user object
        
        :raise GafcFormException: If something went wrong
        """
        
        if Person.objects.filter(is_superuser=True).count() == 1:
            raise GafcFormException("Au moins un superutilisateur doit être défini!")
                
        try:
            superU = self.cleaned_data['person_obj']
            superU.is_superuser = False
            
            if commit:
                superU.save()
            
            return superU
            
        except Exception as e:
            raise GafcFormException(e)
            


class AddGroupUserForm(GafcForm):
    """
    Add an user to the auth group
    """
    
    what = "add_groupuser"
    
    person_obj = forms.ModelChoiceField(Person.objects.all(), empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, group, *args, **kwargs):
        """
        Initialize the form
        
        :param group: Group object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__group = group
    
    def save(self, commit=True):
        """
        Add the user to the group
        
        :param commit: Perform the changes
        
        :return The user object
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            p = self.cleaned_data['person_obj']
            p.groups.add(self.__group)
            
            if commit:
                p.save()
            
            return p
            
        except Exception as e:
            raise GafcFormException(e)
            
class DelGroupUserForm(GafcForm):
    """
    Remove a user to auth group
    """
    
    what = "del_groupuser"
    
    submit_txt = "X"
    submit_class = "btn-danger p-1"
    
    person_obj = forms.ModelChoiceField(Person.objects.all(), empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, group, *args, **kwargs):
        """
        Initialize the form
        
        :param group: Group object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__group = group
    
    def save(self, commit=True):
        """
        Remove the user to the group
        
        :param commit: Perform the changes
        
        :return The user object
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            p = self.cleaned_data['person_obj']
            p.groups.remove(self.__group)
            
            if commit:
                p.save()
            
            return p
            
        except Exception as e:
            raise GafcFormException(e)
            
            
class DelGroupForm(GafcForm):
    """
    Delete the group
    """
    
    what = "del_group"
    
    submit_txt = "Supprimer le groupe"
    submit_class = "btn-danger"
    
    def __init__(self, group, *args, **kwargs):
        """
        Initialize the form
        
        :param group: Group object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__group = group
    
    def save(self):
        """
        Remove the group
        
        :param commit: Perform the changes
        
        :return The user object
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            self.__group.delete()
        except Exception as e:
            raise GafcFormException(e)
            