# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


from django import forms

from django.db import transaction

from django.contrib.auth.password_validation import password_validators_help_text_html, validate_password

from django.views.decorators.debug import sensitive_variables

from crispy_forms.layout import Layout, Field

import datetime as dt

from ..models import Person, License

from .common import GafcModelForm, MyDateInput, GafcForm
from .persons import PersonForm, Federation



class PersonRegisterForm(GafcModelForm):
    """
    Register form
    """
    
    what = "new_person"
    email=forms.EmailField(error_messages={'unique': 'Un compte existe déjà avec cette adresse e-mail'})
    #address=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Rue, numéro')
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['birthdate'].widget = MyDateInput()
        self.fields['gender'].choices = (('M', 'Homme'), ('F', 'Femme'), ('N', 'Non Binaire'))
    
    class Meta:
        model = Person
        fields = ('first_name','last_name','gender','birthdate','street','postcode','city','country','email','phone','emg_contact','emg_phone')


class PersonAndLicenseForm(PersonForm):
    """
    Create a person with license directly
    """
    
    what = "person_license_save"
    
    federation = forms.ChoiceField(label="Fédération", required=False)
    licenseno  = forms.CharField(label="Numéro de licence", required=False, max_length=30)
    date_end   = forms.DateField(label="Fin de validité de la licence", required=False, widget=MyDateInput())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['federation'].choices = [('', 'Pas de licence'),] + [(f.id, f.acronym) for f in Federation.objects.all()]
        
        today = dt.date.today()
        fin = dt.date(day=30, month=9, year=(today.year if today.month < 9 else today.year+1))
        self.fields['date_end'].initial = fin
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes to DB
        
        :return: (Person, License) objects
        """
        p = super().save(commit=False)
        
        l = None
        
        if self.cleaned_data['federation'] != '':
            l = License(person=p,
                        number=self.cleaned_data['licenseno'],
                        federation_id=self.cleaned_data['federation'],
                        date_in=dt.datetime.now(),
                        date_end=self.cleaned_data['date_end'])
            
        if commit:
            with transaction.atomic():
                p.save()
                if l is not None: l.save()
                
        return (p,l)
        
class PersonFormUser(PersonRegisterForm):
    """
    User-side edition form
    """
    
    what = "person_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Field('first_name', readonly=True, css_class="form-control-plaintext"),
            Field('last_name', readonly=True, css_class="form-control-plaintext"),
            'gender',
            'street',
            'postcode',
            'city',
            'country',
            'birthdate',
            Field('email', readonly=True, css_class="form-control-plaintext"),
            'phone',
            'emg_contact',
            'emg_phone'
        )
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes on DB
        """
        
        p = super().save(commit=False)
        if p.pk is not None: # Can't change first and last name
            ins = Person.objects.get(pk=p.pk)
            p.first_name = ins.first_name
            p.last_name = ins.last_name
            p.email = ins.email
            
        if commit:
            p.save()
            
        return p

class ChangePasswordForm(GafcForm):
    """
    Update password form
    """
    
    what = "update_pwd"
    
    old_password = forms.CharField(label="Ancien mot de passe", widget=forms.PasswordInput())
    new_password = forms.CharField(label="Nouveau mot de passe", widget=forms.PasswordInput(), help_text=password_validators_help_text_html())
    new_password2= forms.CharField(label="Répéter le mot de passe", widget=forms.PasswordInput())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    @sensitive_variables('np','np2')
    def clean_new_password2(self):
        """
        Check that both password are consistent
        """
        np = self.cleaned_data['new_password']
        np2 = self.cleaned_data['new_password2']
        
        try:
            validate_password(np)
        except forms.ValidationError as v:
            for err in v:
                self.add_error('new_password', err)
        
        if not np == np2:
            raise forms.ValidationError("Les mots de passe ne correspondent pas")
        
        return np2
        
    
class LoginForm(GafcForm):
    """
    Login form
    """
    
    what = "login"
    submit_txt = "Login"
    email = forms.EmailField(label="Adresse e-mail")
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput())
        
class PasswordRecoveryForm(GafcForm):
    """
    Password recovery form
    """
    
    what = "password_recovery"
    submit_txt = "Envoyer le lien"
    data = forms.CharField(label="Adresse e-mail ou numéro de licence")


