# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Person management forms
"""


from django import forms

from django.shortcuts import get_object_or_404

from django.db import transaction, models

from django.views.decorators.debug import sensitive_variables

from django.contrib.auth.password_validation import password_validators_help_text_html, validate_password

from .common import GafcModelForm, GafcForm, MyDateInput, GafcFormException, normalize_name
from ..models import Person, License, PersonComment, Federation, SympaMailingList

import datetime as dt
from .. import ffcam_utils


class PersonForm(GafcModelForm):
    """
    Save and edit persons
    """
    what = "person_save"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['birthdate'].widget = MyDateInput()
        self.fields['birthdate'].initial = dt.date.today()
        
        self.fields['phone'].required=False
        self.fields['emg_phone'].required=False
        self.fields['emg_contact'].required=False
    
    class Meta:
        model = Person
        fields = ('first_name','last_name','gender','birthdate','street','postcode','city','country','email','phone','emg_contact','emg_phone')



class SetUnusablePasswordForm(GafcForm):
    """
    Set the password of person unusable
    """
    
    what = "delete_password"
    submit_txt = "Retirer les accès"
    submit_class = "btn-danger"
    
    def __init__(self, person, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to set password unusable
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
    
    def save(self, commit=True):
        """
        Perform the changes
        
        :param commit: Commit the changes
        
        :return the person instance
        """
        
        self.__person.set_unusable_password()
        
        if commit:
            self.__person.save()
            
        return self.__person
    
class SetNewPasswordForm(GafcForm):
    """
    Set a new password to person
    """
    
    what = "set_password"
    
    new_password = forms.CharField(label="Nouveau mot de passe", widget=forms.PasswordInput(), help_text=password_validators_help_text_html())
    new_password2= forms.CharField(label="Répéter le mot de passe", widget=forms.PasswordInput())
     
    def __init__(self, person, *args, **kwargs):
        """
        Initialize form
        
        :param person: Person instance
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
            
        self.__person = person
        
    @sensitive_variables('np','np2')
    def clean_new_password2(self):
        """
        Check that both password are the same
        """
        
        np = self.cleaned_data['new_password']
        np2 = self.cleaned_data['new_password2']
        
        try:
            validate_password(np)
        except forms.ValidationError as v:
            for err in v:
                self.add_error('new_password', err)
        
        if not np == np2:
            raise forms.ValidationError("Les mots de passe ne correspondent pas")
        
        return np2
    
    def save(self, commit=True): 
        """
        Set the new password
        
        :param commit: Save the changes
        
        :return Person object
        """
        self.__person.set_password(self.cleaned_data['new_password'])
        if commit:
            self.__person.save()
            
        return self.__person
    
    
class DeleteLicenseForm(GafcForm):
    """
    Delete the license associated to person
    """
    
    what = "delete_license"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    license_obj = forms.ModelChoiceField(License.objects.all(),empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, person, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to delete a license object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
        
        self.fields['license_obj'].queryset = self.__person.license_set.all()
    
    def save(self):
        """
        Perform the deletion
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            self.cleaned_data['license_obj'].delete()
        except Exception as e:
            raise GafcFormException(e)
            
            
    
class MergePersonForm(GafcForm):
    """
    Merge two accounts
    """
    
    what = "merge_accounts"
    submit_txt = "Fusionner"
    
    email = forms.EmailField(label="Fusionner vers")
    check = forms.BooleanField(label="Cocher pour valider")
    
    def __init__(self, person, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to be merged with the one corresponding to given email address
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
        
    def save(self):
        """
        Perform the merge
        
        :raise GafcFormException if something went wrong
        
        :return the merged account
        """
        
        p = self.__person
        p2 = get_object_or_404(Person, email=self.cleaned_data['email'])
        
        
        try:
            with transaction.atomic():
                fields = Person._meta.get_fields(include_parents=True)

                for f in fields:

                    if f.is_relation and type(f) not in [models.fields.reverse_related.ManyToManyRel,models.fields.related.ManyToManyField ] :

                        f.related_model.objects.filter(**{f.remote_field.name: p}).update(**{f.remote_field.name: p2})

                p.delete()
                
        except Exception as e:
            raise GafcFormException(e)
            
        return p2
    
class LicenseForm(GafcModelForm):
    """
    Add a license to person
    """
    
    what = "add_license"
    
    class Meta:
        model=License
        fields = ('federation','number','date_in', 'date_end')
        
    def __init__(self, person, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to be merged with the one corresponding to given email address
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        
        super().__init__(*args, **kwargs)
        
        if type(person) is not Person:
            raise GafcFormException("person must be a Person instance")
        
        self.__person = person
        
        today = dt.date.today()
        self.fields['date_in'].widget = MyDateInput()
        self.fields['date_in'].initial = today
        
        fin = dt.date(day=30, month=9, year=(today.year if today.month < 9 else today.year+1))
        
        self.fields['date_end'].widget = MyDateInput()
        self.fields['date_end'].initial = fin
        
    def save(self, commit=True):
        """
        Create the new licence object
        
        :param commit: Save the changes
        
        :return License object
        """
        l = super().save(commit=False)
        l.person = self.__person
        
        if commit:
            l.save()
            
        return l
    
class PersonCommentForm(GafcModelForm):
    """
    Add a comment to a person object
    """
    
    what = "comment_save"
    submit_txt = "Enregistrer"
    
    text = forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Commentaire')
    
    def __init__(self, person, who, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to add a comment
        :param who: The Person instance who add the comment
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
        self.__who= who
        
    class Meta:
        model = PersonComment
        fields = ('level', 'text')
        
    def save(self ,commit=True):
        """
        Add the comment
        
        :param commit: Save the changes
        
        :raise GafcFormException: if something went wrong
        
        :return PersonComment instance
        """
        
        c = super().save(commit=False)
        c.person = self.__person
        c.who = self.__who
        
        if commit:
            c.save()
            
        return c
        
    
    
class DeletePersonCommentForm(GafcForm):
    """
    Delete the license associated to person
    """
    
    what = "delete_person_comment"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    comment_obj = forms.ModelChoiceField(PersonComment.objects.all(),empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, person, who, *args, **kwargs):
        """
        Initialize form
        
        :param person: The Person instance to delete a license object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        
        :raise GafcFormException: If something went wrong
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
        
        qset = self.__person.personcomment_set.all()
        
        if not who.has_perm('gafc.delete_personcomment'):
            qset = qset.filter(who=who)
        
        self.fields['comment_obj'].queryset = qset
    
    def save(self):
        """
        Perform the deletion
        
        :raise GafcFormException: If something went wrong
        """
        
        try:
            self.cleaned_data['comment_obj'].delete()
        except Exception as e:
            raise GafcFormException(e)
            
            
                    
class DiscoveryLicenseForm(GafcForm):
    """
    Form to create new discovery license from FFCAM
    """
    
    what = "create_discovery_license"
    
    username = forms.CharField(label="Nom d'utilisateur EXTRANET")
    password = forms.CharField(label="Mot de passe EXTRANET", widget=forms.PasswordInput())
    federation=forms.ChoiceField(label="Fédération")
    date_in  = forms.DateField(label="Début", widget=MyDateInput())
    duration = forms.ChoiceField(label="Durée", choices=[(24, '1 jour'),(48, '2 jours'), (72, '3 jours')])
    
    def __init__(self, person, *args, **kwargs):
        """
        Initialize the form
        
        :param person: Person object to add the discovery license
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.__person = person
        
        self.fields['federation'].choices = [(f.id, f.acronym) for f in Federation.objects.all()]
        
        
    def save(self):
        """
        Create discovery license
        
        :return the License object
        """
        
        try:
            with ffcam_utils.FFCAMExtranetConnexxion(self.cleaned_data['username'], self.cleaned_data['password']) as conn:
                
                return conn.create_new_discovery_license(self.__person,
                                                         self.cleaned_data['federation'],
                                                         self.cleaned_data['date_in'],
                                                         self.cleaned_data['duration'])
        except Exception as e:
            raise GafcFormException(e)
            
        
        
        
###################################################################
###                                                             ###
###     OLD FORMS FOR PERSON IMPORT. WILL MAYBE BE USED LATER   ###
###                                                             ###
###################################################################


class BulkLicenceForm(GafcForm):
    """
    Import set date and federation to license bulk import
    """
    
    what = "licence_validate"
    
    federation = forms.ChoiceField(label='Fédération')
    date_end   = forms.DateField(label="Fin de validité des licences", widget=MyDateInput())
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['federation'].choices = [(f.id, f.acronym) for f in Federation.objects.all()]
        
        today = dt.date.today()
        fin = dt.date(day=30, month=9, year=(today.year if today.month < 9 else today.year+1))
        self.fields['date_end'].initial = fin
        
        self.helper.form_tag = False
        
        
class ExtranetForm(forms.Form):
    """
    FFCAM EXTRANET login form
    """
    
    what ="download_from_ffcam"
    
    username = forms.CharField(label="Nom d'utilisateur EXTRANET")
    password = forms.CharField(label="Mot de passe EXTRANET", widget=forms.PasswordInput())
        

        
class MemberFileForm(forms.Form):
    """
    Upload CSV member file
    """
    
    what = "upload_memberfile"
    
    file = forms.FileField(label="Fichier des adhérents")
        
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra argument passed to parent
        :param \\*\\*kwargs: Extra argument passed to parent
        """
        super().__init__(*args, **kwargs)
        self.helper.is_multipart = True
        
class exampleChoiceField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        self.examples = None
    
        if 'examples' in kwargs:
            self.examples = kwargs['examples']
            del kwargs['examples']
            
        super().__init__(*args, **kwargs)
            
class FieldAttributionForm(forms.Form):
    def __init__(self, headers, *args, **kwargs):
        
        self.examples = None
        if 'examples' in kwargs:
            self.examples = kwargs['examples']
            del kwargs['examples']
        
        super().__init__(*args, **kwargs)
        
        i=0
        
        mlcodes = []
        for ml in SympaMailingList.objects.exclude(code=None).exclude(code=''):
            mlcodes += [ml.code,]
        
        for h in headers:
            
            e = []
            for ex in self.examples:
                if i < len(ex):
                    e += [ex[i],]
                else:
                    e += [None,]
                    
            i += 1
            
            n = normalize_name(h)
            
            init = '';
            hh = h.lower()
            
            if hh == 'id':
                init = 'LICENCE'
                
            if hh == 'qualite':
                init = 'GENDER'
                
            if hh == 'nom':
                init = 'LASTNAME'
                
            if hh == 'prenom':
                init = 'FIRSTNAME'
                
            if hh in ['numrue','rue']:
                init = 'NUMRUE'
                
            if hh == 'codpost':
                init = 'CP'
                
            if hh == 'ville':
                init = 'CITY'
                
            if hh == 'pays':
                init = 'COUNTRY'
                
            if hh == 'inscription':
                init = 'LICENCE_DATE'
                
            if hh in ['mel','mail','e-mail']:
                init = 'EMAIL'
                
            if hh in ['por','tel']:
                init = 'TEL'
                
            if 'nais' in hh:
                init = 'DATENAISS'
                
            if hh == 'accidentqui':
                init = 'EMG_CONTACT'
                
            if hh == 'acc':
                init = 'EMG_PHONE'
                
            if hh == 'caci_millesime':
                init = 'CACI'
                
            if h in mlcodes:
                init = 'MAILING'
            
            self.fields[n] = exampleChoiceField(required=False,
                                               label=h,
                                               examples = e,
                                               choices=[('','---'),
                                                        ('LASTNAME','Nom'),
                                                        ('FIRSTNAME', 'Prénom'),
                                                        ('GENDER', 'Genre'),
                                                        ('NUMRUE', 'Rue, numéro'),
                                                        ('CP', 'Code postal'),
                                                        ('CITY', 'Ville'),
                                                        ('COUNTRY', 'Pays'),
                                                        ('LICENCE', 'Numéro de licence'),
                                                        ('LICENCE_DATE','Date de licence'),
                                                        ('EMAIL','Adresse e-mail'),
                                                        ('TEL','Téléphone'),
                                                        ('DATENAISS','Date de naissance'),
                                                        ('EMG_CONTACT','Contact en cas d\'urgence'),
                                                        ('EMG_PHONE','Téléphone en cas d\'urgence'),
                                                        ('CACI', 'Année certificat médical'),
                                                        ('MAILING','Mailing List Sympa')],
                                               initial = init)
            
            