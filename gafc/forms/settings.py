# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Settings forms
"""


from django import forms
from django.db import transaction
from django.db.models import Q

from .common import GafcModelForm, GafcForm, MyDateInput, PersonTypeaheadWidget, GafcFormException
from ..models import Federation, Season, Person, Participation, Payment, InternalPayment, EquipmentTracking, EventState, Cashing, License, StaffEmail, StaffEmailAlias, CashingAbandonValidator

import datetime as dt
import os
from gucem import settings as gs


class FederationForm(GafcModelForm):
    """
    Add/Edit federation
    """
    
    what = 'add_federation'
    
    class Meta:
        model = Federation
        fields = '__all__'
        
        
class SeasonForm(GafcModelForm):
    """
    Add/Edit season
    """
    
    what = 'add_season'
    
    class Meta:
        model = Season
        fields = ('name',)
        
class SeasonChoiceForm(GafcForm):
    """
    Set the default season
    """
    season = forms.ModelChoiceField(Season.objects.order_by('-name'), label='Saison')
    what = "set_season"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.fields['season'].initial = Season.objects.get(is_default=True)
        
    def save(self):
        """
        Set the default season
        """
        
        self.cleaned_data['season'].is_default = True
        self.cleaned_data['season'].save()
        
class DeletePersonsForm(GafcForm):
    """
    Delete inactive accounts since a specified date
    """
    
    what = "persons_delete"
    submit_txt = "Supprimer"
    submit_class="btn-danger"
    
    date = forms.DateField(widget=MyDateInput(), label="Inactifs depuis", initial=dt.date.today()+dt.timedelta(days=-365*2))
    confirm = forms.BooleanField(label="Confirmer")
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        
    def save(self):
        """
        Do the deletion
        
        :return: The number of account deleted
        """
        
        with transaction.atomic():
            d = self.cleaned_data['date']
            
            p = Person.objects.exclude(is_superuser=True).exclude(gender='X').filter(Q(last_login=None) | Q(last_login__lt=d))
            
            ids = License.objects.filter(date_end__gte=d).values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = Participation.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = Payment.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = Payment.objects.all().values_list('encoder__pk')
            p = p.exclude(pk__in=ids)
            
            ids = Payment.objects.exclude(refund_who=None).values_list('refund_who__pk')
            p = p.exclude(pk__in=ids)
            
            ids = InternalPayment.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = EquipmentTracking.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = EventState.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            ids = Cashing.objects.all().values_list('person__pk')
            p = p.exclude(pk__in=ids)
            
            n,_ = p.delete()
            
        return n
        
        
class StaffEmailForm(GafcModelForm):
    """
    Add/Edit Staff Email account
    """
    
    what = "save_staffemail"
    inst = forms.ModelChoiceField(StaffEmail.objects.all(),widget=forms.HiddenInput(), required=False)
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['noauth'].required=False
        
    class Meta:
        model = StaffEmail
        fields = ('email', 'noauth')
        
        
    def save(self, commit=True):
        """
        Add/Edit the StaffEmail object
        
        :param commit: Save the changes on DB
        """
        
        if self.cleaned_data['inst'] is not None:
            inst = self.cleaned_data['inst']
            inst.email = self.cleaned_data['email']
            inst.noauth = self.cleaned_data['noauth']
        else:
            inst = super().save(commit=False)
    
    
        if commit:
            inst.save()
            
        return inst
    
    
class DeleteStaffEmailForm(GafcForm):
    """
    Delete a StaffEmail element
    """
    
    what = "delete_staffemail"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    inst = forms.ModelChoiceField(StaffEmail.objects.all(), widget=forms.HiddenInput())
        
    def save(self):
        """
        Commit the deletion
        """
        
        self.cleaned_data['inst'].delete()
        
class StaffEmailAliasForm(GafcModelForm):
    """
    Add/Edit Staff Email alias
    """
    
    what = "save_staffemailalias"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    class Meta:
        model = StaffEmailAlias
        fields = ('main', 'email')

class DeleteStaffEmailAliasForm(GafcForm):
    """
    Delete a StaffEmailAlias element
    """
    
    what = "delete_staffemailalias"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    inst = forms.ModelChoiceField(StaffEmailAlias.objects.all(), widget=forms.HiddenInput())
        
    def save(self):
        """
        Commit the deletion
        """
        
        self.cleaned_data['inst'].delete()
        
        
class StaffEmailSenderForm(GafcForm):
    """
    Add/Edit Staff Email authorized sender
    """
    
    what = "save_staffemailsender" 
    
    main = forms.ModelChoiceField(StaffEmail.objects.filter(noauth=False))
    sender = forms.ModelChoiceField(Person.objects.all(), widget=PersonTypeaheadWidget())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Add the selected person to authorized senders
        """
        
        self.cleaned_data['main'].authorized.add(self.cleaned_data['sender'])

class DeleteStaffEmailSenderForm(GafcForm):
    """
    Delete a StaffEmailAlias element
    """
    
    what = "delete_staffemailsender"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    main = forms.ModelChoiceField(StaffEmail.objects.all(), widget=forms.HiddenInput())
    sender = forms.ModelChoiceField(Person.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Remove the selected person from authorized senders
        """
        
        self.cleaned_data['main'].authorized.remove(self.cleaned_data['sender'])
        
        
class CashingAbandonValidatorForm(GafcModelForm):
    """
    Add a person as authorized cashing validator
    """
    
    what = "add_abandonvalidator"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['person'].widget = PersonTypeaheadWidget(attrs={"placeholder": "Validateur"})

    class Meta:
        model = CashingAbandonValidator
        fields = ('person', 'titre',)
        
class CashingAbandonValidatorFileForm(GafcForm):
    """
    Add a signature file to CashingAbandonValidator
    """
    
    what = "add_abandonvalidator_file"
    submit_txt = "Télécharger"
    
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False, 'accept':'image/jpeg'}), label="Image signature")
    validator = forms.ModelChoiceField(CashingAbandonValidator.objects.all(), label="Signataire")
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.is_multipart = True
        
    def save(self):
        """
        Add a file to a CashingAbandonValidator instance
        """
        
        sign_img = gs.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%self.cleaned_data['validator'].person.id
        os.makedirs(gs.GAFC_FILEFOLDER+'/signatures/',exist_ok=True)
        
        with open(sign_img+'.tmp','wb') as fd:
            for chunk in self.cleaned_data['file'].chunks():
                fd.write(chunk)
                
        import magic
        mime = magic.Magic(mime=True)
        
        if mime.from_file(sign_img+'.tmp') == 'image/jpeg':
            os.rename(sign_img+'.tmp', sign_img)
        else:
            os.remove(sign_img+'.tmp')
            raise GafcFormException('Seul le format JPEG est accepté')
            
class DeleteCashingAbandonValidatorFileForm(GafcForm):
    """
    Delete a signature file to CashingAbandonValidator
    """
    inst = forms.ModelChoiceField(CashingAbandonValidator.objects.all(), widget=forms.HiddenInput())
    
    what = "delete_cashingabandonvalidator_file"
    submit_txt = "X"
    submit_class = "btn-danger"
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Delete the file to a CashingAbandonValidator instance
        """
        
        sign_img = gs.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%self.cleaned_data['inst'].person.id
        
        if os.path.exists(sign_img):
            os.unlink(sign_img)

        

class DeleteCashingAbandonValidatorForm(GafcForm):
    """
    Delete a CashingAbandonValidator
    """
    
    what = "delete_cashing_abandon_validator"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    inst = forms.ModelChoiceField(CashingAbandonValidator.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Remove the selected person from authorized senders
        """
        
        self.cleaned_data['inst'].delete()
        sign_img = gs.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%self.cleaned_data['inst'].person.id
        
        if os.path.exists(sign_img):
            os.unlink(sign_img)