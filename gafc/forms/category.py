# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""

Forms related to category management

"""


from django import forms

from django.db import transaction

import datetime as dt

from .common import GafcModelForm, GafcForm, GafcFormException, MyMultiDateTime
from ..models import Staff, Validation, Category, Event, EventState



   
class CategoryForm(GafcModelForm):
    """
    Category management form
    """
    
    what = "save_category"
    
    class Meta:
        model = Category
        fields = ('name', 'is_global')


class CategoryStaffForm(GafcModelForm):
    """
    Add a Staff person in category
    """
    
    what = "add_category_staff"
    
    class Meta:
        model = Staff
        fields = ('person','level')
    
    def __init__(self, category, season, *args, **kwargs):
        """
        Initialize the form
        
        :param category: Category of staffing
        :param season: Season of staff person
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.category = category
        self.season = season
        
        super().__init__(*args, **kwargs)
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes in db
        
        :return the Staff object
        """
        
        s = super().save(commit=False)
        s.category = self.category
        s.season = self.season
        
        try:
            if commit:
                s.save()
        except Exception as e:
            raise GafcFormException(e)
        
class DeleteCategoryStaffForm(GafcForm):
    """
    Delete a Staff
    """
    
    what = "del_category_staff"
    staff_obj = forms.ModelChoiceField(Staff.objects.all(),empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, category, season, *args, **kwargs):
        """
        Initialize the form
        
        :param category: The category
        :param season: The season
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['staff_obj'].queryset = Staff.objects.filter(category=category, season=season)
        
        
    def save(self):
        """
        Perform the deletion
        """
        
        try:
            self.cleaned_data['staff_obj'].delete()
        except Exception as e:
            raise GafcFormException(e)
        

class StaffValidationForm(GafcModelForm):
    """
    Add a Staff person in category
    """
    
    what = "add_staff_validation"
    
    class Meta:
        model = Validation
        fields = ('staff',)
    
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
    
        :param user: Validator of the staff
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.user = user
        
        super().__init__(*args, **kwargs)
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes in db
        
        :return the Staff object
        """
        
        v = super().save(commit=False)
        v.validator = self.user
        
        try:
            if commit:
                v.save()
        except Exception as e:
            raise GafcFormException(e)
        
        
class DeleteStaffValidationForm(GafcForm):
    """
    Delete a Validation for a staff
    """
    
    what = "del_staff_validation"
    staff_obj = forms.ModelChoiceField(Staff.objects.all(),empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
        
        :param user: User that validate the staff
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.user = user
        
        super().__init__(*args, **kwargs)
        
        
    def save(self, commit=True):
        """
        Perform the deletion
        
        :param commit: commit the changes to DB
        """
        
        try:
            s = self.cleaned_data['staff_obj']
            v = Validation.objects.get(staff=s, validator=self.user, date_end=None)
            v.date_end=dt.date.today()
    
            if commit:
                v.save()
        
        except Exception as e:
            raise GafcFormException(e)
            
class MinEventForm(GafcModelForm):
    """
    Form with minimum informations, for use with EventsFormset
    """
    
    what = "save_min_event"
    
    description=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':5}),label='Description')
    delete = forms.BooleanField(label='Supprimer', required=False)
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        if 'instance' in kwargs:
            self.prefix = 'event-%i'%(kwargs['instance'].id)
            
        super().__init__(*args, **kwargs)
        
#        date = dt.datetime.now()
        
        self.fields['begin'].widget = MyMultiDateTime(required=False)
        self.fields['begin'].widget.attrs.update({'ref': 'date_begin'})
        #self.fields['begin'].initial = date.replace(hour=8, minute=0)
        self.fields['finish'].widget = MyMultiDateTime(required=False)
        self.fields['finish'].widget.attrs.update({'ref': 'date_finish'})
        #self.fields['finish'].initial = date.replace(hour=18, minute=0)
        self.fields['delete'].widget.attrs.update({'role': 'delete_checkbox'})
        self.fields['place'].required=False
        
        fields = ('title', 'cost', 'description', 'place', 'begin', 'finish')
        
        for f in fields:
            self.fields[f].widget.attrs.update({'class': 'form-control', 'placeholder': self.fields[f].label})
            
        s_fields = ('eventtype','visibility')
        
        for f in s_fields:
            self.fields[f].widget.attrs.update({'class': 'custom-select'})
            
        self.fields['eventtype'].choices = self.fields['eventtype'].choices[:-1]
#        
#        if 'instance' not in kwargs:
#            for k,f in self.fields.items():
#                f.required=False
            
        
    class Meta:
        model = Event
        fields = ('title', 'eventtype', 'cost', 'description', 'place', 'begin', 'finish', 'visibility','public_after_va','link', 'onlyvalid')
        
    def clean_finish(self):
        """
        Ensure begin/finish consistency
        """
        
        begin = self.cleaned_data['begin']
        end   = self.cleaned_data['finish']
        
        if end < begin:
            raise forms.ValidationError(' La fin de l\'événement doit se situer après le début. ')
        
        return end
    
    def clean(self):
        """
        Additionnal cleaning
        """
        super().clean()
        self.fields['finish'].widget.invalid = 'finish' in self._errors
        


class EventsFormset():
    """
    Bulk events form editor
    """
    
    what = "save_events"
    
    def __init__(self, events, season, category, user, data=None, files=None):
        """
        Initialize the forms
        
        :param events: Event list to display initially
        :param season: Season on which the events will be created
        :param category: Category of the events
        :param user: User that perform the event creations
        :param data: POST data
        :param files: FILES data
        """
        
        self.forms=[]
        self.to_be_deleted=[]
        
        self.season = season
        self.category = category
        self.user = user
        
        if data == None:
            for e in events:
                self.forms += [MinEventForm(instance=e)]
            
            self.forms += [MinEventForm(prefix='NEW-1')]
        else:
            eids = dict([(e.id,e) for e in events])
            
            for v in data.getlist('form_id'):
                if v.startswith('event-'):
                    eid = int(v[6:])
                    if not eid in eids.keys():
                        raise ValueError('Event is not in initial event list')
                        
                    self.forms += [ MinEventForm(data, instance=eids[eid]) ,]
                    if '%s-delete'%v in data:
                        self.to_be_deleted += [(eid,len(self.forms)-1),]
                else:
                    chk_fields = ['title', 'eventtype', 'description']
                    filled = False
                    
                    for f in chk_fields:
                        fn = '%s-%s'%(v,f)
                        if fn in data and data[fn].strip() != '':
                            filled = True
                            
                    if not '%s-delete'%v in data:
                        self.forms += [ MinEventForm(data if filled else None, prefix=v) ,]
                    
    def is_valid(self):
        """
        Return if all the forms are valids
        """
        
        r = True
        
        for ff in self.forms:
            if len(ff.errors) > 0:
                for k,f in ff.fields.items():
                    if k in ff.errors:
                        f.widget.attrs.update({'class': (f.widget.attrs['class'] if 'class' in f.widget.attrs else '')+' is-invalid'})
                r = False
            
        return r
                     
                    
    def save(self):
        """
        Save the events
        """
        
        for (i,fid) in sorted(self.to_be_deleted, reverse=True, key=lambda v: v[1]):
            try:
                Event.objects.get(pk=i).delete()
                del self.forms[fid]
            except Exception as e:
                raise GafcFormException(e)
        
        try:
            with transaction.atomic():
                for ff in self.forms:
                    
                    if ff.has_changed():
                        if ff.is_valid():
                            ev = ff.save(commit=False)
                            ev.season = self.season
                            ev.category = self.category
                            isnew = ev.pk == None
                            ev.save()
                            
                            if isnew:
                                EventState.objects.create(event=ev,state='CR',comment="", person=self.user)
                                ff.prefix = 'event-%i'%(ev.id)
                            
                        elif len(ff.errors) > 0:
                            raise GafcFormException(ff.errors)
                        
        except Exception as e:
            raise GafcFormException(e)
                    
    def __iter__(self):
        """
        Forms iterator
        """
        for f in self.forms:
            yield f
            
            