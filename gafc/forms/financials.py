# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
This module contains forms for financial managements
"""

from django import forms

from django.shortcuts import reverse

from django.db import transaction
from django.db.models import Q, Count

from django.core.mail import EmailMessage

import datetime as dt
import csv
import os, re

from .common import GafcModelForm, GafcForm, GafcFormException, PersonTypeaheadWidget, FinancialCategoryWidget, EventTypeaheadWidget, MyDateInput, FinancialCategoryTreeWidget
from ..models import Cashing, Season, ForseenBalance, ForseenBalanceValue, Category, FinancialCategoryTree, AccountOperation, CashingFile, Payment, FinancialCategory, Event, HelloassoPayment, InternalPayment, AccountBalance, EquipmentGroup, Equipment

from gucem import settings
from gafc.templatetags import gafc_tags

class CashingMergeForm(GafcForm):
    """
    Merge multiple cashings togethers
    """
    
    what = "merge_cashing"
    
    cash_ids = forms.ModelMultipleChoiceField(Cashing.objects.filter(person=None, date_done=None))
    
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the forms
        
        :param user: User that perform the merge
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
    
        self.__user = user
        super().__init__(*args, **kwargs)
    
    
    def clean_cash_ids(self):
        """
        Check cashings
        """
        
        id_p = None
        
        for c in self.cleaned_data['cash_ids']:
            if id_p is None:
                id_p = c.person_ask_id
            elif id_p != c.person_ask_id:
                raise forms.ValidationError("All cashing must have the same requestor")
                
        return self.cleaned_data['cash_ids']
    
    def save(self):
        """
        Merge the cashings
        """
        
        with transaction.atomic():
            
            ca = self.cleaned_data['cash_ids'][0]
            
            for c in self.cleaned_data['cash_ids'][1:]:
                c.payment_set.update(cashing=ca)
                c.equipment_set.update(cashing=ca)
                c.cashingfile_set.update(cashing=ca)
                
                c.rejected=True
                c.rejected_co = "Transféré encaissement %i"%ca.id
                c.date_done = dt.date.today()
                c.person = self.__user
                c.save()
                
                
class DuplicateForseenBalanceForm(GafcForm):
    """
    Duplicate the forseen balance
    """
    what = "forseen_copy"
    submit_txt = "Dupliquer"
    
    season = forms.ModelChoiceField(Season.objects.all())
    
    def __init__(self, original, *args, **kwargs):
        """
        Initialize the form
        
        :param original: The original Forseen balance
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__original = original
    
    def save(self):
        """
        Perform the duplication
        
        :return: The newly created forseen balance
        """
        
        nfbal = ForseenBalance.objects.create(season=self.cleaned_data['season'])
            
        for balval in ForseenBalanceValue.objects.filter(balance=self.__original):
            balval.pk = None
            balval.balance = nfbal
            balval.save()
            
        return nfbal
    
class ValidateForseenBalanceForm(GafcForm):
    """
    Duplicate the forseen balance
    """
    what = "forseen_validate"
    submit_txt = "Valider"
    
    def __init__(self, original, user, *args, **kwargs):
        """
        Initialize the form
        
        :param original: The original Forseen balance
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__original = original
        self.__user = user
    
    def save(self):
        """
        Validate the balance
        
        :return: The newly created forseen balance
        """
        
        self.__original.date_valid = dt.date.today()
        self.__original.person_valid = self.__user
        
        self.__original.save()
            
        return self.__original
        
    
class IncomeOutcomeField(forms.MultiValueField):
    """
    A field containing 2 floats, for income and outcome
    """
    
    def __init__(self, uplevel, wattrs=dict(), *args, **kwargs):
        """
        Initialize the field
        
        :param uplevel: The upper level reference
        :param wattrs: widget attributes
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(fields=(forms.FloatField(), forms.FloatField(),),  widget=IncomeOutcomeWidget(uplevel, wattrs=wattrs), *args, **kwargs)
        
    def compress(self, dlist):
        """
        Compress the value to tuple
        
        :param dlist: List of values
        """
        if len(dlist) == 0:
            return (None, None)
        else:
            return (dlist[0], dlist[1])
    
    
class IncomeOutcomeWidget(forms.MultiWidget):
    """
    A widget for the IncomeOutcomeField
    """
    
    template_name = 'gafc/widget/incomeoutcome.html'
    
    def __init__(self, uplevel, wattrs=dict(), *args, **kwargs):
        """
        Initialize the widget
        
        :param uplevel: The upper level reference
        :param wattrs: widget attributes
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(widgets=[forms.NumberInput(attrs={'class': 'form-control col-sm-6', 'uplevel': '%s_0'%uplevel, 'step': 0.01, **wattrs}), forms.NumberInput(attrs={'class': 'form-control col-sm-6', 'uplevel': '%s_1'%uplevel, 'step': 0.01, **wattrs})], *args, **kwargs)
        
    def decompress(self, value):
        """
        Decompress the values from data
        
        :param value: the value to decompress
        """
        if value is None:
            return (None,None)
        return value
     

class ForseenBalanceForm(GafcForm):
    """
    ForseenBalance modification form
    """
    
    what = "forseen_save"
    comment=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Commentaire',required=True)
    
    def __init__(self, *args, instance, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param instance: The ForseenBalance instance
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.instance=instance
        
        self.tree = self.getFintree()
        lst  = self.tree2list(self.tree)
        ilst = self.getIdentFromTree(self.tree)
        
        self.fields['tree'] = IncomeOutcomeField('NoUpLevel', label="TOTAL", required=False, disabled=True)
        
        for t in lst:
            editable = t['type'] == 'fincat'
            self.fields[t['ident']] = IncomeOutcomeField(t['uplevel'], label=t['label'], required=False, disabled=not editable, wattrs=dict() if not editable else {'onchange': 'updateTotals();'})
        
        if self.instance is not None:
            for balval in ForseenBalanceValue.objects.filter(balance=self.instance):
                formfieldid = ilst[(balval.cat_id, balval.fincat_id)]
                
                if formfieldid in self.fields:
                    self.fields[formfieldid].initial = (balval.income, balval.outcome)
                    
            self.fields['comment'].initial = self.instance.comment
    
    def save(self):
        """
        Save the changes
        """
        
        flst = self.getFincatListFromTree(self.tree)
        
        with transaction.atomic():
            
            self.instance.comment = self.cleaned_data['comment']
            self.instance.save()
            ForseenBalanceValue.objects.filter(balance=self.instance).delete()
            
            for k, it in self.cleaned_data.items():
                if k.startswith('tree') and k in flst:
                    income, outcome = it
                    if income is not None or outcome is not None:
                        ForseenBalanceValue.objects.create(balance=self.instance, cat=flst[k]['cat'], fincat=flst[k]['fincat'], income=income, outcome=outcome)
    
    @classmethod
    def tree2list(cls, tree, level=0, basename='tree'):
        """
        Flatten a FinancialCategoryTree to a list
        
        :param tree: The tree to flatten
        :param level: The current deep level
        :param basename: field basename
        
        :return: the flatten tree
        """
        
        ret = []
        
        for t in tree:
            ret += [{'label': ''.join(['...']*level)+t['obj'].name, 'type': 'fincattree', 'ident': '%s_t%i'%(basename, t['obj'].id), 'uplevel': basename}]
            
            for cid, c in t['categories'].items():
                ret += [{'label': ''.join(['...']*(level+1))+c['cat'].name, 'type': 'category', 'ident': '%s_t%i_c%i'%(basename, t['obj'].id, c['cat'].id), 'uplevel': '%s_t%i'%(basename, t['obj'].id)}]
                
                for cf in c['fincat']:
                    ret += [{'label': ''.join(['...']*(level+2))+cf.name, 'type': 'fincat', 'ident': '%s_t%i_c%i_f%i'%(basename, t['obj'].id, c['cat'].id, cf.id), 'uplevel': '%s_t%i_c%i'%(basename, t['obj'].id, c['cat'].id)}]
                    
            ret += cls.tree2list(t['sub'], level+1, '%s_t%i'%(basename, t['obj'].id))
            
        return ret
    
    @classmethod
    def getFincatListFromTree(cls, tree, basename='tree'):
        ret = dict()
        
        for t in tree:           
            for cid, c in t['categories'].items():
                for cf in c['fincat']:
                    ret['%s_t%i_c%i_f%i'%(basename, t['obj'].id, c['cat'].id, cf.id)] = {'cat': c['cat'], 'fincat':cf}
                    
            sub = cls.getFincatListFromTree(t['sub'], '%s_t%i'%(basename, t['obj'].id))
            ret = {**ret, **sub}
            
        return ret
    
    @classmethod
    def getIdentFromTree(cls, tree, basename='tree'):
        ret = dict()
        
        for t in tree:           
            for cid, c in t['categories'].items():
                for cf in c['fincat']:
                    ret[(c['cat'].id, cf.id)] = '%s_t%i_c%i_f%i'%(basename, t['obj'].id, c['cat'].id, cf.id)
                    
            sub = cls.getIdentFromTree(t['sub'], '%s_t%i'%(basename, t['obj'].id))
            ret = {**ret, **sub}
            
        return ret
        
    @classmethod
    def getFintree(cls):
        
        objs = dict()
        ret = []
        
        cats = Category.objects.all()
        
        for t in FinancialCategoryTree.objects.prefetch_related('financialcategory_set').order_by('toplevel_id').all():
            objs[t.id] = {'obj':t, 'sub':[]}
            
        for pk, t in objs.items():
            t['categories'] = dict()
            
            for fc in t['obj'].financialcategory_set.all():
                for c in cats:
                    if fc in c.allowed_fincat.all():
                        if c.id not in t['categories']:
                            t['categories'][c.id] = {'cat': c, 'fincat': []}
                            
                        t['categories'][c.id]['fincat'] += [ fc ,]                
            
            if t['obj'].toplevel_id is not None:
                objs[t['obj'].toplevel_id]['sub'] += [t,]
            else:
                ret += [t, ]
    
        return ret


class AccountFileForm(GafcForm):
    """
    Upload the account file form
    """
    what = "account_addfile"
    submit_txt = "Télécharger"
    file = forms.FileField(label="Ajouter fichier")
    separator = forms.CharField(initial=";", label="Séparateur")
        
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        """
        
        super().__init__(*args, **kwargs)
        self.helper.is_multipart = True
        
    def save(self):
        """
        Process the file
        """
        
        with open(settings.GAFC_TEMPFOLDER+'/account_raw.csv', 'wb+') as file:
            for chunk in self.cleaned_data['file'].chunks():
                file.write(chunk)
                
        lines = None
        length = 0
        
        for codec in ['utf-8','latin-1']:
            try:
                with open(settings.GAFC_TEMPFOLDER+'/account_raw.csv', 'r', encoding=codec) as ifile:
                    reads = csv.reader(ifile, delimiter=self.cleaned_data['separator'])
                    
                    lines = []
                    for r in reads:
                        lines += [r,]
                        length = max(length, len(r))
                    
                # Append missing columns, check if column is actually used
                used = [False,]*length
                for ll in lines:
                    if len(ll) < length:
                        ll += ['',]*(length-len(ll))
                        
                    for i,l in enumerate(ll):
                        if l.strip() != '':
                            used[i] = True
                        
                # Remove unused columns
                tmplines = []
                for ll in lines:
                    L = []
                    for i,u in enumerate(used):
                        if u:
                            L += [ll[i], ]
                    tmplines += [L,]
                lines = tmplines
                break
                    
            except UnicodeDecodeError:
                pass
                
        if lines is not None:
            with open(settings.GAFC_TEMPFOLDER+'/account.csv','w+',encoding='utf-8') as ofile:
                writ = csv.writer(ofile, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
                writ.writerows(lines)
#                print(settings.GAFC_TEMPFOLDER+'/account.csv') 
        else:
            raise GafcFormException("Impossible de lire le fichier CSV.")
        
        
            
class AccountAttributionForm(GafcForm):
    """
    Attribute the columns to the file from CSV
    """
    
    what="import_account_operations"
    submit_txt="Importer"
    
    file_begin = forms.DateField(widget=MyDateInput(), label="Début de période")
    amount_begin = forms.FloatField(label="Solde en début de période", required=False)
    
    file_end   = forms.DateField(widget=MyDateInput(), label="Fin de période")
    amount_end = forms.FloatField(label="Solde en fin de période", required=False)
    
    
    def __init__(self, lines, *args, **kwargs):
        """
        Initialize the forms
        
        :param lines: Lines from file
        """
            
        super().__init__(*args, **kwargs)
        
        self.__lines = lines
        self.filelines = lines 
        
        regd = re.compile(r"([0-3]?[0-9])/([0-1]?[0-9])/(20[0-9]{2})")
        rega = re.compile(r"[+-]?[0-9]+[\\.,][0-9]{2}")
        
        for c in lines[0]:
            if c.strip().lower().startswith('date de début'):
                m = regd.search(c)
                if m is not None:
                    self.fields['file_begin'].initial = dt.date(int(m.group(3)), int(m.group(2)), int(m.group(1)))
            
            if c.strip().lower().startswith('date de fin'):
                m = regd.search(c)
                if m is not None:
                    self.fields['file_end'].initial = dt.date(int(m.group(3)), int(m.group(2)), int(m.group(1)))
        
        for l in lines:
            ll = (' '.join(l)).strip().lower()
            if ll.startswith('solde en début'):
                m = rega.search(ll)
                amount = float(m.group(0).replace(',','.'))
                self.fields['amount_begin'].initial = amount
            if ll.startswith('solde en fin'):
                m = rega.search(ll)
                amount = float(m.group(0).replace(',','.'))
                self.fields['amount_end'].initial = amount
        
        
        headers = lines[4] if len(lines) >= 5 else lines[0]
        for i,h in enumerate(headers):
            
            n = 'col-%i'%i
            
            init = '';
            hh = h.lower()
            
            if hh == 'date':
                init = 'DATEdmy'
                
            if hh == 'numéro d\'opération':
                init = 'OPID'
                
            if hh == 'libellé':
                init = 'LIBELE'
                
            if hh in ['débit','crédit','montant']:
                init = 'AMOUNT'
                
            if hh == 'détail':
                init = 'DETAIL'
            
            self.fields[n] = forms.ChoiceField(required=False,
                                               choices=[('','---'),
                                                        ('DATEdmy','Date dd/mm/yy'),
                                                        ('DATEdmY','Date dd/mm/yyyy'),
                                                        ('DATEYmd','Date yyyy-mm-dd'),
                                                        ('DATEymd','Date yy-mm-dd'),
                                                        ('OPID', 'Identifiant'),
                                                        ('LIBELE', 'Libellé'),
                                                        ('AMOUNT', 'Montant'),
                                                        ('DETAIL', 'Détail'),],
                                               initial = init)
            self.fields[n].widget.attrs.update({'class':'custom-select'})
            
        self.fields['lines'] = forms.MultipleChoiceField(required=False,
                                               widget=forms.CheckboxSelectMultiple,
                                               choices=[(i,i) for i,l in enumerate(lines)])
        
    def save(self):
        """
        Convert file to operations
        
        :return: The number of lines in file, the number of operations imported
        """
        
        roles = [self.cleaned_data['col-%i'%i] for i in range(len(self.__lines[0]))]
        objs = []
        
        try:
            
            for i,l in enumerate(self.__lines):
                if str(i) in self.cleaned_data['lines']:
                    
                    item = {'opid':None,
                            'date':None,
                            'amount':0.,
                            'libele':"",
                            'detail':"",
                            'cashing':None }
                    
                    for j,e in enumerate(l):
                        
                        if roles[j] == 'DATEdmy':
                            item['date'] = dt.datetime.strptime(e, '%d/%m/%y').date()
                        elif roles[j] == 'DATEdmY':
                            item['date'] = dt.datetime.strptime(e, '%d/%m/%Y').date()
                        elif roles[j] == 'DATEymd':
                            item['date'] = dt.datetime.strptime(e, '%y-%m-%d').date()
                        elif roles[j] == 'DATEYmd':
                            item['date'] = dt.datetime.strptime(e, '%Y-%m-%d').date()
                        elif roles[j] == 'OPID':
                            item['opid'] = e
                        elif roles[j] == 'LIBELE':
                            item['libele'] += e
                        elif roles[j] == 'DETAIL':
                            item['detail'] += e
                        elif roles[j] == 'AMOUNT':
                            if e.strip() != '':
                                item['amount'] += float(e.replace(',','.').strip())
                        else:
                            raise GafcFormException("Unknow column title")
                            
                    objs += [AccountOperation(**item),]
                    
            with transaction.atomic():
                objs_ins = AccountOperation.objects.bulk_create(objs, ignore_conflicts=True)
                nins = sum([0 if o.pk is None else 1 for o in objs_ins])
                
                if self.cleaned_data['amount_begin'] is not None:
                    AccountBalance.objects.create(date=self.cleaned_data['file_begin'], amount=self.cleaned_data['amount_begin'])
                
                if self.cleaned_data['amount_end'] is not None:
                    AccountBalance.objects.create(date=self.cleaned_data['file_end'], amount=self.cleaned_data['amount_end'])
                    
                return (len(objs),nins)
                
        except Exception as e:
            raise GafcFormException(e)
            
class IbanWidget(forms.TextInput):
    """
    Custom widget for IBAN number
    """
    def get_context(self, name, value, attrs):
        """
        Return the context for IBAN number
        
        :param name: Field name
        :param value: Field value
        :param attrs: Widget attributes
        """
        return super().get_context(name, self.iban_format(value), attrs)
    
    @classmethod    
    def iban_format(cls, iban:str):
        """
        IBAN formatter function
        """
        
        if type(iban) is not str: return iban
        
        iban = iban.replace(' ','').upper()
        return ' '.join(iban[i:i+4] for i in range(0,len(iban),4))
        
        
class CashingCommentForm(GafcModelForm):
    """
    Form for cashing information with comment
    """
    
    what = "cashing_comment"
    
    def __init__(self, user, requestor, *args, **kwargs):
        """
        Initialize the form
        
        :param user: if True, amount field is mandatory
        :param requestor: if True, current user is the requestor
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['comment'].widget.attrs={'rows':2}
       # self.fields['comment'].required=False
        self.fields['abandon'].help_text = "Cocher pour abandonner les frais au profit du club. Une fois validé, une attestation fiscale permettant un abattement de 66% sera générée."
        self.fields['amount'].required = user
        self.fields['iban'].widget = IbanWidget()
        self.fields['iban'].required = False
        self.fields['bic'].required = False
        self.fields['amount'].help_text = "Montant total des frais"
        
        if not requestor:
            self.fields['abandon'].widget.attrs['disabled'] = "disabled"
            self.fields['amount'].widget.attrs['readonly'] = True
            self.fields['iban'].widget.attrs['readonly'] = True
            self.fields['bic'].widget.attrs['readonly'] = True
        
    class Meta:
        model   = Cashing
        fields  = ('comment','amount','abandon','iban','bic')
        
    @classmethod
    def iban_validate(cls, iban : str):
        """
        Class method to validate IBAN numbers
        
        :param iban: Iban number to validate
        
        :return: The validated IBAN number
        
        :raise ValidationError: if the IBAN is not valid
        """
        import numpy as np
        
        iban = iban.replace(' ','').upper()
        iban_init = iban
        
        if len(iban) > 34:
            raise forms.ValidationError('IBAN trop long!')
            
        iban = iban[4:] + iban[:4]
            
        for i,l in enumerate('ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
            iban = iban.replace(l,'%i'%(i+10))
            
        for l in iban:
            if l not in '0123456789':
                raise forms.ValidationError('Caractère interdit dans le IBAN')
                
        iban_copy = iban
        modulo = 0
        
        
        while len(iban_copy) > 0:
            ib = '%i%s'%(modulo,iban_copy[:7])
            iban_copy = iban_copy[7:]
            
            modulo = np.mod(int(ib),97)
            
        if modulo != 1:
            raise forms.ValidationError('IBAN invalide!')
            
        return iban_init
        
    @classmethod
    def bic_validate(self, bic):
        """
        Class method to validate BIC numbers
        
        :param iban: Iban number to validate
        
        :return: The validated BIC number
        
        :raise ValidationError: if the BIC is not valid
        """
        bic = bic.upper()
        
        for l in bic:
            if l not in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789':
                raise forms.ValidationError('BIC invalide')
                
        if len(bic) not in (8,11):
            raise forms.ValidationError('BIC invalide')
            
        return bic
            
    def clean(self):
        """
        Check whether the IBAN and BIC are provided if abandon is not checked
        """
        super().clean()
        
        if not self.cleaned_data['abandon'] and self.instance is None:
            if 'iban' not in self.errors and self.cleaned_data['iban'] is None: self.add_error('iban', 'IBAN obligatoire sauf si abandon de frais')
            if 'bic' not in self.errors and self.cleaned_data['bic'] is None: self.add_error('bic', 'BIC obligatoire sauf si abandon de frais')
        
    def clean_iban(self):
        """
        Custom IBAN cleaning
        """
        if self.cleaned_data['iban'] is None: return None
        return self.iban_validate(self.cleaned_data['iban'])
    
    def clean_bic(self):
        """
        Custom BIC cleaning
        """
        if self.cleaned_data['bic'] is None: return None
        return self.bic_validate(self.cleaned_data['bic'])
        
class CashingCommentFileForm(GafcModelForm):
    """
    Form for user cashing request
    """
    
    what="create_cashing"
    
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'mallow_multiple_selected' : True, 'accept':'image/jpeg, application/pdf'}), label="Justificatifs")
    check= forms.BooleanField(widget=forms.CheckboxInput(), label="Certifié sincère et véritable", help_text="En cochant cette case, vous certifiez que les frais dont vous demandez le remboursement ou l'abandon ont étés engagés dans l'intérêt du club.")
    
    def __init__(self, request, *args, **kwargs):
        """
        Initialize the form.
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        self.__request = request
        
        super().__init__(*args, **kwargs)
        
        self.fields['comment'].widget.attrs={'rows':2}
       # self.fields['comment'].required=False
        self.fields['abandon'].help_text = "Cocher pour abandonner les frais au profit du club. Une fois validé, une attestation fiscale permettant un abattement de 66% sera générée."
        self.fields['amount'].help_text = "Montant total des frais"
        self.fields['iban'].widget.attrs['maxlength'] = 100
        self.fields['iban'].required = False
        self.fields['bic'].required = False
    
    class Meta:
        model   = Cashing
        fields  = ('comment','amount','abandon','iban','bic')
        
    def clean(self):
        """
        Specific check of iban/bic: only required when this is not an abandon
        """
        super().clean()
        
        if not self.cleaned_data['abandon']:
            if 'iban' not in self.errors and self.cleaned_data['iban'] is None: self.add_error('iban', 'IBAN obligatoire sauf si abandon de frais')
            if 'bic' not in self.errors and self.cleaned_data['bic'] is None: self.add_error('bic', 'BIC obligatoire sauf si abandon de frais')
        
    def clean_iban(self):
        """
        Check the IBAN
        """
        if self.cleaned_data['iban'] is None: return None
        
        return CashingCommentForm.iban_validate(self.cleaned_data['iban'])
    
    def clean_bic(self):
        """
        Check the BIC
        """
        if self.cleaned_data['bic'] is None: return None
        
        return CashingCommentForm.bic_validate(self.cleaned_data['bic'])
    
    def save(self):
        """
        Commit the changes
        """
        
        try:
            with transaction.atomic():
                c = super().save(commit=False)
                c.person_ask = self.__request.user
                c.save()
                for f in self.__request.FILES.getlist('file'):
                    print(f)
                    cf = CashingFile.objects.create(filename=f.name, cashing=c)
                    
                    os.makedirs(cf.disk_fullpath, exist_ok=True)
                            
                    with open(cf.disk_fullfilepath+'.tmp', 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)
                        
                    import magic
                    mime = magic.Magic(mime=True)
            
                    if mime.from_file(cf.disk_fullfilepath+'.tmp') in ('image/jpeg', 'application/pdf'):
                        os.rename(cf.disk_fullfilepath+'.tmp',cf.disk_fullfilepath)
                    else:
                        raise ValueError("Seul les fichiers jpeg et pdf sont acceptés")
                            
                content = """
%s %s vient de créer un nouvel encaissement : %s

Montant demandé: %.02f
Abandon de frais: %s

Commentaire:
%s
    """%(c.person_ask.first_name, c.person_ask.last_name, self.__request.build_absolute_uri(reverse('cashing', args=[c.id])), c.amount, "Oui" if c.abandon else "Non", c.comment)
                
                mail = EmailMessage(
                        "Nouvel encaissement de %s %s"%(c.person_ask.first_name, c.person_ask.last_name),
                        content,
                        settings.CASHING_EMAIL_FROM,
                        settings.CASHING_EMAIL_NOTIFY
                    )
                mail.send(fail_silently=False)
        
        except Exception as e:
            raise GafcFormException(e)
        
        
class PaymentForm(GafcModelForm):
    """
    General form to create payment
    """
    
    what = "add_payment"
    comment=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Raison',required=False)
    
    class Meta:
        model   = Payment
        fields  = ('amount','method','person','category','financial','season', 'event','comment')
        
    def __init__(self, event=None, user=None, category=None, show_comment=True, season=None, show_event=False, cashing=None, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Force event
        :param user: Payment encoder
        :param category: Force payment category
        :param show_comment: Display the comment box
        :param season: Force season
        :param show_event: Dislay the event selector
        :param cashing: Force cashing
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.event = event
        self.user  = user
        self.category = category
        self.season = season
        self.show_comment = show_comment
        self.show_event = show_event
        self.cashing = cashing
            
        super().__init__(*args, **kwargs)
        
        self.fields['person'].widget = PersonTypeaheadWidget(attrs={"placeholder": "Adhérent"})        
        
        if self.event is not None:
            self.fields['category'].queryset = Category.objects.filter(Q(is_global=True) | Q(name=self.event.category.name)) .order_by('name')
            self.fields['category'].initial = self.event.category
        else:
            self.fields['category'].queryset = Category.objects.order_by('name')
            
        self.fields['financial'].widget = FinancialCategoryWidget(self.category)
        self.fields['category'].widget.attrs.update({'data-linked-account': 'financial'})
        
        if self.category is not None:
            del self.fields['category']
            if self.category.default_fincat is not None:
                self.fields['financial'].initial = self.category.default_fincat
            
        if not self.show_comment:
            del self.fields['comment']
            
        if self.season is not None or self.event is not None:
            del self.fields["season"]
                
        if self.show_event:
            self.fields['event'].widget = EventTypeaheadWidget()
            self.fields['event'].required=False
        else:
            del self.fields['event']
            
    def save(self, commit=True):
        """
        Save the payment
        
        :param commit: Actually commit the changes
        
        :return: The payment instance
        """
        p = super().save(commit=False)
        p.pay_done = dt.datetime.now()
        
        if self.season is not None:
            p.season = self.season
        
        if self.event is not None:
            p.event = self.event
            p.season = self.event.season
            
        if self.user is not None:
            p.encoder = self.user
            
        if self.category is not None:
            p.category = self.category
            
        if self.cashing is not None:
            p.cashing = self.cashing
        
        try:
            if commit:
                p.save()
        except Exception as e:
            raise GafcFormException(e)
        
        return p
    
class DeleteCashingFileForm(GafcForm):
    """
    Delete a file from cashing
    """
    
    what = "cashing_delete_file"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    file = forms.ModelChoiceField(CashingFile.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, cashing, *args, **kwargs):
        """
        Initialize the form
        
        :param cashing: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['file'].queryset = cashing.cashingfile_set.all()
        
    def save(self):
        """
        Delete the file from database and on disk
        
        :return: None
        """

        try:
            cf = self.cleaned_data['file']
            with transaction.atomic():
                if os.path.isfile(cf.disk_fullfilepath):
                    os.unlink(cf.disk_fullfilepath)                
                cf.delete()
        except Exception as e:
            raise GafcFormException(e)
        
    
class CashingFileForm(GafcForm):
    """
    File upload form
    """
    
    what = "cashing_addfile"
    submit_txt = "Télécharger"
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'allow_multiple_selected': True, 'accept':'image/jpeg, application/pdf'}), label="Ajouter fichier")
        
    def __init__(self, request, cashing, *args, **kwargs):
        """
        Initialize the form
        
        :param request: Request object
        :param cashing: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__request = request
        self.__cashing = cashing
        
        super().__init__(*args, **kwargs)
        self.helper.is_multipart = True
        self.helper.label_class='col-12'
        self.helper.field_class='col-12'
        
    def save(self):
        """
        Process the file
        """
        
        for f in self.__request.FILES.getlist('file'):
            cf = CashingFile.objects.create(filename=f.name, cashing=self.__cashing)
            
            os.makedirs(cf.disk_fullpath, exist_ok=True)
            
            with open(cf.disk_fullfilepath+'.tmp', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
                
            import magic
            mime = magic.Magic(mime=True)
    
            if mime.from_file(cf.disk_fullfilepath+'.tmp') in ('image/jpeg', 'application/pdf'):
                os.rename(cf.disk_fullfilepath+'.tmp',cf.disk_fullfilepath)
            else:
                raise ValueError("Seul les fichiers jpeg et pdf sont acceptés")

#class PaymentMethodForm(forms.Form):
#    methods = forms.MultipleChoiceField(
#        choices=[('CA', gafc_tags.payMethod('CA')),
#                 ('CH', gafc_tags.payMethod('CH')),
#                 ('BC', gafc_tags.payMethod('BC')),
#                 ('IN', gafc_tags.payMethod('IN')),
#                 ('VM', gafc_tags.payMethod('VM')),
#                 ('BO', gafc_tags.payMethod('BO'))],
#        widget  = forms.CheckboxSelectMultiple,
#        required = False,
#        label="Méthode de payement"
#        )
#    categories = forms.MultipleChoiceField(
#        widget  = forms.CheckboxSelectMultiple,
#        required = False,
#        label="Catégories"
#        )
#    
#    def __init__(self, *args, **kwargs):
#        """
#        Initialize the form
#        
#        :param \\*args: Extra arguments passed to parent
#        :param \\*\\*kwargs: Extra arguments passed to parent
#        """
#        super().__init__(*args, **kwargs)
#        self.fields['categories'].choices = [( c.id, c.name ) for c in Category.objects.all()]


class CashingAcceptForm(GafcModelForm):
    """
    Form for cashing validation
    """
    
    what = "cashing_validate"
    submit_txt = "Accepter"
    
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
        
        :param user: User that validate
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.__user = user
        
        self.fields['date_done'].widget = MyDateInput()
    
    class Meta:
        model   = Cashing
        fields  = ('date_done',)
        
    def clean_date_done(self):
        """
        Custom date_done cleaning: check that the cashing is not validated too far in the future
        """
        
        maxdate = dt.date.today() + dt.timedelta(10)
        
        if self.cleaned_data['date_done'] > maxdate:
            raise forms.ValidationError("La date de validation est trop loin dans le futur!")
            
        return self.cleaned_data['date_done']
    
    def save(self, commit=True):
        """
        Commit the validation
        
        :param commit: Save on db
        
        :return: The Cashing instance
        """
        
        cash = super().save(commit=False)
        
        if cash.payment_set.count() == 0:
            raise GafcFormException("Il n'y a aucun paiement associé à l'encaissement.")
        
        cash.person = self.__user
        
        if commit:
            try:
                cash.save()
            except Exception as e:
                raise GafcFormException(e)
            
        return cash
        
    
class CashingDevalidateForm(GafcForm):
    """
    Devalidate a cashing
    """
    
    what = "cashing_devalidate"
    submit_txt = "Dé-valider"
    submit_class = "btn-danger"
    
    def __init__(self, cash, *args, **kwargs):
        """
        Initialize the form
        
        :param cash: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.__cash = cash
        
        if self.__cash.date_done is None:
            raise GafcFormException("L'encaissement n'est pas encore validé")
            
    def save(self):
        """
        Commit the changes
        """
        
        self.__cash.date_done = None
        self.__cash.person = None
        self.__cash.save()
        
        for cf in self.__cash.cashingfile_set.filter(autogenerate=True):
            if os.path.isfile(cf.disk_fullfilepath):
                os.unlink(cf.disk_fullfilepath)
                cf.delete()
        
        return self.__cash
    
        
class CashingRejectForm(GafcModelForm):
    """
    Form for cashing rejection
    """
    what = "cashing_reject"
    submit_txt = "Rejeter"
    submit_class = "btn-danger"
    
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
        
        :param user: User that reject
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.__user = user
        
        self.fields['rejected_co'].widget.attrs.update({'cols':40, 'rows':3})
    
    class Meta:
        model   = Cashing
        fields  = ('rejected_co',)
    
    def save(self, commit=True):
        """
        Commit the validation
        
        :param commit: Save on db
        
        :return: The Cashing instance
        """
        
        cash = super().save(commit=False)
        
        if cash.payment_set.count() > 0:
            raise GafcFormException("Supprimer tous les paiements avant de rejeter l'encaissement.")
        
        cash.person = self.__user
        cash.date_done = dt.date.today()
        cash.rejected = True
        
        if commit:
            try:
                cash.save()
            except Exception as e:
                raise GafcFormException(e)
            
        return cash
        
class CashingAddPaymentForm(GafcForm):
    """
    Add payments to a cashing
    """
    
    what = "cashing_add_payments"
    
    payments = forms.ModelMultipleChoiceField(Payment.objects.filter(cashing=None))
    
    def __init__(self, cashing, *args, **kwargs):
        """
        Initialize the form
        
        :param cashing: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__cashing = cashing
        super().__init__(*args, **kwargs)
        
    def save(self):
        """
        Attribute cashing to the selected payments
        
        :return: The payment list
        """
        
        ids = [ p.id for p in self.cleaned_data['payments'] ]
        payments = Payment.objects.filter(id__in=ids)
        
        payments.update(cashing=self.__cashing)
            
        return payments
        
    
class CashingDelPaymentForm(GafcForm):
    """
    Remove payments to a cashing
    """
    
    what = "cashing_del_payments"
    
    payments = forms.ModelMultipleChoiceField(Payment.objects.all())
    
    def __init__(self, cashing, *args, **kwargs):
        """
        Initialize the form
        
        :param cashing: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__cashing = cashing
        super().__init__(*args, **kwargs)
        
        self.fields['payments'].queryset = Payment.objects.filter(cashing=self.__cashing)
        
    def save(self):
        """
        Attribute cashing to the selected payments
        
        :return: The payment list
        """
        
        ids = [ p.id for p in self.cleaned_data['payments'] ]
        payments = Payment.objects.filter(id__in=ids)
        
        payments.update(cashing=None)
            
        return payments
    
class CashingAddEquipmentForm(GafcForm):
    """
    Add equipment to a cashing
    """
    
    what = "cashing_add_equipment"
    
    equipment = forms.ModelChoiceField(Equipment.objects.filter(cashing=None), label="Ajouter un équipement")
    
    def __init__(self, cashing, *args, **kwargs):
        """
        Initialize the form
        
        :param cashing: Cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__cashing = cashing
        super().__init__(*args, **kwargs)
        
        lst = EquipmentGroup.tree2list(EquipmentGroup.getTree(cashing_filter=True, cashing=None), withEquipments=True)
        chs = dict()
        last = ''
        
        for l in lst[1:]:
            if l['type'] == 'group':
                last = l['label']
            
                if last not in chs:
                    chs[last] = []
            else:
                chs[last] += [l,]
        
        self.fields['equipment'].choices = [(k, [(l['obj'].id, l['label']) for l in c]) for k,c in chs.items()]
        self.fields['equipment'].widget.attrs = {'class':'form-control rounded-left'}
        
    def save(self, commit=True):
        """
        Attribute cashing to the selected Equipment
        
        :return: The equipment
        """
        
        equipment = self.cleaned_data['equipment']
        if equipment.cashing is None:
            equipment.cashing = self.__cashing
            
            if commit:
                equipment.save()
        else:
            raise GafcFormException("Equipment must belong to no cashing to be attributed")
        
        return equipment
        
    
class DeleteCashingEquipmentForm(GafcForm):
    """
    Delete a financial category
    """
    
    what = "delete_cashingequipment"
    submit_txt = "X"
    submit_class = "btn-danger"
    
    equipment = forms.ModelChoiceField(Equipment.objects.all(), widget=forms.HiddenInput())
    
    def __init__(self, cashing, *args, **kwargs):
        """
        Initialize the form.
        
        :param cashing: cashing object
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.__cashing = cashing
        
        self.fields['equipment'].choices = Equipment.objects.filter(cashing=self.__cashing)
    
    def save(self, commit=True):
        """
        Delete the financial category
        """
        
        eq = self.cleaned_data['equipment']
        eq.cashing = None
        
        if commit:
            eq.save()
        
        return eq
    
    
class DeleteFinancialCategoryForm(GafcForm):
    """
    Delete a financial category
    """
    
    what = "delete_financialcategory"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    fincat = forms.ModelChoiceField(FinancialCategory.objects.annotate(num_p=Count('payment'), num_fc=Count('fincat_c'), num_fd=Count('fincat_d')).filter(num_p=0, num_fc=0, num_fd=0), label="Imputation")
    
    def save(self):
        """
        Delete the financial category
        """
        
        self.cleaned_data['fincat'].delete()
    
    
class DeleteFinancialCategoryTreeForm(GafcForm):
    """
    Delete a financial category tree element
    """
    
    what = "delete_financialcategorytree"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    fincattree = forms.ModelChoiceField(FinancialCategoryTree.objects.annotate(num_fc=Count('financialcategory'), num_fct=Count('financialcategorytree')).filter(num_fc=0, num_fct=0), label="Catégorie d'impuration")
    
    def save(self):
        """
        Delete the financial category tree
        """
        
        self.cleaned_data['fincattree'].delete()

class FinancialCategoryForm(GafcModelForm):
    """
    Add/edit FinancialCategory
    """
    
    what = "add_fincat"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['tree'].widget = FinancialCategoryTreeWidget()
        
    class Meta:
        model  = FinancialCategory
        fields = ('name','tree')
        
        
class FinancialCategoryTreeForm(GafcModelForm):
    """
    Add/edit FinancialCategoryTree
    """
    
    what = "add_fincattree"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.fields['toplevel'].widget = FinancialCategoryTreeWidget()
        self.fields['toplevel'].required=False
        
    class Meta:
        model  = FinancialCategoryTree
        fields = ('name','toplevel')            
        
class DefaultFinancialCategoryForm(GafcForm):
    """
    Set the default financial category for each category
    """

    what="set_default_fincat"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        for c in Category.objects.all():
            self.fields['category_%i'%(c.id)] = forms.ModelChoiceField(queryset=c.allowed_fincat.all(), label=c.name, initial=c.default_fincat, required=False)
        
    def save(self):
        for c in Category.objects.all():
            c.default_fincat = self.cleaned_data['category_%i'%(c.id)]
            c.save()
    
class EditPaymentForm(GafcModelForm):
    """
    Edit payment form
    """
    
    add_comment=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3, 'placeholder': 'Ajouter un commentaire'}),label='Commentaire',required=False)
    
    what='edit_payment'
    
    class Meta:
        model   = Payment
        fields  = ('method','category','financial', 'amount','event')
        
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
        
        :param user: User that perform edition
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__user = user
        
        self.instance = kwargs['instance']
        self.o_instance = {'method': self.instance.method, 'category': self.instance.category, 'financial': self.instance.financial, 'amount': self.instance.amount,'event_id':self.instance.event_id}
        
        super().__init__(*args, **kwargs)
        
        self.fields['category'].widget.attrs.update({'data-linked-account': 'financial'})
        self.fields['financial'].widget = FinancialCategoryWidget()
        self.fields['financial'].widget.attrs.update({'data-initial-force': self.instance.financial.id})
        self.fields['event'].widget = EventTypeaheadWidget()
        self.fields['event'].required=False
        
    def save(self):
        """
        Change and log the changes
        """
        user = self.__user
        cmt = ''
        changed = False
        
        if self.cleaned_data['method'] != self.o_instance['method']:
            cmt += 'Méthode changée: %s -> %s\n'%(gafc_tags.payMethod(self.o_instance['method']), gafc_tags.payMethod(self.cleaned_data['method']))
            changed = True
            
        if self.cleaned_data['category'] != self.o_instance['category']:
            cmt += 'Catégorie changée: %s -> %s\n'%(self.o_instance['category'].name, self.cleaned_data['category'].name)
            changed = True
            
        if self.cleaned_data['financial'] != self.o_instance['financial']:
            cmt += 'Imputation changée: %s -> %s\n'%(self.o_instance['financial'].name, self.cleaned_data['financial'].name)
            changed = True
            
        oevt = Event.objects.get(pk=self.o_instance['event_id']) if self.o_instance['event_id'] is not None else None
        if self.cleaned_data['event'] != oevt:
            cmt += 'Événement changé %s(%i) -> %s(%i)'%(oevt.title if oevt is not None else None, oevt.id if oevt is not None else -1, self.cleaned_data['event'].title if self.cleaned_data['event'] is not None else None, self.cleaned_data['event'].id if self.cleaned_data['event'] is not None else -1)
            changed = True
            
        if self.cleaned_data['add_comment'] != '':
            cmt += 'Commentaire: %s\n'%(self.cleaned_data['add_comment'])
            changed = True
            
        if self.cleaned_data['amount'] != self.o_instance['amount']:
            cmt += 'Montant changé: %.2f -> %.2f\n'%(self.o_instance['amount'], self.cleaned_data['amount'])
            changed = True
            
            
        if changed:
            self.instance.comment += '\n%s %s (%i) le %s\n%s'%(user.first_name, user.last_name, user.id,  dt.datetime.now().strftime('%d/%m/%Y'), cmt)
            self.instance.comment = self.instance.comment.strip()
            self.instance.save()
            
class RefundPaymentForm(GafcModelForm):
    """
    Refund payment form
    """
    
    what='refund_payment'
    submit_txt = "Enregistrer le remboursement"
    submit_class = "btn-danger"
    
    class Meta:
        model   = Payment
        fields  = ('refund_reason',)
        
    def __init__(self, user, *args, **kwargs):
        """
        Initialize the form
        
        :param user: User that perform edition
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        self.__user = user
        self.instance = kwargs['instance']
        
        super().__init__(*args, **kwargs)
        
        self.fields['refund_reason'].widget=forms.Textarea(attrs={'cols':40, 'rows':3, 'placeholder': 'Raison remboursement'})
        
    def save(self):
        """
        Save the refund
        """
        p = super().save(commit=False)
        p.refund_who  = self.__user
        p.refund_date = dt.datetime.now()
        p.save()
    
    
class OperationToPaymentForm(GafcModelForm):
    """
    Create a payment and cashing from bank operation
    """
    
    what = "operation_to_payment"
    
    class Meta:
        model = Payment
        fields = ('method','person','event','season','category','financial','comment')
        
    def __init__(self, operation, user, *args, **kwargs):
        """
        Initialize the form
        
        :param operation: Operation originals
        :param user: User that perform edition
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.operation = operation
        self.user = user
        
        self.fields['method'].initial = 'VM'
        self.fields['event'].required=False
        self.fields['event'].widget = EventTypeaheadWidget(attrs={'placeholder': "Événement"})
        self.fields['person'].widget = PersonTypeaheadWidget(attrs={'placeholder': "Adhérent"})
        self.fields['comment'].initial=operation.detail
        self.fields['season'].initial = Season.objects.get(is_default=True)
        self.fields['category'].widget.attrs.update({'data-linked-account': 'financial'})                    
        self.fields['financial'].widget = FinancialCategoryWidget()
        
    def save(self, commit=True):
        """
        Save the payment and cashing
        """
        inst = super().save(commit=False)
        inst.amount = self.operation.amount
        inst.encoder = self.user
        inst.pay_done = self.operation.date
        
        with transaction.atomic():
            
            cash = Cashing.objects.create(person=self.user,
                                          person_ask=self.user,
                                          date_done=self.operation.date,
                                          comment=inst.comment)
            
            inst.cashing = cash
            self.operation.cashing = cash
             
            if commit:
                inst.save()
                self.operation.save()
                
        return inst
    
    def clean(self):
        """
        Clean the season/event consistency
        """
        super().clean()
            
        if self.cleaned_data['event'] is not None:
            if self.cleaned_data['event'].season != self.cleaned_data['season']:
                self.add_error('event', "L'événement doit être lié à la même saison")

    
class DeleteHelloassoPaymentForm(GafcForm):
    """
    Mark Helloasso payment as deleted
    """ 
    
    what = "delete_helloassopayment"
    submit_txt = "Supprimer"
    submit_class = "btn-danger"
    
    hpayment = forms.ModelChoiceField(HelloassoPayment.objects.filter(deleted=False, payment=None), label="Paiement", widget=forms.HiddenInput())
    
    def save(self):
        """
        Save the deleted mark
        """
        
        self.cleaned_data['hpayment'].deleted=True
        self.cleaned_data['hpayment'].save()
        
class InternalPaymentForm(GafcModelForm):
    """
    Create a transfert
    """
    
    what        = "add_transfer"
    comment     = forms.CharField(widget=forms.Textarea(attrs={'cols':30, 'rows':3}),required=False,label='Raison')
    amount      = forms.DecimalField(widget=forms.NumberInput(attrs={'min':0,'step':0.05}), decimal_places=2, label='Montant')
    
    class Meta:
        model   = InternalPayment
        fields  = ('amount','category_d','financial_d','category_c','financial_c','season','comment')
        
    def __init__(self, user, category_d=None, season=None, event=None, *args, **kwargs):
        """
        Initialize the form
        
        :param user: The requestor
        :param category_d: Set the debitor category
        :param season: set the Season
        :param event: set the event
        """
        
        super().__init__(*args, **kwargs)
        
        self.category_d = category_d
        self.user = user
        self.season = season
        self.event = event
        
        self.fields['category_d'].label='Transférer depuis:'
        self.fields['category_c'].label='Transférer vers:'
        
        self.fields['financial_c'].widget = FinancialCategoryWidget()
        self.fields['category_c'].widget.attrs.update({'data-linked-account': 'financial_c'})
        
        self.fields['financial_d'].widget = FinancialCategoryWidget(self.category_d)
        
        if self.category_d is not None:
            if self.category_d.default_fincat is not None:
                self.fields['financial_d'].initial = self.category_d.default_fincat
                
            del self.fields['category_d']
        else:
            self.fields['category_d'].widget.attrs.update({'data-linked-account': 'financial_d'})
            
        if self.season is not None or self.event is not None:
            del self.fields['season']
            
            
    def clean_amount(self):
        """
        Check that amount is positive
        """
        
        if self.cleaned_data['amount'] < 0:
            raise forms.ValidationError("Amount must be positive!")
            
        return self.cleaned_data['amount']
            
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the database
        
        :return The InternalPayment object
        """
        
        ip = super().save(commit=False)
        
        ip.method = 'NO'
        
        if self.category_d is not None:
            ip.category_d = self.category_d
            
        if self.season is not None:
            ip.season = self.season
            
        if self.event is not None:
            ip.event = self.event
            ip.season = self.event.season
            
        ip.person = self.user
        
        try:
            if commit:
                ip.save()
        except Exception as e:
            raise GafcFormException(e)
            
        return ip