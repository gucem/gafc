# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
GAFC common classes for forms
"""

from django import forms
from django.db.models import Prefetch
from django.forms.utils import to_current_timezone

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Hidden

import unicodedata

import datetime as dt

from ..models import Event, Person, Payment, FinancialCategoryTree

class GafcFormException(Exception):
    """
    Exception when something went wrong in Gafc forms.
    """
    pass

class GafcBaseForm(object):
    """
    Base class for normal forms
    """
    
    what = None
    """
    The form identifier
    """
    
    submit_txt = "Enregistrer"
    """
    Text of the submit button
    """
    
    submit_class = "btn-primary"
    """
    bootstrap class of the submit button
    """
    
    
    def __init__(self, *args, **kwargs):
        """
        Constructor for base forms. Initialize the form helper
        
        :param \\*args: Extra arguments passed to parent constructor
        :param \\*\\*kwargs: Extra arguments passed to parent constructor
        """
        
        if self.what is None:
            raise GafcFormException("what is None")

        self.helper = MyFormHelper()
        self.helper.add_input(Hidden('whattodo', self.what))
        self.helper.add_input(Submit('submit', self.submit_txt, css_class=self.submit_class))


class GafcModelForm(forms.ModelForm, GafcBaseForm):
    """
    Base class for model forms
    """
    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        GafcBaseForm.__init__(self, *args, **kwargs)

class GafcForm(forms.Form, GafcBaseForm):
    """
    Base class for normal forms
    """
    def __init__(self, *args, **kwargs):
        forms.Form.__init__(self, *args, **kwargs)
        GafcBaseForm.__init__(self, *args, **kwargs)
        
class GafcDummyForm(GafcForm):
    """
    Dummy Form
    """
    def __init__(self, what, submit_txt="Enregistrer", submit_class="btn-primary", *args, **kwargs):
        self.what = what
        self.submit_txt = submit_txt
        self.submit_class = submit_class
        super().__init__(*args, **kwargs)
        
    def save(self):
        raise GafcFormException("DO NOT CALL SAVE ON THIS CLASS!")
        
    def is_valid(self):
        return True
    

def set_field_html_name(cls,prefix):
    """
    This creates wrapper around the normal widget rendering, 
    allowing for a custom field name (new_name).
    """
    old_render = cls.widget.render
    def _widget_render_wrapper(name, value, attrs=None, renderer=None):
        return old_render('%s-%s'%(prefix,name), value, attrs,renderer)

    cls.widget.render = _widget_render_wrapper
    

def normalize_name(name):
    nfkd_form = unicodedata.normalize('NFKD', name.strip())
    return nfkd_form.encode('ASCII', 'ignore').lower().decode().replace(' ','_')

class MyFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_method='post'
        self.label_class='col-sm-4'
        self.field_class='col-sm-8'
        self.html5_required=False
        self.form_class = 'form-horizontal'

# HTML5 date and time widgets
class MyDateInput(forms.DateInput):
    def __init__(self, attrs=dict(), format="%Y-%m-%d"):
        super().__init__(attrs={'type': 'date', 'class': 'form-control', **attrs},
                        format=format)
        
class MyTimeInput(forms.TimeInput):
    def __init__(self, attrs=dict(), format="%H:%M"):
        super().__init__(attrs={'type': 'time', 'class': 'form-control', **attrs},
                        format=format)
        
        
# Typeahead search widgets
class TypeaheadWidget(forms.Widget):
    
    template_name = 'gafc/widget/typeahead_widget.html'
    source = ''
    callback = None

    def __init__(self, source, *args, callback=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = source
        self.callback = callback
        
    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['source'] = self.source
        context['widget']['callback'] = self.callback
    
        return context
    
class PersonTypeaheadWidget(TypeaheadWidget):
    def __init__(self, *args, **kwargs):
        super().__init__('persons_json', *args, **kwargs)
        
class EventTypeaheadWidget(TypeaheadWidget):
    def __init__(self, all_evt = False, *args, **kwargs):
        super().__init__('events_json_all' if all_evt else 'events_json', *args, **kwargs)
        
class PaymentTypeaheadWidget(TypeaheadWidget):
    def __init__(self, *args, **kwargs):
        super().__init__('payments_json', *args, **kwargs)
        
        
# ModelChoiceField for typeahead (helpers)
class PersonField(forms.ModelChoiceField):
    def __init__(self, **kwargs):
        super().__init__(queryset=Person.objects.all(), widget=PersonTypeaheadWidget(), **kwargs) 
        
class EventField(forms.ModelChoiceField):
    def __init__(self, all_evt = False, **kwargs):
        super().__init__(queryset=Event.objects.all(), widget=EventTypeaheadWidget(all_evt), **kwargs)
        
class PaymentField(forms.ModelChoiceField):
    def __init__(self, **kwargs):
        super().__init__(queryset=Payment.objects.all(), widget=PaymentTypeaheadWidget(), **kwargs)


class FinancialCategoryWidget(forms.Select):
    def __init__(self, cat=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.choices = self.getFinlist(cat)
    
    @classmethod
    def getFintree(cls, cat=None):
        
        objs = dict()
        ret = []
        
        try:
            if cat is None:
                fct = FinancialCategoryTree.objects.prefetch_related('financialcategory_set')
            else:
                fct = FinancialCategoryTree.objects.prefetch_related(Prefetch('financialcategory_set', queryset=cat.allowed_fincat.all()))
            
            for t in fct:
                objs[t.id] = {'obj':t, 'sub':[], 'N': t.financialcategory_set.count()}
                
            for pk, t in objs.items():
                if t['obj'].toplevel_id is not None:
                    objs[t['obj'].toplevel_id]['sub'] += [t,]
                    objs[t['obj'].toplevel_id]['N'] += t['N']
                else:
                    ret += [t, ]
        except: pass
        return ret
    
    @classmethod
    def getFinlist(cls, cat=None, force=False, treespan='...', catspan=''):
        
        tree = cls.getFintree(cat)
        
        ret = [(None, '---'),]
        for t in tree:
            if t['N'] > 0 or force:
                ret += [(t['obj'].name, [(c.id, catspan+' '+c.name) for c in t['obj'].financialcategory_set.all()] ), *cls.__subGetFinlist(t, 1, force=force, treespan=treespan, catspan=catspan) ]
            
        return ret
        
    
    @classmethod
    def __subGetFinlist(cls, tree, N, force, treespan, catspan):
        ret = []
            
        for t in tree['sub']:
            if t['N'] > 0 or force:
                ret += [('%s %s'%(treespan*N, t['obj'].name), [(c.id, '%s %s'%(catspan*(N+1), c.name)) for c in t['obj'].financialcategory_set.all()] ), *cls.__subGetFinlist(t, N+1, force, treespan, catspan) ]
            
        return ret
        
class FinancialCategoryTreeWidget(forms.Select):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.choices = self.getFinlist()
    
    @classmethod
    def getFintree(cls):
        
        try:
            objs = dict()
            ret = []
            
            for t in FinancialCategoryTree.objects.all():
                objs[t.id] = {'obj':t, 'sub':[]}
                
            for pk, t in objs.items():
                if t['obj'].toplevel_id is not None:
                    objs[t['obj'].toplevel_id]['sub'] += [t,]
                else:
                    ret += [t, ]
                    
        except: pass
    
        return ret
    
    @classmethod
    def getFinlist(cls):
        
        tree = cls.getFintree()
        
        ret = [(None, '---'),]
        for t in tree:
            ret += [(t['obj'].id, t['obj'].name) , *cls.__subGetFinlist(t) ]
            
        return ret
        
    
    @classmethod
    def __subGetFinlist(cls, tree, N=1):
        ret = [] 
            
        for t in tree['sub']:
            ret += [(t['obj'].id, '%s %s'%('...'*N, t['obj'].name)), *cls.__subGetFinlist(t, N+1) ]
            
        return ret

        
class MyMultiDateTime(forms.MultiWidget):
    """
    A widget that splits datetime input into two <input type="text"> boxes.
    """
    supports_microseconds = False
    template_name = 'gafc/widget/multidatetime.html'
    invalid=False
    required=True

    def __init__(self, date_format=None, time_format=None, date_attrs=dict(), time_attrs=dict(), required=True):
        widgets = (
            MyDateInput(attrs={'class': 'form-control col-7', **date_attrs}),
            MyTimeInput(attrs={'class': 'form-control col-5', **time_attrs}),
        )
        self.required=required
        super().__init__(widgets)

    def decompress(self, value):
        if value:
            value = to_current_timezone(value)
            return [value.date(), value.time()]
        return [None, None]
    
    def value_from_datadict(self, data, files, name):
        
        vals = [widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)]
        
        try:
            return (dt.datetime.strptime(' '.join(vals),
                            '%Y-%m-%d %H:%M'))
        except:
            self.invalid=True
            return None
    
    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        if self.is_localized:
            for widget in self.widgets:
                widget.is_localized = self.is_localized
        # value is a list of values, each corresponding to a widget
        # in self.widgets.
        if not isinstance(value, list):
            value = self.decompress(value)

        final_attrs = context['widget']['attrs']
        
        input_type = final_attrs.pop('type', None)
        id_ = final_attrs.get('id')
        subwidgets = []
        for i, widget in enumerate(self.widgets):
            if input_type is not None:
                widget.input_type = input_type
            widget_name = '%s_%s' % (name, i)
            try:
                widget_value = value[i]
            except IndexError:
                widget_value = None
            if id_:
                widget_attrs = final_attrs.copy()
                widget_attrs['id'] = '%s_%s' % (id_, i)
            else:
                widget_attrs = final_attrs
                
            subw = widget.get_context(widget_name, widget_value, widget_attrs)['widget']
            
            if self.invalid and self.required:
                if not 'class' in subw['attrs']:
                    subw['attrs'] = ''
                    
                subw['attrs']['class'] += ' is-invalid'
            
            subwidgets.append(subw)
        context['widget']['subwidgets'] = subwidgets
        context['widget']['invalid'] = self.invalid
        return context
