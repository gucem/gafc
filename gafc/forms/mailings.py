# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Mailing list management forms
"""

from django import forms

from .common import GafcForm, GafcModelForm

from ..models import SympaMailingList
from .. import ffcam_utils

from gucem import settings as gs


class SympaMailingListForm(GafcModelForm):
    """
    Create and modify SympaMailingList objects
    """
    
    what = "save_mailinglist"
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.fields['code'].required=False
        self.fields['description'].widget.attrs.update({'rows': 3})
        
    class Meta:
        model   = SympaMailingList
        fields  = ('name','code','description','public')
        
        
class SympaMailingListUserForm(GafcForm):
    """
    Create and modify SympaMailingList objects
    """
    what = "save_user_mailing"
    
    def __init__(self, user, public=True, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """

        super().__init__(*args, **kwargs)
        
        self.mls = SympaMailingList.objects.all()
        self.user = user
        self.public = public
        
        if self.public:
            self.mls = self.mls.filter(public=True)
            
        self.fields['mailings'] = forms.ModelMultipleChoiceField(queryset=self.mls, widget=forms.CheckboxSelectMultiple(), required=False)
        self.fields['mailings'].initial = [ ml.pk for ml in self.user.sympamailinglist_set.all() ]
        
        
    def save(self):
        """
        Save the changes and perform operations on sympa server 
        """
        if 'mailings' in self.changed_data:
            sub = []
            unsub = []
            current_mailings = self.fields['mailings'].initial
            next_mailing_state = [ ml.pk for ml in self.cleaned_data['mailings'] ]
        
            
            for ml in self.mls:
                if ml.name in current_mailings and ml.name not in next_mailing_state:
                    unsub += [ ml.pk ]
                    ml.subscribers.remove(self.user)
                elif ml.name not in current_mailings and ml.name in next_mailing_state:
                    sub += [ ml.pk ]
                    ml.subscribers.add(self.user)
                    
            with ffcam_utils.SympaConnexxion(gs.GAFC_SYMPA_USER, gs.GAFC_SYMPA_PASSWD, gs.GAFC_SYMPA_SERVER) as conn:
                conn.sympa_sub_unsub(self.user.email, sub, unsub)

