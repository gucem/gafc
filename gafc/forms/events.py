# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
GAFC forms for events
"""

from django import forms

from django.db import transaction

from django.http import QueryDict

from django.contrib import messages

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage

import datetime as dt

import numpy as np

from .common import GafcForm, GafcFormException, GafcModelForm, MyMultiDateTime, set_field_html_name, normalize_name
from .persons import exampleChoiceField
from ..models import Event, EventState, EquipmentTracking, StaffEmail, Participation, EventMetaData, ParticipationMetaData, Category, EventMeta, EquipmentGroup, Person, Federation
from ..emails import process_email
from ..templatetags import gafc_tags

from .. import ffcam_utils


class DuplicateEventForm(GafcForm):
    """
    Event duplication form
    """
    
    what = 'event_duplicate'
    submit_txt = "Dupliquer l'événement"
    
    with_participations = forms.ChoiceField(choices=[(None, '---'),(0, 'Ne pas copier les participants'),(1,'Copier uniquement les participants présent'),(2, 'Copier uniquement les participants acceptés'),(3, 'Copier tous les participants')], label='Copier aussi les participants')
    
    def __init__(self, event, user, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event to be duplicated
        :param user: Current logged-in user
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.event = event
        self.user = user
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit change to DB
        
        :return: Newly created event object
        """
        new_event = Event.objects.get(pk=self.event.pk)
        new_event.pk = None
        new_event.save()
        
        try:
            # Copy meta
            metas = EventMetaData.objects.filter(event=self.event)
            
            for m in metas:
                m.event = new_event
                m.pk = None
                m.save()
        except IndexError:
            pass
        
        # Creation state
        EventState.objects.create(event=new_event,state='CR',comment="", person=self.user)
        
        # Copy participations
        
        withp = int(self.cleaned_data['with_participations'])
        
        if withp > 0:
            plist = Participation.objects.filter(event=self.event)
            
            if withp == 1:
                plist = plist.filter(event_done=True)
                
            if withp == 2:
                plist = plist.filter(waiting=False, cancelled=False)
            
            for p in plist:
                p.pk = None
                p.event = new_event
                p.save(event_copy=self.event)
        
        return new_event

class EventForm(GafcModelForm):
    """
    Event add/edit form
    """
    
    what = "save_event"
    
    description=forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Description')
    class Meta:
        model = Event
        fields = ('title','category', 'eventtype', 'cost', 'description', 'place', 'begin', 'finish','season', 'helloassoLink', 'visibility','public_after_va','link', 'onlyvalid')
        
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        date = dt.datetime.now()
        
        self.fields['begin'].widget = MyMultiDateTime()
        self.fields['begin'].initial = date.replace(hour=8, minute=0)
        self.fields['finish'].widget = MyMultiDateTime()
        self.fields['finish'].initial = date.replace(hour=18, minute=0)
        
        self.fields['place'].required=False
        self.fields['link'].help_text = "Tout adhérent connecté peut s'inscrire"
        self.fields['onlyvalid'].help_text = "Seuls les adhérents avec une licence valide peuvent s'inscrire."
        self.fields['description'].help_text = "Supporte le formattage <a href=\"https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet\" class=\"text-muted\"><b>markdown</b></a>"
        self.fields['visibility'].help_text = "Publique: afficher sur le site, Intranet: N'afficher que sur l'intranet, Caché: accessible avec le lien uniquement, Privé: Affichage admin uniquement"
        self.fields['helloassoLink'].required=False
        
        self.fields['category'].queryset = Category.objects.order_by('name')
        
    def clean_finish(self):
        """
        Check finish date consistency
        """
        
        begin = self.cleaned_data['begin']
        end   = self.cleaned_data['finish']
        
        if end < begin:
            raise forms.ValidationError(' La fin de l\'événement doit se situer après le début. ')
        
        if self.instance is not None and self.instance.pk is not None:
            for t in self.instance.equipmenttracking_set.all():
                av, evts = t.equipment.isAvailable(begin, end, self.instance)
                if not av:
                    raise forms.ValidationError("L'équipement %s n'est pas disponible aux nouvelles dates de l'événement"%t.equipment.name)
        return end
    
    def clean(self):
        """
        Report the errors to widgets
        """
        super().clean()
        self.fields['finish'].widget.invalid = 'finish' in self._errors
        self.fields['begin'].widget.invalid = 'begin' in self._errors


class EventLicenseForm(GafcForm):
    """
    Create discovery license for event
    """
    
    what = "create_event_discovery_license"
    
    extranet_login  = forms.CharField(label="Login EXTRANET")
    extranet_passwd = forms.CharField(widget=forms.PasswordInput(), label="Mot de passe EXTRANET")
    persons         = forms.ModelMultipleChoiceField(Person.objects.all())
    federation      = forms.ModelChoiceField(Federation.objects.all(), label="Fédération")
    
    def __init__(self, event, *args, **kwargs):
        """
        Constructor
        
        :param event: Event to delete
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.event = event
        
        if type(self.event) is not Event:
            raise GafcFormException("The event must be an Event object")
            
        participation_set = self.event.participation_set.select_related('person').all()
        self.date_in = self.event.begin
        
        self.category = np.ceil((self.event.finish - self.event.begin).total_seconds()/86400)
        
        self.participation_set = []
        for p in participation_set:
            if not p.has_valid_license:
                self.participation_set += [ p ,]
            
        self.fields['persons'].queryset = Person.objects.filter(id__in=[p.person.id for p in self.participation_set])


    def save(self):
        """
        Create the discovery licenses on FFCAM extranet and the license objects on database
        """
        
        if self.date_in < dt.datetime.now(tz=self.date_in.tzinfo):
            raise GafcFormException("Le début de l'événement est déjà passé!")
            
        if self.category > 3:
            raise GafcFormException("La durée de l'événement est supérieure à 3 jours!")
        
        with ffcam_utils.FFCAMExtranetConnexxion(self.cleaned_data['extranet_login'], self.cleaned_data['extranet_passwd']) as conn:
            for p in self.cleaned_data['persons']:
                conn.create_new_discovery_license(p, self.cleaned_data['federation'].id, self.date_in, self.category*24)        


class DeleteEventForm(GafcForm):
    """
    Form to delete an event
    """
    
    what = "delete_event"
    """
    Form id
    """
    
    submit_txt = "Supprimer l'événement"
    """
    Button text
    """
    
    submit_class = "btn-danger"
    """
    button class
    """
    
    def __init__(self, event, *args, **kwargs):
        """
        Constructor
        
        :param event: Event to delete
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.event = event
        
        if type(self.event) is not Event:
            raise GafcFormException("The event must be an Event object")

    def save(self):
        """
        Perform the deletion
        """
        
        if self.event.can_delete:
            self.event.delete()
        else:
            raise GafcFormException("Event can't be deleted")
            
class DeleteEventValidationForm(GafcForm):
    """
    Form to remove validation from event
    """
    
    what = "delete_event_validation"
    """
    Form id
    """
    
    submit_txt = "Dé-valider l'événement"
    """
    Button text
    """
    
    submit_class = "btn-danger"
    """
    button class
    """
    
    def __init__(self, event, *args, **kwargs):
        """
        Constructor
        
        :param event: Event to delete
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.event = event
        
        if type(self.event) is not Event:
            raise GafcFormException("The event must be an Event object")

    def save(self):
        """
        Perform the deletion
        """
        
        try:
            EventState.objects.get(event=self.event, state='VA').delete()
        except Exception as e:
            raise GafcFormException(e)
            
class DeleteEventEquipmentTrackingForm(GafcForm):
    """
    Form to remove validation from event
    """
    
    what = "delete_event_equipment"
    """
    Form id
    """
    
    submit_txt = "Supprimer"
    """
    Button text
    """
    
    submit_class = "btn-danger"
    """
    button class
    """
    
    equipment_tracking = forms.ModelChoiceField(EquipmentTracking.objects.all(),empty_label = None, widget=forms.HiddenInput())
    
    def __init__(self, event, *args, **kwargs):
        """
        Constructor
        
        :param event: Event to delete
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.event = event
        
        self.fields['equipment_tracking'].queryset = EquipmentTracking.objects.filter(event=self.event)
        
        if type(self.event) is not Event:
            raise GafcFormException("The event must be an Event object")

    def save(self):
        """
        Perform the deletion
        """
        
        try:
            self.cleaned_data['equipment_tracking'].delete()
        except Exception as e:
            raise GafcFormException(e)

              
class SendEventMailForm(GafcForm):
    """
    Send email form
    """
    
    what = "send_email"
    
    email_to = forms.MultipleChoiceField(required=False)
    email_cc = forms.MultipleChoiceField(required=False)
    email_bcc = forms.MultipleChoiceField(required=False)
    
    email_to_add = forms.CharField(required=False)
    email_cc_add = forms.CharField(required=False)
    email_bcc_add = forms.CharField(required=False)
    
    email_from = forms.ChoiceField()
    email_replyto = forms.ChoiceField()
    
    email_title = forms.CharField()
    email_content = forms.CharField(widget=forms.Textarea(attrs={'rows':10}))
    email_html = forms.BooleanField(required=False)
    
    def __init__(self, event, user, request, *args, **kwargs):
        """
        Initialize the form
        
        :param event: The event the email is related to
        :param user: The logged user that sent the email
        :param request: Request object for Email generation
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        super().__init__(*args, **kwargs)
        
        self.event = event
        self.user = user
        self.request = request
        
        email_list = [ (mail['person__email'],mail['person__email']) for mail in event.participation_set.values('person__email') ]
        
        self.fields['email_to'].choices = email_list
        self.fields['email_cc'].choices = email_list
        self.fields['email_bcc'].choices = email_list
        
        staffmail = []
        
        if user.perms['view_staffemail']:
            staffmail += [ e for e in user.staffemail_set.filter(noauth=False) ]
        
        if user.perms['send_noauth_staffemail']:
            staffmail += [ e for e in StaffEmail.objects.filter(noauth=True) ]
            
        fromset = []
        replytoset = []
        
        for m in staffmail:
            fromset += [ m.email, ]
            
            if not m.noauth:
                replytoset += [ m.email, ]
            
            if not m.noauth or user.perms['view_staffemail']:
                replytoset += [ e.email for e in m.staffemailalias_set.all() ]
            
        replytoset += [ user.email, ]
        
        self.fields['email_from'].choices = [ (m,m) for m in fromset ]
        self.fields['email_replyto'].choices = [ (m,m) for m in replytoset ]
        
    def validateEmail(self, email ):
        """
        Email validation
        
        :param email: Email to be validated
        
        :raise ValidationError: if the email is not valid
        """
        try:
            validate_email( email )
            return True
        except ValidationError:
            return False
        
    def clean_email_to_add(self):
        """
        Custom email_to cleaning
        """
        r = []
        
        for m in self.cleaned_data['email_to_add'].split(','):
            m = m.strip()
            if m != '':
                if self.validateEmail(m):
                    r += [m, ]
                else:
                    raise ValidationError('%s n\'est pas une adresse email valide'%m)
        
        return r
    
    def clean_email_cc_add(self):
        """
        Custom email_cc cleaning
        """
        r = []
        
        for m in self.cleaned_data['email_cc_add'].split(','):
            m = m.strip()
            if m != '':
                if self.validateEmail(m):
                    r += [m, ]
                else:
                    raise ValidationError('%s n\'est pas une adresse email valide'%m)
        
        return r
    
    def clean_email_bcc_add(self):
        """
        Custom email_bcc cleaning
        """
        r = []
        
        for m in self.cleaned_data['email_bcc_add'].split(','):
            m = m.strip()
            if m != '':
                if self.validateEmail(m):
                    r += [m, ]
                else:
                    raise ValidationError('%s n\'est pas une adresse email valide'%m)
        
        return r
        
    def clean(self):
        """
        Report the errors to cleaned fields
        """
        super().clean()
        
        tofields = ('email_to','email_cc', 'email_bcc', 'email_to_add', 'email_cc_add', 'email_bcc_add')
        at_least_one = False
        
        for f in tofields:
            if f in self.cleaned_data and  len(self.cleaned_data[f]) > 0:
                at_least_one = True
                break
        
        if not at_least_one:
            self.add_error('email_to','Au moins un destinataire doit être défini!')
            
    def save(self):
        """
        Send the email
        """
        try:
            md, html, title = process_email(self.cleaned_data['email_content'], self.request, self.event, self.cleaned_data['email_title'])
            
            content = html if self.cleaned_data['email_html'] else md
            
            mail = EmailMessage(
                title,
                content,
                self.cleaned_data['email_from'],
                self.cleaned_data['email_to'] + self.cleaned_data['email_to_add'] + [self.cleaned_data['email_from'], ],
                reply_to = [ self.cleaned_data['email_replyto'], ],
                cc=self.cleaned_data['email_cc'] + self.cleaned_data['email_cc_add'],
                bcc=self.cleaned_data['email_bcc'] + self.cleaned_data['email_bcc_add']
            )
            
            if self.cleaned_data['email_html']:
                mail.content_subtype = "html"  # Main content is now text/html
                
            #mail.attach_alternative("<pre>%s</pre>"%content, "text/html")
            mail.send(fail_silently=False)
            
            messages.success(self.request, "E-mail envoyé avec succès!")

        except Exception as err:
            messages.error(self.request, "Erreur lors de l'envoi de l'e-mail: "+str(err))
        
class ParticipationDeleteForm(GafcForm):
    """
    Form to unsubscribe to event
    """
    
    what = "delete_participation"
    """
    Form id
    """
    
    submit_txt = "Désinscription"
    """
    Button text
    """
    
    submit_class = "btn-danger"
    """
    button class
    """
    
    def __init__(self, participation , *args, **kwargs):
        """
        Constructor
        
        :param event: Event to delete
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        self.participation = participation
        
        if type(self.participation) is not Participation:
            raise GafcFormException("The event must be a Participation object")
            
    def save(self, *args, **kwargs):
        """
        Perform the action
        
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        self.participation.delete()
        
        
class ParticipationRequestForm(GafcForm):
    
    what = "request_participation"
    """
    What
    """
    
    def __init__(self, event, user, participation, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event the participation request is related to
        :param user: Current logged-in user
        :param participation: Current participation object
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """

        self.event = event
        self.person = user
        self.instance = participation
        
        metas = self.event.eventmetadata_set.all()
        
        super().__init__(*args, **kwargs)
        
        for m in metas:
            name = m.meta.nrmname
            label = m.meta.name
            if m.cost != 0.:
                label += " (%.2f €)"%(m.cost)
                
            if m.meta.datatype == 'ST':
                self.fields[name] = forms.CharField(max_length=200, required=False, label=label)
            elif m.meta.datatype == 'BO':
                self.fields[name] = forms.BooleanField(required=False, label=label)
            elif m.meta.datatype == 'IN':
                self.fields[name] = forms.IntegerField(label=label)
            elif m.meta.datatype == 'FL':
                self.fields[name] = forms.DecimalField(label=label)
            elif m.meta.datatype == 'UN':
                self.fields[name] = forms.ChoiceField(label=label, choices=m.available_choices)
                
            self.fields[name].widget.attrs = {'title': m.description, 'aria-label': m.description}
            self.fields[name].help_text = m.description
            
        if self.instance is not None:
            for pmd in self.instance.participationmetadata_set.all():
                if pmd.eventMeta.meta.datatype == 'BO':
                    self.fields[pmd.eventMeta.meta.nrmname].initial = pmd.data == "True"
                else:
                    self.fields[pmd.eventMeta.meta.nrmname].initial = pmd.data
                
    def save(self):
        """
        Save the participation request
        """
        if self.is_valid():
            if self.instance is None:
                self.instance = Participation.objects.create(event   = self.event,
                                                            person  = self.person,
                                                            role    = '0PA',
                                                            waiting = True)
            
            metas = EventMetaData.objects.filter(event=self.event)
            
            for m in metas:
                name = m.meta.nrmname
                ParticipationMetaData.objects.update_or_create(eventMeta = m,
                                                               participation=self.instance,
                                                               defaults={'data': self.cleaned_data[name]})
        
        
        
class EventMetaDataForm(forms.Form):
    """
    Create EventMetaData object
    """
    
    what = "add_event_meta"
    
    name    = forms.CharField(label="Titre", max_length=29)
    datatype= forms.ChoiceField(choices=EventMeta.datatype.field.choices, label="Type")
    cost    = forms.DecimalField(decimal_places=2, label="Coût", initial=0.)
    description= forms.CharField(label="Description", required=False)
    availables= forms.CharField(label="Choix", required=False)
    
    def __init__(self, event, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event the EventMetaData request is related to
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        
        super().__init__(*args, **kwargs)
        self.event = event
        
        
    def save(self):
        """
        Create EventMeta if needed and EventMetaData objects
        """
        with transaction.atomic():
            meta,cr = EventMeta.objects.get_or_create(name=self.cleaned_data['name'], datatype=self.cleaned_data['datatype'])
            metadata, cr = EventMetaData.objects.update_or_create(meta=meta, event=self.event, defaults={'cost':self.cleaned_data['cost'], 'description':self.cleaned_data['description'], 'availables':self.cleaned_data['availables']})
            return metadata
        
class EventStateForm(GafcModelForm):
    """
    Add a state to event
    """
    
    what = "add_state"
    comment     = forms.CharField(widget=forms.Textarea(attrs={'cols':40, 'rows':3}),label='Commentaire',required=False)
    class Meta:
        model = EventState
        fields = ('state','comment')
        
    def __init__(self, event, user, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event the EventMetaData request is related to
        :param user: User that add the state
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.event = event
        self.user = user
        
        self.fields['state'].choices=[ ('','---'),
                                       ('OP', gafc_tags.eventState('OP')),
                                       ('CL', gafc_tags.eventState('CL')),
                                       ('CN', gafc_tags.eventState('CN')),
                                       ('VA', gafc_tags.eventState('VA')),
                                       ('CO', gafc_tags.eventState('CO'))]
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes on DB
        
        :raise GafcFormException: If something went wrong
        """
        s = super().save(commit=False)
        s.event = self.event
        s.person = self.user
        
        try:
            with transaction.atomic():
                s.save()
                
                if s.state == 'CN':
                    s.pk = None
                    s.state = 'VA'
                    s.save()
        
        except ValueError as err:
            raise GafcFormException(err)
        
        
class EventEquipmentForm(GafcModelForm):
    """
    Add an equipment to an event
    """
    
    what = "event_add_equipment"
    
    def __init__(self, event, user, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event to which the equipment will be appended
        :param user: Current logged-in user
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        
        super().__init__(*args, **kwargs)
        
        self.event = event
        self.user = user
        
        lst = EquipmentGroup.tree2list(EquipmentGroup.getTree(), withEquipments=True)
        chs = dict()
        last = ''
        
        for l in lst[1:]:
            if l['type'] == 'group':
                last = l['label']
            
                if last not in chs:
                    chs[last] = []
            else:
                chs[last] += [l,]
        
        self.fields['equipment'].choices = [(k, [(l['obj'].id, l['label']) for l in c]) for k,c in chs.items()]
        self.fields['equipment'].widget.attrs = {'class':'form-control rounded-left'}
        
    class Meta:
        model = EquipmentTracking
        fields = ('equipment',)
        
    def save(self, commit=True):
        """
        Save the changes
        
        :param commit: Commit the changes to DB
        
        :raise GafcFormException: If something went wrong
        """
        eq = self.cleaned_data['equipment']
                
        avail, evts = eq.isAvailable(self.event.begin, self.event.finish)
        if avail:
            try:
                EquipmentTracking.objects.create(equipment=eq,
                                                 event=self.event,
                                                 person=self.user,
                                                 date=self.event.begin,
                                                 trackingtype="US")
            
            except Exception as ex:
                raise GafcFormException(str(ex))
        else:
            raise GafcFormException("L'équipement %s n'est pas disponible. Utilisé dans les événements suivants: %s"%(eq.name, ' '.join([ev.title for ev in evts])))
        
        
class EventEquipmentGroupForm(GafcForm):
    """
    Add an equipment group to an event
    """
    
    what = "event_add_equipment_group"
    
    groups = forms.ChoiceField(label="Groupe de matériel")
    def __init__(self, event, user, request=None, *args, **kwargs):
        """
        Initialize the form
        
        :param event: Event to which the equipments will be appended
        :param user: Current logged-in user
        :param request: The request object to log the errors
        :param \\*args: Extra parameters passed to parent
        :param \\*\\*kwargs: Extra parameters passed to parent
        """
        
        
        super().__init__(*args, **kwargs)
        
        self.event = event
        self.user = user
        self.request = request
        
        self.fields['groups'].choices = [(t['obj'].id if t['obj'] is not None else None, t['label'] if t['obj'] is not None else '---') for t in EquipmentGroup.tree2list(EquipmentGroup.getTree())]
        self.fields['groups'].widget.attrs = {'class':'form-control rounded-left'}
        
    def save(self, commit=True):
        """
        Append equipments to event. Log error or succes using the request object in constructor
        
        :param commit: Commit the changes to DB
        """
        
        gid = self.cleaned_data['groups']
        
        eqs = EquipmentGroup.tree2list(EquipmentGroup.getTree(int(gid)), withEquipments=True)
        
        N = 0
        
        for t in eqs:
            if t['type'] == 'group': continue
            if t['obj'].date_scrap is not None: continue
            
            eq = t['obj']
            avail, evts = eq.isAvailable(self.event.begin, self.event.finish)
            
            if avail:
                EquipmentTracking.objects.update_or_create(equipment=eq, event=self.event, defaults={'person':self.user, 'date':self.event.begin, 'trackingtype':"US"})
            elif not(len(evts) == 1 and evts[0] == self.event):
                N += 1
                msg = "L'équipement %s n'est pas disponible. Utilisé dans les événements suivants: %s"%(eq.name, ' '.join([ev.title for ev in evts]))
                if self.request is not None:
                    messages.error(self.request, msg)
                else:
                    raise GafcFormException(msg)
                    
        if self.request is not None:
            messages.success(self.request, "%i équipements ajoutés."%N)
    

        
class ParticipationMetaForm(forms.Form):
    """
    Base form for participationformset. Not used anymore with ASGI.
    """
    role = forms.ChoiceField(choices = Participation.role.field.choices)
    waiting = forms.BooleanField(required=False, label="Liste d'attente")
    cancelled = forms.BooleanField(required=False, label="Annulé")
    done = forms.BooleanField(required=False, label="Présent")
    
    def __init__(self, event, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.fields['role'].widget.attrs = {'meta_id': 'role'}
        self.fields['waiting'].widget.attrs = {'meta_id': 'waiting', 'title': "En liste d'attente"}
        self.fields['cancelled'].widget.attrs = {'meta_id': 'cancelled', 'title': "Annulé"}
        self.fields['done'].widget.attrs = {'meta_id': 'done', 'title': "Participation confirmée"}
        
        metas = event.eventmetadata_set.all()
        
        for m in metas:
            name = m.meta.nrmname
            if m.meta.datatype == 'ST':
                self.fields[name] = forms.CharField(max_length=200, required=False, label=m.meta.name)
            elif m.meta.datatype == 'BO':
                self.fields[name] = forms.BooleanField(required=False, label=m.meta.name)
            elif m.meta.datatype == 'IN':
                self.fields[name] = forms.IntegerField(label=m.meta.name)
            elif m.meta.datatype == 'FL':
                self.fields[name] = forms.DecimalField(label=m.meta.name)
            elif m.meta.datatype == 'UN':
                self.fields[name] = forms.ChoiceField(label=m.meta.name, choices=m.available_choices, required=False)
                
            self.fields[name].widget.attrs = {'cost': '%.2f'%(m.cost), 'meta_id':'%i'%(m.id), 'title': m.description, 'aria-label': m.description}
            
        for (k,v) in self.fields.items():
            self.fields[k].widget.attrs.update({'class': 'form-control p-1'})
    
class ParticipationFormset:
    """
    Participation formset. Not used anymore with ASGI.
    """
        
    def __init__(self,event,*args,**kwargs):
        self.event = event
        self.parts = event.participation_set.all()
        self.forms = []
        self.idx = 0
        self.prefix = kwargs['prefix'] if 'prefix' in kwargs  else 'form'
        provided_values = args.__len__() > 0 and args[0] is not None
        self.values = args[0] if provided_values else dict()
        self.clean_values = dict()
        self.metas = event.eventmetadata_set.all()
        #self.sum_by_meta = {'waiting': 0, 'done': 0}
        #self.cost_by_part = []
        #self.cost_total = 0.
        
        kdefault = {'role':'0PA','waiting':False,'done':False}
        
        for m in self.metas:
            if m.meta.datatype == 'BO':
                kdefault[m.meta.nrmname] = False
            elif m.meta.datatype in ('IN', 'FL'):
                kdefault[m.meta.nrmname] = '0'
            else:
                kdefault[m.meta.nrmname] = ''
            #if m.meta.datatype == 'ST':
                #self.sum_by_meta['%i'%(m.id)] = None
            #else:
                #self.sum_by_meta['%i'%(m.id)] = 0
                
        db_data = dict()
        
        #partmeta = ParticipationMetaData.objects.filter(eventMeta__event=self.event)
        for emd in event.eventmetadata_set.all():
            for pm in emd.participationmetadata_set.all():
                
                if pm.eventMeta.meta.datatype == "UN":
                    if pm.data not in pm.eventMeta.available_choices_keys:
                        pm.data = ''
                        pm.save()
            
                db_data["%s-%i-%s"%(self.prefix,pm.participation.id,pm.eventMeta.meta.nrmname)] = pm.data
            
        for p in self.parts:
            db_data["%s-%i-role"%(self.prefix,p.id)] = p.role
            db_data["%s-%i-waiting"%(self.prefix,p.id)] = p.waiting
            db_data["%s-%i-cancelled"%(self.prefix,p.id)] = p.cancelled
            db_data["%s-%i-done"%(self.prefix,p.id)] = p.event_done
            vals = dict()
            
            # Take final value by priority: given value > db value > default value
            for d in kdefault:
                nname = d
                cname = "%s-%i-%s"%(self.prefix,p.id,nname)
                if provided_values and cname in self.values:
                    vals[nname] = self.values[cname]
                elif not provided_values and cname in db_data:
                    vals[nname] = db_data[cname]
                    self.values[cname] = db_data[cname]
                else:
                    vals[nname] = kdefault[nname]
                    self.values[cname] = kdefault[nname]
            
            for m in self.metas:
                if m.meta.datatype in ('IN', 'FL'):
                    nname = m.meta.nrmname
                    cname = "%s-%i-%s"%(self.prefix,p.id,nname)
                    
                    try:
                        int(vals[nname])
                    except:
                        vals[nname] = kdefault[nname]
                        self.values[cname] = kdefault[nname]
                        
                if m.meta.datatype == 'UN':
                    nname = m.meta.nrmname
                    cname = "%s-%i-%s"%(self.prefix,p.id,nname)
                    
                    if vals[nname] not in m.available_choices_keys:
                        vals[nname] = kdefault[nname]
                        self.values[cname] = kdefault[nname]
            
            # prepend prefix to field name to allow fieldset
            mf = ParticipationMetaForm(p.event,vals)
            mf.is_valid()
            
            #self.sum_by_meta['waiting'] += mf.cleaned_data['waiting']
            #self.sum_by_meta['done'] += mf.cleaned_data['done']
            
#            i = 2
#            total = float(self.event.cost)
                
            #for m in self.metas:
                #if not m.meta.datatype == 'ST':
                    #v = mf.cleaned_data[nrmname(m)]
                    #self.sum_by_meta['%i'%(m.id)] += v
                    #total += float(m.cost) * v
                    #i += 1
                    
            #self.cost_by_part += [total, ]
            #self.cost_total += total * (not mf.cleaned_data['waiting'])
            
            for k in mf.fields:
                set_field_html_name(mf.fields[k],'%s-%i'%(self.prefix,p.id))
                self.clean_values['%s-%i-%s'%(self.prefix,p.id,k)] = mf.cleaned_data[k]
               # mf.fields[k].widget.attrs = {**mf.fields[k].widget.attrs,'ref':p.id}
                
            self.forms += [ mf ]
            
    def __iter__(self):
        return self
    
    def __next__(self):
        try:
            result = (self.parts[self.idx], self.forms[self.idx])
        except IndexError:
            raise StopIteration
        self.idx += 1
        return result
    
    def is_valid(self):
        for f in self.forms:
            if not f.is_valid():
                return False
        return True
    
    def save(self,commit=True):
        data = []
        
        for p in self.parts:
            dd = dict()
            p.waiting       = self.clean_values["%s-%i-waiting"%(self.prefix,p.id)]
            p.cancelled     = self.clean_values["%s-%i-cancelled"%(self.prefix,p.id)]
            p.role          = self.clean_values["%s-%i-role"%(self.prefix,p.id)]
            p.event_done    = self.clean_values["%s-%i-done"%(self.prefix,p.id)]
            
            if commit:
                p.save()
            
            dd['particip']  = p
            dd['meta']      = dict()
            
            for m in self.metas:
                try:
                    pmd = ParticipationMetaData.objects.get(eventMeta = m, participation=p)
                    pmd.data=self.clean_values["%s-%i-%s"%(self.prefix,p.id,m.meta.nrmname)]
                except ParticipationMetaData.DoesNotExist:
                    pmd = ParticipationMetaData(eventMeta = m, participation=p,data=self.clean_values["%s-%i-%s"%(self.prefix,p.id,m.meta.nrmname)])
                    
                dd['meta'][m.meta.nrmname] = pmd
                
                if commit:
                    pmd.save()
            
            data += [dd]
            
        return data;
        
    
    
class EventsFieldAttributionForm(forms.Form):
    """
    Load events from a CSV file.
    """
    def __init__(self, headers, *args, **kwargs):
        self.headers = headers
        self.examples = []
        self.errors_str = []
        if 'examples' in kwargs:
            self.examples = kwargs['examples']
            del kwargs['examples']
        
        super().__init__(*args, **kwargs)
        
        i=0
        
        for h in headers:
            
            e = []
            for ex in self.examples:
                if i < len(ex):
                    e += [ex[i],]
                else:
                    e += [None,]
                    
            i += 1
            
            #['Subject', 'Start date', 'Start time', 'End date', 'End time', 'All Day Event', 'Description', 'Location', 'before', 'symbol', 'Prix', 'Categories', 'Label', 'Abstract']

            
            n = normalize_name(h)
            
            init = '';
            hh = h.lower()
            
            if hh == 'id':
                init = 'EVENT_ID'
            
            if hh == 'type':
                init = 'TYPE'
            
            if hh == 'subject':
                init = 'TITLE'
                
            if hh == 'start date':
                init = 'BEGIN_DATE'
                
            if hh == 'start time':
                init = 'BEGIN_TIME'
                
            if hh == 'end date':
                init = 'END_DATE'
                
            if hh == 'end time':
                init = 'END_TIME'
                
            if hh == 'visibility':
                init = 'VISIBILITY'
                
            if hh == 'description':
                init = 'DESCRIPTION'
                
            if hh == 'location':
                init = 'PLACE'
                
            if hh == 'prix':
                init = 'COST'
                
            if hh == 'public after validation':
                init = 'PUBLIC_VA'
                
            if hh == 'form':
                init = 'FORM'
                
            if hh == 'license':
                init = 'ONLYVALID'
            
            self.fields[n] = exampleChoiceField(required=False,
                                               label=h,
                                               examples = e,
                                               choices=[('','---'),
                                                        ('TITLE','Titre'),
                                                        ('EVENT_ID', 'ID événement'),
                                                        ('TYPE', 'Type d\'évenement'),
                                                        ('BEGIN_DATE', 'Date de début'),
                                                        ('BEGIN_TIME', 'Heure de début'),
                                                        ('END_DATE', 'Date de fin'),
                                                        ('END_TIME', 'Heure de fin'),
                                                        ('DESCRIPTION', 'Description'),
                                                        ('VISIBILITY', 'Visibilité'),
                                                        ('PLACE', 'Lieu'),
                                                        ('COST','Prix'),
                                                        ('PUBLIC_VA','Rendre publique après validation'),
                                                        ('FORM','Formulaire d\'inscription'),
                                                        ('ONLYVALID','Licence valide uniquement'),],
                                               initial = init)
            self.fields[n].widget.attrs.update({'class': 'custom-select'})
            
    def process(self, lines, ids):
        print(ids)
        self.is_valid()
        i=0
        ret = QueryDict(mutable=True)
        
        id_key = None
        j = 0
        
        for k in self.headers:
            if self.cleaned_data[normalize_name(k)] == 'EVENT_ID':
                id_key = j
                break
            j += 1
            
            
        for l in lines:
            pre = 'NEW-%i'%(i)
            
            if id_key is not None:
                try:
                    event_id = int(l[id_key])
                    if event_id in ids:
                        pre = 'event-%i'%event_id
                    else:
                        self.errors_str += ['Event %i not in allowed list'%event_id,]
                        continue
                except:
                    pass
                
            
            ret.appendlist('form_id', pre)
            i+=1
            for (k,v) in tuple(zip(self.headers, l)):
                k = normalize_name(k)
                if k not in self.cleaned_data or self.cleaned_data[k] == '':
                    continue
                elif self.cleaned_data[k] == 'TITLE':
                    ret[pre+'-title'] = v
                elif self.cleaned_data[k] == 'TYPE':
                    ret[pre+'-eventtype'] = v
                elif self.cleaned_data[k] == 'VISIBILITY':
                    ret[pre+'-visibility'] = v
                elif self.cleaned_data[k] == 'BEGIN_DATE':
                    ret[pre+'-begin_0'] = v
                elif self.cleaned_data[k] == 'BEGIN_TIME':
                    ret[pre+'-begin_1'] = v
                elif self.cleaned_data[k] == 'END_DATE':
                    ret[pre+'-finish_0'] = v
                elif self.cleaned_data[k] == 'END_TIME':
                    ret[pre+'-finish_1'] = v
                elif self.cleaned_data[k] == 'DESCRIPTION':
                    ret[pre+'-description'] = v
                elif self.cleaned_data[k] == 'PLACE':
                    ret[pre+'-place'] = v
                elif self.cleaned_data[k] == 'COST':
                    ret[pre+'-cost'] = v
                elif self.cleaned_data[k] == 'PUBLIC_VA':
                    if v.strip().lower() not in ['','false']:
                        ret[pre+'-public_after_va'] = 'on'
                elif self.cleaned_data[k] == 'FORM':
                    if v.strip().lower() not in ['','false']:
                        ret[pre+'-link'] = 'on'
                elif self.cleaned_data[k] == 'ONLYVALID':
                    if v.strip().lower() not in ['','false']:
                        ret[pre+'-onlyvalid'] = 'on'
                    
            if pre+'-finish_0' in ret and ret[pre+'-finish_0'] == '' and pre+'-begin_0' in ret:
                ret[pre+'-finish_0'] = ret[pre+'-begin_0']
                
            if pre+'begin_1' not in ret or ret[pre+'-begin_1'] == '':
                ret[pre+'-begin_1'] = '08:00'
                
            if pre+'finish_1' not in ret or ret[pre+'-finish_1'] == '':
                ret[pre+'-finish_1'] = '18:00'
                
        return ret