# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Tags used on templates
"""


from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe
from ..permissions import GAFC_PERMISSIONS_DICT
import unicodedata, json
import markdown, re
from mdx_linkify.mdx_linkify import LinkifyExtension
import gucem.settings

register = template.Library()

#from gafc.forms import SeasonForm


class GafcFilterException(Exception):
    """
    Exception raised if key isn't known
    """
    pass

@register.filter
def normalize(name):
    nfkd_form = unicodedata.normalize('NFKD', name)
    return nfkd_form.encode('ASCII', 'ignore').lower().decode()

@register.simple_tag
def setFieldValue(form, **kwargs):
    """
    Set the initial value of specific field of the form. Usefull for deletion forms, when multiple forms are generated
    """
    
    for k, v in kwargs.items():    
        form.fields[k].widget.attrs.update({'value' : v})
    
    return ""

@register.filter
def colorfade(val, absmax, maxcolor="#33cc33", mincolor="#ff4d4d", zerocolor="#FFFFFF"):
    
    try:
        val = val.replace(',','.')
    except:
        pass
        
    try:
        absmax = absmax.replace(',','.')
    except:
        pass
    
    val = float(val)
    absmax = abs(float(absmax))
    
    reg = re.compile('#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})', re.IGNORECASE)
    
    mm = reg.match(mincolor)
    mM = reg.match(maxcolor)
    mz = reg.match(zerocolor)
    
    if mm is None:
        raise GafcFilterException('min Color format mismatch')
    if mM is None:
        raise GafcFilterException('max Color format mismatch')
    if mz is None:
        raise GafcFilterException('zero Color format mismatch')
    
    if val > absmax:
        return maxcolor
    elif val < -absmax:
        return mincolor
    
    mr, mg, mb = int(mm.group(1),16), int(mm.group(2),16), int(mm.group(3),16)
    Mr, Mg, Mb = int(mM.group(1),16), int(mM.group(2),16), int(mM.group(3),16)
    r, g, b = int(mz.group(1),16), int(mz.group(2),16), int(mz.group(3),16)
    
    x = abs(val / absmax)
    if val < 0:
        r += int(x * float(mr - r))
        g += int(x * float(mg - g))
        b += int(x * float(mb - b))
    else:
        r += int(x * float(Mr - r))
        g += int(x * float(Mg - g))
        b += int(x * float(Mb - b))
    
    return '#%2x%2x%2x'%(r,g,b)

@register.filter
def jsonify(val):
    """
    Render json pretty
    """
    try:
        parsed = json.loads(val.replace('\'','"'))
        return json.dumps(parsed, indent=4)
    except Exception as e:
        return '%s: %s'%(str(e), val)
    

@register.filter
def GafcPermission(val):
    """
    Convert permission to custom text
    """
    
    key = "%s.%s"%(val.content_type.app_label, val.codename)
    
    if key not in GAFC_PERMISSIONS_DICT:
        raise GafcFilterException("Permission %s is not in GAFC_PERMISSIONS_DICT"%key)
    
    return GAFC_PERMISSIONS_DICT[key]

@register.filter
def payMethod(value):
    """
    Convert payment method key to human-readable value
    """
    if value == 'CA':
        return 'Espèces'
    elif value == 'BC':
        return 'Carte'
    elif value == 'VM':
        return 'Virement'
    elif value == 'CH':
        return 'Chèque'
    elif value == 'BO':
        return 'Bon'
    elif value == 'IN':
        return 'Internet'
    elif value == 'NO':
        return 'Transfert interne'
    else:
        raise GafcFilterException('Unknown payment method: %s'%value)
    
@register.filter 
def repeat(st, val):
    val = int(val)
    r = ''
    for i in range(val):
        r += st
        
    return r

@register.filter
def levelTxt(val):
    """
    Convert level to human-readable text
    """
    
    if val == "IN":
        return "Information"
    elif val == "WA":
        return "Attention"
    elif val == "IM":
        return "Important"
    else:
        raise GafcFilterException('Unknown level: '+val)

@register.filter
def eventVisibility(val):
    """
    Convert visibility level to human-readable text
    """
    
    if val == 0:
        return "Publique"
    elif val == 1:
        return "Intranet"
    elif val == 2:
        return "Caché"
    elif val == 3:
        return "Privé"
    else:
        raise GafcFilterException('Unknown level: '+val)

@register.filter
def eventState(value):
    """
    Convert event state key to human-readable value
    """
    if value == 'CR': return 'Créé'
    elif value == 'OP': return 'Ouvert aux inscriptions'
    elif value == 'CL': return 'Inscriptions fermées'
    elif value == 'CN': return 'Annulé'
    elif value == 'DN': return 'Effectué'
    elif value == 'VA': return 'Validé'
    elif value == 'CO': return ' Commentaire'
    else: raise GafcFilterException('Unknown state: %s'%value)

@register.filter
def eventType(value):
    """
    Convert event type key to human-readable value
    """
    if value in gucem.settings.EVENT_TYPES:
        return gucem.settings.EVENT_TYPES[value]['name']
    else: raise GafcFilterException('Unknown event type: %s'%(value))

@register.filter
def eventRole(value):
    """
    Convert event participant role key to human-readable value
    """
    if value == '0PA': return 'Participant'
    elif value == '1CO': return 'Co-encadrant'
    elif value == '2SU': return 'Encadrant'
    elif value == '3PR': return 'Professionnel'
    else: raise GafcFilterException('Unknown role: '+value)

@register.filter
def datatype(value):
    """
    Convert datatype key to human-readable value
    """
    if value == 'IN': return 'Nombre entier'
    elif value == 'ST': return 'Texte'
    elif value == 'FL': return 'Nombre décimal'
    elif value == 'BO': return 'Booléen'
    elif value == 'UN': return 'Choix unique'
    else: raise GafcFilterException('Unknown datatype: '+value)

@register.filter
def gender(value):
    """
    Convert gender key to human-readable value
    """
    
    if value == 'M': return "Homme"
    elif value == 'F': return "Femme"
    elif value == 'N': return "Non Binaire"
    elif value == 'X': return "Indéfini"
    else: raise GafcFilterException('Unknown gender: '+value)

@register.filter
def genderItem(value):
    """
    Convert gender key to human-readable value
    """
    
    if value == 'M': return mark_safe("&#9794;")
    elif value == 'F': return mark_safe("&#9792;")
    elif value == 'N': return mark_safe("&#9906;")
    else: return ""

@register.filter
def trackingtype(value):
    """
    Convert tracking type to human readable value
    """
    
    if value == 'US': return "Utilisation"
    elif value == 'PE': return "Contrôle périodique"
    elif value == 'EX': return "Contrôle exceptionnel"
    elif value == 'CO': return "Commentaire"
    else: raise GafcFilterException('Unknown tracking type: '+value)

@register.filter
def parseSuccess(val):
    if val.__class__ == str:
        return mark_safe('<div class="alert alert-success">%s</div>'%(val))
    else:
        r = ''
        try:
            for v in val:
                r += '<div class="alert alert-success">%s</div>'%(v)
        except:
            pass
        return mark_safe(r)
 
@register.filter
def parseWarning(val): 
    if val.__class__ == str:
        return mark_safe('<div class="alert alert-warning">%s</div>'%(val))
    else:
        r = ''
        try:
            for v in val:
                r += '<div class="alert alert-warning">%s</div>'%(v)
        except:
            pass
        return mark_safe(r)

@register.filter
def parseError(val):
    if val.__class__ == str:
        return mark_safe('<div class="alert alert-danger">%s</div>'%(val))
    else:
        r = ''
        try:
            for v in val:
                r += '<div class="alert alert-danger">%s</div>'%(v)
        except:
            pass
        return mark_safe(r)
    
@register.filter
def temperatureRange(value):
    if value == 0: return 'Glacée'
    elif value == 1: return 'Froide'
    elif value == 2: return 'Frisquet'
    elif value == 3: return 'Bon'
    elif value == 4: return 'Chaud'
    elif value == 5: return 'Caniculaire'
    else: raise GafcFilterException("Unknown temperature range: "+value)

@register.filter
def flowRange(value):
    if value == 0: return 'Sec'
    elif value == 1: return 'Petit filet'
    elif value == 2: return 'Débit correct'
    elif value == 3: return 'Gros débit'
    elif value == 4: return 'Très gros débit'
    elif value == 5: return 'Impraticable'
    else: raise GafcFilterException("Unknown flow range: "+value)

@register.filter
def rainRange(value):
    if value == 0: return 'Dégagé'
    elif value == 1: return 'Nuageux'
    elif value == 2: return 'Couvert'
    elif value == 3: return 'Neige'
    elif value == 4: return 'Pluie'
    elif value == 5: return 'Déluge'
    else: raise GafcFilterException("Unknown rain range: "+value)

@register.filter
def snowRange(value):
    if value == 0: return 'Fondante'
    elif value == 1: return 'Molle'
    elif value == 2: return 'Poudreuse'
    elif value == 3: return 'Souple'
    elif value == 4: return 'Dure'
    elif value == 5: return 'Congelée'
    else: raise GafcFilterException("Unknown snow range: "+value)

@register.filter
def parse_markdown(txt):
    return mark_safe(markdown.markdown(txt, extensions=['tables', LinkifyExtension(linker_options={"parse_email": True})]))

@register.inclusion_tag('gafc/bootstrap_icon.html')
def b_icon(iname, size=16):
    return { 'iname': iname,
             'size':  size }

@register.simple_tag(takes_context=True)
def abs_url(context, view_name, *args, **kwargs):
    # Could add except for KeyError, if rendering the template 
    # without a request available.
    return context['request'].build_absolute_uri(
        reverse(view_name, args=args, kwargs=kwargs)
    )



