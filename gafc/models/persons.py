# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Person related models
"""

from django.db import models
from django.contrib.auth.models import Permission

from gafc.templatetags import gafc_tags

from .user import Person
import os

gsets = None

try:
    import gucem.settings as gsets
except:
    pass

import datetime
        
class StaffEventPermission(models.Model):
    perm        = models.ForeignKey(Permission, on_delete=models.CASCADE)

class PersonComment(models.Model):
    person  = models.ForeignKey(Person, on_delete=models.CASCADE)
    who     = models.ForeignKey(Person, related_name='who', on_delete=models.CASCADE)
    date    = models.DateTimeField(auto_now_add=True)
    text    = models.TextField()
    level   = models.CharField(max_length=2, choices=[('IN', gafc_tags.levelTxt('IN')),
                                                      ('WA', gafc_tags.levelTxt('WA')),
                                                      ('IM', gafc_tags.levelTxt('IM'))], verbose_name="Niveau")
    
    class Meta:
        default_permissions = ('add','delete','view')
        
class LostPasswordRequest(models.Model):
    person  = models.OneToOneField(Person, on_delete=models.CASCADE)
    date    = models.DateTimeField(auto_now_add=True)
    token   = models.CharField(max_length=32)
    
class Federation(models.Model):
    name        = models.CharField(max_length=100, verbose_name='Nom complet')
    acronym     = models.CharField(max_length=10, verbose_name='Acronyme')
    
    class Meta:
        default_permissions = ('add','change','delete')
        
    def __str__(self):
        return str(self.acronym)
    
class License(models.Model):
    person      = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Adhérent')
    number      = models.CharField(max_length=30, verbose_name='Numéro de licence')
    federation  = models.ForeignKey(Federation, on_delete=models.PROTECT, verbose_name='Fédération')
    date_in     = models.DateField(verbose_name='Date d\'adhésion')
    date_end    = models.DateField(verbose_name='Fin de validité')
    
    def save(self, *args, **kwargs):
        if isinstance(self.date_end, datetime.datetime):
            self.date_end = self.date_end.date()

        if isinstance(self.date_in, datetime.datetime):
            self.date_in = self.date_in.date()

        if self.date_in > self.date_end:
            raise ValueError('End date is before adhesion date')
        
        return super().save(*args, **kwargs)
    
    class Meta:
        default_permissions = ('add','delete','import')
        unique_together = [['person', 'number', 'federation', 'date_in']]
        

class SympaMailingList(models.Model):
    name        = models.CharField(max_length=50, verbose_name="Nom de la liste sur le serveur Sympa", primary_key=True)
    code        = models.CharField(max_length=10, verbose_name="Code EXTRANET", null=True)
    description = models.TextField(verbose_name="Description")
    public      = models.BooleanField(verbose_name="Publique")
    subscribers = models.ManyToManyField(Person)
    
    class Meta:
        default_permissions = ('add','change','delete') 
        
    def __str__(self):
        return self.name
    
    def remotesync(self, conn):
        
        liste = conn.get(url=gsets.GAFC_SYMPA_SERVER+'/export_member/%s/light'%self.name)
        self.subscribers.set(Person.objects.filter(email__in=liste.decode('utf-8').strip().split('\n')))
        