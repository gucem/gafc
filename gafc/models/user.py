# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
User related model, restricted to be usable on other modules
"""

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.validators import validate_email

gsets = None

try:
    import gucem.settings as gsets
except:
    pass

import datetime, re

class PersonManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        # create user here
        if not email:
            raise ValueError('Users must have a valid email address')
        
        validate_email(email)
        
        if 'gender' not in kwargs:
            kwargs['gender'] = 'X'
            
        if 'first_name' not in kwargs:
            kwargs['first_name'] = 'user'

        user = self.model(
            email=self.normalize_email(email),
            **kwargs
        )

        user.set_password(password);
        user.save()
        return user
    
    def create_superuser(self, email, password=None, **kwargs):
        # create superuser here
        if not email:
            raise ValueError('Users must have a valid email address')
        
        if not password:
            raise ValueError('Super users must have a password')
        
        validate_email(email)
        
        if 'gender' not in kwargs:
            kwargs['gender'] = 'X'
            
        if 'first_name' not in kwargs:
            kwargs['first_name'] = 'superuser'

        user = self.model(
            email=self.normalize_email(email),
            **kwargs
        )

        user.is_superuser = True
        user.set_password(password)
        user.save()
        return user

class Person(AbstractBaseUser, PermissionsMixin):
    first_name  = models.CharField(max_length=50, verbose_name='Prénom')
    last_name   = models.CharField(max_length=50, verbose_name='Nom')
    gender      = models.CharField(max_length=1, choices=[('M', 'Homme'),
                                                          ('F', 'Femme'),
                                                          ('N', 'Non Binaire'),
                                                          ('X', 'Entreprise')],
                                    verbose_name='Genre')
    street      = models.CharField(max_length=250, verbose_name='Rue, numéro')
    postcode    = models.CharField(max_length=10, verbose_name='Code postal')
    city        = models.CharField(max_length=100, verbose_name='Localité')
    country     = models.CharField(max_length=100, verbose_name='Pays')
    email       = models.EmailField(default='', verbose_name='Email', unique=True)
    phone       = models.CharField(max_length=100, default='', verbose_name='Téléphone')
    
    birthdate   = models.DateField(default='1900-01-01', verbose_name='Date de naissance')
    emg_phone   = models.CharField(max_length=100, default='', verbose_name='Tél en cas d\'urgence')
    emg_contact = models.CharField(max_length=100, default='', verbose_name='Contact en cas d\'urgence')
    
    caci        = models.IntegerField(null=True, default=None, verbose_name="Certificat médical")
    
    USERNAME_FIELD='email'
    EMAIL_FIELD='email'
    REQUIRED_FIELDS=()
    
    event_for_perms = None
    
    objects = PersonManager()
    
    def __str__(self):
        return "%s %s"%(self.first_name,self.last_name)
    
    def valid_license(self, beg, fin):
        """
        Recover a license which is valid for this event
        - Same season
        - Same person
        - License starts at most the date of event begin and stops as least at the end of event
        """
        
        lic = []
        
        for l in self.license_set.all():
            if l.date_in <= beg and l.date_end >= fin:
                lic += [l,]
                
        return lic
    
    def has_club_licence(self, date):
        
        if gsets is None: return False
        
        patt = re.compile(gsets.GAFC_LICENSESCHEME)
        for l in self.license_set.filter(date_in__lte=date, date_end__gte=date):
            if patt.match(l.number):
                return True
            
        return False
    
    def caci_valid(self, date=None):
        
        if gsets is None: return False
        
        if date is None:
            date = datetime.datetime.now()
            
        if not self.has_club_licence(date):
            return True
            
        return self.caci is not None and self.caci > date.year - gsets.GAFC_CACI_DURATION
    
    @property
    def caci_tol_days(self):
        if gsets is None: return 0
        
        return gsets.GAFC_TOLERANCE_CACI - self.remaining_tol_days()
    
    
    def remaining_tol_days(self, date=None):
        
        if gsets is None: return 0
        
        if date is None:
            date = datetime.date.today()
        
        lastd = None
        
        patt = re.compile(gsets.GAFC_LICENSESCHEME)
        for l in self.license_set.filter(date_in__lte=date, date_end__gte=date):
            if patt.match(l.number):
                if lastd is None or lastd > l.date_in:
                    lastd = l.date_in
                    

        if lastd is None: # No licence found!
            return gsets.GAFC_TOLERANCE_CACI-1

        return (date - lastd).days
    
    
    def can_register_to_caci_events(self, date):
        
        if gsets is None: return False

        if self.caci_valid(date): return True

        return self.remaining_tol_days(date.date()) < gsets.GAFC_TOLERANCE_CACI
    
    @property
    def is_caci_valid(self):
        return self.caci_valid()
    
    @property
    def address(self):
        return "%s\n%s %s\n%s"%(self.street, self.postcode, self.city, self.country)
    
    def available_role(self, category, season):
        from .events import Staff
        try:
            st = Staff.objects.get(person=self, category=category, season=season)
            
            if st.valid_validations.count() == 0:
                return ('0PA',)
            else:
                if st.level == '1CO':
                    return ('0PA', '1CO')
                elif st.level == '2SU':
                    return ('0PA', '1CO', '2SU')
                elif st.level == '3PR':
                    return ('0PA', '1CO', '2SU', '3PR')                
            
        except Staff.DoesNotExist:
            return ('0PA',)  
        
    #TODO: Refactor permissions checker and write tests for it!
    def a_is_staff(self):
        return self.has_perm('gafc.staff_person') or self.is_superuser
    
    @property
    def wiki_group(self):
        
        from .gafc import Staff, Season
        season = Season.objects.get(is_default=True)
       
        return Staff.objects.exclude(level='0PA').filter(person=self, season=season).values_list('category', flat=True)
    
    def wiki_group_permission(self, category):
        
        from .gafc import Staff, Season
        season = Season.objects.get(is_default=True)
        
        try:
            st = Staff.objects.exclude(level='0PA').get(person=self, category=category, season=season)
            return True
        except Staff.DoesNotExist:
            return False        
    
    @property
    def is_staff(self):
        return self.a_is_staff()
    
    @property
    def perms(self):
        from .events import Participation
        from .persons import StaffEventPermission
        class UserPermDictChecker:
            instance = None
            def __init__(self,instance):
                self.instance = instance
                
            def __getitem__(self, key):
                if self.instance.has_perm('gafc.%s'%(key)):
                    return True
                elif self.instance.event_for_perms is not None:
                    try:
                        p = Participation.objects.get(person=self.instance, event=self.instance.event_for_perms)
                        
                        if p.role != '0PA':
                            try:
                                StaffEventPermission.objects.get(perm__content_type__app_label='gafc', perm__codename=key)
                                return True
                            except StaffEventPermission.DoesNotExist:
                                pass
                        
                    except Participation.DoesNotExist:
                        pass
                    
                return False
            
        return UserPermDictChecker(self)
    
    @property
    def fullperms(self):
        class FullPermChecker:
            instance = None
            def __init__(self, instance):
                self.instance = instance
                
            def __getitem__(self, key):
                class SubPermChecker:
                    instance = None
                    applabel = None
                    
                    def __init__(self, instance, applabel):
                        self.instance = instance
                        self.applabel = applabel
                        
                    def __getitem__(self, key):
                        return self.instance.has_perm('%s.%s'%(self.applabel, key))
                    
                return SubPermChecker(self.instance, key)
        return FullPermChecker(self)
    
    @property
    def global_perms(self):
        class UserPermDictChecker:
            instance = None
            def __init__(self,instance):
                self.instance = instance
                
            def __getitem__(self, key):
                return self.instance.has_perm('gafc.%s'%(key))
            
        return UserPermDictChecker(self)
    
    @property
    def can_view_permission(self):
        return self.has_perm('auth.view_permission')
    
    class Meta:
        default_permissions = ('add','change','delete','view','staff','cant_login', 'import','merge')
