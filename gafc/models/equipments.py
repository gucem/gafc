# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Equipment models
"""

from django.db import models
from django.utils import timezone


from .gafc import Category
from .financials import Cashing
from .events import Event
from .persons import Person
from gafc.templatetags import gafc_tags
import gucem.settings as settings



class EquipmentGroup(models.Model):
    name        = models.CharField(max_length=100, verbose_name='Nom')
    description = models.TextField(verbose_name='Description')
    parent      = models.ForeignKey('EquipmentGroup', null=True, default=None, verbose_name="Parent", on_delete=models.SET_NULL)
    
    class Meta:
        default_permissions = ('add','change','delete','view')
        
    class EquipmentGroupTree(object):
        
        def __init__(self, obj=None):
            if obj is not None and type(obj) is not EquipmentGroup:
                raise ValueError('This is an EquipmentGroup Tree!')
                
            self.__sub = []
            self.__obj = obj
            
        @property
        def obj(self):
            return self.__obj
        
        @property
        def sub(self):
            return self.__sub
        
        def addSub(self, sub):
            if type(sub) is not self.__class__:
                raise ValueError('This is an EquipmentGroup Tree!')
            self.__sub += [sub,]
        
    def __str__(self):
        return self.name
    
    @property
    def equipments(self):
        return self.equipment_set.filter(belong_to=self.id).order_by('date_scrap', 'category_id')
    
    @classmethod
    def getTree(cls, eg_id=None, cashing_filter=False, cashing=None):
        nodes = dict()        
        
        nodes[None] = cls.EquipmentGroupTree()
        
        eqs = Equipment.objects.order_by('date_scrap','category_id')
        if cashing_filter:
            eqs = eqs.filter(cashing=cashing) 
        
        for t in cls.objects.prefetch_related(models.Prefetch('equipment_set', queryset=eqs)).order_by('parent_id').all():
            nodes[t.id] = cls.EquipmentGroupTree(t)
            
        for k,t in nodes.items():
            if k is not None:
                nodes[t.obj.parent_id].addSub(t)
                
        return nodes[eg_id]
    
    @classmethod
    def tree2list(cls, tree, level=-1, withEquipments=False):
        
        ret = [{'label': '%s %s'%('...'*level,tree.obj.name if tree.obj is not None else ''), 'obj': tree.obj, 'type': 'group'}]
        
        if withEquipments and tree.obj is not None:
            for e in tree.obj.equipment_set.all():
                ret += [{'label': '%s %s'%('   '*(level+1), e.name), 'obj': e, 'type': 'equipment'}]
                
        for t in tree.sub:
            ret += cls.tree2list(t, level+1, withEquipments)
            
        return ret
        
    
class Equipment(models.Model):
    name        = models.CharField(max_length=100, verbose_name='Nom')
    reference   = models.CharField(max_length=30, verbose_name='Numéro de série')
    description = models.TextField(verbose_name='Description')
    category    = models.ForeignKey(Category, on_delete=models.PROTECT, verbose_name='Catégorie')
    belong_to   = models.ForeignKey(EquipmentGroup, on_delete=models.PROTECT, verbose_name='Groupe')
    parent      = models.ForeignKey('Equipment', on_delete=models.PROTECT, verbose_name="Parent", null=True, default=None)
    reserve     = models.BooleanField(default=False, verbose_name="Réservation nécessaire")
    date_buy    = models.DateField(verbose_name='Date d\'achat')
    date_service= models.DateField(verbose_name='Mise en service', null=True, default=None)
    date_eol    = models.DateField(verbose_name='Fin de vie', null=True, default=None)
    date_scrap  = models.DateField(verbose_name='Date de mise au rebus', null=True, default=None)
    cashing     = models.ForeignKey(Cashing, on_delete=models.PROTECT, null=True, default=None)
    
    def __str__(self):
        return self.name
        
    def save(self, *args, **kwargs):
        if self.pk is not None:
            eq = Equipment.objects.get(pk=self.pk)
            if eq.date_scrap is not None:
                raise ValueError("Can't change equipment after scrapping")
                
        if self.parent is not None:
            if self.parent.date_scrap is None:
                raise ValueError("Parent must be a scrap")
        
        super(Equipment, self).save(*args, **kwargs)
    
    class Meta:
        default_permissions = ('add','view','change', 'scrap')
        
    @property
    def metadata_fields(self):
        return EquipmentMeta.objects.filter(equipment=self).order_by('id')
    
    def isAvailable(self, begin, finish, ignoreevent=None):
        if not self.reserve:
            return True, []
        
        if self.date_scrap is not None:
            return False, []
    
        tracks = EquipmentTracking.objects.select_related('event').filter(equipment=self).exclude(event__begin__gte=finish).exclude(event__finish__lte=begin).exclude(event=None)
        
        if ignoreevent is not None:
            tracks = tracks.exclude(event=ignoreevent)
        
        ret = []
        for t in tracks:
            if not t.event.is_cancelled:
                ret = [t.event,]
        
        return len(ret) == 0, ret
    
class EquipmentPicture(models.Model):
    equipment   = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    filename    = models.CharField(max_length=250, verbose_name="Filename")
    
    @property
    def disk_filename(self):
        ext = self.filename.split('.')
        if len(ext) == 1:
            ext = ''
        else:
            ext = '.'+ext[-1]
            
        return '%06i%s'%(self.pk, ext)
    
    @property
    def disk_fullpath(self):
        return settings.GAFC_FILEFOLDER+'/equipments/'
    
    @property
    def disk_fullfilepath(self):
        return '%s/%s'%(self.disk_fullpath, self.disk_filename)
        
    
class EquipmentTracking(models.Model):
    equipment   = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    event       = models.ForeignKey(Event, on_delete=models.SET_NULL, null=True)
    trackingtype= models.CharField(max_length=2,choices=[('US', gafc_tags.trackingtype('US')),
                                                         ('PE', gafc_tags.trackingtype('PE')),
                                                         ('EX', gafc_tags.trackingtype('EX')),
                                                         ('CO', gafc_tags.trackingtype('CO')),],  
                                    verbose_name='Type')
    comment     = models.TextField()
    date        = models.DateField(default=timezone.now, verbose_name="Date")
    person      = models.ForeignKey(Person, on_delete=models.PROTECT, verbose_name='Utilisateur')
    
    def save(self, *args, **kwargs):
        if self.event is not None and self.pk is None and self.event.current_state == 'VA':
            if not ('force_event' in kwargs and kwargs['force_event']):
                raise ValueError('Can\'t add an equipment to validated event')
        
        if self.equipment.date_scrap is not None:
            raise ValueError('Can\'t add tracking to scrap equipment')
            
        super(EquipmentTracking, self).save(*args, **kwargs)
    
    class Meta:
        default_permissions = ('add','change','delete')
        
    @property
    def metadata(self):
        EMD = []
        
        for em in self.equipment.equipmentmeta_set.all().order_by('id'):
            d = em.equipmentmetadata_set.filter(tracking=self)
            
            if len(d) == 0:
                EMD += [(em.name, em.datatype, ''),]
            else:
                EMD += [(em.name, em.datatype, d[0].data)]
                
        return EMD
    
class EquipmentMeta(models.Model):
    equipment   = models.ForeignKey(Equipment, on_delete=models.CASCADE, verbose_name='Équipement')
    mandatory   = models.BooleanField(default=False, verbose_name='Obligatoire')
    name        = models.CharField(max_length=30, verbose_name='Nom')
    datatype    = models.CharField(max_length=2,choices=[('ST', gafc_tags.datatype('ST')),
                                                         ('BO', gafc_tags.datatype('BO')),
                                                         ('IN', gafc_tags.datatype('IN')),
                                                         ('FL', gafc_tags.datatype('FL'))],verbose_name='Type')
    class Meta:
        unique_together = (('name','equipment'))
        default_permissions = ()
        
    def __str__(self):
        return '%s'%self.name
    
class EquipmentMetaData(models.Model):
    tracking    = models.ForeignKey(EquipmentTracking, on_delete=models.CASCADE)
    meta        = models.ForeignKey(EquipmentMeta, on_delete=models.CASCADE)
    data        = models.CharField(max_length=250, null=True)
    
    def save(self, *args, **kwargs):
        if self.tracking.equipment != self.meta.equipment:
            raise ValueError('Tracking and Meta Equipment are not the same')
        
        super(EquipmentMetaData, self).save(*args, **kwargs)
    
    class Meta:
        unique_together = (('tracking', 'meta'))
        default_permissions = ()
