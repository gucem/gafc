# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Generic models
"""


from django.db import models
from .user import Person
#import gucem.settings

class Season(models.Model):
    name        = models.CharField(max_length=30, verbose_name='Saison', unique=True)
    is_default  = models.BooleanField(default=False, verbose_name='Default')
    
    def save(self, *args, **kwargs): # Ensure one defaut Season
        try:
            temp = Season.objects.get(is_default=True)
            if self.is_default and self != temp:
                temp.is_default = False
                super(Season, temp).save()
        except Season.DoesNotExist:
            self.is_default = True;
            
        super(Season, self).save(*args, **kwargs)
        
    class Meta:
        default_permissions = ('add','view','change','delete')
        
    def __str__(self):
        return str(self.name)
    
    
class StaffEmail(models.Model):
    email       = models.EmailField(unique=True)
    authorized  = models.ManyToManyField('Person')
    noauth      = models.BooleanField(default=False, verbose_name='Adresse "noreply"')
    
    class Meta:
        default_permissions = ('add', 'view', 'change', 'delete', 'send_noauth')
        
    def __str__(self):
        return self.email
    
class StaffEmailAlias(models.Model):
    email       = models.EmailField()
    main        = models.ForeignKey(StaffEmail, on_delete=models.CASCADE)
    
    
class EmailModel(models.Model):
    title       = models.CharField(max_length=250, verbose_name="Titre")
    content     = models.TextField(verbose_name="Contenu")
    
    class Meta:
        default_permissions = ('add', 'view', 'change', 'delete')
        
class FinancialCategoryTree(models.Model):
    name        = models.CharField(max_length=100, verbose_name='Nom')
    toplevel    = models.ForeignKey('FinancialCategoryTree', null=True, on_delete=models.PROTECT, verbose_name="Parent")
    
    def __str__(self):
        return self.name
    
class FinancialCategory(models.Model):
    tree        = models.ForeignKey(FinancialCategoryTree, on_delete=models.PROTECT, verbose_name="Parent")
    name        = models.CharField(max_length=100, verbose_name='Nom')
    
    def __str__(self):
        return self.name
    
    class Meta:
        default_permissions = ('add', 'change', 'delete', 'view', 'balance')        


class Category(models.Model):
    name        = models.CharField(max_length=30, verbose_name='Catégorie')
    is_global   = models.BooleanField(verbose_name='Global', default=False)
    allowed_fincat = models.ManyToManyField('FinancialCategory', related_name='allowed_categories')
    default_fincat = models.ForeignKey('FinancialCategory', null=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        return self.name
    
    class Meta:
        default_permissions = ('add','change','delete', 'view', 'financial')
        
    @property
    def finids(self):
        return [ f.id for f in self.allowed_fincat.all() ]
    

class Staff(models.Model):
    person      = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Encadrant')
    category    = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Catégorie')
    season      = models.ForeignKey(Season, on_delete=models.CASCADE, verbose_name='Saison')
    level       = models.CharField(max_length=3,  choices=[('1CO', 'Co-encadrant'),
                                                           ('2SU', 'Encadrant'),
                                                           ('3PR', 'Professionnel')],)
    
    class Meta:
        unique_together = (('person','category','season'))
        default_permissions = ('add','change','delete')
        
    @property
    def valid_validations(self):
        r = []
        for v in self.validation_set.all():
            if v.date_end is None:
                r += [v,]
        return r
    
    def save(self, *args, **kwargs):
        if self.pk is not None and len(self.valid_validations) > 0:
            raise ValueError('Can\'t change staff subscription after validation')
        
        return super().save(*args, **kwargs)
