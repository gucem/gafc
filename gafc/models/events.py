# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Event related models
"""

from django.db import models
from django.db.models import Q
from django.db.models import Sum

from .persons import Person
from .gafc import Category, Season, Staff
from gafc.templatetags import gafc_tags

import gucem.settings

import unicodedata, math, re
        
class Validation(models.Model):
    staff       = models.ForeignKey(Staff, on_delete=models.CASCADE, verbose_name='Encadrant')
    validator   = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Validateur')
    date        = models.DateField(auto_now_add=True, verbose_name='Date de validation')
    date_end    = models.DateField(null=True, default=None, verbose_name='Date de retrait')
    
    class Meta:
        default_permissions = ('add',)

class Event(models.Model):
    
    begin       = models.DateTimeField(verbose_name='Début')
    finish      = models.DateTimeField(verbose_name='Fin')
    title       = models.CharField(max_length=50, verbose_name='Titre')
    description = models.TextField(null=True, default='', verbose_name='Description')
    eventtype   = models.CharField(max_length=2, choices=[(t, gafc_tags.eventType(t)) for t in gucem.settings.EVENT_TYPES ],
                                  verbose_name="Type")
    place       = models.CharField(max_length=250, verbose_name='Lieu')
    link        = models.BooleanField(default=False, verbose_name='Formulaire d\'inscription')
    onlyvalid   = models.BooleanField(default=False, verbose_name='Licence valide uniquement')
    category    = models.ForeignKey(Category,on_delete=models.PROTECT, verbose_name='Catégorie')
    cost        = models.DecimalField(decimal_places = 2, max_digits = 10, default=0., verbose_name='Coût')
    season      = models.ForeignKey(Season, on_delete=models.PROTECT, verbose_name='Saison', null=True)
    helloassoLink = models.URLField(max_length=500, default="", verbose_name="lien de paiement Helloasso")
    visibility  = models.PositiveIntegerField(default=0, choices=[(0, gafc_tags.eventVisibility(0)),
                                                                  (1, gafc_tags.eventVisibility(1)),
                                                                  (2, gafc_tags.eventVisibility(2)),
                                                                  (3,gafc_tags.eventVisibility(3))], verbose_name="Visibilité")
    public_after_va = models.BooleanField(default=False, verbose_name='Rendre publique après validation')
    
    def save(self, *args, **kwargs):
        
        if self.begin > self.finish:
            raise ValueError('Beginning must be before Finish')
        
        if self.season is None and self.eventtype != "AU":
            raise ValueError('Only AU type can have season=null')
        
        if self.pk is None:
            super(Event, self).save(*args, **kwargs)            
        else:
            if self.current_state == 'VA':
                raise ValueError('Event can\'t be changed after validation')
            
            super(Event, self).save(*args, **kwargs)
        
    @property
    def current_state(self):
        return self.a_current_state()
        
    def a_current_state(self):
#        st = EventState.objects.filter(Q(event=self) & ~Q(state='CO')).order_by('-date').values('state')
#        
#        if len(st) == 0:
#            return 'CR'
#        else:
#            return st[0]['state']
        
        date = None
        state = "CR"
        
        for s in self.eventstate_set.all():
            if s.state != 'CO':
                if date is None or s.date > date:
                    date = s.date
                    state = s.state
        
        return state
        
    @property
    def created(self):
        try:
            return EventState.objects.filter(Q(event=self) & Q(state='CR')).order_by('-date')[0].date
        except IndexError:
            return self.begin
        
    @property
    def escaped_description(self):
        return self.description.replace('\n','\\n')
    
    @property
    def can_delete(self):
        return self.current_state != 'VA' and self.payment_set.count() == 0 and self.internalpayment_set.count() == 0 and self.participation_set.count() == 0
        
    @property
    def is_cancelled(self):
#        try:
#            EventState.objects.get(event=self, state='CN')
#        except EventState.DoesNotExist:
#            return False;
#        except EventState.MultipleObjectsReturned:
#            pass
        
        for s in self.eventstate_set.all():
            if s.state == 'CN': return True
        
        return False
    
    @property
    def formSlug(self):
        if self.helloassoLink is None:
            return ''
        elif self.helloassoLink.count('/') == 0:
            return self.helloassoLink
        else:
            return self.helloassoLink.split('/')[-1]
        
    @property
    def metadata_fields(self):
        return EventMetaData.objects.filter(event=self).order_by('id')
    
    @staticmethod
    def calc_duration(begin, finish):
        """
        1 day is 12h of staffing
        Count per half-day
        """
        d = finish - begin        
        hours = min(math.ceil(d.seconds/3600),12) + 12*d.days
        return math.ceil(hours/6)/2
    
    @property
    def duration(self):
        return Event.calc_duration(self.begin, self.finish)
    
    @property
    def real_duration(self):
        return math.ceil((self.finish - self.begin).seconds/1800)/2.
    
    @property
    def p_count(self):
        if self.eventtype == 'AU':
            return self.eventreport.nb_part if hasattr(self, 'eventreport') else 0
        else:
#            return Participation.objects.filter(waiting=False, event=self, role='0PA').count()
            c = 0
            for p in self.participation_set.all():
                if not p.waiting and p.role == '0PA':
                    c += 1
                    
            return c
    
    @property
    def s_count(self):
#        return Participation.objects.filter(waiting=False, event=self).exclude(role='0PA').count()
        c = 0
        for p in self.participation_set.all():
            if not p.waiting and p.role != '0PA':
                c += 1
                
        return c
    
    @property
    def s_done_count(self):
#        return Participation.objects.filter(event_done=True, event=self).exclude(role='0PA').count()
        c = 0
        for p in self.participation_set.all():
            if p.event_done and p.role != '0PA':
                c += 1
                
        return c
    
    @property
    def p_done_count(self):
#        return Participation.objects.filter(event_done=True, event=self, role='0PA').count()
        c = 0
        for p in self.participation_set.all():
            if p.event_done and p.role == '0PA':
                c += 1
                
        return c
    
    @property
    def p_waiting_count(self):
#        return Participation.objects.filter(waiting=True, event_done=False, event=self, role='0PA').count()
        c = 0
        for p in self.participation_set.all():
            if p.waiting and not p.event_done and p.role == '0PA' and not p.cancelled:
                c += 1
                
        return c
    
    @property
    def p_accepted_count(self):
#        return Participation.objects.filter((Q(waiting=False) | Q(event_done=True)) & Q(event=self) & Q(role='0PA')).count()
        c = 0
        for p in self.participation_set.all():
            if (not p.waiting or p.event_done) and p.role == '0PA' and not p.cancelled:
                c += 1
                
        return c
        
    
    class Meta:
        default_permissions = ('add', 'change', 'delete', 'view')
    
class EventState(models.Model):    
    event       = models.ForeignKey(Event, on_delete=models.CASCADE)
    state       = models.CharField(max_length=2 , choices=[('CR', gafc_tags.eventState('CR')),
                                                           ('OP', gafc_tags.eventState('OP')),
                                                           ('CL', gafc_tags.eventState('CL')),
                                                           ('CN', gafc_tags.eventState('CN')),
                                                           ('DN', gafc_tags.eventState('DN')),
                                                           ('VA', gafc_tags.eventState('VA')),
                                                           ('CO', gafc_tags.eventState('CO'))], verbose_name='État')
    comment     = models.TextField(null=True, verbose_name='Commentaire')
    person      = models.ForeignKey(Person, on_delete=models.PROTECT)
    date        = models.DateTimeField(auto_now_add=True, verbose_name="Date")
    
    def save(self, *args, **kwargs):
        
        if self.event.current_state == 'VA':
            raise ValueError('Event can\'t be changed after validation')
        
        if self.state == 'VA' and not self.event.is_cancelled:
            
            if self.event.public_after_va:
                self.event.visibility = 0
                self.event.save()
            
            from .equipments import EquipmentTracking, EquipmentMetaData
            eqs = EquipmentTracking.objects.filter(event=self.event)
            
            for eq in eqs:
                metas = eq.equipment.equipmentmeta_set.all()
                for m in metas: 
                    if m.mandatory:
                        try:
                            mds = m.equipmentmetadata_set.get(tracking=eq)
                        except EquipmentMetaData.DoesNotExist:
                            mds = None
                            
                        if mds is None or mds.data is None or mds.data == '':
                            raise ValueError('%s: Field %s is mandatory before validation'%(eq.equipment.name, m.name))
                        
            if self.event.place.strip() == "":
                raise ValueError('Vous devez définir le lieu pour valider l\'événement')
            
        super().save(*args, **kwargs)
    
    class Meta:
        default_permissions = ('add', 'delete')
        
class EventReport(models.Model):
    event       = models.OneToOneField(Event, on_delete=models.CASCADE, primary_key=True)
    owner       = models.ForeignKey(Person, on_delete=models.CASCADE, null=True)
    equipment_used  = models.TextField(verbose_name="Équipement utilisé")
    general_comment = models.TextField(verbose_name="Bilan de la sortie")
    nb_part     = models.IntegerField(null=True, verbose_name="Nombre de participants")
    rain        = models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.rainRange(0)),
                                            (1, gafc_tags.rainRange(1)),
                                            (2, gafc_tags.rainRange(2)),
                                            (3, gafc_tags.rainRange(3)),
                                            (4, gafc_tags.rainRange(4)),
                                            (5, gafc_tags.rainRange(5)),], verbose_name="Pluie")
    flow        = models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.flowRange(0)),
                                            (1, gafc_tags.flowRange(1)),
                                            (2, gafc_tags.flowRange(2)),
                                            (3, gafc_tags.flowRange(3)),
                                            (4, gafc_tags.flowRange(4)),
                                            (5, gafc_tags.flowRange(5)),], verbose_name="Débit")
    snow_quality= models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.snowRange(0)),
                                            (1, gafc_tags.snowRange(1)),
                                            (2, gafc_tags.snowRange(2)),
                                            (3, gafc_tags.snowRange(3)),
                                            (4, gafc_tags.snowRange(4)),
                                            (5, gafc_tags.snowRange(5)),], verbose_name="Qualité de la neige")
    T_int       = models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.temperatureRange(0)),
                                            (1, gafc_tags.temperatureRange(1)),
                                            (2, gafc_tags.temperatureRange(2)),
                                            (3, gafc_tags.temperatureRange(3)),
                                            (4, gafc_tags.temperatureRange(4)),
                                            (5, gafc_tags.temperatureRange(5)),], verbose_name="Température intérieure")
    T_ext       = models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.temperatureRange(0)),
                                            (1, gafc_tags.temperatureRange(1)),
                                            (2, gafc_tags.temperatureRange(2)),
                                            (3, gafc_tags.temperatureRange(3)),
                                            (4, gafc_tags.temperatureRange(4)),
                                            (5, gafc_tags.temperatureRange(5)),], verbose_name="Température extérieure")
    T_water     = models.IntegerField(null=True, default=None, 
                                   choices=[(None, '---'),
                                            (0, gafc_tags.temperatureRange(0)),
                                            (1, gafc_tags.temperatureRange(1)),
                                            (2, gafc_tags.temperatureRange(2)),
                                            (3, gafc_tags.temperatureRange(3)),
                                            (4, gafc_tags.temperatureRange(4)),
                                            (5, gafc_tags.temperatureRange(5)),], verbose_name="Température de l'eau")
    class Meta:
        default_permissions = ('add', 'delete')
        
class Participation(models.Model): 
    person      = models.ForeignKey(Person, on_delete=models.PROTECT)
    event       = models.ForeignKey(Event, on_delete=models.CASCADE)
    role        = models.CharField(max_length=3, default='0PA',
                                                  choices=[('0PA', gafc_tags.eventRole('0PA')),
                                                           ('1CO', gafc_tags.eventRole('1CO')),
                                                           ('2SU', gafc_tags.eventRole('2SU')),
                                                           ('3PR', gafc_tags.eventRole('3PR'))],)
    event_done  = models.BooleanField(default=False)
    waiting     = models.BooleanField(default=False)
    cancelled   = models.BooleanField(default=False)
    
    class Meta:
        unique_together = (('person','event')) # Can only participate one time to event
        default_permissions = ('add', 'change', 'delete')
        
    def save(self, *args, **kwargs):
        if self.event.current_state == 'VA':
            raise ValueError('Event can\'t be changed after validation')
        
        isnew = self.pk is None
        eventcopy = None
        
        if 'event_copy' in kwargs:
            eventcopy = kwargs['event_copy']
            del kwargs['event_copy']
        
        if not isnew or self.event.eventtype == 'AU' or self.event.current_state == 'OP' or eventcopy is not None:
            super(Participation, self).save(*args, **kwargs)
        else:
            raise ValueError('Event is not open to new participant')  
            
            
        if isnew:
            metas = EventMetaData.objects.filter(event=self.event)
            
            for md in metas:
                try:
                    if eventcopy is None:
                        last_meta = ParticipationMetaData.objects.filter(eventMeta__meta=md.meta,participation__person = self.person).order_by('-participation__event__begin')[0]
                    else:
                        last_meta = ParticipationMetaData.objects.get(eventMeta__meta=md.meta,participation__person = self.person, participation__event=eventcopy)
                    
                    data = last_meta.data
                    
                    if md.meta.datatype == "UN" and data not in md.available_choices_keys:
                        data = ''
                    
                    ParticipationMetaData.objects.create(eventMeta=md, participation=self, data=data)
                except IndexError:
                    pass
                except ParticipationMetaData.DoesNotExist:
                    pass
    
    @property
    def metadata(self):
        PMD = []
        
        for em in self.event.eventmetadata_set.all().order_by('id'):
            try:
                d = ParticipationMetaData.objects.get(eventMeta=em, participation=self)
                PMD += [(em.meta.nrmname, em.meta.datatype, d.data)]
            except ParticipationMetaData.DoesNotExist:
                PMD += [(em.meta.nrmname, em.meta.datatype, ''),]
                
        return PMD
    
    @property
    def total_cost(self):
        tot = float(self.event.cost)
        
        for d in self.participationmetadata_set.all():
            if d.eventMeta.meta.datatype == 'BO' and d.data == 'True':
                tot += float(d.eventMeta.cost)
            elif d.eventMeta.meta.datatype in ['IN','FL']:
                tot += float(d.data) * float(d.eventMeta.cost)
        
        return tot
    
    @property
    def metadata_txt(self):
        PMD = dict()
        
        for em in self.event.eventmetadata_set.all():
            if em.meta.datatype in ('IN','FL'):
                PMD[em.id] = [em.meta.name, em.meta.datatype, '0', em.cost]
            elif em.meta.datatype == 'BO':
                PMD[em.id] = [em.meta.name, em.meta.datatype, 'NON', em.cost]
            else:
                PMD[em.id] = [em.meta.name, em.meta.datatype, '', em.cost]
            
        
        for d in self.participationmetadata_set.all():
            if d.eventMeta_id in PMD:
                val = PMD[d.eventMeta_id][2]
                if PMD[d.eventMeta_id][1] == 'BO':
                    val = 'OUI' if d.data == 'True' else 'NON'
                elif PMD[d.eventMeta_id][1] == 'UN':
                    if d.data not in d.eventMeta.available_choices_keys:
                        val = ''
                    else:
                        val = d.eventMeta.available_choices_values[d.eventMeta.available_choices_keys.index(d.data)]
                else:
                    val = d.data
            
                PMD[d.eventMeta_id][2] = val

        return [v for k,v in PMD.items()]

    @property
    def person_comments(self):
        return self.person.personcomment_set.all()
    
    @property
    def valid_license(self):        
        return self.person.valid_license(self.event.begin.date(), self.event.finish.date())
    
    @property
    def pay_done(self):      
        from .financials import Payment
        vals = dict(Payment.objects.filter(event=self.event, person=self.person,amount__gt=0., category=self.event.category, refund_date=None).aggregate(amount_sum=Sum('amount')))['amount_sum']
        
        if vals is None:
            return 0.
        else:
            return float(vals)
    
    @property
    def badge_class(self):
        c = self.person_comments
        
        if c.count() == 0:
            return 'd-none'
        elif c.filter(level="IM").count() > 0:
            return 'badge-danger'
        elif c.filter(level="WA").count() > 0:
            return 'badge-warning'
        else:
            return 'badge-info'
    
    @property
    def has_valid_license(self):
        return len(self.valid_license) > 0
    
    @property
    def has_club_license(self):
        patt = re.compile(gucem.settings.GAFC_LICENSESCHEME)
        
        lics = self.valid_license
        
        if lics is None: return False 
        
        for l in self.valid_license:
            if patt.match(l.number):
                return True
            
        return False
    
    @property
    def caci_valid(self):
        return self.person.caci_valid(self.event.begin)
    
    @property
    def available_roles(self):
        """
        Get the roles the person is validated for this event
        """
        for st in self.person.staff_set.all():
            if st.category_id == self.event.category_id and st.season_id == self.event.season_id:
                vl = st.validation_set.filter(date__lte=self.event.begin.date()).filter(
                                               Q(date_end__gte=self.event.finish.date()) | Q(date_end=None))
                
                if vl.count() == 0:
                    return ('0PA',)
                else:
                    if st.level == '1CO':
                        return ('0PA', '1CO')
                    elif st.level == '2SU':
                        return ('0PA', '1CO', '2SU')
                    elif st.level == '3PR':
                        return ('0PA', '1CO', '2SU', '3PR')
        return ('0PA',)
        
    @property
    def is_role_valid(self):
        """
        Check if role is compatible with staff list
        """
        return self.role in self.available_roles
    
class EventMeta(models.Model):
    name        = models.CharField(max_length=30, verbose_name='Nom')
    datatype    = models.CharField(max_length=2,choices=[('ST', gafc_tags.datatype('ST')),
                                                         ('BO', gafc_tags.datatype('BO')),
                                                         ('IN', gafc_tags.datatype('IN')),
                                                         ('FL', gafc_tags.datatype('FL')),
                                                         ('UN', gafc_tags.datatype('UN'))],verbose_name='Type')
    
    def __str__(self):
        return "%s"%self.name
    
    class Meta:
        default_permissions = ()
        unique_together = (('name','datatype'))
    
    @property
    def last_emd(self):
        try:
            return self.eventmetadata_set.all()[0]
        except IndexError:
            return None
        
    @property
    def nrmname(self):
        nfkd_form = unicodedata.normalize('NFKD', self.name.strip())
        nname = nfkd_form.encode('ASCII', 'ignore').lower().decode().replace(' ','_')
        return '%s_%i'%(nname, self.id)
    
class MetaModel(models.Model):
    name        = models.CharField(max_length=30, verbose_name='Nom', primary_key=True)
    
class EventMetaModel(models.Model):
    meta        = models.ForeignKey(EventMeta, on_delete=models.PROTECT)
    model       = models.ForeignKey(MetaModel, on_delete=models.CASCADE)
    cost        = models.DecimalField(decimal_places=2, max_digits=10)
    description = models.TextField(verbose_name="Description")
    availables  = models.TextField(verbose_name="Valeurs")    
    
    class Meta:
        unique_together=(('meta','model')) # Only one meta type for event
        default_permissions = ()
    
class EventMetaData(models.Model):
    meta        = models.ForeignKey(EventMeta, on_delete=models.PROTECT)
    event       = models.ForeignKey(Event, on_delete=models.CASCADE)
    cost        = models.DecimalField(decimal_places=2, max_digits=10)
    description = models.TextField(verbose_name="Description")
    availables  = models.TextField(verbose_name="Valeurs")
    
    class Meta:
        unique_together=(('meta','event')) # Only one meta type for event
        default_permissions = ()
        
    def save(self, *args, **kwargs):
        
        if self.meta.datatype == 'UN':
            pms = ParticipationMetaData.objects.filter(eventMeta=self)
            keys = self.available_choices_keys
            for p in pms:
                if p.data not in keys:
                    p.data = ''
                    p.save()
        
        super().save(*args, **kwargs)
        
    def choice_valid(self, c):
        return c in self.available_choices_keys
        
    @property
    def available_choices_keys(self):
        if self.availables == '':
            return []
        
        lst = self.availables.split(',')
        lstn = []
        ret = []
        
        for e in lst:
            if e.strip() == '':
                continue
            
            n = self.__normalize(e)
            
            i=0
            while('%s_%i'%(n,i) in lstn):
                i+=1
                
            n = '%s_%i'%(n,i)
            lstn += [n,]
            
        return lstn
    
    @property
    def available_choices_values(self):
        if self.availables == '':
            return []
        
        ret = []
        for e in self.availables.split(','):
            n = e.strip()
            
            if n != '':
                ret += [n,]
        
        return ret
        
    @property
    def available_choices(self):
        return (('','---'),*tuple(zip(self.available_choices_keys, self.available_choices_values)))
    
    def __normalize(self, name):
        nfkd_form = unicodedata.normalize('NFKD', name.lower().strip())
        return re.sub("[^0-9a-z]+",'_',nfkd_form.encode('ASCII', 'ignore').decode())
    
class ParticipationMetaData(models.Model):
    eventMeta   = models.ForeignKey(EventMetaData, on_delete=models.CASCADE)
    participation= models.ForeignKey(Participation, on_delete=models.CASCADE)
    data        = models.CharField(max_length=250)
    
    class Meta:
        unique_together = (('eventMeta','participation')) # Only one metadata type per participation
        default_permissions = ()
    
    def save(self, *args, **kwargs):
        if self.eventMeta.event != self.participation.event:
            raise ValueError('Event in participation and metadata is not the same')
        
        super(ParticipationMetaData, self).save(*args, **kwargs)
