# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Financials related models
"""

from django.db import models

from .persons import Person
from .gafc import Season, Category, FinancialCategory
from .events import Event
from gafc.templatetags import gafc_tags
import gucem.settings as settings

import os

class Cashing(models.Model):
    person      = models.ForeignKey(Person, null=True, default=None, on_delete=models.PROTECT, verbose_name="Encaisseur")
    person_ask  = models.ForeignKey(Person, related_name='cashing_ask', on_delete=models.PROTECT, verbose_name="Demandeur")
    date        = models.DateField(auto_now_add=True, verbose_name="Date")
    date_done   = models.DateField(null=True, default=None, verbose_name="Date de réalisation")
    comment     = models.TextField(verbose_name="Commentaire")
    abandon     = models.BooleanField(verbose_name="Abandon de frais", default=False)
    amount      = models.DecimalField(decimal_places=2, max_digits=10, null=True, default=None, verbose_name="Montant")
    
    iban        = models.CharField(null=True, default=None, max_length=34, verbose_name="IBAN")
    bic         = models.CharField(null=True, default=None, max_length=11, verbose_name="BIC")
    
    rejected    = models.BooleanField(default=False, verbose_name="Rejetée")
    rejected_co = models.TextField(default='', verbose_name="Raison rejet")
    
#    season      = models.ForeignKey(Season, on_delete=models.PROTECT, verbose_name='Saison')
    
    class Meta:
        default_permissions = ('add', 'change', 'view', 'validate', 'delete')
        
    def save(self, force=False, *args, **kwargs):
        if not force:
            if self.person is not None and self.abandon:
                if self.total_amount != 0.:
                    raise ValueError('Abandon de frais: la somme des paiements associés doit être nulle.')
                    
                try:
                    CashingAbandonValidator.objects.get(person=self.person)
                except:
                    raise ValueError("%s n'est pas autorisé à valider des abandons de frais"%self.person)
                    
            if self.rejected:
                if self.payment_set.count() > 0:
                    raise ValueError('Pour rejeter une note de frais, aucun paiement ne doit lui être associé.')
        
        super().save(*args, **kwargs)
        
    @property
    def total_amount(self):
        t = self.payment_set.aggregate(total=models.Sum('amount'))
        return float(t['total']) if t['total'] is not None else 0.
    
    @property
    def total_neg_amount(self):
        t = self.payment_set.filter(amount__lte=0).aggregate(total=models.Sum('amount'))
        return -float(t['total']) if t['total'] is not None else 0.

class CashingAbandonValidator(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE, verbose_name="Validator")
    titre  = models.CharField(max_length=250, verbose_name="Titre")
    
    def __str__(self):
        return '%s %s'%(self.person.first_name, self.person.last_name)

    @property
    def has_image(self):
        sign_img = settings.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%self.person_id
        
        return os.path.isfile(sign_img)

class CashingFile(models.Model):
    filename    = models.CharField(max_length=250, verbose_name="Filename")
    cashing     = models.ForeignKey(Cashing, on_delete=models.CASCADE, verbose_name="Encaissement")
    autogenerate= models.BooleanField(default=False)
    
    @property
    def disk_filename(self):
        ext = self.filename.split('.')
        if len(ext) == 1:
            ext = ''
        else:
            ext = '.'+ext[-1]
            
        return '%06i%s'%(self.pk, ext)
    
    @property
    def disk_fullpath(self):
        return settings.GAFC_FILEFOLDER+'/%i/'%self.cashing.date.year
    
    @property
    def disk_fullfilepath(self):
        return '%s/%s'%(self.disk_fullpath, self.disk_filename)
        
class HelloassoPayment(models.Model):
    amount      = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Montant")
    email       = models.EmailField()
    name        = models.CharField(max_length=250)
    formSlug    = models.CharField(max_length=250)
    date        = models.DateTimeField()
    hello_id    = models.IntegerField(unique=True)
    raw         = models.TextField()
    deleted     = models.BooleanField(default=False)
    payment     = models.OneToOneField('Payment', on_delete=models.CASCADE, default=None, null=True)
    
    @property
    def default_person(self):
        try:
            return Person.objects.get(email=self.email)
        except:
            return None
            
    @property
    def default_event(self):
        try:
            return Event.objects.filter(helloassoLink__endswith=self.formSlug)[0]
        except:
            return None
    
class AccountOperation(models.Model):
    opid        = models.CharField(max_length=250, unique=True)
    date        = models.DateField()
    amount      = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Montant")
    libele      = models.TextField()
    detail      = models.TextField()
    cashing     = models.ForeignKey(Cashing, null=True, on_delete=models.PROTECT, verbose_name="Encaissement")
     
class AccountBalance(models.Model):
    date        = models.DateField(unique=True)
    amount      = models.DecimalField(decimal_places=2, max_digits=10)
    
class Payment(models.Model):
    amount      = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Montant") # +: income / -: outcome
    method      = models.CharField(max_length=2, choices=[('CA', gafc_tags.payMethod('CA')),
                                                          ('CH', gafc_tags.payMethod('CH')),
                                                          ('BC', gafc_tags.payMethod('BC')),
                                                          ('IN', gafc_tags.payMethod('IN')),
                                                          ('VM', gafc_tags.payMethod('VM')),
                                                          ('BO', gafc_tags.payMethod('BO'))], verbose_name="Méthode")
    person      = models.ForeignKey(Person, on_delete=models.PROTECT,verbose_name="Contrepartie", related_name="debtor")
    encoder     = models.ForeignKey(Person, on_delete=models.PROTECT,verbose_name="Encaisseur", related_name="encoder")
    category    = models.ForeignKey(Category, on_delete=models.PROTECT,verbose_name="Catégorie")
    event       = models.ForeignKey(Event, on_delete=models.PROTECT, null=True, default=None,verbose_name="Événement")
    pay_done    = models.DateTimeField(verbose_name='Date payement')
    comment     = models.TextField(default="", verbose_name="Commentaire")
    cashing     = models.ForeignKey(Cashing, on_delete=models.PROTECT, verbose_name="Encaissement", null=True, default=None)
    season      = models.ForeignKey(Season, on_delete=models.PROTECT, verbose_name='Saison')
    refund_who  = models.ForeignKey(Person, on_delete=models.PROTECT, verbose_name="Remboursé par", related_name="refunder", null=True, default=None)
    refund_date = models.DateTimeField(null=True, default=None, verbose_name="Date de remboursement")
    refund_reason = models.TextField(null=True, default=None, verbose_name="Raison du remboursement")
    financial   = models.ForeignKey(FinancialCategory, on_delete=models.PROTECT, verbose_name="Imputation")
    
    def save(self, *args, **kwargs):
#        if self.cashing is not None:
#            if self.cashing.season != self.season:
#                raise ValueError('Cashing and payment must be related to same season')
            
        if self.event is not None:
            if self.event.season != self.season:
                raise ValueError('Event and payment must be related to same season')
            
#            if self.event.current_state == 'VA' and self.pk is None:
#                raise ValueError('Event is validated: can\'t add payment')
            
        if not ((self.refund_date is None and self.refund_who is None) or (self.refund_date is not None and self.refund_who is not None)):
            raise ValueError('When refunded, you must fill _date and _who field')
        
        if self.refund_date is not None and self.cashing is not None:
            raise ValueError('Refunded payment can\'t be cashed')
            
        super(Payment, self).save(*args, **kwargs)
        
    @property
    def next_id(self):
        r = None
        try:
            r = Payment.objects.filter(season=self.season, id__gt=self.id).order_by('id').first().id
        except:
            pass
        
        return r
    
    @property
    def prev_id(self):
        r = None
        try:
            r = Payment.objects.filter(season=self.season, id__lt=self.id).order_by('-id').first().id
        except:
            pass
        
        return r
    
    class Meta:
        default_permissions = ('add','addevent','change','view','delete')
        
class ForseenBalance(models.Model):
    comment     = models.TextField(verbose_name="Commentaire")
    date_valid  = models.DateField(default=None, null=True, verbose_name="Date de validation")
    person_valid= models.ForeignKey(Person, null=True, on_delete=models.PROTECT, verbose_name="Validateur")
    season      = models.ForeignKey(Season, on_delete=models.CASCADE, verbose_name="Saison")
    
class ForseenBalanceValue(models.Model):
    balance     = models.ForeignKey(ForseenBalance, on_delete=models.CASCADE)
    cat         = models.ForeignKey(Category, on_delete=models.CASCADE)
    fincat      = models.ForeignKey(FinancialCategory, on_delete=models.CASCADE)
    income      = models.DecimalField(decimal_places=2, max_digits=10, null=True, verbose_name="Produit")
    outcome     = models.DecimalField(decimal_places=2, max_digits=10, null=True, verbose_name="Charge")
    
    class Meta:
        unique_together= (('balance','cat','fincat'),)
    
class InternalPayment(models.Model):
    amount      = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Montant") # +: income / -: outcome
    method      = models.CharField(max_length=2, choices=[('CA', gafc_tags.payMethod('CA')),
                                                            ('CH', gafc_tags.payMethod('CH')),
                                                            ('BC', gafc_tags.payMethod('BC')),
                                                            ('IN', gafc_tags.payMethod('IN')),
                                                            ('VM', gafc_tags.payMethod('VM')),
                                                            ('BO', gafc_tags.payMethod('BO')),
                                                            ('NO', gafc_tags.payMethod('NO'))], verbose_name="Méthode")
    person      = models.ForeignKey(Person, on_delete=models.PROTECT,verbose_name="Ordonateur")
    event       = models.ForeignKey(Event, on_delete=models.PROTECT, null=True, default=None,verbose_name="Événement")
    category_c  = models.ForeignKey(Category, on_delete=models.PROTECT,verbose_name="Créditeur", related_name='creditor')
    financial_c = models.ForeignKey(FinancialCategory, on_delete=models.PROTECT, verbose_name='Imputation créditée', related_name='fincat_c')
    category_d  = models.ForeignKey(Category, on_delete=models.PROTECT,verbose_name="Débiteur", related_name='debtor') # Debtor pay to Creditor
    financial_d = models.ForeignKey(FinancialCategory, on_delete=models.PROTECT, verbose_name='Imputation débitée', related_name='fincat_d')
    pay_done    = models.DateTimeField(auto_now_add=True,verbose_name='Date payement')
    comment     = models.TextField(default="")
    season      = models.ForeignKey(Season, on_delete=models.PROTECT, verbose_name='Saison')
    
    def save(self, *args, **kwargs):
        if self.event is not None:
            if self.event.season != self.season:
                raise ValueError('Event and payment must be related to same season')
            
            if self.event.current_state == 'VA' and self.pk is None:
                raise ValueError('Event is validated: can\'t add payment')
            
        if self.category_c == self.category_d and self.financial_c == self.financial_d:
            raise ValueError('Creditor and Debitor and categories are the same')
            
        super(InternalPayment, self).save(*args, **kwargs)
    
    class Meta:
        default_permissions = ('add','addevent','view','delete')
