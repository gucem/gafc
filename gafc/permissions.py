# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Used permission tree
"""

GAFC_PERMISSIONS = [
    ('Saison',(
        ('gafc.add_season', 'Peut créer une saison'),
        ('gafc.view_season', 'Peut télécharger une archive de la saison'),
        ('gafc.change_season', 'Peut changer la saison par défaut'),
        ('gafc.delete_season', 'Peut supprimer une saison'),
        )
    ),
    
    ('Adhérents',(
        ('gafc.add_person', 'Peut créer un adhérent'),
        ('gafc.change_person','Peut modifier un adhérent'),
        ('gafc.delete_person','Peut purger les adhérents'),
        ('gafc.view_person','Peut voir les données des adhérents'),
        ('gafc.merge_person', 'Peut fusionner des comptes d\'adhérents'),
        ('gafc.staff_person', 'Accès administrateur'),
        ('gafc.cant_login_person','Interdiction de se connecter'),
        ('gafc.import_person','Importer des adhérents'),
        ('gafc.add_personcomment','Peut créer un commentaire sur un adhérent'),
        ('gafc.delete_personcomment','Peut supprimer un commentaire sans en être le propriétaire'),
        ('gafc.view_personcomment','Peut voir les commentaires des autres utilisateurs'),
        ('auth.view_permission', 'Peut voir les permissions'),
        )
    ),
    
    ('Licences',(
        ('gafc.add_license','Peut ajouter une licence à un adhérent'),
        ('gafc.delete_license','Peut supprimer licence'),
        ('gafc.import_license','Peut importer des licences découvertes'),
        ('gafc.add_federation','Peut ajouter une fédération'),
        #('gafc.change_federation','Peut modifier une fédération'), #TODO
        #('gafc.delete_federation','Peut supprimer une fédération'), #TODO
        )
    ),
    
    ('Catégories',(
        ('gafc.add_category','Peut créer une catégorie'),
        ('gafc.change_category','Peut modifier une catégorie'),
        #('gafc.delete_category','Peut supprimer une catégorie'), #TODO
        ('gafc.view_category','Peut voir les catégories'),
        ('gafc.financial_category', 'Peut voir les finances des catégories'),
        
        ('gafc.add_staff','Peut ajouter un encadrant'),
        #('gafc.change_staff','Peut changer le niveau d\'un encadrant'), #TODO
        ('gafc.delete_staff','Peut supprimer un encadrant'), #TODO
        
        ('gafc.add_validation','Peut valider un encadrant'),
        )
    ),
    
    ('Événements',(
        
        ('gafc.add_event','Peut créer un événement'),
        ('gafc.change_event','Peut modifier un événement'),
        ('gafc.delete_event','Peut supprimer un événement'),
        ('gafc.view_event','Peut voir les événements'),
        
        ('gafc.add_eventstate','Peut changer l\'etat d\'un événement'),
        ('gafc.delete_eventstate','Peut dé-valider un événement'),
        
        ('gafc.add_eventreport','Peut créer un rapport de sortie'),
        #('gafc.delete_eventreport','Peut supprimer tout rapport de sortie'), #TODO
        
        ('gafc.add_participation','Peut inscrire un adhérent à un événement'),
        ('gafc.change_participation','Peut changer une inscription à un événement'),
        ('gafc.delete_participation','Peut supprimer une inscription'),
        )
    ),
    
    ('E-Mail', (
        ('gafc.add_staffemail', 'Peut créer une adresse email d\'envoi'),
        ('gafc.view_staffemail', 'Peut envoyer des emails'),
        ('gafc.change_staffemail', 'Peut modifier les autorisations d\'envoi'),
        ('gafc.delete_staffemail', 'Peut supprimer une adresse email d\'envoi'),
        ('gafc.send_noauth_staffemail', 'Peut envoyer depuis une boite noreply'),
        )
    ),
    
    ('Finances',(
        ('gafc.add_cashing','Peut créer un encaissement'),
        ('gafc.validate_cashing','Peut valider un encaissement'),
        ('gafc.view_cashing', 'Peut voir les encaissements'),
        ('gafc.change_cashing', 'Peut éditer les encaissements'),
        ('gafc.delete_cashing', 'Peut dévalider un encaissement'),
        
        ('gafc.view_cashingabandonvalidator', 'Peut voir les personnes autorisées à générer une attestation fiscale'),
        ('gafc.change_cashingabandonvalidator', 'Peut éditer les personnes autorisées à générer une attestation fiscale'),
        
        ('gafc.add_payment','Peut créer un paiement'),
        ('gafc.change_payment','Peut modifier un paiement'),
        ('gafc.addevent_payment','Peut créer un paiement lié à un événement'),
        ('gafc.view_payment', 'Peut voir les paiements'),
        ('gafc.delete_payment','Peut rembourser un paiement'),
        
        ('gafc.add_internalpayment','Peut créer un transfert'),
        ('gafc.addevent_internalpayment','Peut créer un transfert lié à un événement'),
        ('gafc.view_internalpayment', 'Peut voir les transferts'),
        #('gafc.delete_internalpayment','Peut supprimer un transfert'), #TODO
        
        ('gafc.view_accountoperation', 'Peut voir les opérations bancaires'),
        ('gafc.change_accountoperation', 'Peut éditer les opérations bancaires'),
        
        ('gafc.view_helloassopayment', 'Peut voir les paiements helloasso'),
        ('gafc.change_helloassopayment', 'Peut transformer un paiement helloasso'),
        ('gafc.delete_helloassopayment', 'Peut supprimer un paiement helloasso'),
        
        ('gafc.view_financialcategory', 'Peut voir une imputation financière'),
        ('gafc.add_financialcategory', 'Peut créer une imputation financière'),
        ('gafc.change_financialcategory', 'Peut changer une imputation financière'),
        ('gafc.delete_financialcategory', 'Peut supprimer une imputation financière'),
        ('gafc.balance_financialcategory', 'Peut afficher le bilan'),
        
        ('gafc.add_forseenbalance', 'Peut créer un budget prévisionnel'),
        ('gafc.view_forseenbalance', 'Peut afficher les budgets prévisionnels'),
        ('gafc.change_forseenbalance', 'Peut modifier les budgets prévisionnels'),
        
        ('gafc.view_financialcategorytree', 'Peut voir l\'arbre des imputations financière'),
        ('gafc.add_financialcategorytree', 'Peut créer l\'arbre des imputations financière'),
        ('gafc.change_financialcategorytree', 'Peut changer l\'arbre des imputations financière'),
        ('gafc.delete_financialcategorytree', 'Peut supprimer un élément de l\'arbre des imputaitons financière'),
    
        )
    ),
    
    ('Équipements',(        
        ('gafc.add_equipmentgroup','Peut créer un groupe d\'équipements'),
        ('gafc.change_equipmentgroup','Peut modifier un groupe d\'équipements'),
        ('gafc.delete_equipmentgroup','Peut supprimer un groupe d\'équipements'),
        ('gafc.view_equipmentgroup','Peut voir les groupe d\'équipements'),
        
        ('gafc.add_equipment','Peut créer un équipement'),
        ('gafc.view_equipment','Peut voir les équipements'),
        ('gafc.change_equipment','Peut modifier un équipement'),
        ('gafc.scrap_equipment', 'Peut mettre un équipement au rebus'),
        
        ('gafc.add_equipmenttracking','Peut créer un suivi sur un équipement'),
        ('gafc.change_equipmenttracking','Peut modifier un suivi'),
        ('gafc.delete_equipmenttracking','Peut supprimer un suivi'),
        )
    ),
    
    ('Véhicules',(
        ('vehicles.view_vehicle', 'Peut voir le suivi des véhicules'),
        ('vehicles.change_vehicle', 'Peut configurer le module "véhicule"'),
        ('vehicles.add_vehicletrackingcheck', 'Peut valider un tracking'),
        ('vehicles.financial', 'Peut voir le résumé financier des véhicules'),
        )        
    ),

    ('Formation',(
            ('formation.view_valorisation','Peut voir les valorisations'),
            ('formation.add_valorisation','Peut ajouter une valorisation'),
            ('formation.delete_valorisation','Peut supprimer une valorisation'),
            ('formation.view_valorisationrule','Peut voir les règles de valorisations'),
            ('formation.add_valorisationrule','Peut ajouter une règle de valorisation'),
            ('formation.change_valorisationrule','Peut changer une règle de valorisation'),
            ('formation.delete_valorisationrule','Peut supprimer une règle de valorisation'),
        ),
    ),

    ('Clés',(
            ('keys.view_key', 'Peut voir les clés'),
            ('keys.change_key', 'Peut gérer les clés'),
        ),
    ),
    
]

GAFC_PERMISSIONS_DICT = { p[0]:p[1] for P in GAFC_PERMISSIONS for p in P[1] }

