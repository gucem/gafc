# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Email processing functions
"""



from django.template import Template, Context
from .templatetags import gafc_tags
from django.urls import reverse
from gucem import settings
import markdown
from mdx_linkify.mdx_linkify import LinkifyExtension

def process_email(content, request, e, title=''):
    template = Template(content)
    context = Context({'event': {
                            'title': e.title,
                            'description': e.description,
                            'begin': e.begin,
                            'finish': e.finish,
                            'type': gafc_tags.eventType(e.eventtype),
                            'place': e.place,
                            'category': e.category.name,
                            'cost': e.cost,
                            'season': e.season.name,
                            'helloasso_link': e.helloassoLink},
                       'participation_link': request.build_absolute_uri(reverse('event_participation', args=[e.id])),
                       'admin_link': request.build_absolute_uri(reverse('event', args=[e.id])),                       
                       })
    intermediate = template.render(context)
    
    tmpl_title = Template(title)
    title = tmpl_title.render(context)
    
    html = "<html><head><style>td { border-spacing: 1em; border-collapse: separate; } tr:nth-child(even) { background: #EEE; } tr:nth-child(odd) { background: #FFF; }</style></head><body>%s</body></html>"%markdown.markdown(intermediate, extensions=['tables', LinkifyExtension(linker_options={"parse_email": True})])
    
    return (intermediate, html, title)

    



