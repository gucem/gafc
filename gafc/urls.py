# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
GAFC main URL router
"""

from django.urls import path, re_path, register_converter
from django.views.decorators.csrf import csrf_exempt
from .views import callback, categories, equipments, events, financials, home, logins, persons, settings, dashboard, jsondata, permissions, mailings
from datetime import datetime


class DateConverter:
    """
    Custom converter for dates in url
    """
    regex = '\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        """
        Convert the date to python date
        :param value: Date in Y-m-d format
        :type value: str
        :return date
        :rtype datetime.datetime
        """
        return datetime.strptime(value, '%Y-%m-%d')

    def to_url(self, value):
        """
        Convert the date to string
        :param value: Date
        :type value: datetime.datetime or datetime.date
        :return date
        :rtype str
        """
        return value.strftime('%Y-%m-%d')
    
register_converter(DateConverter, 'dateYmd')


urlpatterns = [
    # Home
    path('', home.HomeView.as_view() , name='home'),
    path('season/set', home.SetSeasonView.as_view(), name='set_season'),
    
    # Dashboard
    path('dashboard', dashboard.DashboardView.as_view(), name='dashboard'),
    path('dashboard/<dateYmd:today>', dashboard.DashboardView.as_view(), name='dashboard'),
    
    path('dashboard/plot/members_ages', dashboard.DashboardGraphsView.as_view(action='members_by_age'), name="dashboard_members_ages"),
    path('dashboard/plot/members_ages/<dateYmd:today>', dashboard.DashboardGraphsView.as_view(action='members_by_age'), name="dashboard_members_ages"),
    
    path('dashboard/plot/members_timeline', dashboard.DashboardGraphsView.as_view(action='adhesions_timeline'), name="dashboard_adhesions_timeline"),
    path('dashboard/plot/members_timeline/<dateYmd:today>', dashboard.DashboardGraphsView.as_view(action='adhesions_timeline'), name="dashboard_adhesions_timeline"),
    
    path('dashboard/plot/members_firstyear', dashboard.DashboardGraphsView.as_view(action='members_by_firstyear'), name="dashboard_members_firstyear"),
    path('dashboard/plot/members_firstyear/<dateYmd:today>', dashboard.DashboardGraphsView.as_view(action='members_by_firstyear'), name="dashboard_members_firstyear"),
    
    path('dashboard/plot/mixity/<int:sid>', dashboard.MixityGraphsView.as_view(), name='mixityPlot'),
    path('dashboard/plot/mixity/<int:sid>/<int:catid>', dashboard.MixityGraphsView.as_view(), name='mixityPlot'),
    
    
    # Json data
    path('jsondata/persons', jsondata.JsonDataView.as_view(action='persons'), name="persons_json"),
    path('jsondata/events', jsondata.JsonDataView.as_view(action='events'), name="events_json"),
    path('jsondata/events/all', jsondata.JsonDataView.as_view(action='events'), {'allevt': True}, name="events_json_all"),
    path('jsondata/payments', jsondata.JsonDataView.as_view(action='payments'), name="payments_json"),
    
    # Events
    path('event/calendar', events.EventsView.as_view(), name='list_events'),
    path('event/calendar/<int:year>/<int:month>', events.EventsView.as_view(), name='list_events'),
    path('event/<int:event_id>',events.EventView.as_view(), name='event'), 
    path('event/<int:event_id>/licenses', events.EventLicenseView.as_view(), name='event_licenses'),
    path('event/participation/<int:event_id>', events.EventParticipationView.as_view(), name='event_participation'),
    path('event/<int:eid>/export',events.EventExportView.as_view(), name='event_export'),   
    
    path('events/calendar/<int:cid>', events.EventICSCalendarView.as_view()),
    
    # Login Logout, Register, ...
    path('login', logins.LoginView.as_view(), name='login'),
    path('logout', logins.LogoutView.as_view(), name='logout'),
    path('register', logins.RegisterView.as_view(), name='new_account'),
    path('lost_password', logins.LostPasswordRequestView.as_view(), name="lost_password"),
    path('new_lost_password/<int:id>/<str:token>', logins.NewPasswordProcessView.as_view(), name="new_lost_password"),
    
    # Persons
    path('person/list', persons.PersonListView.as_view(), name='list_persons'),
    path('person/list/<int:page>',persons.PersonListView.as_view(), name='list_persons'),
    re_path(r'^person/list/(?P<name_first_letter>[a-zA-Z])$',persons.PersonListView.as_view(), name='list_persons'),
    re_path(r'^person/list/(?P<name_first_letter>[a-zA-Z])/(?P<page>[0-9]+)$',persons.PersonListView.as_view(), name='list_persons'),
    path('person/staffing',persons.PersonsStaffingView.as_view(), name='staffing_all'),
    path('person/<int:person_id>', persons.PersonView.as_view(), name='person'),
    path('person/var_person_id', persons.PersonView.as_view(), name='person_js'),
    #path('import_members/', persons.upload_from_memberlist, name='import_members'),
    #path('import_licenses/', persons.import_licenses, name='import_licenses'),
    
    # Mailing list
    path('mailing/', mailings.MailingListListView.as_view(), name='mailings'),
    path('mailing/<str:mlname>', mailings.MailingListView.as_view(), name='mailing'),
    
    # Permissions
    path('permissions/', permissions.PermissionsView.as_view(), name='permissions'),
    path('permissions/<int:group_id>', permissions.PermissionsGroupView.as_view() , name="permission_group"),
    
    # Categories
    path('category/<int:category_id>/events/export', categories.CategoryEventsCSVExportView.as_view(), name='category_events_export'),
    path('category/<int:category_id>/stats/participations', categories.CategoryParticipationGraphView.as_view() , name='category_p_repartition'),
    path('category/<int:category_id>', categories.CategoryView.as_view(), name='category'),
    path('category/<int:category_id>/events/edit', categories.CategoryEventsEdit.as_view(), name='category_events_edit'),
    path('category/<int:category_id>/events/edit/<int:year>/<int:month>', categories.CategoryEventsEdit.as_view(), name='category_events_edit'),
    
    # Cashing
    path('financials/cashing/list', financials.CashingListView.as_view(), name='financial'),
    path('financials/cashing/mylist', financials.CashingUserView.as_view(), name='financial_refund' ),
    path('financials/cashing/list/<int:year>', financials.CashingListView.as_view(), name='financial'),
    path('financials/cashing/<int:cid>', financials.CashingView.as_view(), name='cashing'),
    path('financials/cashing/file/<int:fid>', financials.CashingFileDownloadView.as_view(), name='cashing_file'),
    path('financials/cashing/signature/<int:fid>', financials.CashingSignatureFileDownloadView.as_view(), name='cashing_signature'),
    
    # Forseenbalance
    path('financials/forseenbalance/list', financials.ForseenBalanceListView.as_view(), name="fin_forseenbalances"),
    path('financials/forseenbalance/<int:balid>', financials.ForseenBalanceView.as_view(), name="fin_forseenbalance"),
    
    #Bank account operation
    path('financials/bankaccount/', financials.AccountOperationView.as_view(), name='accountoperations'),
    path('financials/bankaccount/balances', financials.AccountBalanceView.as_view(), name='accountbalance'),
    path('financials/bankaccount/<int:year>', financials.AccountOperationView.as_view(), name='accountoperations'),
    path('financials/bankaccount/processfile', financials.AccountOperationFileView.as_view(), name='accountoperations_file'),
    
    # Financial category manager
    path('financial/categories', financials.FinancialCategoryView.as_view(), name='fincat'),
    
    # Single operations
    path('financials/payment/<int:payment_id>', financials.PaymentView.as_view(), name='payment'),
    path('financials/transfer/<int:tid>', financials.TransferView.as_view(), name='transfer'),
    path('financials/operation/<int:opid>', financials.OperationView.as_view(), name='operation'),
    
    # Helloasso
    path('financials/payments/helloasso', financials.HelloassoPaymentView.as_view(), name='helloasso_payments'),
    
    # Balances
    path('financials/balance/daily', financials.BalanceView.as_view(action='balance_tmp'), name="fin_balance"),
    path('financials/balance/done', financials.BalanceView.as_view(action='balance_done'), name="fin_balance_done"),
    
    # Equipments
    path('equipment/list', equipments.EquipmentListView.as_view(), name='list_equipment'),
    path('equipment/<int:eqid>', equipments.EquipmentView.as_view(), name='equipment'),
    path('equipment/picture/<int:picture_id>', equipments.EquipmentPictureView.as_view() , name='equipment_picture'),
    path('equipment/calendar', equipments.EquipmentCalendarView.as_view(), name='calendar_equipment'),
    path('equipment/calendar/<int:year>/<int:month>', equipments.EquipmentCalendarView.as_view(), name='calendar_equipment'),
    path('equipment/group/<int:eqgrp_id>', equipments.EquipmentGroupView.as_view(), name='equipment_grp'),
    path('equipment/tracking/<int:tracking_id>', equipments.EquipmentTrackingView.as_view(), name='equipmenttracking'),
    
    # Settings
    path('settings', settings.SettingsView.as_view(), name='settings'),
    
    #Export
    path('export/season_archive/<int:sid>', settings.SeasonArchiveView.as_view(), name='season_archive'),
    path('export/staff_list/<int:sid>', settings.StaffListView.as_view(), name='staff_list'),
    
    # Callbacks
    path('callback/helloasso', callback.HelloassoCallbackView.as_view()),
    path('callback/eventmail', callback.EventMailCallbackView.as_view(), name="cbk_event"),
    
]

 
