# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
GAFC-specific decorators
"""


from functools import wraps
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django import http
import datetime as dt
import re
from .models import Participation

def is_staff(fnc):
    @login_required
    @wraps(fnc)
    def wrap(request, *args, **kwargs):
        if request.user.has_perm('gafc.staff_person') or request.user.is_superuser:
            return fnc(request, *args, **kwargs)
        else:
            raise PermissionDenied('Vous ne disposez pas de l\'accès administrateur.')
        
    return wrap

def is_superuser(fnc):
    @login_required
    @wraps(fnc)
    def wrap(request, *args, **kwargs):
        if request.user.is_superuser:
            return fnc(request, *args, **kwargs)
        else:
            raise PermissionDenied('Vous ne disposez pas de l\'accès superutilisateur.')
        
    return wrap

def view_auth(auth, error_msg='Vous n\'avez pas l\'autorisation de voir cette page.'):
    def decorator(fnc):
        @wraps(fnc)
        def wrap(request, *args, **kwargs):
            if request.user.has_perm(auth):
                return fnc(request, *args, **kwargs)
            else:
                raise PermissionDenied(error_msg)

        return wrap
    return decorator


def json_cache_key(uri, dataname):
    """
    Get cache key from function and uri
    """
    
    import hashlib
    
    return 'json_response_cache:{}:{}'.format(
                dataname,
                hashlib.md5(uri.encode()).hexdigest()
            )

def json_global_data_cache(seconds, dataname):
    """
    Cache only when there's a healthy http.JsonResponse response.
    Return 304 if not modified
    """
    
    from django.core.cache import cache
    from django.urls import resolve

    def decorator(func):

        @wraps(func)
        def inner(request, *args, **kwargs):
            
            if not request.user.is_staff:
                if request.META.get('HTTP_REFERER') is not None:
                    referer = request.META.get('HTTP_REFERER')
                    referer = re.sub('^https?:\/\/', '', referer).split('/')
                    
                    hostname = referer[0].split(':')[0]
                    
                    if not (hostname == request.META.get('SERVER_NAME') or ('HTTP_X_FORWARDED_HOST' in request.META and hostname in request.META['HTTP_X_FORWARDED_HOST'])):
                        raise PermissionDenied()
                    referer = u'/' + u'/'.join(referer[1:])
                    
                    referer_view = resolve(referer)
                    
                    if referer_view.url_name != 'event':
                        raise PermissionDenied()
                        
                    try:
                        p = Participation.objects.get(person=request.user, event__id=referer_view.kwargs['event_id'])
                        if p.role == '0PA':
                            raise PermissionDenied()
                    except:
                        raise PermissionDenied()
            
            cache_key = json_cache_key(request.path, dataname)
            content = cache.get(cache_key)
            
            response = None
            time = None
            
            if type(content) is dict and 'time' in content and 'content' in content:
                
                if 'HTTP_IF_MODIFIED_SINCE' in request.META:
                    req_time = dt.datetime.strptime(request.META['HTTP_IF_MODIFIED_SINCE'], '%a %d %b %Y %H:%M:%S GMT')
                    if content['time'] == int(req_time.timestamp()):
                        response = http.HttpResponseNotModified()
                    
                else:
                    response = http.HttpResponse(
                        content['content'],
                        content_type='application/json'
                    )

                time = content['time']
                
            if response is None:
                response = func(request, *args, **kwargs)
                if isinstance(response, http.JsonResponse) and response.status_code in (200, 304):
                    time = int(dt.datetime.utcnow().timestamp())
                    cache.set(cache_key, {'content': response.content, 'time': time }, seconds)
                
            response['Cache-Control'] = 'must-revalidate'
            response['Last-Modified'] = dt.datetime.strftime(dt.datetime.fromtimestamp(time),'%a %d %b %Y %H:%M:%S GMT')
            response['Content-Location'] = request.path
                
            return response

        return inner

    return decorator
