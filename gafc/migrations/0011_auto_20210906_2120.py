# Generated by Django 3.1.13 on 2021-09-06 21:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0010_auto_20210601_0743'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='payment',
            options={'default_permissions': ('add', 'addevent', 'change', 'view', 'delete')},
        ),
    ]
