# Generated by Django 3.1.2 on 2021-06-01 07:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0009_auto_20210515_1853'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='financialcategory',
            options={'default_permissions': ('add', 'change', 'delete', 'view', 'balance')},
        ),
        migrations.AddField(
            model_name='helloassopayment',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='helloassopayment',
            name='payment',
            field=models.OneToOneField(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='gafc.payment'),
        ),
    ]
