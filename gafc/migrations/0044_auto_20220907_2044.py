# Generated by Django 3.2.15 on 2022-09-07 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0043_equipmentpicture'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashing',
            name='bic',
            field=models.TextField(default=None, null=True, verbose_name='BIC'),
        ),
        migrations.AddField(
            model_name='cashing',
            name='iban',
            field=models.TextField(default=None, null=True, verbose_name='IBAN'),
        ),
    ]
