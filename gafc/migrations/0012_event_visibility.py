# Generated by Django 3.1.13 on 2021-09-14 22:14

from django.db import migrations, models


def initialize_visibility(apps, schema_editor):
    Event = apps.get_model('gafc', 'Event')
    Event.objects.filter(hidden=True).update(visibility=2)
    
class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0011_auto_20210906_2120'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='visibility',
            field=models.PositiveIntegerField(choices=[(0, 'Publique'), (1, 'Intranet'), (2, 'Caché'), (3, 'Privé')], default=0),
        ),
        migrations.RunPython(initialize_visibility),
    ]
