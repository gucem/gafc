# Generated by Django 3.2.15 on 2022-09-29 22:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0048_sympamailinglist'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sympamailinglist',
            options={'default_permissions': ('add', 'change', 'delete')},
        ),
    ]
