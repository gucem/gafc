# Generated by Django 3.2 on 2022-04-18 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0030_person_caci'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashing',
            name='abandon',
            field=models.BooleanField(default=False, verbose_name='Abandon de frais'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='eventtype',
            field=models.CharField(choices=[('PR', 'Professionnel'), ('BN', 'Bénévole'), ('RE', 'Réunion'), ('SE', 'Vente')], max_length=2, verbose_name='Type'),
        ),
    ]
