# Generated by Django 3.2.13 on 2022-05-22 16:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0035_alter_cashingabandonvalidator_person'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForseenBalance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField(verbose_name='Commentaire')),
                ('date_valid', models.DateField(default=None, null=True, verbose_name='Date de validation')),
                ('person_valid', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Validateur')),
                ('season', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gafc.season', verbose_name='Saison')),
            ],
        ),
        migrations.AlterField(
            model_name='cashing',
            name='abandon',
            field=models.BooleanField(default=False, verbose_name='Abandon de frais'),
        ),
        migrations.CreateModel(
            name='ForseenBalanceValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('income', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Produit')),
                ('outcome', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Charge')),
                ('balance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gafc.forseenbalance')),
                ('cat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gafc.category')),
                ('fincat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gafc.financialcategory')),
            ],
            options={
                'unique_together': {('balance', 'cat', 'fincat')},
            },
        ),
    ]
