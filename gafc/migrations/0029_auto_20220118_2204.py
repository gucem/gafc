# Generated by Django 3.1.13 on 2022-01-18 22:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0028_auto_20220118_2143'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='license',
            unique_together={('person', 'number', 'federation', 'date_in')},
        ),
    ]
