# Generated by Django 3.1.2 on 2021-05-14 12:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gafc', '0004_auto_20210514_1241'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cashing',
            name='category',
        ),
        migrations.DeleteModel(
            name='CashingCategory',
        ),
    ]
