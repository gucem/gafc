# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
FFCAM & SYMPA extranet access & processing functions
"""

#from django.views.decorators.debug import sensitive_variables
import pycurl
import re, json
from io import BytesIO
from urllib.parse import urlencode
import pandas as pd
import datetime as dt
import os

import gucem.settings as gsets

from gafc.models import SympaMailingList, License
from html.parser import HTMLParser

class ConnexxionException(Exception):
    pass


class HtmlPortalConnexxion(object):
    """
    Perform GET and POST request in a password-protected intranet
    """
    
    def get_hidden_field(self, payload, field_id):
        """
        Read an hidden field in the payload
        
        :param payload: The payload to be analyzed
        :param field_id: The field name to look at
        """
        
        reg = r"<input(?:.*)name=\"%s\"(?:.*)value=\"(.+)\"(?:.*)/>"%field_id
        m = re.search(reg, payload)
        if m is not None:
            return m.group(1)
        else:
            return None
        
    def _get_temp_filename(self):
        """
        Return a valid temporary filename
        """
        
        import random
        
        random.seed()
        
        fn = None
        
        while fn is None:
            fn = gsets.GAFC_TEMPFOLDER+'/%x.tmp'%(random.randrange(2**32-1))
            
            if os.path.exists(fn):
                fn = None
            
        return fn
        
    def post(self, page, getargs={}, postargs={}, cookiefile=None):
        """
        Perform a POST request
        
        :param getargs: GET arguments
        :param portargs: POST arguments
        :param cookiefile: The file where the cookies are read/written
        
        :return: The page content if return code is 200, None if not.
        """
        
        b_obj = BytesIO()
        crl = pycurl.Curl()
        crl.setopt(crl.WRITEDATA, b_obj)
        crl.setopt(crl.URL, "%s?%s"%(page,urlencode(getargs)))
        crl.setopt(crl.FOLLOWLOCATION, True)
        
        if cookiefile is not None:
            crl.setopt(pycurl.COOKIEJAR, cookiefile)
            crl.setopt(pycurl.COOKIEFILE, cookiefile)
    
        # Sets request method to POST,
        # Content-Type header to application/x-www-form-urlencoded
        # and data to send in request body.
        crl.setopt(crl.POSTFIELDS, urlencode(postargs))
        crl.perform()
        
        ok = crl.getinfo(pycurl.HTTP_CODE) == 200
        
        crl.close()
        return b_obj.getvalue() if ok else None
    
    def get(self, page, args={}, cookiefile=None):
        """
        Perform a GET request
        
        :param args: GET arguments
        :param cookiefile: The file where the cookies are read/written
        
        :return: The page content if return code is 200, None if not.
        """
        b_obj = BytesIO()
        crl = pycurl.Curl()
        crl.setopt(crl.WRITEDATA, b_obj)
        crl.setopt(crl.URL, "%s?%s"%(page,urlencode(args)))
        crl.setopt(crl.FOLLOWLOCATION, True)
        
        if cookiefile is not None:
            crl.setopt(pycurl.COOKIEJAR, cookiefile)
            crl.setopt(pycurl.COOKIEFILE, cookiefile)
            
        crl.perform()
        
        ok = crl.getinfo(pycurl.HTTP_CODE) == 200
        
        crl.close()
        
        return b_obj.getvalue() if ok else None
    
    
    @classmethod
    def validateEmail(cls, email ):
        """
        Email validation function
        """
        
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError
        try:
            validate_email( email )
            return True
        except ValidationError:
            return False
        
    
class FFCAMExtranetConnexxion(HtmlPortalConnexxion):
    """
    Hold a connection to FFCAM extranet
    """
    
    def __init__(self, user, passwd):
        """
        Initialize the connection to FFCAM extranet
        
        :param user: Extranet username
        :param passwd: Extranet password
        
        :raise ConnexxionException: if login is wrong
        """
        
        data = self.post('https://extranet-clubalpin.com/app/Effectifs/accueil.php',{},{'bool_force_session': '1',
            'str_action_login': 'login',
            'str_login': user,
            'str_password': passwd})
    
        self.__sid = self.__get_sid(data.decode('utf-8')) if data is not None else None
        self.__check_sid()
        
        
    def __check_sid(self):
        """
        Check sid is not None. Raise exception accordingly.
        """
        
        if self.__sid is None:
            raise ConnexxionException("The sid is None: last request failed.")
    
    def __get_sid(self, payload):
        """
        Return the sid field, usefull for next request
        """
        # <input type="hidden" id="sid"  name="sid" value="UsypKMI9eV4YYBIgItrP" />
        return self.get_hidden_field(payload, "sid")
    
    def logout(self):
        """
        Logout from server. Reset the sid.
        """
        self.get('https://extranet-clubalpin.com/app/Effectifs/disconnect.php', {'sid':self.__sid})
        self.__sid = None
        
    def __enter__(self):
        """
        Allow with statement
        """
        return self
    
    def __exit__(self, *args, **kwargs):
        """
        Allow with statement
        """
        self.logout()
        
    
    def create_new_discovery_license(self, person, federation, date_in, category):
        """
        Create a new discovery license on the server and create the object accordingly if it was successfull on the server.
        
        :param person: Person on which the license is requested
        :param federation: Federation object
        :param date_in: The first date of the license
        :param category: 24, 48 or 72, number of days for the license
        """
        
        self.__check_sid()
        
        val = self.post('https://extranet-clubalpin.com/app/Effectifs/carte_decouverte.php',{},
                    {'sid': self.__sid,
                        'str_action': 'add',
                        'bool_already_post': "1",
                        'str_etat': "record",
                        'int_adherent_id': "D",
                        'str_section': "3801",  # CLUB ID
                        'str_categor': category,
                        'str_date_adhesion': date_in.strftime('%d/%m/%Y'),
                        'str_heure_adhesion': "05:00",
                        'str_date_naissance': person.birthdate.strftime('%d/%m/%Y'),
                        'adhtempo_act_0': "1",
                        'adhtempo_act_1': "1",
                        'adhtempo_act_2': "1",
                        'adhtempo_act_3': "1",
                        'adhtempo_act_4': "1",
                        'adhtempo_act_5': "1",
                        'adhtempo_act_6': "1",
                        'adhtempo_act_7': "1",
                        'adhtempo_act_8': "1",
                        'adhtempo_act_9': "1",
                        'adhtempo_act_10': "1",
                        'adhtempo_act_11': "1",
                        'adhtempo_act_12': "1",
                        'adhtempo_act_13': "1",
                        'adhtempo_act_14': "1",
                        'adhtempo_act_15': "1",
                        'adhtempo_act_16': "1",
                        'str_qualite': 'M' if person.gender == "M" else "MME",
                        'str_nom': person.last_name.upper(),
                        'str_prenom': person.first_name.upper(),
                        'str_complement_adresse': "",
                        'str_batiment': "",
                        'str_rue': person.street.upper(),
                        'str_localite': "",
                        'str_code_postal': person.postcode,
                        'str_ville': person.city.upper(),
                        'str_rgpd': "1",
                        'str_tel_domicile': "",
                        'str_tel_bureau': "",
                        'str_email': person.email.upper(),
                        'str_telportable': "",
                        'str_telaccident': person.emg_phone,
                        'str_quiaccident': person.emg_contact.upper(),
                        'representantLegal_id': "",
                        'representantLegal_qualite': "",
                        'representantLegal_nom': "",
                        'representantLegal_prenom': "",
                        'representantLegal_tel': "",
                        'flo_montant': "%.2f"%(float(category)/24.*5.),
                        'flo_reglement1': "%.2f"%(float(category)/24.*5.),
                        'str_type_reg1': "CH",
                        'str_date_reglt1': dt.datetime.now().strftime('%d/%m/%Y'),
                        })
     
        class MyHTMLParser(HTMLParser):
            raise_next_data = False
            errors = []
            def handle_starttag(self, tag, attrs):
                for A in attrs:
                    if A[0] == 'role' and A[1] == 'alerte':
                        self.raise_next_data = True
        
            def handle_endtag(self, tag):
                pass
        
            def handle_data(self, data):
                if self.raise_next_data:
                    if data.strip() != '':
                        self.errors += [data.strip(),]
                        self.raise_next_data = False
        
        parser = MyHTMLParser()
        parser.feed(val.decode('latin-1'))
        
        if len(parser.errors) > 0:
            raise ValueError(', '.join(parser.errors))
     
        data_json = self.get('https://extranet-clubalpin.com/app/Effectifs/jx_jqGrid.php',
                                    {'sid': self.__sid,
                                    'def' : 'adhtempo',
                                    'mode': 'liste',
                                    'rows': 50,
                                    'page': 1,
                                    'sidx': 'jqGrid_adhtempo_ID',
                                    'sord': 'desc'}) # Take last 50 discovery licences. This should be the first.
        data = json.loads(data_json)
        
        if 'userData' in data and 'caliData' in data['userData']:
            if type(data['userData']['caliData']) == dict:
                for (k,v) in data['userData']['caliData'].items():
                    if v['_BASE_EMAIL'].lower() == person.email.lower() and v['_BASE_DATADHES'] == date_in.strftime('%Y-%m-%d'):
                        return License.objects.create(number=v['_BASE_ID'], federation_id=federation, person=person, date_in=date_in, date_end=date_in + dt.timedelta(days=float(category)/24-1))
                    
        raise ValueError("Unable to recover created license")
    
    
    def save_memberfile(self, filename):
        """
        Get the memberfile and save to filename
        
        :param filename: Path to save the file to
        """
        
        self.__check_sid()
        
        datafile = self.get('https://extranet-clubalpin.com/app/Effectifs/report_manager.php',
                                    {'sid':self.__sid,
                                    'report_name':'specExp01'})
        
        with open(filename, 'wb+') as destination:
            destination.write(datafile)



def check_cols(listes):
    for k,v in listes.items():
        N = len(v['field'])
        
        if v['N'] == '1':
            if N != 1: raise ValueError('%s: %i'%(v['N'],N))
        elif v['N'] == '?':
            if N < 0 or N > 1: raise ValueError('%s: %i'%(v['N'],N))
        elif v['N'] == '+':
            if N < 1: raise ValueError('%s: %i'%(v['N'],N))
            
def extract_from_data(lists, data):
    
    check_cols(lists)
    
    ret = pd.DataFrame()
    
    for k,v in lists.items():
        dat = data[v['field']]
        dat = dat.apply(lambda ss: ' '.join(['%s'%s for s in ss]).strip(),1)
        
        if v['transform'] is not None:
            dat = dat.apply(v['transform'],1)
        
        ret[k] = dat
        
        
    return ret


def extranet_process_memberfile(memberfile):
    """
    Process a memberfile to person, license and mailing list subscription objects
    """
    
    def tr_gender(s):
        if s in ('M','MR'):
            return 'M'
        elif s in ('MME','MLLE','MRS'):
            return 'F'
        else:
            return 'X'
        
    def tr_date(s):
        try:
            return dt.datetime.strptime(s, '%Y-%m-%d')
        except:
            return None
        
    def tr_date_valid(s):
        try:
            dt.strptime(s, '%Y-%m-%d')
            return True
        except:
            return False
    
    person_titles = {
            'first_name':       {'N':       '+',
                                 'field':   ['PRENOM',],
                                 'transform': None},
            'last_name':        {'N':       '+',
                                 'field':   ['NOM',],
                                 'transform': None},
            'gender':           {'N':       '1',
                                 'field':   ['QUALITE',],
                                 'transform': tr_gender},
            'email':            {'N':       '1',
                                 'field':   ['MEL',],
                                 'transform': None},
            'street':           {'N':       '+',
                                 'field':   ['BATIMENT','COMPNOM','NUMRUE',],
                                 'transform': None},
            'postcode':         {'N':       '1',
                                 'field':   ['CODPOST',],
                                 'transform': None},
            'city':             {'N':       '+',
                                 'field':   ['VILLE',],
                                 'transform': None},
            'country':          {'N':       '1',
                                 'field':   ['PAYS',],
                                 'transform': None},
            'phone':            {'N':       '1',
                                 'field':   ['POR',],
                                 'transform': None},
            'birthdate':        {'N':       '1',
                                 'field':   ['DATNAISS',],
                                 'transform': tr_date},
            'emg_contact':      {'N':       '?',
                                 'field':   ['ACCIDENTQUI',],
                                 'transform': None},
            'emg_phone':        {'N':       '?',
                                 'field':   ['ACC',],
                                 'transform': None},
            'caci':             {'N':       '?',
                                 'field':   ['CACI_MILLESIME',],
                                 'transform': lambda s: s if s != '0' else None},
            }
            
    licence_titles = {
            'number':           {'N':       '1',
                                 'field':   ['ID',],
                                 'transform': None},
            'date_in':          {'N':       '1',
                                 'field':   ['INSCRIPTION',],
                                 'transform': tr_date}
            }
            
    mailing_titles = { ml.name: {'N': '1', 'field': [ml.code,],'transform': tr_date_valid} for ml in SympaMailingList.objects.exclude(code=None).exclude(code='') }
    
    data = pd.read_csv(memberfile, sep='\t', encoding='latin-1', dtype=str)
    data.fillna('', inplace=True)
    
    dat_l = extract_from_data(licence_titles, data)
    dat_p = extract_from_data(person_titles, data)
    dat_m = extract_from_data(mailing_titles, data)
    
    msk = dat_l['date_in'].isnull() == False
    
    return dat_p[msk], dat_l[msk], dat_m[msk]
    


class SympaConnexxion(HtmlPortalConnexxion):
    """
    Hold a connexion to Sympa server through HTTP
    """
    
    def __init__(self, user, passwd, url):
        """
        Initialize the connection: login
        
        :param user: Sympa server username
        :param password: Sympa server password
        :param url: Server url
        """
        
        self.__cookiefile = self._get_temp_filename()
        self.__url = url
        
        self.__login(user, passwd)
        
    def __del__(self):
        """
        Take care of deleting the cookie file
        """
        
        if os.path.exists(self.__cookiefile):
            os.unlink(self.__cookiefile)
        
        
    def __enter__(self):
        """
        Allow with statement
        """
        return self
    
    def __exit__(self, *args, **kwargs):
        """
        Allow with statement
        """
        self.__csrf = None
        
    def __login(self, user, passwd):
        """
        Login action
        
        :param user: Sympa server username
        :param passwd: Sympa server password
        """
        
        self.get()
        self.post(postargs={'action_login': 'Login'})
        self.post(postargs={'action_login': 'Login',
                            'submit': 'submit',
                            'action': 'login',
                            'email': user,
                            'passwd': passwd,
                            })
        
        
        
    def get(self, args={}, *, url=None):
        """
        Patched get function to recover csrf automatically
        """
        
        payload = super().get(self.__url if url is None else url, args, self.__cookiefile)
        self.__csrf = self.get_sympa_csrf(payload.decode('utf-8'))
        return payload
        
    def post(self, getargs={}, postargs={}, *, url=None):
        """
        Patched post method. Add automatically csrftoken.
        """
        
        postargs['csrftoken'] = self.__csrf
        payload = super().post(self.__url if url is None else url, getargs, postargs, self.__cookiefile)
        self.__csrf = self.get_sympa_csrf(payload.decode('utf-8'))
        return payload
        
    
    def get_sympa_csrf(self, payload):
        """
        Analyse payload and return csrf token
        """
        
        return self.get_hidden_field(payload, "csrftoken")


    def sympa_bulk_subscribe(self, emails, ml):
        """
        Bulk subscription of emails
        """
        
        self.post(postargs={'list': ml,
                           'action_import': 'Ajout+d\'abonnés',
                           #'quiet': 1,
                           'dump': '\n'.join(emails),
                           })


    def sympa_sub_unsub(self, email, sub, unsub):
        """
        Subscribe an email to mailing list in sub, and unsubscribe from unsub
        """
        
        for ml in sub:
            
            self.post(postargs={'list': ml,
                           'action_import': 'Ajout+d\'abonnés',
                           #'quiet': 1,
                           'dump': email,
                           })
            
        for ml in unsub:
            self.post(postargs={'list': ml,
                               'action': 'del',
                               #'quiet': 1,
                               'email': email,
                               'action_del': "Désabonner+les+adresses+sélectionnées"
                               })
                
            self.post(postargs={'list': ml,
                               'action': 'del',
                               'family': "",
                               #'quiet': 1,
                               'email': email,
                               "response_action_confirm": "Confirmer",
                               })
