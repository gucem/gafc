from django.test import TestCase
from django.core.exceptions import ValidationError

from datetime import timedelta
from datetime import datetime as dt
from django.utils import timezone

from ..models import *

class TestModelsPersons(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        cls.user0 = Person.objects.create_user('user0@gafc.fr', 'MySuperPassword')
        cls.user1 = Person.objects.create_user('user1@gafc.fr', 'MySuperPassword',
                                               first_name='First',
                                               last_name='Last',
                                               gender='M',
                                               street='Rue du Sport, 1',
                                               city='Grenoble',
                                               country='France',
                                               phone='000-000')
        
        cls.suser2 = Person.objects.create_superuser('superuser@gafc.fr', 'RootPassword')
        cls.suser1 = Person.objects.create_superuser('superuser2@gafc.fr', 'RootPassword',
                                               first_name='First',
                                               last_name='Last',
                                               gender='F',
                                               street='Rue du Sport, 1',
                                               city='Grenoble',
                                               country='France',
                                               phone='000-000')
        
        cls.fede = Federation.objects.create(acronym='MSF', name='Ma Super Fédé')
        
        License.objects.create(person=cls.user1,
                               number='1234',
                               federation=cls.fede,
                               date_in='2020-09-01',
                               date_end='2021-09-30')
        
        
        License.objects.create(person=cls.user1,
                               number='1234',
                               federation=cls.fede,
                               date_in='2021-09-01',
                               date_end='2022-09-30')
        
    def test_user(self):
        self.assertIs(self.user1.is_superuser, False)
        self.assertIs(self.user1.has_usable_password(), True)
        self.assertEqual(self.user1.address, "%s\n%s %s\n%s"%(self.user1.street, self.user1.postcode, self.user1.city, self.user1.country))
        self.assertEqual(self.user1.first_name, 'First')
        self.assertEqual(self.user1.last_name, 'Last')
        self.assertEqual(self.user1.gender, 'M')
        
    def test_user_default(self):
        self.assertEqual(self.user0.first_name, 'user')
        self.assertEqual(self.user0.gender, 'X')
        self.assertIs(self.user0.is_staff, False)
        
    def test_superuser(self):
        self.assertIs(self.suser1.is_superuser, True)
        self.assertIs(self.suser1.has_usable_password(), True)
        self.assertEqual(self.suser1.address, "%s\n%s %s\n%s"%(self.suser1.street, self.suser1.postcode, self.suser1.city, self.suser1.country))
        self.assertEqual(self.suser1.first_name, 'First')
        self.assertEqual(self.suser1.last_name, 'Last')
        self.assertEqual(self.suser1.gender, 'F')
        
    def test_superuser_default(self):
        self.assertEqual(self.suser2.first_name, 'superuser')
        self.assertEqual(self.suser2.gender, 'X')
        self.assertIs(self.suser2.is_staff, True)
        
    def test_user_check_mail(self):
        E = False
        
        try:
            p = Person.objects.create_user("nonvalid_email", "password")
        except ValidationError:
            E = True
            
        self.assertIs(E, True)
        
    def test_superuser_check_mail(self):
        E = False
        
        try:
            p = Person.objects.create_superuser("nonvalid_email", "passworsd")
        except ValidationError:
            E = True
            
        self.assertIs(E, True)
        
    def test_superuser_check_password(self):
        E = False
        
        try:
            p = Person.objects.create_superuser("email@gafc.be", None)
        except ValueError:
            E = True
            
        self.assertIs(E, True)
        
    def test_licences(self):
        
        self.assertIs(self.user1.valid_license('2020-08-29', '2020-08-30'), None)
        self.assertIs(self.user1.valid_license('2020-08-29', '2020-09-02'), None)
        self.assertEqual(self.user1.valid_license('2020-09-01', '2020-09-10').count(), 1)
        self.assertEqual(self.user1.valid_license('2021-01-01', '2021-02-10').count(), 1)
        self.assertEqual(self.user1.valid_license('2021-08-31', '2021-09-01').count(), 1)
        self.assertEqual(self.user1.valid_license('2021-09-01', '2021-09-01').count(), 2)
        self.assertEqual(self.user1.valid_license('2021-09-30', '2021-10-01').count(), 1)
        self.assertEqual(self.user1.valid_license('2022-09-30', '2022-09-30').count(), 1)
        self.assertIs(self.user1.valid_license('2022-09-29', '2022-10-01'), None)
        self.assertIs(self.user1.valid_license('2022-10-01', '2022-10-01'), None)
        
    def test_licences_dates(self):
        E = False
        
        try:
            l = License.objects.create(person=self.user1,
                                    number="123456",
                                    federation=self.fede,
                                    date_in="2020-09-30",
                                    date_end="2020-09-01"
                                    )
        except ValueError as err:
            E = err.args[0] == 'End date is before adhesion date'
            
        self.assertIs(E, True)
