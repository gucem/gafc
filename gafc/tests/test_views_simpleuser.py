

from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser
from datetime import timedelta
from datetime import datetime as dt
from django.utils import timezone
from .models import *

class SimpleUser(TestCase):
    """
    Non logged users have access only to loggin page
    """
    def setUp(self):
        self.president = Person.objects.create_superuser('president@gafc.be', 'MySuperPassword')
        self.john = Person.objects.create_user('john@gafc.be', 'MySuperPassword')
        self.alice = Person.objects.create_user('alice@gafc.be')
        self.bob = Person.objects.create_user('bob@gafc.be')
        
        self.client = Client()
        self.client.login(username='john@gafc.be', password="MySuperPassword")
        
        self.season_2019 = Season.objects.create(name="2019-2020")
        self.season_2020 = Season.objects.create(name="2020-2021")
        
        self.category_speleo = Category.objects.create(name="Spéléo", is_global=False)
        self.category_canyon = Category.objects.create(name="Canyon", is_global=False)
        
        self.event_not_open = Event.objects.create(title="NotOpenEvent",
                                                   begin=dt.now(tz=timezone.utc),
                                                   finish=dt.now(tz=timezone.utc) + timedelta(days=2),
                                                   category=self.category_speleo,
                                                   description="",
                                                   link=False,
                                                   cost=10.,
                                                   season=self.season_2019)
        
        self.event_open_link = Event.objects.create(title="OpenEventWithLink",
                                                   begin=dt.now(tz=timezone.utc),
                                                   finish=dt.now(tz=timezone.utc) + timedelta(days=2),
                                                   category=self.category_canyon,
                                                   description="",
                                                   link=True,
                                                   cost=10.,
                                                   season=self.season_2019)
        
        EventState.objects.create(event=self.event_open_link, state='OP', person=self.president)
        
        self.event_open_nolink = Event.objects.create(title="OpenEventWithoutLink",
                                                   begin=dt.now(tz=timezone.utc),
                                                   finish=dt.now(tz=timezone.utc) + timedelta(days=2),
                                                   category=self.category_canyon,
                                                   description="",
                                                   link=False,
                                                   cost=10.,
                                                   season=self.season_2019)
        
        EventState.objects.create(event=self.event_open_nolink, state='OP', person=self.president)
        
        self.event_validated_pa = Event.objects.create(title="ValidatedEventWithParticipation",
                                                   begin=dt.now(tz=timezone.utc),
                                                   finish=dt.now(tz=timezone.utc) + timedelta(days=2),
                                                   category=self.category_speleo,
                                                   description="",
                                                   link=False,
                                                   cost=10.,
                                                   season=self.season_2019)
                                                   
        EventState.objects.create(event=self.event_validated_pa, state='OP', person=self.president)
        Participation.objects.create(person=self.john, event=self.event_validated_pa)
        
        EventState.objects.create(event=self.event_validated_pa, state='VA', person=self.president)
        
        self.event_validated_npa = Event.objects.create(title="ValidatedEventWithoutParticipation",
                                                   begin=dt.now(tz=timezone.utc),
                                                   finish=dt.now(tz=timezone.utc) + timedelta(days=2),
                                                   category=self.category_speleo,
                                                   description="",
                                                   link=False,
                                                   cost=10.,
                                                   season=self.season_2019)
                                                   
        EventState.objects.create(event=self.event_validated_npa, state='OP', person=self.president)
        EventState.objects.create(event=self.event_validated_npa, state='VA', person=self.president)
        
                                            
    
    def test_login_view(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_login_action_ok(self):
        self.client.logout()
        url = reverse('login')
        
        response = self.client.post(url, {'whattodo': 'login', 'email': 'john@gafc.be', 'password': 'MySuperPassword'}, follow=True)

        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1], 302)
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.wsgi_request.user, self.john)
        self.assertTemplateUsed(response, 'gafc/home.html')
        
    def test_login_action_error(self):
        self.client.logout()
        url = reverse('login')
        
        response = self.client.post(url, {'whattodo': 'login', 'email': 'john@gafc.be', 'password': 'MySuperPasword'}, follow=True)

        self.assertEqual(len(response.redirect_chain),0)
        self.assertContains(response, 'Mauvais nom d\'utilisateur/mot de passe')
        self.assertEqual(response.context['error_txt'], 'Mauvais nom d\'utilisateur/mot de passe')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.wsgi_request.user.__class__, AnonymousUser)
        self.assertTemplateUsed(response, 'gafc/login.html')
        
    def test_logout_view(self):
        url = reverse('logout')
        response = self.client.get(url)
        self.assertContains(response, 'Vous êtes maintenant déconnecté')
        self.assertEqual(response.context['success_txt'], 'Vous êtes maintenant déconnecté')
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.wsgi_request.user.__class__, AnonymousUser)
        
    # Authorized views for every logged users
    def test_home(self):
        url = reverse('home')
        response = self.client.get(url)
        
        self.assertContains(response, self.event_open_link.title)
        self.assertNotContains(response, self.event_open_nolink.title)
        self.assertContains(response, self.event_validated_pa.title)
        self.assertNotContains(response, self.event_validated_npa.title)
        self.assertTemplateUsed(response, 'gafc/home.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event_participation1(self):
        url = reverse('event_participation', kwargs={'event_id': self.event_not_open.id})
        response = self.client.get(url)
        
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
    
    def test_event_participation2(self):
        url = reverse('event_participation', kwargs={'event_id': self.event_open_link.id})
        response = self.client.get(url)
        
        self.assertTemplateUsed(response, 'gafc/event_not_staff.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event_participation3(self):
        url = reverse('event_participation', kwargs={'event_id': self.event_open_nolink.id})
        response = self.client.get(url)
        
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_event_participation4(self):
        url = reverse('event_participation', kwargs={'event_id': self.event_validated_pa.id})
        response = self.client.get(url)
        
        self.assertTemplateUsed(response, 'gafc/event_not_staff.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event_participation5(self):
        url = reverse('event_participation', kwargs={'event_id': self.event_validated_npa.id})
        response = self.client.get(url)
        
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)

    # For all other views, check the error handler and 403 error
        
    def test_set_season(self):
        url = reverse('set_season')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_persons(self):
        url = reverse('list_persons')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_persons2(self):
        url = reverse('list_persons', kwargs={'page': 0})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_persons3(self):
        url = reverse('list_persons', kwargs={'name_first_letter': 'a'})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_persons4(self):
        url = reverse('list_persons', kwargs={'page': 0,'name_first_letter': 'a'})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_person(self):
        url = reverse('person', kwargs={'person_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_import_members(self):
        url = reverse('import_members')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
    
    def test_list_events(self):
        url = reverse('list_events')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_event1(self):
        url = reverse('event', kwargs={'event_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_event2(self):
        url = reverse('event_del_part', kwargs={'event_id': 2, 'part_id': 5})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_event3(self):
        url = reverse('event_del_eq', kwargs={'event_id': 2, 'eq_id': 6})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_event4(self):
        url = reverse('event_del_meta', kwargs={'event_id': 2, 'meta_id': 3})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_category(self):
        url = reverse('list_category')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_category(self):
        url = reverse('category', kwargs={'category_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_financial(self):
        url = reverse('financial')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_payment(self):
        url = reverse('payment', kwargs={'payment_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_transfer(self):
        url = reverse('transfer', kwargs={'transfer_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_equipment(self):
        url = reverse('list_equipment')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_list_equipment2(self):
        url = reverse('list_equipment', kwargs={'catid': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_equipment(self):
        url = reverse('equipment', kwargs={'equipment_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_equipment_grp1(self):
        url = reverse('equipment', kwargs={'eqgrp_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_equipment_grp1(self):
        url = reverse('equipment_grp', kwargs={'eqgrp_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_equipment_grp2(self):
        url = reverse('equipment_grp_del', kwargs={'eqgrp_id': 2, 'eq_id': 5})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    def test_settings(self):
        url = reverse('settings')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),0)
        self.assertTemplateUsed(response, 'gafc/error_handler.html')
        self.assertEqual(response.status_code, 403)
        
    
