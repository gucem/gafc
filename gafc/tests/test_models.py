from django.test import TestCase
from django.core.exceptions import ValidationError

from datetime import timedelta
from datetime import datetime as dt
from django.utils import timezone

from ..models import *

# Create your tests here.

class test_Season(TestCase):   
    def test_first_Season_auto_default(self):
        s = Season.objects.create(name="2019-2020", is_default=False)
        
        self.assertIs(s.is_default, True)
        
    def test_next_Season_auto_update_default(self):
        Season.objects.create(name="2019-2020", is_default=True)
        Season.objects.create(name="2020-2021", is_default=True)
        
        s0 = Season.objects.get(name="2019-2020")
        s1 = Season.objects.get(name="2020-2021")
        
        self.assertIs(s0.is_default, False)
        self.assertIs(s1.is_default, True)
  

class test_Participation(TestCase):
    def test_normal_behaviour(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e)
    
    def test_validated_event(self):
        E = False
        try:
            c = Category.objects.create(name="Canyon")
            s = Season.objects.create(name="2019-2020")
            p = Person.objects.create_user("user@gafc.be", None)
            e = Event.objects.create(title="Event",
                                     category=c,
                                     season=s,
                                     begin=dt.now(tz=timezone.utc),
                                     finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                     cost=0.)
            
            EventState.objects.create(event=e, state='VA', person=p)
            
            pp = Participation.objects.create(person=p, event=e)
            
            
        except ValueError as err:
           E = err.args[0] == 'Event can\'t be changed after validation'
           
        self.assertIs(E, True)
        
    def test_non_open_event(self):
        E = False
        try:
            c = Category.objects.create(name="Canyon")
            s = Season.objects.create(name="2019-2020")
            p = Person.objects.create_user("user@gafc.be", None)
            e = Event.objects.create(title="Event",
                                     category=c,
                                     season=s,
                                     begin=dt.now(tz=timezone.utc),
                                     finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                     cost=0.)
            
            pp = Participation.objects.create(person=p, event=e)
            
            
        except ValueError as err:
           E = err.args[0] == 'Event is not open to new participant'
           
        self.assertIs(E, True)
        
    def test_valid_license(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        f = Federation.objects.create(name='Federation GUC', acronym='GUC')
        l = License.objects.create(person=p,number="0001",federation=f, date_in=dt.now(tz=timezone.utc).date(), date_end=(dt.now(tz=timezone.utc) + timedelta(days=10)))
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e)
        
        self.assertIs(pp.has_valid_license, True)
        self.assertEqual(pp.valid_license[0], l)
        
    def test_invalid_license1(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        f = Federation.objects.create(name='Federation GUC', acronym='GUC')
        l = License.objects.create(person=p,number="0001",federation=f, date_in=dt.now(tz=timezone.utc).date(), date_end=dt.now(tz=timezone.utc).date())
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e)
        
        self.assertIs(pp.has_valid_license, False)
        self.assertIs(pp.valid_license, None)
        
    def test_invalid_license2(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        f = Federation.objects.create(name='Federation GUC', acronym='GUC')
        l = License.objects.create(person=p,number="0001",federation=f, date_in=(dt.now(tz=timezone.utc) + timedelta(days=1)).date(), date_end=(dt.now(tz=timezone.utc) + timedelta(days=10)))
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e)
        
        self.assertIs(pp.has_valid_license, False)
        self.assertIs(pp.valid_license, None)
        
    def test_valid_staff1(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        st = Staff.objects.create(person=p, category=c, season=s, level='3PR')
        va = Validation.objects.create(staff=st, validator=p, date=dt.now(tz=timezone.utc).date())
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e, role='3PR')
        
        self.assertIs(pp.is_role_valid, True)
        self.assertIs('3PR' in pp.available_roles, True)
        self.assertIs('2SU' in pp.available_roles, True)
        self.assertIs('1CO' in pp.available_roles, True)
        self.assertIs('0PA' in pp.available_roles, True)
        
        pp.role = '2SU'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
        pp.role = '1CO'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
        pp.role = '0PA'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
    def test_valid_staff2(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        st = Staff.objects.create(person=p, category=c, season=s, level='2SU')
        va = Validation.objects.create(staff=st, validator=p, date=dt.now(tz=timezone.utc).date())
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e, role='3PR')
        
        self.assertIs(pp.is_role_valid, False)
        self.assertIs('3PR' in pp.available_roles, False)
        self.assertIs('2SU' in pp.available_roles, True)
        self.assertIs('1CO' in pp.available_roles, True)
        self.assertIs('0PA' in pp.available_roles, True)
        
        pp.role = '2SU'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
        pp.role = '1CO'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
        pp.role = '0PA'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
    def test_valid_staff3(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        st = Staff.objects.create(person=p, category=c, season=s, level='1CO')
        va = Validation.objects.create(staff=st, validator=p, date=dt.now(tz=timezone.utc).date())
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e, role='3PR')
        
        self.assertIs(pp.is_role_valid, False)
        self.assertIs('3PR' in pp.available_roles, False)
        self.assertIs('2SU' in pp.available_roles, False)
        self.assertIs('1CO' in pp.available_roles, True)
        self.assertIs('0PA' in pp.available_roles, True)
        
        pp.role = '2SU'
        pp.save()
        
        self.assertIs(pp.is_role_valid, False)
        
        pp.role = '1CO'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
        pp.role = '0PA'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
    def test_nonvalid_staff(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        st = Staff.objects.create(person=p, category=c, season=s, level='3PR')
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e, role='3PR')
        
        self.assertIs(pp.is_role_valid, False)
        self.assertIs('3PR' in pp.available_roles, False)
        self.assertIs('2SU' in pp.available_roles, False)
        self.assertIs('1CO' in pp.available_roles, False)
        self.assertIs('0PA' in pp.available_roles, True)
        
        pp.role = '2SU'
        pp.save()
        
        self.assertIs(pp.is_role_valid, False)
        
        pp.role = '1CO'
        pp.save()
        
        self.assertIs(pp.is_role_valid, False)
        
        pp.role = '0PA'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)
        
    def test_nonvalidated(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        p = Person.objects.create_user("user@gafc.be", None)
        st = Staff.objects.create(person=p, category=c, season=s, level='3PR')
        va = Validation.objects.create(staff=st, validator=p, date=(dt.now(tz=timezone.utc) + timedelta(days=-10)).date(), date_end=(dt.now(tz=timezone.utc) + timedelta(days=-5)).date())
        e = Event.objects.create(title="Event",
                                    category=c,
                                    season=s,
                                    begin=dt.now(tz=timezone.utc),
                                    finish=dt.now(tz=timezone.utc) + timedelta(days=1),
                                    cost=0.)
        
        EventState.objects.create(event=e, state='OP', person=p)
        
        pp = Participation.objects.create(person=p, event=e, role='3PR')
        
        self.assertIs(pp.is_role_valid, False)
        self.assertIs('3PR' in pp.available_roles, False)
        self.assertIs('2SU' in pp.available_roles, False)
        self.assertIs('1CO' in pp.available_roles, False)
        self.assertIs('0PA' in pp.available_roles, True)
        
        pp.role = '2SU'
        pp.save()
        
        self.assertIs(pp.is_role_valid, False)
        
        pp.role = '1CO'
        pp.save()
        
        self.assertIs(pp.is_role_valid, False)
        
        pp.role = '0PA'
        pp.save()
        
        self.assertIs(pp.is_role_valid, True)

class test_EventMeta(TestCase):
    pass

class test_EventMetaData(TestCase):
    pass

class test_ParticipationMetaData(TestCase):
    def test_metadata_event_check(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        pers = Person.objects.create_user("user@gafc.be", None)
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        e1 = Event.objects.create(title="Event 2",
                                 category=c,
                                 season=s,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        EventState.objects.create(event=e1, state='OP', person=pers)
        
        em = EventMeta.objects.create(name='EventMeta', datatype='ST')
        emd = EventMetaData.objects.create(meta=em, event=e0, cost=0.)
        
        p = Participation.objects.create(person=pers, event=e1)
        E = False
        
        try:
            ParticipationMetaData.objects.create(eventMeta = emd, participation=p, data="data")
        except ValueError as err:
            E = err.args[0] == 'Event in participation and metadata is not the same'
            
        self.assertIs(E, True)
        
    def test_metadata_event_check2(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        pers = Person.objects.create_user("user@gafc.be", None)
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        EventState.objects.create(event=e0, state='OP', person=pers)
        
        em = EventMeta.objects.create(name='EventMeta', datatype='ST')
        emd = EventMetaData.objects.create(meta=em, event=e0, cost=0.)
        
        p = Participation.objects.create(person=pers, event=e0)
        
        ParticipationMetaData.objects.create(eventMeta = emd, participation=p, data="data")

class test_Cashing(TestCase):
    pass

class test_Payment(TestCase):
    def test_payment_cashing(self):
        c = Category.objects.create(name="Canyon")
        s0 = Season.objects.create(name="2019-2020")
        s1 = Season.objects.create(name="2020-2021")
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        pers = Person.objects.create_user("user@gafc.be", None)
        
        cash = Cashing.objects.create(person=pers, comment="Cmt", season=s1)
        E = False
        try:
            pay = Payment.objects.create(amount=10.,
                                        method="CA",
                                        person=pers,
                                        encoder=pers,
                                        category=c,
                                        event=e0,
                                        cashing=cash,
                                        season=s0)
        except ValueError as err:
            E = err.args[0] == 'Cashing and payment must be related to same season'
            
        self.assertIs(E, True)
        
    def test_payment(self):
        c = Category.objects.create(name="Canyon")
        s = Season.objects.create(name="2019-2020")
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        pers = Person.objects.create_user("user@gafc.be", None)
        
        cash = Cashing.objects.create(person=pers, comment="Cmt", season=s)
        
        pay = Payment.objects.create(amount=10.,
                                     method="CA",
                                     person=pers,
                                     encoder=pers,
                                     category=c,
                                     event=e0,
                                     cashing=cash,
                                     season=s)
            
                                     
    def test_payment_event(self):
        c = Category.objects.create(name="Canyon")
        s0 = Season.objects.create(name="2019-2020")
        s1 = Season.objects.create(name="2020-2021")
        pers = Person.objects.create_user("user@gafc.be", None)
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s1,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        
        cash = Cashing.objects.create(person=pers, comment="Cmt", season=s0)
        
        try:
            pay = Payment.objects.create(amount=10.,
                                        method="CA",
                                        person=pers,
                                        encoder=pers,
                                        category=c,
                                        event=e0,
                                        cashing=cash,
                                        season=s0)
        except ValueError as err:
            E = err.args[0] == 'Event and payment must be related to same season'
            
        self.assertIs(E, True)
        
    def test_payment_validated_event(self):
        c = Category.objects.create(name="Canyon")
        s0 = Season.objects.create(name="2019-2020")
        pers = Person.objects.create_user("user@gafc.be", None)
        e0 = Event.objects.create(title="Event 1",
                                 category=c,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        EventState.objects.create(event=e0, state='VA', person=pers)
        
        try:
            pay = Payment.objects.create(amount=10.,
                                        method="CA",
                                        person=pers,
                                        encoder=pers,
                                        category=c,
                                        event=e0,
                                        season=s0)
        except ValueError as err:
            E = err.args[0] == 'Event is validated: can\'t add payment'
            
        self.assertIs(E, True)

class test_InternalPayment(TestCase):
    def test_internalPayment(self):
        c0 = Category.objects.create(name="Canyon")
        c1 = Category.objects.create(name="BVC")
        s0 = Season.objects.create(name="2019-2020")
        pers = Person.objects.create_user("user@gafc.be", None)
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        
        pay = InternalPayment.objects.create(amount=10.,
                                             method="CA",
                                             person=pers,
                                             event=e,
                                             category_c=c0,
                                             category_d=c1,
                                             season=s0)
        
    def test_internalPayment_category(self):
        c0 = Category.objects.create(name="Canyon")
        c1 = Category.objects.create(name="BVC")
        s0 = Season.objects.create(name="2019-2020")
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        pers = Person.objects.create_user("user@gafc.be", None)
        
        E=False
        try:
            pay = InternalPayment.objects.create(amount=10.,
                                                method="CA",
                                                person=pers,
                                                event=e,
                                                category_c=c0,
                                                category_d=c0,
                                                season=s0)
        except ValueError as err:
            E = err.args[0] == 'Creditor and Debitor category are the same'
            
        self.assertIs(E, True)
        
    def test_internalPayment_season(self):
        c0 = Category.objects.create(name="Canyon")
        c1 = Category.objects.create(name="BVC")
        s0 = Season.objects.create(name="2019-2020")
        s1 = Season.objects.create(name="2020-2021")
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        pers = Person.objects.create_user("user@gafc.be", None)
        
        E=False
        try:
            pay = InternalPayment.objects.create(amount=10.,
                                                method="CA",
                                                person=pers,
                                                event=e,
                                                category_c=c0,
                                                category_d=c1,
                                                season=s1)
        except ValueError as err:
            E = err.args[0] == 'Event and payment must be related to same season'
            
        self.assertIs(E, True)
        
    def test_internalPayment_event_validated(self):
        c0 = Category.objects.create(name="Canyon")
        c1 = Category.objects.create(name="BVC")
        s0 = Season.objects.create(name="2019-2020")
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)        
        pers = Person.objects.create_user("user@gafc.be", None)
        EventState.objects.create(event=e, state='VA', person=pers)
        
        E=False
        try:
            pay = InternalPayment.objects.create(amount=10.,
                                                method="CA",
                                                person=pers,
                                                event=e,
                                                category_c=c0,
                                                category_d=c1,
                                                season=s0)
        except ValueError as err:
            E = err.args[0] == 'Event is validated: can\'t add payment'
            
        self.assertIs(E, True)

class test_Equipment(TestCase):
    def test_normal_behaviour0(self):
        c0 = Category.objects.create(name="Speleo")
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        eq0.is_scrap = True
        eq0.save()
        
    def test_normal_behaviour1(self):
        c0 = Category.objects.create(name="Speleo")
        eqg = EquipmentGroup.objects.create(name="Baudrier 1",
                                      description="Baudrier avec 1 simple, 3 bloqueurs, 1 pédale")
        
        eq0 = Equipment.objects.create(name="Simple Baudrier 1",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now(),
                                      belong_to=eqg)
        
        eq0.is_scrap = True
        eq0.save()
        
    def test_scrap(self):
        c0 = Category.objects.create(name="Speleo")
        pers = Person.objects.create_user("user@gafc.be", None)
        
        eq1 = Equipment.objects.create(name="Simple",
                                    reference="05462",
                                    description="Simple descendeur spéléo",
                                    category=c0,
                                    date_buy=dt.now(),
                                    belong_to=None)
        
        eq1.date_scrap=dt.now()
        eq1.save()
        
        E = False
        try:
            eq1.save()
        except ValueError as err:
            E = err.args[0] == 'Can\'t change equipment after scrapping'
            
        self.assertIs(E, True)

class test_EquipmentTracking(TestCase):
    def test_equipment_tracking_normal(self):
        c0 = Category.objects.create(name="Speleo")
        s0 = Season.objects.create(name="2019-2020")
        pers = Person.objects.create_user("user@gafc.be", None)
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        tr = EquipmentTracking.objects.create(equipment=eq0,
                                              event=e,
                                              comment="Cmt",
                                              person=pers)
        
    def test_equipment_tracking_validated_event(self):
        s0 = Season.objects.create(name="2019-2020")
        c0 = Category.objects.create(name="Speleo")
        pers = Person.objects.create_user("user@gafc.be", None)
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        EventState.objects.create(event=e, state='VA', person=pers)
        E = False
        try:
            tr = EquipmentTracking.objects.create(equipment=eq0,
                                              event=e,
                                              comment="Cmt",
                                              person=pers)
        
        except ValueError as err:
            E = err.args[0] == 'Can\'t add an equipment to validated event'
            
        self.assertIs(E, True)
        
    def test_equipment_tracking_scrap(self):
        s0 = Season.objects.create(name="2019-2020")
        c0 = Category.objects.create(name="Speleo")
        pers = Person.objects.create_user("user@gafc.be", None)
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now(),
                                      date_scrap=dt.now())
        
        E = False
        try:
            tr = EquipmentTracking.objects.create(equipment=eq0,
                                              comment="Cmt",
                                              person=pers)
        
        except ValueError as err:
            E = err.args[0] == 'Can\'t add tracking to scrap equipment'
            
        self.assertIs(E, True)

class test_EquipmentMeta(TestCase):
    pass

class test_EquipmentMetaData(TestCase):
    def test_equipment_meta_data(self):
        c0 = Category.objects.create(name="Speleo")
        pers = Person.objects.create_user("user@gafc.be", None)
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        tr = EquipmentTracking.objects.create(equipment=eq0,
                                              event=None,
                                              comment="Cmt",
                                              person=pers)
        
        meta = EquipmentMeta.objects.create(equipment=eq0,
                                            name="Poulies",
                                            datatype="IN")
        
        metadata = EquipmentMetaData.objects.create(tracking=tr,
                                                    meta=meta,
                                                    data="2")
        
    def test_equipment_meta_data_equipment_notsame(self):
        s0 = Season.objects.create(name="2019-2020")
        c0 = Category.objects.create(name="Speleo")
        pers = Person.objects.create_user("user@gafc.be", None)
        eq0 = Equipment.objects.create(name="Simple",
                                      reference="458265315-2",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        eq1 = Equipment.objects.create(name="Simple",
                                      reference="458265315-3",
                                      description="Simple: descendeur Spéléo",
                                      category=c0,
                                      date_buy=dt.now())
        
        e = Event.objects.create(title="Event 1",
                                 category=c0,
                                 season=s0,
                                 begin=dt.now(tz=timezone.utc),
                                 finish=dt.now(tz=timezone.utc) + timedelta(hours=8),
                                 cost=0.)
        
        
        tr = EquipmentTracking.objects.create(equipment=eq0,
                                              event=e,
                                              comment="Cmt",
                                              person=pers)
        
        meta = EquipmentMeta.objects.create(equipment=eq1,
                                            name="Poulies",
                                            datatype="IN")
        
        EventState.objects.create(event=e, state='VA', person=pers)
        
        E=False
        try:
            metadata = EquipmentMetaData.objects.create(tracking=tr,
                                                        meta=meta,
                                                        data="2")
        except ValueError as err:
            E = err.args[0] == 'Tracking and Meta Equipment are not the same'
            
        self.assertIs(E, True)
