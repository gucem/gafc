

from django.test import TestCase, Client
from django.urls import reverse
from datetime import datetime as dt

class NonLoggedUser(TestCase):
    """
    Non logged users have access only to loggin page
    """
    def test_login_view(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_logout_view(self):
        url = reverse('logout')
        response = self.client.get(url)
        self.assertContains(response, 'Vous êtes maintenant déconnecté')
        self.assertEqual(response.context['success_txt'], 'Vous êtes maintenant déconnecté')
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)

    # For all other views, check the redirection to login page
    def test_home(self):
        url = reverse('home')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_set_season(self):
        url = reverse('set_season')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_persons(self):
        url = reverse('list_persons')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_persons2(self):
        url = reverse('list_persons', kwargs={'page': 0})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_persons3(self):
        url = reverse('list_persons', kwargs={'name_first_letter': 'a'})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_persons4(self):
        url = reverse('list_persons', kwargs={'page': 0,'name_first_letter': 'a'})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_person(self):
        url = reverse('person', kwargs={'person_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_import_members(self):
        url = reverse('import_members')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
    
    def test_list_events(self):
        url = reverse('list_events')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event1(self):
        url = reverse('event', kwargs={'event_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event2(self):
        url = reverse('event_del_part', kwargs={'event_id': 2, 'part_id': 5})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event3(self):
        url = reverse('event_del_eq', kwargs={'event_id': 2, 'eq_id': 6})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event4(self):
        url = reverse('event_del_meta', kwargs={'event_id': 2, 'meta_id': 3})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_event_participation(self):
        url = reverse('event_participation', kwargs={'event_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_category(self):
        url = reverse('list_category')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_category(self):
        url = reverse('category', kwargs={'category_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_financial(self):
        url = reverse('financial')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_payment(self):
        url = reverse('payment', kwargs={'payment_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_transfer(self):
        url = reverse('transfer', kwargs={'transfer_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_equipment(self):
        url = reverse('list_equipment')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_list_equipment2(self):
        url = reverse('list_equipment', kwargs={'catid': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_equipment(self):
        url = reverse('equipment', kwargs={'equipment_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_equipment_grp1(self):
        url = reverse('equipment', kwargs={'eqgrp_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_equipment_grp1(self):
        url = reverse('equipment_grp', kwargs={'eqgrp_id': 2})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_equipment_grp2(self):
        url = reverse('equipment_grp_del', kwargs={'eqgrp_id': 2, 'eq_id': 5})
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    def test_settings(self):
        url = reverse('settings')
        response = self.client.get(url, follow=True)
        
        self.assertEqual(len(response.redirect_chain),1)
        self.assertEqual(response.redirect_chain[0][1],302)
        self.assertTemplateUsed(response, 'gafc/login.html')
        self.assertEqual(response.status_code, 200)
        
    
