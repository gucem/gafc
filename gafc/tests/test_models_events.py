from django.test import TestCase
from django.core.exceptions import ValidationError

from datetime import timedelta
from datetime import datetime as dt
from django.utils import timezone

from ..models import *

class TestModelsEvents(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.suser = Person.objects.create_superuser('suser@gafc.fr', 'MySuperPassword')
        cls.user1 = Person.objects.create_user('user0@gafc.fr', 'MySuperPassword')
        cls.user2 = Person.objects.create_user('user1@gafc.fr', 'MySuperPassword')
        
        cls.cat = Category.objects.create(name='canyon')        
        cls.season = Season.objects.create(name='2020-2021')
        
        cls.event1 = Event.objects.create(title='Sortie pro',
                                         category=cls.cat,
                                         season=cls.season,
                                         begin='2020-05-01 08:00',
                                         finish='2020-05-01 18:00',
                                         place="Somewhere",
                                         cost=0.)
        
        EventState.objects.create(event=cls.event1, state='OP', comment='', person=cls.suser)
        
        
        cls.event2 = Event.objects.create(title='Sortie bénévole',
                                         category=cls.cat,
                                         season=cls.season,
                                         begin='2020-05-01 08:00',
                                         finish='2020-05-01 18:00',
                                         place="Elsewhere",
                                         cost=0.)
                                         
    def test_default_state(self):
        
        self.assertEqual(self.event1.current_state, 'OP')
        self.assertEqual(self.event2.current_state, 'CR')
        
    def test_comment_state(self):
        EventState.objects.create(event=self.event2, state='CO', comment='Comment', person=self.suser)
        self.assertEqual(self.event2.current_state, 'CR')
        
    def test_cancelled_state(self):
        self.assertIs(self.event2.is_cancelled, False)
        
        EventState.objects.create(event=self.event2, state='CN', comment='Comment', person=self.suser)
        self.assertEqual(self.event2.current_state, 'CN')
        
        self.assertIs(self.event2.is_cancelled, True)
        
        # Once cancelled, still marked as cancelled
        EventState.objects.create(event=self.event2, state='OP', comment='Comment', person=self.suser)
        self.assertEqual(self.event2.current_state, 'OP')
        
        self.assertIs(self.event2.is_cancelled, True)
        
    def test_begin_finish_time(self):
        E = False
        
        try:
            e = Event.objects.create(title="Event",
                                     category=self.cat,
                                     season=self.season,
                                     begin='2020-05-01 08:00',
                                     finish='2020-04-27 18:00',
                                     cost=0.)
        except ValueError as err:
           E = err.args[0] == 'Beginning must be before Finish'
            
        self.assertIs(E, True)
        
    def test_validated_event(self):
        E=False
        EventState.objects.create(event=self.event1, state='VA', person=self.suser)
        
        try:    
            self.event1.save()
        except ValueError as err:
           E = err.args[0] == 'Event can\'t be changed after validation'
           
        self.assertIs(E, True)
        
        E = False
        
        try:
            EventState.objects.create(event=self.event1, state='OP', person=self.suser)
        except ValueError as err:
           E = err.args[0] == 'Event can\'t be changed after validation'
           
        self.assertIs(E, True)
        
    def test_validation_noplace(self):
        
        E=False
        self.event1.place=''
        self.event1.save()
        
        try:
            EventState.objects.create(event=self.event1, state='VA', person=self.suser)
        except ValueError as err:
            E = err.args[0] == 'Vous devez définir le lieu pour valider l\'événement'
            
        self.assertIs(E, True)
        
    def test_formslug(self):
        self.event1.helloassoLink = 'http://helloassso.com/some/stuffs/form-slug'
        
        self.assertEqual(self.event1.formSlug, 'form-slug')
        
