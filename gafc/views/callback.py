# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Callbacks from Helloasso or xhr request
"""

from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.template.loader import render_to_string
from django.db import IntegrityError

import glob
from json import loads, dumps, JSONDecodeError
import dateutil.parser as dp

from ..models import HelloassoPayment, Event, EmailModel, Participation
from ..emails import process_email


class HelloassoCallbackView(View):
    """
    Helloasso callback view
    """

  
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        """
        Dispatcher. Used here to disable CSRF check
        """
        return super().dispatch(*args, **kwargs)
    
    def post(self, request):
        """
        Only POST method is allowed
        
        :param request: Request object
        """
        
        try:
            data = loads(request.body)
        except JSONDecodeError:
            raise PermissionDenied
    
        if 'data' not in data or 'eventType' not in data:
            raise PermissionDenied
        
        if data['eventType'] == 'Payment':
            try:
                HelloassoPayment.objects.create(amount  = data['data']['amount']/100.,
                                                email   = data['data']['payer']['email'],
                                                name    = "%s %s"%(data['data']['payer']['firstName'],data['data']['payer']['lastName']),
                                                formSlug= data['data']['order']['formSlug'],
                                                date    = dp.parse(data['data']['date']),
                                                hello_id= data['data']['id'],
                                                raw     = dumps(data['data']))
            except IntegrityError:
                pass
    
        return HttpResponse('OK')



class EventMailCallbackView(View):
    """
    Get/Set EventMail
    """
    
    is_staff = False
    """
    Available for all users
    """
  
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        """
        Dispatcher. Used here to disable CSRF check
        """
        return super().dispatch(*args, **kwargs)
    
    
    def post(self, request):
        """
        POST method
        
        :param request: Request object
        """
        
        if request.user.is_anonymous:
            raise PermissionDenied
        
        
        def RepresentsInt(s):
            try: 
                int(s)
                return True
            except ValueError:
                return False
            
        try:
                
            if request.POST is None or 'what' not in request.POST:
                raise PermissionDenied
                
            
            event = get_object_or_404(Event, pk=request.POST['event_id'])
            request.user.event_for_perms=event
            
            if not request.user.perms['view_staffemail']:
                raise PermissionDenied
            
            what = request.POST['what']
            
            if what == "save_new":
                if 'title' not in request.POST or 'content' not in request.POST:
                    raise PermissionDenied
                
                EmailModel.objects.create(title=request.POST['title'], content=request.POST['content'])
                return HttpResponse(dumps({
                        'saved': True
                        }), content_type='application/json')
            elif what == "save":
                if 'id' not in request.POST or 'title' not in request.POST or 'content' not in request.POST or not RepresentsInt(request.POST['id']):
                    raise PermissionDenied
                
                EmailModel.objects.filter(id=request.POST['id']).update(title=request.POST['title'], content=request.POST['content'])
                return HttpResponse(dumps({
                        'saved': True
                        }), content_type='application/json')
            elif what == "delete":
                if 'id' not in request.POST or not RepresentsInt(request.POST['id']):
                    raise PermissionDenied
                
                get_object_or_404(EmailModel,id=request.POST['id']).delete()
                return HttpResponse(dumps({
                        'saved': True
                        }), content_type='application/json')
            elif what == "preview":
                if 'event_id' not in request.POST or not RepresentsInt(request.POST['event_id']) or 'content' not in request.POST or 'title' not in request.POST:
                    raise PermissionDenied
                
                md, html, title = process_email(request.POST['content'], request, event, request.POST['title'])
                return HttpResponse(dumps({
                        'content_html': html,
                        'content_md': md,
                        'title': title
                        }), content_type='application/json')
            elif what == "load":
                if 'id' not in request.POST or 'event_id' not in request.POST:
                    raise PermissionDenied
                    
                print(request.POST)
                
                admin_mail = {}
                
                for f in glob.glob('gafc/templates/gafc/email/event_system/*.txt'):
                    fn = f.split('/')[-1]
                    admin_mail[fn] = '/'.join(f.split('/')[2:])
                    
                    print(admin_mail)
                
                mail_id = request.POST['id']
                
                if mail_id in admin_mail:
                    email = admin_mail[mail_id]
                    participations = Participation.objects.filter(event=event).order_by('waiting', '-role', 'id')
    
                    msg = render_to_string(email, {'event': event, 'participations': participations, 'request': request})
                    
                    print(msg)
                    
                    return HttpResponse(dumps({
                        'title': event.title,
                        'content': msg,
                        'admin': True,
                        'id': -1
                        }), content_type='application/json')
                else:
                    email = get_object_or_404(EmailModel, id=mail_id)
                    
                    print(email.content)
                    return HttpResponse(dumps({
                        'title': email.title,
                        'content': email.content,
                        'admin': False,
                        'id': mail_id
                        }), content_type='application/json')
        except PermissionDenied:
            return HttpResponse(dumps({'error': "403: Vous n'avez pas l'autorisation de faire ça."}), status=400, content_type='application/json')
            
        except Exception as e:
            return HttpResponse(dumps({'error': str(e)}), status=400, content_type='application/json')
        
        raise PermissionDenied
    
    
