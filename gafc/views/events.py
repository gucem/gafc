# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

from django.shortcuts import render, redirect, get_object_or_404

from django.http import Http404, HttpResponse

from django.core.exceptions import PermissionDenied

from django.db.models.functions import TruncMonth
from django.db.models import Prefetch
from django.db import transaction

from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.utils.decorators import method_decorator
 
from django.views.decorators.cache import cache_page

import os, glob

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

import datetime as dt
from dateutil.tz import tzutc

from ..forms import EventForm, EventStateForm, PersonAndLicenseForm, EventEquipmentForm, EventEquipmentGroupForm, DeleteEventEquipmentTrackingForm, PaymentForm, GafcDummyForm, DeleteEventValidationForm, DuplicateEventForm, SendEventMailForm, DeleteEventForm, EquipmentTrackingFormset, InternalPaymentForm, ParticipationRequestForm, ParticipationDeleteForm, FinancialCategoryWidget, EventLicenseForm
from ..models import Event, Season, Participation, EventState, EventMeta, EventMetaData, ParticipationMetaData, PersonComment, Payment, InternalPayment, EquipmentTracking, HelloassoPayment, EmailModel, MetaModel, Person, Category, FinancialCategory
from ..templatetags import gafc_tags as gtags

from .common import GafcFormView, GafcFormException, GafcPermView

from gucem import settings

"""

Events view:
    - Calendar
    - Admin view of event
    - User view of event
    - Event ICS calendar
    - Data export
"""


@method_decorator(login_required, name="dispatch")
class EventsView(GafcFormView):
    """
    Event calendar view
    """

    is_staff = False
    
    def setup(self, request, *args, **kwargs):
        """
        Setup the forms
        
        :param request: request object
        :param \\*args: request extra arguments
        :param \\*\\*kwargs: extra arguments
        """
        
        if request.user.has_perm('gafc.add_event'):
            self.add_form(EventForm, initial={'season': Season.objects.get(id=request.session['season_id'])})
        
        super().setup(request, *args, **kwargs)
    
    def get(self, request, year=None, month=None):
        """
        Calendar view of events
        
        :param request: request object
        :param year: Year to display
        :param month: Month to display
        """
        
        # Ensure to get a month-year
        if year is None or month is None:
            return redirect('list_events', year=dt.date.today().year, month=dt.date.today().month)
        
        firstdayofmonth = dt.datetime(year, month, 1)
        lastdayofmonth = firstdayofmonth.replace(year + 1 if firstdayofmonth.month == 12 else year, firstdayofmonth.month % 12 +1, day = 1)-dt.timedelta(seconds=1)
        
        # Get events
        events = Event.objects.select_related('category').prefetch_related('participation_set').prefetch_related('eventstate_set').exclude(begin__gt=lastdayofmonth).exclude(finish__lt=firstdayofmonth).order_by('-begin')
        
        events = events.filter(visibility__lte=1 if not request.user.is_staff else 2)
            
        # Order events in days of month
        events_per_day = dict()
        
        for e in events:
            d_beg = e.begin.day if e.begin.month == month and e.begin.year == year else 1
            d_end = e.finish.day if e.finish.month == month and e.finish.year == year else lastdayofmonth.day
            
            for d in range(d_beg, d_end+1):
                if d not in events_per_day:
                    events_per_day[d] = []
                    
                events_per_day[d] += [e, ]
            
        d = dt.date(year, month, 1)
        today = dt.date.today()
        weeks = []
        
        w=[]
        for i in range(d.weekday()):
            w += [{'date': None, 'events': None},]
        
        while d.month == month:
            w += [{'date': d, 'events': events_per_day[d.day] if d.day in events_per_day else []}]
            
            if d.weekday() == 6:
                weeks += [w, ]
                w = []
            
            d += dt.timedelta(days=1)
            
        if d.weekday() != 0:
            while d.weekday() != 0:
                w += [{'date': None, 'events': None},]
                d += dt.timedelta(days=1)
                
            weeks += [w, ]
            
        # Available months
        months = Event.objects.filter(season__id=request.session['season_id']).order_by('begin').annotate(Month=TruncMonth('begin')).values_list('Month').distinct()
        
        # User participations
        participations = Participation.objects.select_related('event','event__category','event__season').prefetch_related('event__eventstate_set').filter(person=request.user).exclude(event__eventtype='AU').order_by('event__season', '-event__begin')
        
        return render(request, 'gafc/events.html', locals())
    
    def form_save_event(self, request, *args, **kwargs):
        """
        Custom processing for form event
        
        :param request: request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        try:
            with transaction.atomic():
                # Save event
                e = self.forms['save_event'].save()
                
                # Add creation state
                EventState.objects.create(event=e,state='CR',comment="", person=request.user)
                
                messages.success(request, "Événement créé")
                return redirect('event', event_id=e.pk)
    
        except Exception as e:
            raise GafcFormException(e)
            
class EventLicenseView(GafcFormView):
    """
    Display a summary of licenses of participants, allow to create discovery licenses
    """
    
    specific_perms = ['gafc.view_person','gafc.add_license']
    
    def setup_forms(self, request, event_id):
        """
        Setup the licence creation form
        
        :param request: request object
        :param event_id: Event id
        
        :raise Http404: If event do not exists
        """
        
        request.event = get_object_or_404(Event, pk=event_id)
        
        self.add_form(EventLicenseForm, request.event)
        
    def get(self, request, event_id):
        """
        GET method
        
        :param request: request object
        :param event_id: Event id        
        """
        
        return render(request, "gafc/event_license.html", locals())
        
    
            
class EventView(GafcFormView):
    """
    Event admin view.
    v0.4: Drop support for WSGI. Permissions will be check according to global and event scheme.
    If permissions are missing, redirect to user view silently.
    """
    
    login_required = False
    """
    Redirect anonymous users
    """
    
    is_staff = False
    """
    Allow everyone to access this url.
    """
    
    def dispatch(self, request, event_id):
        """
        Dispatch method for Event. Check if user has right to see admin view, redirect to user view otherwise.
        
        :param request: request object
        :param event_id: event id
        
        :raises Http404: If event do not exists
        """
        
        if request.user.is_anonymous or (not request.user.is_staff and not request.user.perms['view_event']):
            return redirect('event_participation', event_id=event_id)
        
        # Some specific form settings
        if EventForm.what in self.forms:
            self.forms[EventForm.what].fields['season'].disabled = request.event.payment_set.count() > 0 or request.event.internalpayment_set.count() > 0
        
        
        return super().dispatch(request, event_id)
    
    def setup_forms(self, request, event_id, *args, **kwargs):
        """
        Setup the forms for event view
        
        :param request: request object
        :param event_id: event id
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """
        
        if request.user.is_anonymous:
            return
        
        # Recover event first
        try:
            e = Event.objects.prefetch_related('eventstate_set','eventstate_set__person','eventmetadata_set','eventmetadata_set__meta','eventmetadata_set__participationmetadata_set').get(pk=event_id)
            request.event = e
        except Event.DoesNotExist:
            raise Http404("L'événement n'existe pas!")
        
        # Set the event for event-based permission of user and check that user has right to see this
        request.user.event_for_perms = e
        
        if request.event.current_state != 'VA':            
            if request.user.perms['change_event']:
                self.add_form(EventForm, instance=request.event)
            
            if request.user.perms['add_eventstate']:
                self.add_form(EventStateForm, request.event, request.user)
                
            if request.event.current_state == 'OP' and request.user.perms['add_person'] and request.user.perms['add_participation']:
                self.add_form(PersonAndLicenseForm)
                
            if request.user.perms['add_equipmenttracking']:
                self.add_form(EventEquipmentForm, request.event, request.user)
                self.add_form(EventEquipmentGroupForm, request.event, request.user, request)
                self.add_form(EquipmentTrackingFormset, request.user, request.event)
                self.add_form(DeleteEventEquipmentTrackingForm, request.event)
                
            if request.user.perms['addevent_payment']:
                self.add_form(PaymentForm, event=request.event, user=request.user)
                self.add_form(GafcDummyForm, what='convert_hello', form_id='convert_hello')
                
            if request.user.perms['addevent_internalpayment']:
                self.add_form(InternalPaymentForm, event=request.event, user=request.user)

        else:
            if request.user.perms['delete_eventstate']:
                self.add_form(DeleteEventValidationForm, request.event)
                
        if request.user.perms['add_event']:
            self.add_form(DuplicateEventForm, request.event, request.user)
            
        if request.user.perms['view_staffemail']:
            self.add_form(SendEventMailForm, request.event, request.user, request)
            
        if request.event.can_delete and request.user.perms['delete_event']:
            self.add_form(DeleteEventForm, request.event)
        
    def get(self, request, *args, **kwargs):
        """
        GET admin view of event
        
        :param request: request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        e = request.event
    
        participations = Participation.objects.select_related('person','event').prefetch_related(Prefetch('event__eventmetadata_set',queryset=EventMetaData.objects.order_by('id')), 'participationmetadata_set','event__eventmetadata_set__meta','participationmetadata_set__eventMeta','participationmetadata_set__eventMeta__meta','person__license_set','person__license_set__federation','person__staff_set',Prefetch('person__personcomment_set',queryset=PersonComment.objects.all())).filter(event=e).order_by('waiting', 'cancelled', '-role', 'id')
        
        payments = Payment.objects.select_related('person','financial','category').filter(event=e).order_by('id')
        transfers = InternalPayment.objects.select_related('category_c','category_d','financial_c','financial_d').filter(event=e)
        avail_eventMeta = EventMeta.objects.prefetch_related(Prefetch('eventmetadata_set',queryset=EventMetaData.objects.order_by('-id'))).all()
        equipments = EquipmentTracking.objects.filter(event=e).order_by('equipment__name')
        
        if 'convert_hello' in self.forms:
            hello_payments = HelloassoPayment.objects.filter(formSlug=e.formSlug, deleted=False, payment=None)
            
        if request.user.perms['view_staffemail']:            
            admin_mail = {}
            
            for f in glob.glob('gafc/templates/gafc/email/event_system/*.txt'):
                fn = f.split('/')[-1]
                admin_mail[fn] = '/'.join(f.split('/')[2:])
                
            emails_custom = EmailModel.objects.all()
        
        totaux = dict()
        
        for p in payments:
            if p.refund_date is not None:
                continue
            
            if p.category.id not in totaux:
                totaux[p.category.id] = {'cat': p.category}
                
            if p.method not in totaux[p.category.id]:
                totaux[p.category.id][p.method] = {'N': 0, 't': 0.}
                
            totaux[p.category.id][p.method]['N']+=1
            totaux[p.category.id][p.method]['t']+=float(p.amount)
        
        is_async = os.environ.get('GAFC_RUN_MODE') == 'asgi'
        
        treeval = FinancialCategoryWidget.getFinlist()
        del treeval[0]
        
        metamodels = MetaModel.objects.all()
        
        return render(request, 'gafc/event.html', locals())
        
    def form_person_license_save(self, request, *args, **kwargs):
        """
        Specific handler for add_state form
        
        :param request: request object
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        
        :raises GafcFormException: If something went wrong
        """
        
        (person, l) = self.forms['person_license_save'].save()
        person.set_unusable_password()
        person.save()
        Participation.objects.create(person=person, event=request.event, role='0PA')
        
    def form_event_duplicate(self, request, *args, **kwargs):
        """
        Specific handler for event_duplicate form: redirect to newly created event
        
        :param request: request object
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """
        
        ne = self.forms['event_duplicate'].save()
        return redirect('event', event_id=ne.pk)
    
    def form_delete_event(self, request, *args, **kwargs):
        """
        Specific handler for event_duplicate form: redirect to newly created event
        
        :param request: request object
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """
        
        self.forms['delete_event'].save()
        return redirect('list_events')
    
    def form_convert_hello(self, request, *args, **kwargs):
        """
        Convert the Helloasso payments
        
        :param request: request object
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """

        with transaction.atomic():
            for i in request.POST.getlist('payment'):
                p = get_object_or_404(HelloassoPayment, pk=i)
                
                if p.payment is not None:
                    messages.error(request, ' Le paiement %s a déjà été encodé! '%(i))
                    continue
                
                try:
                    person = get_object_or_404(Person, pk=request.POST['hello-person-%s'%i])
                    category = get_object_or_404(Category, pk=request.POST['hello-category-%s'%i])
                    account = get_object_or_404(FinancialCategory, pk=request.POST['hello-account-%s'%i])
                
                    p.payment = Payment.objects.create(amount=p.amount,
                                        pay_done=p.date,
                                        method='IN',
                                        person=person,
                                        encoder=request.user,
                                        category=category,
                                        financial=account,
                                        event=request.event,
                                        season=request.event.season)
                    
                    p.save()
                except ValueError as err:
                    raise GafcFormException(request, "Erreur conversion paiement helloasso %s: %s"%(i,str(err)))


class EventParticipationView(GafcFormView):
    """
    User view of event. Allow to register/unregister or just display event data.
    """
    
    login_required=False
    """
    This view also display informations for non-logged users.
    """
    
    is_staff = False
    """
    This view is ruled by event level permission.
    """
    
    def setup_forms(self, request, event_id, *args, **kwargs):
        """
        Setup the forms for participation view
        
        :param request: request object
        :param event_id: event id
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """
        
        # Recover event first
        try:
            e = Event.objects.prefetch_related('eventstate_set','eventstate_set__person','eventmetadata_set','eventmetadata_set__meta','eventmetadata_set__participationmetadata_set').get(pk=event_id)
            request.event = e
        except Event.DoesNotExist:
            raise Http404("L'événement n'existe pas!")
            
        # Get user participation if exist
        request.participation = None
        has_overlap = False
        show_form = request.event.link and request.event.current_state == 'OP'
        
        evts_need_caci = []
        evts_no_overlap = []
        
        for k, it in settings.EVENT_TYPES.items():
            if it['valo_formation']:
                evts_need_caci += [k,] 
            if it['nooverlap']:
                evts_no_overlap += [k,]
                    
        if not request.user.is_anonymous:
            try:
                request.participation = Participation.objects.prefetch_related('participationmetadata_set' ).get(event=request.event, person=request.user)
                
                if not request.participation.waiting:
                    show_form = False
            except Participation.DoesNotExist:
                pass
            
            # Check if overlaping events
            if e.eventtype in evts_no_overlap:
                ov_events = Participation.objects.filter(person=request.user, event__eventtype__in=evts_no_overlap) .exclude(event__finish__lt=e.begin) .exclude(event__begin__gt=e.finish).exclude(event=e) .exclude(cancelled=True)
                
                overlapping_events = []
                for oev in ov_events:
                    if not oev.event.is_cancelled:
                        overlapping_events += [oev.event,]
                        has_overlap = True
                        
                if has_overlap:
                    messages.warning(request, "Vous êtes déjà inscits aux événements suivant aux mêmes dates: %s"%(" ".join([ev.title for ev in overlapping_events])))
                    show_form = False
            
        else: # If anonymous and not public
            if request.event.visibility != 0:
                raise PermissionDenied('L\'événement n\'est pas publique.')
                
            show_form = False
            messages.info(request, "Veuillez vous connecter pour voir le formulaire d'inscription.")
            
        # If event is private
        if request.event.visibility == 3 and request.participation is None:
            raise PermissionDenied("Vous n'avez pas l'autorisation de voir l'événement")
            
        if not request.user.is_anonymous:
            # Check the CACI
            # if not request.user.can_register_to_caci_events(e.begin) and e.eventtype in evts_need_caci:
            #     messages.warning(request, "Le certificat médial est obligatoire pour s'inscrire à cet événement. Veuillez en apporter un à la permanence.")
            #     show_form = False
                
            # Check licence
            if request.event.onlyvalid and not request.user.valid_license(e.begin.date(), e.finish.date()):
                messages.info(request, "Cet événement n'est ouvert qu'aux adhérents en ordre de licence.")
                show_form = False
                
        if show_form:
            self.add_form(ParticipationRequestForm, request.event, request.user, request.participation)
            
        if request.participation is not None and request.participation.waiting and request.event.current_state == 'OP':
            self.add_form(ParticipationDeleteForm, request.participation)
            
    def get(self, request, *args, **kwargs):
        """
        Display the view
        
        :param request: request object
        :param event_id: event id
        :param \\*args: Extra request arguments ignored
        :param \\*\\*kwargs: Extra request arguments ignored
        """
        
        return render(request, 'gafc/event_not_staff.html', locals())



        

@method_decorator(cache_page(60*30), name="dispatch")
class EventICSCalendarView(GafcPermView):
    """
    ICS public calendar view
    """
    
    login_required = False
    """
    Available for everyone
    """
    
    is_staff = False
    """
    Available for everyone
    """
    
    def get(self, request, cid):
        """
        Return the public calendar as ICS
        
        :param request: Request object
        :param cid: Category id
        """
        
        events = Event.objects.select_related('category').filter(category__id=cid, finish__gt=dt.datetime.now()+dt.timedelta(days=-365), begin__lt=dt.datetime.now()+dt.timedelta(days=365), visibility=0)    
        
        return render(request, 'gafc/calendar.ics', {'events': events, 'cid':cid}, content_type='text/calendar')
    
    
@method_decorator(cache_page(60*30), name="dispatch")
class EventExportView(GafcPermView):
    """
    Export the event data as XLSX file
    """
    
    specific_perms = ['gafc.view_person', 'gafc.view_event']
    """
    Require view_person and view_event permissions
    """
    
    def get(self, request, eid):
        """
        Return the ICS calendar view
        
        :param request: Request object
        :param eid: Event id
        """
        
        event = get_object_or_404(Event, pk=eid)
        
        wb = Workbook()
        sheet = wb.active
        sheet.title = 'Evenement'
                
        r = 1
        
        sheet.cell(row=r, column=1, value="Date export:")
        sheet.cell(row=r, column=2, value=dt.datetime.now().isoformat())
        
        r+=1
        sheet.cell(row=r, column=1, value="Événement:")
        sheet.cell(row=r, column=2, value=event.title)
        
        r+=1
        sheet.cell(row=r, column=1, value="Description:")
        sheet.cell(row=r, column=2, value=event.description)
        sheet.row_dimensions[r].height=80
        
        r+=1
        sheet.cell(row=r, column=1, value="Catégorie:")
        sheet.cell(row=r, column=2, value=event.category.name)
        
        r+=1
        sheet.cell(row=r, column=1, value="Type:")
        sheet.cell(row=r, column=2, value=gtags.eventType(event.eventtype))
        
        r+=1
        sheet.cell(row=r, column=1, value="Début:")
        sheet.cell(row=r, column=2, value=event.begin.astimezone(tzutc()).replace(tzinfo=None))
        
        r+=1
        sheet.cell(row=r, column=1, value="Fin:")
        sheet.cell(row=r, column=2, value=event.finish.astimezone(tzutc()).replace(tzinfo=None))
        
        r+=1
        sheet.cell(row=r, column=1, value="Lieu:")
        sheet.cell(row=r, column=2, value=event.place)
        
        r+=1
        sheet.cell(row=r, column=1, value="Saison:")
        sheet.cell(row=r, column=2, value=event.season.name)
        
        r+=1
        sheet.cell(row=r, column=1, value="Coût participation:")
        sheet.cell(row=r, column=2, value=event.cost)
        
        r+=1
        sheet.cell(row=r, column=1, value="Lien de paiement:")
        sheet.cell(row=r, column=2, value=event.helloassoLink)
        
        
        r += 3
        sheet.cell(row=r, column=1, value="Prénom NOM")
        sheet.cell(row=r, column=2, value="Role")
        sheet.cell(row=r, column=3, value="Licence")
        sheet.cell(row=r, column=4, value="Email")
        sheet.cell(row=r, column=5, value="Tel")
        sheet.cell(row=r, column=6, value="Liste d'attente")
        sheet.cell(row=r, column=7, value="Annulation")
        sheet.cell(row=r, column=8, value="Présent")
        
        c=8
        for m in event.metadata_fields:
           c += 1
           sheet.cell(row=r, column=c, value=m.meta.name)
           
           if m.meta.datatype in ('BO','FL','IN') and m.cost != 0:
               c += 1
               sheet.cell(row=r, column=c, value=m.cost)
        
        for p in event.participation_set.select_related('person'):
            r += 1
            sheet.cell(row=r, column=1, value="%s %s"%(p.person.first_name, p.person.last_name.upper()))
            sheet.cell(row=r, column=2, value=gtags.eventRole(p.role))
            sheet.cell(row=r, column=3, value=' '.join([l.number for l in p.valid_license]))
            sheet.cell(row=r, column=4, value=p.person.email)
            sheet.cell(row=r, column=5, value=p.person.phone)
            sheet.cell(row=r, column=6, value="OUI" if p.waiting else "NON")
            sheet.cell(row=r, column=7, value="OUI" if p.cancelled else "NON")
            sheet.cell(row=r, column=8, value="OUI" if p.event_done else "NON")
            
            c=8
            
            for em in event.metadata_fields:
                try:
                    d = ParticipationMetaData.objects.get(eventMeta=em, participation=p)
                    c += 1
                    
                    if em.meta.datatype == 'BO':
                        sheet.cell(row=r, column=c, value="OUI" if d.data == 'True' else "NON")
                    else:
                        sheet.cell(row=r, column=c, value=d.data)
                    
                    if em.meta.datatype in ('BO','FL','IN') and em.cost != 0:
                        c += 1
                        v=0
                        
                        if em.meta.datatype == 'BO' and d.data == 'True':
                            v = 1
                            
                        sheet.cell(row=r, column=c, value=float(em.cost)*v)
                    
                except ParticipationMetaData.DoesNotExist:
                    c += 1
                    
                    if em.meta.datatype == 'BO':
                        sheet.cell(row=r, column=c, value="NON")
                        
                    if em.cost != 0:
                        c += 1
                        
        for cid in range(5):
            sheet.column_dimensions[chr(cid+65)].width = 25
        
    
        response = HttpResponse(content=save_virtual_workbook(wb), content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=event-%i.xlsx'%eid
        return response
