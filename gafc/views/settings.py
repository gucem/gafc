# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.db.models import Q, Prefetch

import git
import os, shutil
import datetime as dt
import unicodedata
import zipfile
import mimetypes

from openpyxl import Workbook
from openpyxl.styles import Font
from openpyxl.writer.excel import save_virtual_workbook

from ..templatetags import gafc_tags

from .common import GafcPermView, GafcFormView
from ..forms import SeasonForm, SeasonChoiceForm, CategoryForm, StaffEmailForm, DeleteStaffEmailForm, StaffEmailAliasForm, CashingAbandonValidatorForm, CashingAbandonValidatorFileForm, DeleteStaffEmailAliasForm, DeleteCashingAbandonValidatorForm, DeleteCashingAbandonValidatorFileForm, FederationForm, StaffEmailSenderForm, DeleteStaffEmailSenderForm
from ..models import Category, Season, Federation, StaffEmail, Staff, Payment, InternalPayment, Event, Participation, Cashing, CashingAbandonValidator

class SettingsView(GafcFormView):
    """
    Display software information
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        """

        if request.user.is_superuser:
            self.add_form(FederationForm)
            self.add_form(SeasonForm)
            self.add_form(SeasonChoiceForm) 
            self.add_form(CategoryForm)
            
            self.add_form(StaffEmailForm)
            self.add_form(DeleteStaffEmailForm)
            self.add_form(StaffEmailAliasForm)
            self.add_form(DeleteStaffEmailAliasForm)
            self.add_form(StaffEmailSenderForm)
            self.add_form(DeleteStaffEmailSenderForm)
            
            self.add_form(CashingAbandonValidatorForm)
            self.add_form(DeleteCashingAbandonValidatorForm)
            self.add_form(CashingAbandonValidatorFileForm)
            self.add_form(DeleteCashingAbandonValidatorFileForm)
    

    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
        version = 'v0.0'
        version_date = dt.datetime(2020,8,2)
        here = os.path.dirname(__file__)
        
        try:
            repo = git.Repo(here, search_parent_directories=True)
            version = '%s.git:%s-%s'%(repo.tags[-1],repo.active_branch.name,repo.head.object.hexsha[0:10])
            version_date = repo.head.object.committed_datetime
        except:
            pass
        
        if 'add_season' in self.forms:
            self.forms['add_season'].fields['name'].label='Nouvelle saison'
        
        cats = Category.objects.all()
        seasons = Season.objects.all()
        feds = Federation.objects.all()
        staffmail = StaffEmail.objects.all()       
        fiscal_validators = CashingAbandonValidator.objects.all()
    
        return render(request, 'gafc/settings.html', locals())
        

    
def __normalize(name):
    nfkd_form = unicodedata.normalize('NFKD', name.strip())
    return nfkd_form.encode('ASCII', 'ignore').lower().decode().replace(' ','_')
    
    
def __zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            fn = os.path.join(root, file)
            
            ziph.write(fn, fn[len(path):])
            

class StaffListView(GafcPermView):
    """
    Export Staff list as xlsx file
    """
    
    specific_perms = ['gafc.view_staff']
    """
    Need gafc.view_staff permission
    """
    
    def get(self, request, sid):
        season = get_object_or_404(Season, pk=sid)
        
        #outfile = gs.GAFC_TEMPFOLDER+'/staffs_%s.xlsx'%(dt.datetime.now().timestamp())
        
        cats = Category.objects.prefetch_related(Prefetch('staff_set', queryset=Staff.objects.filter(season_id=sid)), 'staff_set__person', 'staff_set__person__license_set', 'staff_set__person__license_set__federation', 'staff_set__validation_set', 'staff_set__validation_set__validator').all()
        
        wb = None
        
        wb = Workbook()
        sheet = wb.active
        
        f_header = Font(name="Arial",
                        size=11,
                        bold=True,
                        italic=False,
                        vertAlign=None,
                        underline='none',
                        strike=False,
                        color='FF000000')
                
        sheet.cell(row=1, column=1, value="Date export fichier des encadrants:").font=f_header
        sheet.cell(row=1, column=2, value=dt.datetime.now().isoformat())
        sheet.cell(row=2, column=1, value="Saison:").font=f_header
        sheet.cell(row=2, column=2, value=season.name)
                
        
        sheet.column_dimensions['A'].width = 50
        sheet.column_dimensions['B'].width = 20
        
        for c in cats:
            sheet = wb.create_sheet(c.name)
                
            sheet.cell(row=1, column=1, value="Nom").font=f_header
            sheet.cell(row=1, column=2, value="Licence").font=f_header
            sheet.cell(row=1, column=3, value="Niveau").font=f_header
            sheet.cell(row=1, column=4, value="Validé par").font=f_header
            sheet.cell(row=1, column=5, value="Date validation").font=f_header
            sheet.cell(row=1, column=6, value="Date retrait validation").font=f_header
    
            
            sheet.column_dimensions['A'].width = 30
            sheet.column_dimensions['B'].width = 30
            sheet.column_dimensions['C'].width = 30
            sheet.column_dimensions['D'].width = 30
            sheet.column_dimensions['E'].width = 30
            sheet.column_dimensions['F'].width = 30
    
    
            i=2
            for s in c.staff_set.all():
                sheet.cell(row=i, column=1, value="%s %s (%i)"%(s.person.first_name, s.person.last_name, s.person.id))
                l = []
                for lic in s.person.license_set.all():
                    L = '%s %s'%(lic.federation.acronym, lic.number)
                    if L not in l:
                        l += [L, ]
                    
                sheet.cell(row=i, column=2, value=" ".join(l))
                sheet.cell(row=i, column=3, value="%s"%(gafc_tags.eventRole(s.level)))
                
                for v in s.validation_set.all():
                    sheet.cell(row=i, column=4, value="%s %s (%i)"%(v.validator.first_name, v.validator.last_name, v.validator.id))
                    sheet.cell(row=i, column=5, value=v.date.strftime('%d/%m/%Y'))
                    if v.date_end is not None:
                        sheet.cell(row=i, column=6, value=v.date_end.strftime('%d/%m/%Y'))
                    i += 1
                
    
        response = HttpResponse(content=save_virtual_workbook(wb), content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=stafflist-%i.xlsx'%sid
        return response
    
    

class SeasonArchiveView(GafcPermView):
    """
    Download Season Archive as zip file
    """
    
    specific_perms = ['gafc.view_season']
    """
    Need gafc.view_season permission
    """
    
    def get(self, request, sid):
        season = get_object_or_404(Season, pk=sid)
        
        dirn = 'archive_season_'+str(sid)+'_'+season.name
        tmp = '/tmp/gafc_export/'+dirn
        
        i=0
        #while os.path.exists('%s_%i'%(tmp,i)):
        #    i += 1
            
        tmp = '%s_%i'%(tmp,i)
        
        os.makedirs(tmp, exist_ok=True)
        
        st_title = Font(name="Arial",
                        size=14,
                        bold=True,
                        italic=False,
                        vertAlign=None,
                        underline='none',
                        strike=False,
                        color='FF000000')
                        
                        
        st_header = Font(name="Arial",
                        size=11,
                        bold=True,
                        italic=False,
                        vertAlign=None,
                        underline='none',
                        strike=False,
                        color='FF000000')
        
        for C in Category.objects.all():
            wb = Workbook()
            
            # First sheet is statistics
            s_stats = wb.active
            s_stats.title = "Résumé"        
            
            s_stats.cell(row=2, column=2, value="Début").font=st_header
            s_stats.cell(row=2, column=3, value="Fin").font=st_header
            s_stats.cell(row=2, column=4, value="Durée").font=st_header
            s_stats.cell(row=2, column=5, value="Événement").font=st_header
            s_stats.cell(row=2, column=6, value="Professionnels").font=st_header
            s_stats.cell(row=2, column=8, value="Encadrants").font=st_header
            s_stats.cell(row=2, column=10, value="Co-encadrants").font=st_header
            s_stats.cell(row=2, column=12, value="Participants").font=st_header
            
            # Second sheet is money balance
            s_balance = wb.create_sheet("Finances")
            
            s_balance.cell(row=2, column=2, value="Date").font=st_header
            s_balance.cell(row=2, column=3, value="Contrepartie").font=st_header
            s_balance.cell(row=2, column=4, value="Événement").font=st_header
            s_balance.cell(row=2, column=5, value="Motif").font=st_header
            s_balance.cell(row=2, column=6, value="Crédit").font=st_header
            s_balance.cell(row=2, column=7, value="Débit").font=st_header
            s_balance.cell(row=2, column=8, value="Encodeur").font=st_header
            
            f_event = {}
            f_alone = []
            
            
            payments = Payment.objects.filter(category=C, refund_date=None, season=season)
            
            for p in payments:
                
                if p.event is not None:
                    if p.event.id not in f_event:
                        f_event[p.event.id] = {'ID': '',
                                               'date':      p.event.begin.replace(tzinfo=None),
                                               'person':    '',
                                               'event':     p.event.title,
                                               'comment': '',
                                               'encoder': '',
                                               'credit': 0.,
                                               'debit':0.}
                    
                    if p.amount > 0.:
                        f_event[p.event.id]['credit'] += float(p.amount)
                    else:
                        f_event[p.event.id]['debit']  -= float(p.amount)
                else:
                    f_alone += [{'ID': 'P%i'%(p.id),
                                'date': p.pay_done.replace(tzinfo=None),
                                'person': '%s %s'%(p.person.first_name, p.person.last_name),
                                'event': '',
                                'comment': p.comment,
                                'encoder': '%s %s'%(p.encoder.first_name, p.encoder.last_name),
                                'credit': p.amount if p.amount > 0 else 0.,
                                'debit': -p.amount if p.amount < 0 else 0.},]
                        
            transfers = InternalPayment.objects.filter((Q(category_c=C) | Q(category_d=C)) & Q(season=season))
            
            for p in transfers:
                
                if p.event is not None:
                    if p.event.id not in f_event:
                        f_event[p.event.id] = {'ID': '',
                                               'date':      p.event.begin.replace(tzinfo=None),
                                               'person':    '',
                                               'event':     p.event.title,
                                               'comment': '',
                                               'encoder': '',
                                               'credit': 0.,
                                               'debit':0.}
                    
                    if p.category_c == C:
                        f_event[p.event.id]['credit'] += float(p.amount)
                    else:
                        f_event[p.event.id]['debit']  += float(p.amount)
                else:
                    f_alone += [{'ID': 'T%i'%(p.id),
                                'date': p.pay_done.replace(tzinfo=None),
                                'person': p.category_c.name if p.category_d == C else p.category_d.name,
                                'event': '',
                                'comment': p.comment,
                                'encoder': '%s %s'%(p.person.first_name, p.person.last_name),
                                'credit': p.amount if p.category_c == C else 0.,
                                'debit':  p.amount if p.category_d == C else 0.},]
            
            f_alone += [ p for (k,p) in f_event.items() ]
            
            f_alone.sort(key=lambda p: p['date'])
            
            l_fin = 3
            
            for f in f_alone:
                s_balance.cell(row=l_fin, column=1, value=f['ID'])
                s_balance.cell(row=l_fin, column=2, value=f['date'])
                s_balance.cell(row=l_fin, column=3, value=f['person'])
                s_balance.cell(row=l_fin, column=4, value=f['event'])
                s_balance.cell(row=l_fin, column=5, value=f['comment'])
                s_balance.cell(row=l_fin, column=6, value=f['credit']).number_format = '0.00'
                s_balance.cell(row=l_fin, column=7, value=f['debit']).number_format = '0.00'
                s_balance.cell(row=l_fin, column=8, value=f['encoder'])
                
                l_fin += 1
                
            s_balance.column_dimensions['A'].width = 10
            s_balance.column_dimensions['B'].width = 25
            s_balance.column_dimensions['C'].width = 25
            s_balance.column_dimensions['D'].width = 25
            s_balance.column_dimensions['E'].width = 25
            s_balance.column_dimensions['F'].width = 10
            s_balance.column_dimensions['G'].width = 10
            s_balance.column_dimensions['H'].width = 25
            
            # Then create one sheet per event
            evts = Event.objects.filter(season=season, category=C).exclude(eventtype='AU')
            
            l_stat = 3
            
            partrecap = {}
            
            for e in evts:
                
                n = e.title
                
                if n in wb:
                    i=0
                    while '%s_%i'%(n,i) in wb:
                        i += 1
                    n = '%s_%i'%(n,i)
                
                sheet = wb.create_sheet(n.replace('/',''))
                parts = Participation.objects.filter(event=e).exclude(Q(waiting=True) & Q(event_done=False)).order_by('-role')
                
                s_stats.cell(row=l_stat, column=1, value=l_stat)
                s_stats.cell(row=l_stat, column=2, value=e.begin.replace(tzinfo=None))
                s_stats.cell(row=l_stat, column=3, value=e.finish.replace(tzinfo=None))
                s_stats.cell(row=l_stat, column=4, value=e.duration)
                s_stats.cell(row=l_stat, column=5, value=e.title)
                s_stats.cell(row=l_stat, column=6, value=parts.filter(role="3PR").count())
                s_stats.cell(row=l_stat, column=8, value=parts.filter(role="2SU").count())
                s_stats.cell(row=l_stat, column=10, value=parts.filter(role="1CO").count())
                s_stats.cell(row=l_stat, column=12, value=parts.filter(role="0PA").count())
                l_stat += 1
                
                sheet.cell(row=2, column=2, value="Titre").font=st_header
                sheet['C2'] = e.title
                
                sheet.cell(row=3, column=2, value="Type").font=st_header
                sheet['C3'] = gafc_tags.eventType(e.eventtype)
                
                sheet.cell(row=4, column=2, value="Description").font=st_header
                sheet['C4'] = e.description
                
                sheet.row_dimensions[4].height = (e.description.count('\n')+1)*13+2
                
                sheet.cell(row=5, column=2, value="Début").font=st_header
                sheet['C5'] = e.begin.replace(tzinfo=None)
                
                sheet.cell(row=6, column=2, value="Fin").font=st_header
                sheet['C6'] = e.finish.replace(tzinfo=None)
                
                sheet.cell(row=7, column=2, value="Coût").font=st_header
                sheet['C7'] = e.cost
                sheet['C7'].number_format='0.00'
                
                sheet.cell(row=8, column=2, value="Encadrement").font=st_header
                sheet['C8'] = e.duration
                
                
                ptype = ''
                r = 10
                
                sheet.cell(row=r, column=2, value="Historique").font=st_title
                sheet.row_dimensions[r].height = 25
                
                r += 1
                
                sheet.cell(row=r, column=2, value="Date").font=st_header
                sheet.cell(row=r, column=3, value="État").font=st_header
                sheet.cell(row=r, column=4, value="Qui").font=st_header
                sheet.cell(row=r, column=5, value="Commentaire").font=st_header
                
                r += 1
                
                for st in e.eventstate_set.all():
                    sheet.cell(row=r, column=2, value=st.date.replace(tzinfo=None))
                    sheet.cell(row=r, column=3, value=gafc_tags.eventState(st.state))
                    sheet.cell(row=r, column=4, value="%s %s"%(st.person.first_name, st.person.last_name))
                    sheet.cell(row=r, column=5, value=st.comment)
                    r += 1
                    
                metas = {}
                    
                for p in parts:
                    
                    if p.person.id not in partrecap:
                        partrecap[p.person.id] = {'person': p.person, '0PA': [0,0.], '1CO': [0,0.], '2SU': [0,0.], '3PR':[0,0.]}
                        
                    if p.event_done:
                        partrecap[p.person.id][p.role][0] += 1
                        partrecap[p.person.id][p.role][1] += e.duration
                    
                    if ptype != p.role:
                        r += 1
                        
                        sheet.cell(row=r, column=2, value = gafc_tags.eventRole(p.role)).font=st_title
                        sheet.row_dimensions[r].height = 25
                        ptype = p.role
                        r += 1
                        
                        sheet.cell(row=r, column=2, value = 'Licence').font=st_header
                        sheet.cell(row=r, column=3, value = 'Prénom').font=st_header
                        sheet.cell(row=r, column=4, value = 'Nom').font=st_header
                        sheet.cell(row=r, column=5, value = 'Présent').font=st_header
                        c = 6
                        
                        for m in p.metadata_txt:
                            if m[3] != 0:
                                sheet.cell(row=r, column=c, value="%s (%.2f €)"%(m[0], m[3])).font=st_header
                                c += 1
                        r += 1
                        id=1
                    
                    sheet.cell(row=r, column=1, value = id)
                    l = [ '%s %s'%(l.federation.acronym,l.number) for l in p.valid_license ] if p.has_valid_license else []
                    sheet.row_dimensions[r].height = max(13*len(l)+2,15)
                    sheet.cell(row=r, column=2, value = '\n'.join(l))
                    sheet.cell(row=r, column=3, value = p.person.first_name)
                    sheet.cell(row=r, column=4, value = p.person.last_name)
                    sheet.cell(row=r, column=5, value = 'OUI' if p.event_done else 'NON')
                    c=6
                    for m in p.metadata_txt:
                        if m[3] != 0:
                            sheet.cell(row=r, column=c, value=m[2])
                                
                            if c not in metas:
                                metas[c] = {'N': 0, 'cost': float(m[3])}
                                
                            metas[c]['N'] += (m[2] == 'OUI') if m[1] == 'BO' else float(m[2])
                                
                            c += 1
                    r += 1
                    id += 1
                    
                for (c,v) in metas.items():
                    sheet.cell(row=r, column=c, value=v['N'])
                    sheet.cell(row=r+1, column=c, value=v['cost']*v['N']).number_format = '0.00'
                    
                
                fin = Payment.objects.filter(event=e).order_by('category__id', 'pay_done')
                
                r += 3
                
                sheet.cell(row=r, column=2, value="Paiements").font=st_title
                sheet.row_dimensions[r].height = 25
                
                r += 1
                
                sheet.cell(row=r, column=1, value="ID").font=st_header
                sheet.cell(row=r, column=2, value="Date").font=st_header
                sheet.cell(row=r, column=3, value="Prénom").font=st_header
                sheet.cell(row=r, column=4, value="Nom").font=st_header
                sheet.cell(row=r, column=5, value="Catégorie").font=st_header
                sheet.cell(row=r, column=6, value="Méthode").font=st_header
                sheet.cell(row=r, column=7, value="Montant").font=st_header
                sheet.cell(row=r, column=8, value="Encodeur").font=st_header
                sheet.cell(row=r, column=9, value="Commentaire").font=st_header
                sheet.cell(row=r, column=10, value="Encaissement").font=st_header
                sheet.cell(row=r, column=13, value="Remboursement").font=st_header
                
                r += 1
                idc = -1
                
                totaux = dict()
                
                for f in fin:
                    if idc != f.category.id:
                        if idc != -1:
                            r += 1
                        idc = f.category.id
                    
                    sheet.cell(row=r, column=1, value=f.id)
                    sheet.cell(row=r, column=2, value=f.pay_done.replace(tzinfo=None))
                    sheet.cell(row=r, column=3, value=f.person.first_name)
                    sheet.cell(row=r, column=4, value=f.person.last_name)
                    sheet.cell(row=r, column=5, value=f.category.name)
                    sheet.cell(row=r, column=6, value=gafc_tags.payMethod(f.method))
                    sheet.cell(row=r, column=7, value=f.amount).number_format='0.00'
                    sheet.cell(row=r, column=8, value="%s %s"%(f.encoder.first_name, f.encoder.last_name))
                    sheet.cell(row=r, column=9, value=f.comment)
                    
                    if f.cashing is not None:
                        sheet.cell(row=r, column=10, value=f.cashing.id)
                        sheet.cell(row=r, column=11, value=f.cashing.date)
                        sheet.cell(row=r, column=12, value="%s %s"%(f.cashing.person.first_name, f.cashing.person.last_name))
                    
                    if f.refund_date is not None:
                        sheet.cell(row=r, column=13, value=f.refund_date.replace(tzinfo=None))
                        sheet.cell(row=r, column=14, value="%s %s"%(f.refund_who.first_name, f.refund_who.last_name))
                        
                        font = sheet.cell(row=r, column=1).font.copy()
                        font.strike = True
                        
                        for c in range(1,9):
                            sheet.cell(row=r, column=c).font = font
                    
                    r += 1
                    
                    
                    if f.refund_date is not None:
                        continue
                    
                    if f.category.id not in totaux:
                        totaux[f.category.id] = {'cat': f.category}
                        
                    if f.method not in totaux[f.category.id]:
                        totaux[f.category.id][f.method] = {'N': 0, 't': 0.}
                        
                    totaux[f.category.id][f.method]['N']+=1
                    totaux[f.category.id][f.method]['t']+=float(f.amount)
                    
                r += 1
                
                sheet.cell(row=r, column=3, value="Méthode").font=st_header
                sheet.cell(row=r, column=4, value="Nombre").font=st_header    
                sheet.cell(row=r, column=5, value="Total").font=st_header
                
                r += 1
                
                for (K,T) in totaux.items():
                    sheet.cell(row=r, column=2, value=T['cat'].name).font=st_header
                    for (k,t) in T.items():
                        if k != 'cat':
                            sheet.cell(row=r, column=3, value=gafc_tags.payMethod(k))
                            sheet.cell(row=r, column=4, value=t['N'])
                            sheet.cell(row=r, column=5, value=t['t']).number_format = '0.00'
                            
                            r += 1
                    
                fin = InternalPayment.objects.filter(event=e)
                
                r += 2
                
                sheet.cell(row=r, column=2, value="Transferts").font=st_title
                sheet.row_dimensions[r].height = 25
                
                r += 1
                
                sheet.cell(row=r, column=1, value="ID").font=st_header
                sheet.cell(row=r, column=2, value="Date").font=st_header
                sheet.cell(row=r, column=3, value="De").font=st_header
                sheet.cell(row=r, column=4, value="Vers").font=st_header
                sheet.cell(row=r, column=6, value="Méthode").font=st_header
                sheet.cell(row=r, column=7, value="Montant").font=st_header
                sheet.cell(row=r, column=8, value="Encodeur").font=st_header
                sheet.cell(row=r, column=9, value="Commentaire").font=st_header
                
                r += 1
                idc = -1
                
                for f in fin:
                    
                    sheet.cell(row=r, column=1, value=f.id)
                    sheet.cell(row=r, column=2, value=f.pay_done.replace(tzinfo=None))
                    sheet.cell(row=r, column=3, value=f.category_d.name)
                    sheet.cell(row=r, column=4, value=f.category_c.name)
                    sheet.cell(row=r, column=6, value=gafc_tags.payMethod(f.method))
                    sheet.cell(row=r, column=7, value=f.amount).number_format='0.00'
                    sheet.cell(row=r, column=8, value="%s %s"%(f.person.first_name, f.person.last_name))
                    sheet.cell(row=r, column=9, value=f.comment)
                    
                    r += 1
                    
                
                sheet.column_dimensions['A'].width = 5
                sheet.column_dimensions['B'].width = 25
                sheet.column_dimensions['C'].width = 20
                sheet.column_dimensions['D'].width = 20
                sheet.column_dimensions['E'].width = 10
                sheet.column_dimensions['F'].width = 10
                sheet.column_dimensions['G'].width = 10
                sheet.column_dimensions['H'].width = 20
                sheet.column_dimensions['I'].width = 20
                sheet.column_dimensions['J'].width = 10
                sheet.column_dimensions['K'].width = 20
                sheet.column_dimensions['L'].width = 20
                sheet.column_dimensions['M'].width = 20
                sheet.column_dimensions['N'].width = 20
                
            l_stat += 2
            
            s_stats.cell(row=l_stat, column=2, value="Prénom").font=st_header
            s_stats.cell(row=l_stat, column=3, value="Nom").font=st_header
            s_stats.cell(row=l_stat, column=5, value="Rôle validé").font=st_header
            
            l_stat += 1
            
            partrecap = dict(sorted(partrecap.items(), key=lambda v: '%i-%s'%(4-len(v[1]['person'].available_role(C, season)), v[1]['person'].last_name.upper())))
            
            for k,p in partrecap.items():
                s_stats.cell(row=l_stat, column=2, value=p['person'].first_name)
                s_stats.cell(row=l_stat, column=3, value=p['person'].last_name)
                             
                roles = p['person'].available_role(C,season)
                r_txt = ""
                if '3PR' in roles:
                    r_txt = gafc_tags.eventRole('3PR')
                elif '2SU' in roles:
                    r_txt = gafc_tags.eventRole('2SU')
                elif '1CO' in roles:
                    r_txt = gafc_tags.eventRole('1CO')
                    
                s_stats.cell(row=l_stat, column=5, value=r_txt)
                
                s_stats.cell(row=l_stat, column=6, value=p['3PR'][0] if p['3PR'][0] != 0 else '')
                s_stats.cell(row=l_stat, column=7, value=p['3PR'][1] if p['3PR'][1] != 0 else '').number_format='0.0'
                s_stats.cell(row=l_stat, column=8, value=p['2SU'][0] if p['2SU'][0] != 0 else '')
                s_stats.cell(row=l_stat, column=9, value=p['2SU'][1] if p['2SU'][1] != 0 else '').number_format='0.0'
                s_stats.cell(row=l_stat, column=10, value=p['1CO'][0] if p['1CO'][0] != 0 else '')
                s_stats.cell(row=l_stat, column=11, value=p['1CO'][1] if p['1CO'][1] != 0 else '').number_format='0.0'
                s_stats.cell(row=l_stat, column=12, value=p['0PA'][0] if p['0PA'][0] != 0 else '')
                s_stats.cell(row=l_stat, column=13, value=p['0PA'][1] if p['0PA'][1] != 0 else '').number_format='0.0'
                
                l_stat += 1
            
            s_stats.column_dimensions['A'].width = 5
            s_stats.column_dimensions['B'].width = 20
            s_stats.column_dimensions['C'].width = 20
            s_stats.column_dimensions['D'].width = 10
            s_stats.column_dimensions['E'].width = 20
            s_stats.column_dimensions['F'].width = 10
            s_stats.column_dimensions['G'].width = 10
            s_stats.column_dimensions['H'].width = 10
            s_stats.column_dimensions['I'].width = 10
            s_stats.column_dimensions['J'].width = 10
            s_stats.column_dimensions['K'].width = 10
            s_stats.column_dimensions['L'].width = 10
            s_stats.column_dimensions['M'].width = 10
            
            wb.save(tmp+'/'+str(C.id)+'_'+__normalize(C.name)+".xlsx")
            
            
        wb = Workbook()
        s_balance = wb.active
        s_balance.title = "Paiements"
        
        s_balance.cell(row=2, column=2, value="Date").font=st_header
        s_balance.cell(row=2, column=3, value="Catégorie").font=st_header
        s_balance.cell(row=2, column=4, value="Contrepartie").font=st_header
        s_balance.cell(row=2, column=5, value="Événement").font=st_header
        s_balance.cell(row=2, column=6, value="Motif").font=st_header
        s_balance.cell(row=2, column=7, value="Crédit").font=st_header
        s_balance.cell(row=2, column=8, value="Débit").font=st_header
        s_balance.cell(row=2, column=9, value="Encodeur").font=st_header
            
        f_event = {}
        f_alone = []
        
        payments = Payment.objects.filter(refund_date=None, season=season)
        
        for p in payments:
            
            if p.event is not None:
                k = '%i-%i'%(p.category.id, p.event.id)
                if k not in f_event:
                    f_event[k] = {'ID': '',
                                    'date':      p.event.begin.replace(tzinfo=None),
                                    'category': p.category.name,
                                    'person':    '',
                                    'event':     p.event.title,
                                    'comment': '',
                                    'encoder': '',
                                    'credit': 0.,
                                    'debit':0.}
                
                if p.amount > 0.:
                    f_event[k]['credit'] += float(p.amount)
                else:
                    f_event[k]['debit']  -= float(p.amount)
            else:
                f_alone += [{'ID': 'P%i'%(p.id),
                            'date': p.pay_done.replace(tzinfo=None),
                            'category': p.category.name,
                            'person': '%s %s'%(p.person.first_name, p.person.last_name),
                            'event': '',
                            'comment': p.comment,
                            'encoder': '%s %s'%(p.encoder.first_name, p.encoder.last_name),
                            'credit': p.amount if p.amount > 0 else 0.,
                            'debit': -p.amount if p.amount < 0 else 0.},]
        
        f_alone += [ p for (k,p) in f_event.items() ]
        
        f_alone.sort(key=lambda p: p['date'])
        
        l_fin = 3
            
        for f in f_alone:
            s_balance.cell(row=l_fin, column=1, value=f['ID'])
            s_balance.cell(row=l_fin, column=2, value=f['date'])
            s_balance.cell(row=l_fin, column=3, value=f['category'])        
            s_balance.cell(row=l_fin, column=4, value=f['person'])
            s_balance.cell(row=l_fin, column=5, value=f['event'])
            s_balance.cell(row=l_fin, column=6, value=f['comment'])
            s_balance.cell(row=l_fin, column=7, value=f['credit']).number_format = '0.00'
            s_balance.cell(row=l_fin, column=8, value=f['debit']).number_format = '0.00'
            s_balance.cell(row=l_fin, column=9, value=f['encoder'])
            
            l_fin += 1
            
        s_balance.column_dimensions['A'].width = 10
        s_balance.column_dimensions['B'].width = 25
        s_balance.column_dimensions['C'].width = 25
        s_balance.column_dimensions['D'].width = 25
        s_balance.column_dimensions['E'].width = 25
        s_balance.column_dimensions['F'].width = 25
        s_balance.column_dimensions['G'].width = 10
        s_balance.column_dimensions['H'].width = 10
        s_balance.column_dimensions['I'].width = 25
        
        cashings = Cashing.objects.filter(season=season)
        
        for c in cashings:
            sheet = wb.create_sheet('Encaissement_%i'%(c.id))
            
            r = 2
            
            sheet.cell(row=r, column=2, value="Date").font=st_header
            sheet.cell(row=r, column=3, value=c.date)
            r += 1
            
            sheet.cell(row=r, column=2, value="Encodeur").font=st_header
            sheet.cell(row=r, column=3, value="%s %s"%(c.person.first_name, c.person.last_name))
            r += 1
            
            sheet.cell(row=r, column=2, value="Commentaire").font=st_header
            sheet.cell(row=r, column=3, value=c.comment)
            sheet.row_dimensions[r].height = max(13*len(c.comment.split('\n'))+2,15)
            r += 2
            
            sheet.cell(row=r, column=1, value="ID").font=st_header
            sheet.cell(row=r, column=2, value="Date").font=st_header
            sheet.cell(row=r, column=3, value="Prénom").font=st_header
            sheet.cell(row=r, column=4, value="Nom").font=st_header
            sheet.cell(row=r, column=5, value="Catégorie").font=st_header
            sheet.cell(row=r, column=6, value="Méthode").font=st_header
            sheet.cell(row=r, column=7, value="Montant").font=st_header
            sheet.cell(row=r, column=8, value="Encodeur").font=st_header
            sheet.cell(row=r, column=9, value="Commentaire").font=st_header
            
            payments = c.payment_set.all()
            totaux = {}
            
            r += 1
            
            for f in payments:
                if f.category.id not in totaux:
                    totaux[f.category.id] = {'cat': f.category}
                        
                    if f.method not in totaux[f.category.id]:
                        totaux[f.category.id][f.method] = {'N': 0, 't': 0.}
                        
                    totaux[f.category.id][f.method]['N']+=1
                    totaux[f.category.id][f.method]['t']+=float(f.amount)
                    
                sheet.cell(row=r, column=1, value=f.id)
                sheet.cell(row=r, column=2, value=f.pay_done.replace(tzinfo=None))
                sheet.cell(row=r, column=3, value=f.person.first_name)
                sheet.cell(row=r, column=4, value=f.person.last_name)
                sheet.cell(row=r, column=5, value=f.category.name)
                sheet.cell(row=r, column=6, value=gafc_tags.payMethod(f.method))
                sheet.cell(row=r, column=7, value=f.amount).number_format='0.00'
                sheet.cell(row=r, column=8, value="%s %s"%(f.encoder.first_name, f.encoder.last_name))
                sheet.cell(row=r, column=9, value=f.comment)
                
                r += 1
            
            r += 1
                
            sheet.cell(row=r, column=3, value="Méthode").font=st_header
            sheet.cell(row=r, column=4, value="Nombre").font=st_header    
            sheet.cell(row=r, column=5, value="Total").font=st_header
    
            r += 1
    
            for (K,T) in totaux.items():
                sheet.cell(row=r, column=2, value=T['cat'].name).font=st_header
                for (k,t) in T.items():
                    if k != 'cat':
                        sheet.cell(row=r, column=3, value=gafc_tags.payMethod(k))
                        sheet.cell(row=r, column=4, value=t['N'])
                        sheet.cell(row=r, column=5, value=t['t']).number_format = '0.00'
                        
                        r += 1
                        
            sheet.column_dimensions['A'].width = 5
            sheet.column_dimensions['B'].width = 20
            sheet.column_dimensions['C'].width = 20
            sheet.column_dimensions['D'].width = 20
            sheet.column_dimensions['E'].width = 15
            sheet.column_dimensions['F'].width = 15
            sheet.column_dimensions['G'].width = 10
            sheet.column_dimensions['H'].width = 25
            sheet.column_dimensions['I'].width = 25
        
        wb.save(tmp+'/paiements.xlsx')
        wb.close()
        
        zipf = zipfile.ZipFile(tmp+'.zip', 'w', zipfile.ZIP_DEFLATED)
        __zipdir(tmp, zipf)
        zipf.close()
        
        
        mime_type, _ = mimetypes.guess_type(tmp+'.zip')
        data = open(tmp+'.zip', 'rb')
                      
        response = HttpResponse(data, content_type=mime_type)
        response['Content-Disposition'] = 'attachment; filename="'+dirn+'.zip"'
        
        #Cleanup
        shutil.rmtree(tmp, ignore_errors=True)
        os.remove(tmp+'.zip')
        
        return response
