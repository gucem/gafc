# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Permissions management views
"""
from django.shortcuts import render, redirect

from django.contrib import auth

from django.http import Http404

from .common import GafcFormView

from ..forms.permissions import GroupForm, EventStaffPermissionForm, AddSuperUserForm, DelSuperUserForm, AddGroupUserForm, DelGroupUserForm, DelGroupForm
from ..models import Person, StaffEventPermission

class PermissionsView(GafcFormView):
    """
    Show the permissions
    """
    
    specific_perms = ['auth.view_permission']
    """
    Need view_permission to see this view
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """
        
        if request.user.is_superuser:
            self.add_form(GroupForm)
            self.add_form(EventStaffPermissionForm)
            self.add_form(AddSuperUserForm)
            self.add_form(DelSuperUserForm)
            
    def get(self, request):
        """
        Show the view
        
        :param request: Request object
        """
        
        superusers = Person.objects.filter(is_superuser=True)
        groups = auth.models.Group.objects.prefetch_related('permissions','permissions__content_type','user_set').all()
        avail_persons = Person.objects.all()
        perms_ev_staff = StaffEventPermission.objects.all()
        
        return render(request, 'gafc/permissions.html', locals())


class PermissionsGroupView(GafcFormView):
    """
    Edit the Permission group
    """
    
    is_superuser = True
    """
    Only superusers can change permissions
    """
    
    def setup_forms(self, request, group_id):
        """
        Setup the forms
        
        :param request: Request object
        :param group_id: Group id
        """
        
        try:
            self.__group = auth.models.Group.objects.prefetch_related('permissions', 'permissions__content_type', 'user_set').get(pk=group_id)
        except auth.models.Group.DoesNotExist:
            raise Http404('Group not found')
            
        self.add_form(GroupForm, instance=self.__group)
        self.add_form(AddGroupUserForm, self.__group)
        self.add_form(DelGroupUserForm, self.__group)
        self.add_form(DelGroupForm, self.__group)
        
    def get(self, request, group_id):
        """
        Setup the forms
        
        :param request: Request object
        :param group_id: Group id
        """
        
        grp = self.__group
        
        return render(request, 'gafc/permission_group.html', locals())
    
    def form_del_group(self, request, group_id):
        """
        Redirect to group list after deletion
        """
        
        self.forms['del_group'].save()
        
        return redirect('permissions')
    


