# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Mailing list management views
"""

from django.shortcuts import render, redirect, get_object_or_404

from django.contrib import messages

from .common import GafcFormView

from .. import ffcam_utils
from ..forms.mailings import SympaMailingListForm
from ..forms.common import GafcDummyForm, GafcFormException
from ..models import SympaMailingList

import gucem.settings as gsets

class MailingListListView(GafcFormView):
    """
    View the list of mailing lists
    """
    
    specific_perms = ['gafc.view_sympamailinglist']
    """
    Need view_sympamailinglist
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """

        self.add_form(SympaMailingListForm)
        
    def get(self, request):
        """
        Return the view
        
        :param request: Request object
        """
        
        mls = SympaMailingList.objects.all()
        return render(request, 'gafc/mailings.html', locals())
    
    
    def form_save_mailinglist(self, request):
        """
        Specific processing of sympa mailing list
        
        :param request: Request object
        """
        
        ml = self.forms['save_mailinglist'].save()
        return redirect('mailing', mlname=ml.name)


class MailingListView(GafcFormView):
    """
    View the list of mailing lists
    """
    
    specific_perms = ['gafc.view_sympamailinglist']
    """
    Need view_sympamailinglist
    """
    
    def setup_forms(self, request, mlname):
        """
        Setup the forms
        
        :param request: Request object
        :param mlname: Mailing list name
        """
        
        self.__ml = get_object_or_404(SympaMailingList, pk=mlname)

        self.add_form(SympaMailingListForm, instance=self.__ml)
        self.add_form(GafcDummyForm, what='sync_ml', submit_txt="Synchroniser", form_id='sync_ml')
        
    def get(self, request, mlname):
        """
        Return the view
        
        :param request: Request object
        :param mlname: Mailing list name
        """
        
        ml = self.__ml
        return render(request, 'gafc/mailing.html', locals())
    
    
    def form_sync_ml(self, request, mlname):
        """
        Synchronize the database with sympa server
        
        :param request: Request object
        :param mlname: Mailing list name
        """
        try:
            with ffcam_utils.SympaConnexxion(gsets.GAFC_SYMPA_USER, gsets.GAFC_SYMPA_PASSWD, gsets.GAFC_SYMPA_SERVER) as conn:
                self.__ml.remotesync(conn)
                messages.success(request, "Synchronisation effectuée depuis le serveur SYMPA")
        except Exception as e:
            raise GafcFormException(e)
    
    


