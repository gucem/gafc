from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse, FileResponse
from django.core.paginator import Paginator
from django.db.models.functions import Upper, Lower, Substr, TruncMonth, TruncYear
from django.db.models import Sum
from django.db import transaction
from django.forms import formset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from ..forms import *
from ..models import *
from ..decorators import *

from .common import GafcFormView, GafcPermView
from ..forms import EquipmentForm

import os

class EquipmentListView(GafcFormView):
    """
    Display the list of equipments
    """
    
    specific_perms = ['gafc.view_equipment']
    """
    Need gafc.view_equipment permission
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        """
        
        if request.user.has_perm('gafc.add_equipment'):
            self.add_form(EquipmentForm)
        
        if request.user.has_perm('gafc.add_equipmentgroup'):
            self.add_form(EquipmentGroupForm)
            
    def get(self, request):
        """
        GET method
        """
        
        tree = EquipmentGroup.getTree()        
        return render(request, 'gafc/list_equipment.html', locals())
        

class EquipmentView(GafcFormView):
    """
    Display an equipment with all tracking/meta details
    """
    
    def setup_forms(self, request, eqid):
        """
        Setup the forms
        
        :param request: Request object
        :param eqid: Equipment id
        """
        
        self.__equipment = get_object_or_404(Equipment, pk=eqid)
        
        if self.__equipment.date_scrap is None:
            if request.user.has_perm('gafc.change_equipment'):
                self.add_form(EquipmentForm, instance=self.__equipment)
                self.add_form(EquipmentMetaForm, self.__equipment)
                self.add_form(EquipmentMetaUpdateFormset, self.__equipment)
                self.add_form(EquipmentPictureForm, self.__equipment, request)
                self.add_form(DeleteEquipmentPictureForm, self.__equipment)
                
            if request.user.has_perm('gafc.add_equipmenttracking'):
                self.add_form(EquipmentTrackingForm, request.user, self.__equipment, required=True)
                
            if request.user.has_perm('gafc.scrap_equipment'):
                self.add_form(ScrapEquipmentForm, self.__equipment)

        elif request.user.has_perm('gafc.add_equipment'):
            self.add_form(EquipmentDeriveForm, self.__equipment)

    def get(self, request, eqid):
        """
        GET method
        
        :param request: Request object
        :param eqid: Equipment id
        """
        e = self.__equipment
        metas = EquipmentMeta.objects.filter(equipment=e)
        tracks = EquipmentTracking.objects.filter(equipment=e).order_by('date')
    
        return render(request, 'gafc/equipment.html', locals())


class EquipmentPictureView(GafcPermView):
    """
    Return the Picture associated to equipment
    """
    
    specific_perms = ['gafc.view_equipment',]
    """
    Need gafc.view_equipment permission
    """
    
    def get(self, request, picture_id):
        """
        GET method only
        
        :param request: Request object
        :param picture_id: Picture id
        """
        
        try:
            cf = EquipmentPicture.objects.get(pk=picture_id)
        except EquipmentPicture.DoesNotExist:
            raise Http404('Cette image n\'existe pas!')
            
        filename = settings.GAFC_FILEFOLDER+'/equipments/%s'%(cf.disk_filename)
        
        return FileResponse(open(filename, 'rb'), filename=cf.filename)

class EquipmentCalendarView(GafcPermView):
    """
    Display the reservation calendar
    """
    
    specific_perms = ['gafc.view_equipment',]
    
    
    def get(self, request, year=None, month=None):
        """
        GET method, restrict calendar to month/year
        
        :param year: Year to display
        :param month: Month to display
        """
        
        if year is None or month is None:
            return redirect('calendar_equipment', year=dt.date.today().year, month=dt.date.today().month)
        
        d_begin = dt.date(year, month, 1)
        d_end = dt.date(year, month, 1)
        while d_end.month == month:
            d_end += dt.timedelta(days=1)
            
        d_end -= dt.timedelta(days=1)
        
        tracks = EquipmentTracking.objects.select_related('event', 'equipment').filter(equipment__reserve=True).exclude(event__begin__gt=d_end).exclude(event__finish__lt=d_begin).exclude(event=None).order_by('-date', 'event')
            
        tracks_per_day = dict()
        
        for t in tracks:
            from_d = t.event.begin.day if t.event.begin.month == month else 1
            to_d   = t.event.finish.day if t.event.finish.month == month else d_end.day
            
            for d in range(from_d, to_d+1):
                if d not in tracks_per_day:
                    tracks_per_day[d] = []
                    
                tracks_per_day[d] += [t, ]
            
        d = dt.date(year, month, 1)
        today = dt.date.today()
        weeks = []
        
        w=[]
        for i in range(d.weekday()):
            w += [{'date': None, 'tracks': None},]
        
        while d.month == month:
            w += [{'date': d, 'tracks': tracks_per_day[d.day] if d.day in tracks_per_day else []}]
            
            if d.weekday() == 6:
                weeks += [w, ]
                w = []
            
            d += dt.timedelta(days=1)
            
            
        if d.weekday() != 0:
            while d.weekday() != 0:
                w += [{'date': None, 'tracks': None},]
                d += dt.timedelta(days=1)
                
            weeks += [w, ]
            
        months = EquipmentTracking.objects.exclude(event=None).filter(event__season__id=request.session['season_id']).order_by('event__begin').annotate(Month=TruncMonth('event__begin')).values_list('Month').distinct()
        
        return render(request, 'gafc/calendar_equipments.html', locals())
    
    
class EquipmentGroupView(GafcFormView):
    """
    Display the equipment group
    """
    
    specific_perms = ['gafc.view_equipmentgroup',]
    
    def setup_forms(self, request, eqgrp_id):
        """
        Setup the forms
        
        :param request: Request object
        :param eqgrp_id: Equipment group id
        """
        self.__group = get_object_or_404( EquipmentGroup, pk=eqgrp_id) 
        self.__tree = EquipmentGroup.getTree(eqgrp_id)
        
        if request.user.has_perm('gafc.add_equipmenttracking'):
            self.add_form(EquipmentTrackingFormCheck, self.__tree, request.user)
        
        if request.user.has_perm('gafc.change_equipmentgroup'):
            self.add_form(EquipmentGroupForm, instance=self.__group)
            
        if request.user.has_perm('gafc.delete_equipmentfgroup') and Equipment.objects.filter(belong_to__id=eqgrp_id).count() == 0:
            self.add_form(DeleteEquipmentGroupForm, self.__group)
            
    def get(self, request, eqgrp_id):
        """
        GET method
        
        :param request: Request object
        :param eqgrp_id: EquipmentGroup id
        """
        
        g = self.__group
        tree = self.__tree 
        return render(request, 'gafc/equipment_group.html', locals())
    
    def form_delete_equipment_group(self, request, eqgrp_id):
        """
        Redirect to eqlist when deleted
        
        :param request: Request object
        :param eqgrp_id: EquipmentGroup id
        """
        
        self.forms['delete_equipment_group'].save()
        messages.success(request, 'Groupe d\'équipement supprimé')
        return redirect("list_equipment")
        
    
class EquipmentTrackingView(GafcFormView):
    """
    EquipmentTracking view
    """
    
    specific_perms = ['gafc.change_equipmenttracking',]
    """
    Need gafc.change_equipmenttracking permission
    """
    
    def setup_forms(self, request, tracking_id):
        """
        Setup the forms
        
        :param request: Request object
        :param tracking_id: EquipmentTracking id
        """
        
        self.__tracking = get_object_or_404(EquipmentTracking, pk=tracking_id)
        self.add_form(EquipmentTrackingForm, request.user, instance=self.__tracking, required=True, hide_event=False)
        
        
    def get(self, request, tracking_id):
        """
        GET method
        
        :param request: Request object
        :param tracking_id: EquipmentTracking id
        """
        
        t = self.__tracking        
        return render(request, 'gafc/equipmenttracking.html', locals())
        





