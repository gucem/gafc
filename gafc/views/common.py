# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
This module contains Generic views for GAFC-specific stuffs.
"""

from django.core.exceptions import PermissionDenied
from django.utils.decorators import classonlymethod
from django.views.generic import View
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.views import redirect_to_login

import traceback

from ..forms import GafcFormException

class GafcPermView(View):
    """
    Base view class that check the permissions before doing anything. Permission check is done much before decorating the dispatch method.
    """
          
    login_required=True
    """
    If True, this view require the user to be logged in.
    """
    
    is_staff=True
    """
    If True, this view require the 'staff' permission.
    """
    
    is_superuser = False
    """
    If True, only superusers can access the view
    """
    
    specific_perms = []
    """
    Required extra permissions to see this view.
    """
    
    @classonlymethod
    def as_view(cls, **initkwargs):
        """
        Return the view that check permissions first.
        
        :param \\*\\*initkwargs: Arguments passed to as_view function
        """
        
        view = super().as_view(**initkwargs)
        
        def wrappedview(request, *args, **kwargs):
            """
            Check loggin and permissions and redirect or raise if needed.
            
            :param request: request object
            :param \\*args: Extra request arguments passed to view
            :param \\*\\*kwargs: Extra request arguments passes to view
            
            :raises PermissionDenied: if a permission is missing to display the view
            """
            if cls.login_required:
                if request.user.is_anonymous:
                    messages.error(request, "Veuillez vous identifier pour accéder à cette page.")
                    return redirect_to_login(request.path_info)
            
                if cls.is_staff and not request.user.is_staff:
                    raise PermissionDenied("L'accès administrateur est requis pour accéder à cette page.")
                    
                if cls.is_superuser and not request.user.is_superuser:
                    raise PermissionDenied("L'accès superutilisateur est requis pour accéder à la page.")
                
                for p in cls.specific_perms:
                    if not request.user.has_perm(p):
                        raise PermissionDenied("Vous n'avez pas la permission d'accéder à cette page.")
                    
            return view(request, *args, **kwargs)
        
        return wrappedview


class GafcFormView(GafcPermView):
    """
    Generic view for Gafc class-based View
    """
    
    def add_form(self, formtype, *args, form_id=None, **kwargs):
        """
        Add a form to the current view. Must be called BEFORE super().setup()
        
        :param form_id: Form
        :param formtype: The form instance to add
        :param \\*args: Extra positionnal arguments passed to form at instanciation time
        :param form_id: Optional form identifier. Keyword-only.
        :param \\*\\*kwargs: Extra keyword arguments passed to form at instanciation time
        """
        
        if not hasattr(self, 'forms_def'):
            self.forms_def = []
            
        self.forms_def += [(formtype, args, kwargs, form_id),]
        
    def setup_forms(self, request, *args, **kwargs):
        """
        This is the place where the forms should be instanciated. Override this method to setup the forms.
        
        :param request: request instance
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        pass
    
    def setup(self, request, *args, **kwargs):
        """
        Setup the view. In addition to parent setup, initialize the forms
        
        :param request: request instance
        :param \\*args: Extra positional parameters passed to super().setup
        :param \\*\\*kwargs: Extra keyword parameters passed to super().setup
        """
        super().setup(request, *args, **kwargs)
        
        self.setup_forms(request, *args, **kwargs)
        self.forms = {}
        
        if hasattr(self, 'forms_def'):
            what = request.POST['whattodo'] if 'whattodo' in request.POST else None
        
            for f in self.forms_def:
                fwhat = f[0].what if f[3] is None else f[3]
                self.forms[fwhat] = f[0](*f[1], data=request.POST if what == fwhat else None, files=request.FILES if what == fwhat else None,**f[2])
                

    def post(self, request, *args, **kwargs):
        """
        Default POST implementation
        If the form is valid, call form.save or form_(form.what) if the function is defined.
        Return get if not valid.
        
        :param request: request instance
        :param \\*args: Extra positional parameters passed to super().setup
        :param \\*\\*kwargs: Extra keyword parameters passed to super().setup
        
        :return redirection to same view if success, get method if not
        """
        
        what = request.POST['whattodo'] if 'whattodo' in request.POST else None
        
        try:
            if what in self.forms:
                if self.forms[what].is_valid():
                    if hasattr(self, 'form_%s'%(what)):
                        r = getattr(self, 'form_%s'%(what))(request, *args, **kwargs)
                        if r is not None: return r
                    else:
                        self.forms[what].save()
                    
                    has_success = False
                    for m in messages.get_messages(request):
                        if m.level == messages.SUCCESS:
                            has_success = True
                        
                        messages.add_message(request, m.level, m.message) # Put back on the stack!
                        
                    if not has_success:
                        messages.success(request, "Modifcations enregistrées!")
                else:
                    raise GafcFormException("Erreur dans la validation du formulaire.")
                return HttpResponseRedirect(self.request.path_info)
            else:
                raise GafcFormException("Action inconnue.")
        except GafcFormException as e:
            messages.error(self.request, str(e))
            traceback.print_exc()
            return self.get(request, *args, **kwargs)


class GafcMultiGetView(GafcPermView):
    """
    Generic view for class-based view containing multiple GET functions
    The function used is selected using the "action" parameter of as_view
    """
    
    action = None
    """
    GET method selector.
    """
    
    def get(self, *args, **kwargs):
        """
        GET method implementation. Redirect the request according to "action"
        
        :param \\*args: positional arguments, passed to child method
        :param \\*\\*kwargs: keyword arguments, passed to child method
        
        :raises Http404: if the method is not found in child class
        
        :return what the child method return
        """
        if hasattr(self, self.action):
            return getattr(self, self.action)(*args, **kwargs)
        else:
            raise Http404('Action inconnue')



