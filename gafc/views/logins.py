# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

from django.shortcuts import render,redirect,get_object_or_404

from django.db import transaction

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from django.template.loader import render_to_string

from django.views.decorators.debug import sensitive_post_parameters

from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail

from django.utils.decorators import method_decorator
from django.utils.http import url_has_allowed_host_and_scheme

import datetime as dt
from datetime import timezone
import hashlib
from smtplib import SMTPException

from ..forms import PersonRegisterForm, PasswordRecoveryForm, SetNewPasswordForm, LoginForm, GafcFormException
from ..models import LostPasswordRequest, Person, License, Season

from .common import GafcFormView, GafcPermView

"""

This file contains views for login, logout, account creations, ...

"""


class RegisterView(GafcFormView):
    """
    View to create a new account
    """
    
    login_required = False
    """
    Available for everyone
    """
    
    def setup_forms(self, request):
        """
        Configure the forms
        
        :param request: Request object
        """
        
        self.add_form(PersonRegisterForm)
        
    
    def get(self, request):
        """
        Return the view
        
        :param request: Request object
        """
        print(self.forms)
        
        return render(request, 'gafc/new_account.html', locals())
        
    
    def form_new_person(self, request):
        """
        Special processing of form
        
        :param request: Request object
        """
        try:
            with transaction.atomic():
                p = self.forms['new_person'].save(commit=False)
                p.set_unusable_password()
                p.save()
                
                token = hashlib.md5((p.email+dt.datetime.now().isoformat()).encode("utf-8")).hexdigest()
                LostPasswordRequest.objects.create(person=p, token=token)
                
                send_mail(
                    'Votre compte GUCEM',
                    render_to_string('gafc/email/new_user.txt', {'person': p, 'request': request}),
                    'no-reply@gucem.fr',
                    [ p.email ],
                    fail_silently=False,
                )
                messages.success(request, "Votre compte a été créé. Un e-mail contenant le lien d'activation de votre compte vous a été envoyé. Veuillez vérifier vos e-mail ainsi que le dossier Spam.")
                    
        except SMTPException:
            messages.error(request, "Erreur lors de l'envoi de l'e-mail.")
        
class LostPasswordRequestView(GafcFormView):
    """
    View to request a new password
    """
    
    login_required = False
    """
    Accessible to everybody
    """
    
    def setup_forms(self, request):
        """
        Setup the form
        
        :param request: Request object
        """
        self.add_form(PasswordRecoveryForm)
    
    
    def get(self, request):
        """
        Return the view
        
        :param request: Request object
        """
        return render(request, 'gafc/lost_password.html', locals())
    
    
    def form_password_recovery(self, request):
        """
        Process the password recovery request
        
        :param request: Request object
        """
        
        form_lost_password = self.forms['password_recovery']
        try:
            p = Person.objects.get(email=form_lost_password.cleaned_data['data'])
        except Person.DoesNotExist:
            try:
                p = License.objects.filter(number=form_lost_password.cleaned_data['data'])[0].person
            except IndexError:
                raise GafcFormException("Erreur: Aucun compte n'est associé à cet adresse e-mail/numéro de licence")
                
        if hasattr(p, 'lostpasswordrequest'):
            if (dt.datetime.now(tz=timezone.utc) - p.lostpasswordrequest.date).total_seconds() < 3600*3 :
                raise GafcFormException("Vous avez déjà fait une demande il y a moins de 3h. Veuillez vérifier vos e-mail, y compris dans le dossier Spam")
        
        try:
            with transaction.atomic():
                token = hashlib.md5((p.email+dt.datetime.now().isoformat()).encode("utf-8")).hexdigest()
                obj,cr = LostPasswordRequest.objects.update_or_create(person=p, defaults={'token':token, 'date': dt.datetime.now(tz=timezone.utc)})
                
                send_mail(
                    'Réinitialisation de votre mot de passe',
                    render_to_string('gafc/email/lost_password.txt', {'person': p, 'request': request, 'pwdrequest': obj}),
                    'no-reply@gucem.fr',
                    [ p.email ],
                    fail_silently=False,
                )
                    
                messages.success(request, "Votre demande a été prise en compte. Veuillez vérifier vos e-mail ainsi que le dossier Spam.")
                
        except SMTPException:
            raise GafcFormException("Erreur lors de l'envoi de l'e-mail.")

    
@method_decorator(sensitive_post_parameters('new_password','new_password2'), name="dispatch")
class NewPasswordProcessView(GafcFormView):
    """
    Actually change the password view
    """
    
    login_required=False
    """
    Accessible to everybody
    """
    
    def setup_forms(self, request, id, token):
        """
        Forms setup
        
        :param request: Request object
        :param id: LostPasswordRequest id
        :param token: LostPasswordRequest token
        """
        
        request.passRequest = get_object_or_404(LostPasswordRequest, id=id, token=token)
        
        if (dt.datetime.now(tz=timezone.utc) - request.passRequest.date).total_seconds() > 86400:
            request.passRequest.delete()
            raise PermissionDenied('Le lien n\'est plus valide. Les liens ne sont valable que 24h. Veuillez faire une nouvelle demande.')
        
        self.add_form(SetNewPasswordForm, request.passRequest.person )
        
    def get(self, request, *args, **kwargs):
        """
        Display the new password form
        
        :param request: Request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """        
        return render(request, 'gafc/new_lost_password.html', locals())
    
    def form_set_password(self, request, *args, **kwargs):
        """
        Special processing of form
        
        :param request: Request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """        
        
        self.forms['set_password'].save()
        request.passRequest.delete()
        messages.success(request, "Mot de passe modifié avec succès. Vous pouvez maintenant vous connecter.")
        return redirect('login')
        
@method_decorator(sensitive_post_parameters('password'), name="dispatch")
class LoginView(GafcFormView):
    """
    Login to the site
    """
    
    login_required = False
    """
    Accessible to everyone
    """
    
    def setup_forms(self, request):
        """
        Setup the login form
        
        :param request: Request object
        """        
        self.add_form(LoginForm)
        
    def get(self, request):
        """
        Display the login view
        
        :param request: Request object
        """        
        
        if 'next' in request.GET:
            self.forms['login'].helper.form_action = "login?next=%s"%(request.GET['next'])
            
        return render(request, 'gafc/login.html', locals())
    
    def form_login(self, request):
        """
        Login form processing
        
        :param request: Request object
        """
                    
        user = authenticate(request, username=self.forms['login'].cleaned_data['email'], password=self.forms['login'].cleaned_data['password'])

        if user is None:
            raise GafcFormException("Mauvais nom d'utilisateur/mot de passe")
        
        if user.has_perm('gafc.cant_login_person') and not user.is_superuser:
            raise PermissionDenied('Vous n\'avez pas l\'autorisation de vous connecter')
        
        try:
            user.lostpasswordrequest.delete()
        except:
            pass
        
        try:
            season = Season.objects.get(is_default=True)
        except Season.DoesNotExist:
            seasons = Season.objects.all();
            if seasons.count() > 0: # Season found. Set first as default
                season = seasons[0];
                season.is_default = True;
                season.save();
            else: # No season found, create one default
                season = Season.objects.create(name="default", is_default=True)
        
        login(request, user)
        
        request.session['season_id'] = season.id;
        
        return redirect(request.GET['next'] if 'next' in request.GET and url_has_allowed_host_and_scheme(request.GET['next'], allowed_hosts=None) else 'home') 
        
    
class LogoutView(GafcPermView):
    """
    Perform the logout and redirect to login
    """
    
    login_required = False
    """
    Accessible to everyone
    """
    
    def get(self, request):
        """
        Logout page
        
        :param request: Request object
        """
        
        logout(request)
        messages.success(request, "Vous êtes maintenant déconnecté")
        return redirect('login')
