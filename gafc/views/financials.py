# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
This module contains Views for financial stuffs
"""


from django.shortcuts import render,redirect, get_object_or_404

from django.core.exceptions import PermissionDenied

from django.contrib import messages

from django.http import FileResponse, Http404

from django.db import transaction
from django.db.models import Sum, Subquery, OuterRef, Count, F, Q
from django.db.models.functions import Sign

import os, csv, datetime

import datetime as dt

import gucem.settings as settings

from .common import GafcFormView, GafcPermView, GafcMultiGetView
from ..models import Cashing, Season, ForseenBalance, ForseenBalanceValue, Payment, AccountOperation, CashingFile, Category, FinancialCategory, InternalPayment, Person, HelloassoPayment, Event, FinancialCategoryTree, AccountBalance
from ..forms import GafcDummyForm, CashingMergeForm, ForseenBalanceForm, DuplicateForseenBalanceForm, ValidateForseenBalanceForm, AccountAttributionForm, AccountFileForm, CashingCommentForm, CashingCommentFileForm, PaymentForm, CashingFileForm, DeleteCashingFileForm, CashingAddPaymentForm, CashingDelPaymentForm, CashingAcceptForm, CashingRejectForm, CashingDevalidateForm, DeleteFinancialCategoryForm, DeleteFinancialCategoryTreeForm, FinancialCategoryForm, FinancialCategoryTreeForm, DefaultFinancialCategoryForm, FinancialCategoryWidget, EditPaymentForm, RefundPaymentForm, OperationToPaymentForm, DeleteHelloassoPaymentForm, GafcFormException, CashingAddEquipmentForm, DeleteCashingEquipmentForm


class NoPaymentException(Exception):
    pass

class CashingListView(GafcFormView):
    """
    List cashings
    """
    
    specific_perms = ['gafc.view_cashing']
    """
    Need gafc.view_cashing permission
    """

    def setup_forms(self, request, year=None):
        """
        Setup the forms
        
        :param request: Request object
        :param year: Year to display
        """
        
        if request.user.has_perm('gafc.add_cashing'):
            self.add_form(GafcDummyForm, "create_cashing", form_id="create_cashing")
            
        if request.user.has_perm('gafc.validate_cashing'):
            self.add_form(CashingMergeForm, request.user)
            
            
    def form_create_cashing(self, request, year=None):
        """
        Create a nes cashing and redirect
        
        :param request: Request object
        :param year: Year to display
        """

        c = Cashing.objects.create(person_ask=request.user, abandon=False)
        return redirect('cashing', cid=c.id)
    
    def get(self, request, year=None):
        """
        GET answer
        
        :param request: Request object
        :param year: Year to display
        """
        
        if year is None:
            year = dt.date.today().year
    
        cashings = Cashing.objects.select_related('person','person_ask').prefetch_related('payment_set','payment_set__event','payment_set__category','payment_set__financial','payment_set__person').filter(Q(date__year=year) | Q(date_done__year=year) | Q(date_done=None)).order_by('-date', '-date_done')
        
        cash_done = []
        cash_pending = []
        
        for cash in cashings:
            
            c = {'id': cash.id, 'date': cash.date, 'comment': cash.comment, 'person_ask__last_name': cash.person_ask.last_name, 'person_ask__first_name': cash.person_ask.first_name, 'person_ask__id': cash.person_ask.id, 'date_done': cash.date_done, 'rejected': cash.rejected}
            
            if cash.person is not None:
                c['person__id'] = cash.person.id
                c['person__last_name'] = cash.person.last_name
                c['person__first_name'] = cash.person.first_name
            else:
                c['person__id'] = None
                c['person__last_name'] = None
                c['person__first_name'] = None
            
            if c['date_done'] is None:
                cash_pending += [c,]
            else:
                cash_done += [c,]
            
            c['payments'] = cash.payment_set.all()
            
            meth = []
            cats = []
            
            c['g_payments'] = dict()
            
            for p in c['payments']:
                
                e_id = None
                
                if p.event is not None:
                    e_id = '%s-%s'%(p.event.id, p.category.id)
                    
                if not e_id in c['g_payments']:
                    c['g_payments'][e_id] = dict()
                    c['g_payments'][e_id]['P_category'] = p.category
                    c['g_payments'][e_id]['P_event'] = p.event
                    
                c['g_payments'][e_id][p.id] = p       
                
                method = p.method
                cat = p.category.name
                
                if not method in meth:
                    meth += [method, ]
                    
                if not cat in cats:
                    cats += [cat, ]
                    
            for (k,g) in c['g_payments'].items():
                totaux = dict()
                for (k2, p) in g.items():
                    if isinstance(k2, str) and k2.startswith('P_'): continue
    
                    if not p.method in totaux:
                        totaux[p.method] = 0.
                    totaux[p.method] += float(p.amount)
                g['P_total'] = totaux
                
            c['totaux'] = dict()
            c['totaux']['total'] = dict()
            c['totaux']['total']['total'] = 0.
            
            for m in meth:
                c['totaux'][m] = dict()
                for ca in cats:
                    c['totaux'][m][ca] = 0.
                    c['totaux']['total'][ca] = 0.
                c['totaux'][m]['total'] = 0.
            
            
            for p in c['payments']:
                method = p.method
                cat = p.category.name
                c['totaux'][method][cat] += float(p.amount)
                c['totaux'][method]['total'] += float(p.amount)
                c['totaux']['total'][cat] += float(p.amount)
                c['totaux']['total']['total'] += float(p.amount)
            
        
        years = Cashing.objects.values('date__year').annotate(tot=Count('id'))
        
        return render(request, 'gafc/financial.html', locals())

class ForseenBalanceListView(GafcFormView):
    """
    Display the list of forseen balances
    """
    
    specific_perms = ['gafc.view_forseenbalance',]
    """
    Need gafc.view_forseenbalance permission
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: request object
        """
        
        if request.user.has_perm('gafc.add_forseenbalance'):
            self.add_form(GafcDummyForm, what='add_forseenbalance', form_id='add_forseenbalance')
            
    def get(self, request):
        """
        Process GET request
        
        :param request: request object
        """
        
        bals = ForseenBalance.objects.filter(season__id=request.session['season_id'])
    
        return render(request, 'gafc/forseenbalances.html', locals())
    
    def form_add_forseenbalance(self, request):
        """
        Create a new forseenbalance and redirect to it
        
        :param request: request object
        """
        bal = ForseenBalance.objects.create(season=Season.objects.get(pk=request.session['season_id']))
        return redirect('fin_forseenbalance', balid=bal.id)
    
    
class ForseenBalanceView(GafcFormView):
    """
    The ForseenBalance View
    """
    
    specific_perms = ['gafc.view_forseenbalance',]
    """
    Need gafc.view_forseenbalance permission
    """
    
    def __addUpTotal(self, key, lst, income, outcome):
        """
        Propagate totals
        """
        if income is not None:
            lst[key]['income'] += float(income)
            
        if outcome is not None:
            lst[key]['outcome'] += float(outcome)
            
        if lst[key]['uplevel'] is not None:
            self.__addUpTotal(lst[key]['uplevel'], lst, income, outcome)
    
    
    def setup_forms(self, request, balid):
        """
        Setup the forms
        
        :param request: Request object
        :param balid: ForseenBalance id
        """
        self._fbal = get_object_or_404(ForseenBalance, id=balid)
        
        if request.user.has_perm('gafc.add_forseenbalance'):
            self.add_form(DuplicateForseenBalanceForm, self._fbal)
            
        if self._fbal.date_valid is None:
            if request.user.has_perm('gafc.change_forseenbalance'):
                self.add_form(ForseenBalanceForm, instance=self._fbal)
                
            if request.user.has_perm('gafc.validate_forseenbalance'):
                self.add_form(ValidateForseenBalanceForm, self._fbal, request.user)
            
    def form_forseen_copy(self, request, balid):
        """
        Duplicate the forseen balance
        
        :param request: Request object
        :param balid: ForseenBalance id
        """
        
        nfbal = self.forms['forseen_copy'].save()
        return redirect('fin_forseenbalance', balid=nfbal.id)
    
    
    def get(self, request, balid):
        """
        GET method
        
        :param request: Request object
        :param balid: ForseenBalance id
        """
        
        if self._fbal.date_valid is not None:
            tree = ForseenBalanceForm.getFintree()
            lst  = ForseenBalanceForm.tree2list(tree)
            ilst = ForseenBalanceForm.getIdentFromTree(tree)
            
            treelst = dict()
            treelst['tree'] = {'label': 'TOTAL', 'uplevel': None, 'income': 0., 'outcome': 0., 'tree': True}
            
            for t in lst:
                treelst[t['ident']] = {'label':t['label'], 'uplevel': t['uplevel'], 'income': 0., 'outcome': 0., 'tree': t['type'] != 'fincat'}
            
            for balval in self._fbal.forseenbalancevalue_set.all():
                formfieldid = ilst[(balval.cat_id, balval.fincat_id)]
                
                if formfieldid in treelst:
                    self.__addUpTotal(formfieldid, treelst, balval.income, balval.outcome)
    
        return render(request, 'gafc/forseenbalance.html', locals())
    
    
class AccountBalanceView(GafcPermView):
    """
    Account balance view
    """
    
    specific_perms = ['gafc.view_accountoperation',]
    """
    Need gafc.view_accountoperation permission
    """
    
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
        
        bals = []
        
        for b in AccountBalance.objects.order_by('date'):
            bals += [{ 'balance': b, 'Nops': 0, 'cumop': 0., 'diff': 0. if len(bals) == 0 else b.amount - bals[-1]['balance'].amount},]
        
        i = 0
        print(bals)
        
        if len(bals) > 1:
            for op in AccountOperation.objects.filter(date__gte=bals[0]['balance'].date,
                                                      date__lt=bals[-1]['balance'].date).order_by('date'):                
                while op.date >= bals[i]['balance'].date:
                    i += 1
                    
                bals[i]['Nops']  += 1
                bals[i]['cumop'] += float(op.amount)
                
        return render(request, 'gafc/account_balance.html', locals())
                        

class AccountOperationView(GafcFormView):
    """
    The accout operation view
    """
    
    specific_perms = ['gafc.change_accountoperation']
    """
    Need gafc.change_accountoperation permission
    """
    
    def setup_forms(self, request, year=None):
        """
        Setup the forms
        """
        
        self.add_form(AccountFileForm)
        
    def form_account_addfile(self, request, year=None):
        """
        Redirect to the file column attribution
        
        :param request: Request object
        :param year: Year of operations
        """
        
        self.forms['account_addfile'].save()
        return redirect('accountoperations_file')
        
        
    def get(self, request, year=None):
        """
        GET request
        
        :param request: Request object
        :param year: Year of operations
        """
        if year is None:
            return redirect('accountoperations', year=dt.datetime.now().year)
        
        min_date = dt.date(year-1,12,1)
        max_date = dt.date(year+1,1,31)
        
        cashings = Cashing.objects.prefetch_related('accountoperation_set').annotate(total_p=Subquery(Payment.objects.filter(cashing=OuterRef('pk')).values('cashing_id').annotate(total=Sum('amount')).values('total')), total_op=Subquery(AccountOperation.objects.filter(cashing=OuterRef('pk')).values('cashing_id').annotate(total=Sum('amount')).values('total'))).exclude(date_done=None).filter(date_done__gte=min_date, date_done__lte=max_date, rejected=False).order_by('date_done')
        opers = AccountOperation.objects.filter(cashing=None,date__gte=min_date, date__lte=max_date).order_by('date')
        
        elms = dict()
        
        for c in cashings:
            k = c.date_done.isoformat()
            
            if k not in elms:
                elms[k] = {'date': c.date_done,
                           'cashings': [],
                           'lonely_opers': []}
                
            elms[k]['cashings'] += [c, ]
            
        for o in opers:
            k = o.date.isoformat()
            
            if k not in elms:
                elms[k] = {'date': o.date,
                           'cashings': [],
                           'lonely_opers': []}
                
            elms[k]['lonely_opers'] += [o, ]
            
        elms = dict([(k,i) for k,i in sorted(list(elms.items()),key=lambda v: v[0])])
        
        years_c = Cashing.objects.values('date_done__year').annotate(tot=Count('id'))
        years_o = AccountOperation.objects.values('date__year').annotate(tot=Count('id'))
        
        years_a = [*[y['date_done__year'] for y in years_c], *[y['date__year'] for y in years_o]]
        years = []
        
        for y in years_a:
            if y is not None and y not in years:
                years += [y, ]
        
        return render(request, 'gafc/account_operations.html', locals())


class AccountOperationFileView(GafcFormView):
    """
    The file upload processing view
    """
    
    specific_perms = ['gafc.change_accountoperation']
    """
    Need gafc.change_accountoperation permission
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """
        
        if os.path.exists(settings.GAFC_TEMPFOLDER+'/account.csv'):
            with open(settings.GAFC_TEMPFOLDER+'/account.csv', 'r') as ifile:
                reads = csv.reader(ifile, delimiter=';')
                lines = [r for r in reads]
                
                self.add_form(AccountAttributionForm, lines)
                
    def form_import_account_operations(self, request):
        """
        Process the form
        
        :param request: Request object
        """
        nobj, nin = self.forms['import_account_operations'].save()
        messages.success(request, f"{nobj} objets dans le fichier, {nin} objets ajoutés dans la bdd")
        
        os.unlink(settings.GAFC_TEMPFOLDER+'/account.csv')
        
        return redirect('accountoperations')
        
    def get(self, request):
        """
        GET request
        
        :param request: Request object
        """
        
        if 'import_account_operations' not in self.forms:
            return redirect('accountoperations')
        
        return render(request, 'gafc/account_operations_file.html', locals())
    
class CashingUserView(GafcFormView):
    """
    Display the list of cashings owned by user
    """
    
    is_staff = False
    """
    Everyone can access this view
    """
    
    def setup_forms(self, request):
        """
        Setup the cashing form
        
        :param request: Request object
        """
        
        self.add_form(CashingCommentFileForm, request)
        
    def get(self, request):
        """
        Process GET request
        
        :param request: Request object
        """
        
        not_single_user = Payment.objects.exclude(cashing=None).exclude( person=F('cashing__person_ask')).values('cashing_id').distinct()
    
        cashs = Cashing.objects.select_related('person', 'person_ask').prefetch_related( 'payment_set').filter(person_ask=request.user).exclude(id__in=not_single_user)
        
        return render(request, 'gafc/feerequest.html', locals())
        
class CashingView(GafcFormView):
    """
    Display cashing
    """
    
    is_staff = False
    """
    Is available for everyone, restricted to own cashing for users
    """
    
    def setup_forms(self, request, cid):
        """
        Setup the forms
        
        :param request: Request object
        :param cid: Cashing id
        
        :raise Http404: If the cashing not exist
        :raise PermissionDenied: If missing rights
        """
        
        try:
            self.__cash = Cashing.objects.select_related('person','person_ask').prefetch_related('accountoperation_set').get(pk=cid)
        except Cashing.DoesNotExist:
            raise Http404('Cet encaissement n\'existe pas!')
            
        if not ((request.user.has_perm('gafc.staff_person') and request.user.has_perm('gafc.view_cashing')) or request.user.is_superuser or self.__cash.person_ask == request.user):
            raise PermissionDenied("Vous n'avez pas l'autorisation de voir cet encaissement!")
            
        if self.__cash.date_done is None:
            
            if request.user.has_perm('gafc.change_cashing') or self.__cash.person_ask == request.user:
                self.add_form(CashingCommentForm, not request.user.is_staff, request.user == self.__cash.person_ask, instance=self.__cash)
                self.add_form(CashingFileForm, request, self.__cash)
                self.add_form(DeleteCashingFileForm, self.__cash)
                
            if request.user.has_perm('gafc.change_cashing'):
                self.add_form(PaymentForm, user=request.user, cashing=self.__cash, initial={'person': self.__cash.person_ask}, show_event=True)
                self.add_form(CashingAddPaymentForm, self.__cash)
                self.add_form(CashingDelPaymentForm, self.__cash)
                self.add_form(CashingAddEquipmentForm, self.__cash)
                self.add_form(DeleteCashingEquipmentForm, self.__cash) 
                
            if request.user.has_perm('gafc.validate_cashing'):
                if self.__cash.payment_set.count() > 0:
                    self.add_form(CashingAcceptForm, request.user, instance=self.__cash)
                else:
                    self.add_form(CashingRejectForm, request.user, instance=self.__cash)
                    
        elif request.user.has_perm('gafc.validate_cashing'):
            self.add_form(CashingDevalidateForm, self.__cash)
                
    def form_cashing_validate(self, request, cid):
        """
        Generate the certificate if needed
        
        :param request: Request object
        :param cid: Cashing id
        """
        
        cash = self.forms['cashing_validate'].save()
        
        if(cash.abandon):
            self.generate_abandon_report(cash)
            
    def generate_abandon_report(self, cash):
        """
        Generate an abandon certificate
        
        :param cash: The Cashing instance
        """
    
        from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_RIGHT
        from reportlab.lib.pagesizes import A4
        from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
        from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
#        from reportlab.lib.units import inch
        
        with transaction.atomic():
            
            dbfile = CashingFile.objects.create(filename="recu_fiscal_%08i.pdf"%(cash.id), cashing=cash, autogenerate=True)
            
            doc = SimpleDocTemplate(dbfile.disk_fullfilepath,pagesize=A4,
                            rightMargin=72,leftMargin=72,
                            topMargin=72,bottomMargin=18, title="recu fiscal %08i"%(cash.id))
            
            
            Story = []
            
            styles=getSampleStyleSheet()
            styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
            styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
            styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))
            styles.add(ParagraphStyle(name='Title1', alignment=TA_CENTER, fontSize=32))
            styles.add(ParagraphStyle(name='Title2', alignment=TA_CENTER, fontSize=16))
            
            address = ["G.U.C.E.M.", "Grenoble Université Club Escalade Montagne", "388 Rue de la Passerelle","38400 Saint Martin D’Hères", "France"]
            
            Story.append(Paragraph("Numéro d'ordre: %08i"%cash.id, styles["Right"]))
            Story.append(Spacer(1,12))
            
            Story.append(Paragraph("Recu fiscal", styles['Title1']))
            Story.append(Spacer(1,20))
            Story.append(Paragraph("Don à organisme d'intérêt général", styles['Title2']))
            Story.append(Spacer(1,12))
            
            
            Story.append(Paragraph("<b>Bénéficiaire:</b>", styles["Normal"]))
            Story.append(Spacer(1,12))
            for a in address:
                Story.append(Paragraph(a, styles["Normal"]))
            
            Story.append(Spacer(1,12))
            Story.append(Paragraph("Association d'intérêt général à caractère sportif.", styles["Normal"]))            
            
            Story.append(Spacer(1,12))
            
            Story.append(Paragraph("<b>Donnateur:</b>", styles["Normal"]))  
            Story.append(Spacer(1,12))    
            
            donnator = ["%s %s"%(cash.person_ask.first_name, cash.person_ask.last_name), cash.person_ask.street, "%s %s"%(cash.person_ask.postcode, cash.person_ask.city), cash.person_ask.country]
            
            for a in donnator:
                Story.append(Paragraph(a, styles["Normal"]))
            
            Story.append(Spacer(1,30))
            Story.append(Paragraph("Le bénéficiaire reconnaît avoir reçu au titre des dons et versements ouvrant droit à réduction d'impôts, prévu à l'article 200 du CGI la somme de:", styles["Normal"]))      
            Story.append(Spacer(1,25))
            Story.append(Paragraph("<b>%.2f euros</b>"%cash.total_neg_amount, styles["Center"]))   
            Story.append(Spacer(1,25)) 
            Story.append(Paragraph("Date du don: %s"%(cash.date_done.strftime("%d/%m/%Y")), styles["Normal"]))   
            Story.append(Spacer(1,12)) 
            Story.append(Paragraph("Nature du don: Abandon de frais", styles["Normal"]))     
            Story.append(Spacer(1,30))
            Story.append(Paragraph("Fait pour servir et faire valoir ce que de droit", styles["Normal"]))
            Story.append(Spacer(1,30))
            Story.append(Paragraph("Fait à Grenoble,", styles["Normal"]))
            Story.append(Paragraph("le %s"%datetime.datetime.now().strftime("%d/%m/%Y"), styles["Normal"]))   
            Story.append(Spacer(1,12))
            Story.append(Paragraph("%s %s, %s"%(cash.person.first_name, cash.person.last_name ,cash.person.cashingabandonvalidator.titre), styles["Normal"]))
            
            sign_img = os.path.abspath( settings.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%cash.person.id )
            print(sign_img, os.path.isfile(sign_img))
            
            if os.path.isfile(sign_img):
                I = Image(sign_img)
                inchs = 100
                I.drawHeight = inchs*I.drawHeight / I.drawWidth
                I.drawWidth = inchs
                I.hAlign = 'LEFT'
                
                Story.append(Spacer(1,12))
                Story.append(I)
            
            doc.build(Story)        

    def get(self, request, cid):
        """
        Process GET request
        
        :param request: Request object
        :param cid: Cashing id
        """
        
        cash = self.__cash

        if request.user.has_perm('gafc.change_cashing'):
            payments = Payment.objects.select_related('category','event','event__season','person','financial','season').filter(Q(cashing=None) & Q(refund_date=None)).order_by('event','method')
            
            g_payments = dict()
            
            f_pid = []
            f_dates = dict()
            f_persons = dict()
            f_cat = dict()
            f_methods = []
            
            for p in payments:
                
                e_id = None
                
                if p.event is not None:
                    e_id = '%s-%s'%(p.event.id, p.category.id)
                    
                if not e_id in g_payments:
                    g_payments[e_id] = dict()
                    g_payments[e_id]['P_category'] = p.category
                    g_payments[e_id]['P_event'] = p.event
                    
                g_payments[e_id][p.id] = p
                
                f_pid += [p.id,]
                
                month = p.pay_done.strftime('%m-%Y')
                if month not in f_dates:
                    f_dates[month] = p.pay_done
    
                if p.category.id not in f_cat:
                    f_cat[p.category.id] = p.category
                    
                if p.method not in f_methods:
                    f_methods += [p.method]
                    
                if p.person.id not in f_persons:
                    f_persons[p.person.id] = p.person
                    
            for (k,g) in g_payments.items():
                totaux = dict()
                for (k2, p) in g.items():
                    if isinstance(k2, str) and k2.startswith('P_'): continue
        
                    if not p.method in totaux:
                        totaux[p.method] = 0.
                    totaux[p.method] += float(p.amount)
                g['P_total'] = totaux
                
            f_dates = dict(sorted(f_dates.items(), key=lambda k: k[1]))
            f_persons = dict(sorted(f_persons.items(), key=lambda k: k[1].last_name.upper()))
                
            
        c = dict()        
        c['payments'] = Payment.objects.select_related('category','event','event__season','person','financial', 'season').filter(cashing__id=cash.id).order_by('event','method')
        
        meth = []
        cats = []
        
        c['g_payments'] = dict()
        
        for p in c['payments']:
            
            e_id = None
            
            if p.event is not None:
                e_id = '%s-%s'%(p.event.id, p.category.id)
                
            if not e_id in c['g_payments']:
                c['g_payments'][e_id] = dict()
                c['g_payments'][e_id]['P_category'] = p.category
                c['g_payments'][e_id]['P_event'] = p.event
                
            c['g_payments'][e_id][p.id] = p       
            
            method = p.method
            cat = p.category.name
            
            if not method in meth:
                meth += [method, ]
                
            if not cat in cats:
                cats += [cat, ]
                
        for (k,g) in c['g_payments'].items():
            totaux = dict()
            for (k2, p) in g.items():
                if isinstance(k2, str) and k2.startswith('P_'): continue
    
                if not p.method in totaux:
                    totaux[p.method] = 0.
                totaux[p.method] += float(p.amount)
            g['P_total'] = totaux
            
        c['totaux'] = dict()
        c['totaux']['total'] = dict()
        c['totaux']['total']['total'] = 0.
        
        for m in meth:
            c['totaux'][m] = dict()
            for ca in cats:
                c['totaux'][m][ca] = 0.
                c['totaux']['total'][ca] = 0.
            c['totaux'][m]['total'] = 0.
    
        
        for p in c['payments']:
            method = p.method
            cat = p.category.name
            c['totaux'][method][cat] += float(p.amount)
            c['totaux'][method]['total'] += float(p.amount)
            c['totaux']['total'][cat] += float(p.amount)
            c['totaux']['total']['total'] += float(p.amount)
        
        return render(request, 'gafc/cashing.html', locals())

class CashingFileDownloadView(GafcPermView):
    """
    Download a file associated with a cashing
    """
    
    is_staff=False
    """
    Normal users can download their own files
    """
    
    def get(self, request, fid):
        """
        GET method
        
        :param request: Request object
        :param fid: CashingFile id
        """
    
        try:
            #cash = get_object_or_404(Cashing, pk=cid)
            cf = CashingFile.objects.select_related('cashing', 'cashing__person_ask' ).get(pk=fid)
        except Cashing.DoesNotExist:
            raise Http404('Cet encaissement n\'existe pas!')
            
        if not ((request.user.has_perm('gafc.staff_person') and request.user.has_perm('gafc.view_cashing'))
            or request.user.is_superuser or cf.cashing.person_ask == request.user):
            raise PermissionError("Vous n'avez pas l'autorisation de voir ce fichier!")
        
        filename = settings.GAFC_FILEFOLDER+'/%i/%s'%(cf.cashing.date.year,cf.disk_filename)
        
        return FileResponse(open(filename, 'rb'), filename=cf.filename)

class CashingSignatureFileDownloadView(GafcPermView):
    """
    Download the signature file
    """
    
    specific_perms = ["gafc.change_cashing",]
    """
    Need gafc.change_cashing permission
    """
    
    def get(self, request, fid):
        """
        GET method
        
        :param request: Request object
        :param fid: CashingSignatureFile id
        """
    
        filename = settings.GAFC_FILEFOLDER+'/signatures/%05i.jpg'%(fid)
        return FileResponse(open(filename, 'rb'), filename=filename)


class FinancialCategoryView(GafcFormView):
    """
    FinancialCategory management view
    """
    
    specific_perm = ['gafc.view_financialcategory']
    """
    Need gafc.view_financialcategory permission
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """
        if request.user.has_perm('gafc.delete_financialcategory'):
            self.add_form(DeleteFinancialCategoryForm)
            
        if request.user.has_perm('gafc.delete_financialcategorytree'):
            self.add_form(DeleteFinancialCategoryTreeForm)
            
        if request.user.has_perm('gafc.add_financialcategory'):
            self.add_form(FinancialCategoryForm)
            
        if request.user.has_perm('gafc.add_financialcategorytree'):
            self.add_form(FinancialCategoryTreeForm)
            
        if request.user.has_perm('gafc.change_financialcategory'):
            self.add_form(DefaultFinancialCategoryForm)
            self.add_form(GafcDummyForm, what="change_allowedcategories", form_id="change_allowedcategories")
            
    def get(self, request):
        """
        Process GET method
        
        :param request: Request object
        """
        
        tree = FinancialCategoryWidget.getFinlist(force=True, treespan='&nbsp;&nbsp;&nbsp;', catspan='&nbsp;&nbsp;&nbsp;')
        del tree[0]
        
        return render(request, 'gafc/fincat.html', locals())
    
    def form_change_allowedcategories(self, request):
        """
        Change allowed categories special form
        
        :param request: Request object
        """
        with transaction.atomic():
            # Clear
            for c in Category.objects.all():
                c.allowed_fincat.clear()
            
            # Set
            linklist = {}
            for a in request.POST.getlist('allowed'):
                fid, cid = a.split('-')
                
                if cid not in linklist:
                    linklist[cid] = []
                    
                linklist[cid] += [fid, ]
                
            for cid, lst in linklist.items():
                Category.objects.get(pk=cid).allowed_fincat.set(FinancialCategory.objects.filter(pk__in=lst))
        
class PaymentView(GafcFormView):
    """
    View/Edit payment
    """
    
    specific_perms=['gafc.view_payment']
    """
    Need gafc.view_payment permission
    """
    
    def setup_forms(self, request, payment_id):
        """
        Setup the forms
        
        :param request: Request object
        :param payment_id: Payment id
        """
        
        try:
            self.__payment = Payment.objects.select_related('person', 'event', 'category', 'encoder', 'cashing', 'season', 'helloassopayment' ).get(pk=payment_id)
        except Payment.DoesNotExist:
            raise Http404('Ce paiement n\'existe pas!')
            
        if request.user.has_perm('gafc.change_payment') and self.__payment.cashing is None and self.__payment.refund_who is None:
            self.add_form(EditPaymentForm, request.user, instance=self.__payment)
            
        if request.user.has_perm('gafc.delete_payment') and self.__payment.cashing is None and self.__payment.refund_who is None:
            self.add_form(RefundPaymentForm, request.user, instance=self.__payment)
            

    def get(self, request, payment_id):
        """
        Setup the forms
        
        :param request: Request object
        :param payment_id: Payment id
        """
        payment = self.__payment
        return render(request, 'gafc/singlepayment.html', locals())

class TransferView(GafcFormView):
    """
    View a single transfer
    """
    
    specific_perms = ["gafc.view_internalpayment",]
    """
    Need gafc.view_internalpayment
    """
    
    def get(self, request, tid):
        """
        GET method
        
        :param request: Request object
        :param tid: Transfer id
        """
    
        transfer = get_object_or_404(InternalPayment, pk=tid)
        return render(request, 'gafc/singletransfer.html', locals())

class OperationView(GafcFormView):
    """
    View single Operation
    """
    
    specific_perms = ['gafc.view_accountoperation',]
    """
    Need gafc.view_accountoperation
    """
    
    def setup_forms(self, request, opid):
        """
        Setup the forms
        
        :param request: Request object
        :param opid: Operation id
        """
        
        self.__operation = get_object_or_404(AccountOperation, pk=opid)
        
        if self.__operation.cashing is None and request.user.has_perm('gafc.change_accountoperation'):
            self.add_form(OperationToPaymentForm, self.__operation, request.user)
            
    def get(self, request, opid):
        """
        GET method
        
        :param request: Request object
        :param opid: Operation id
        """
        operation = self.__operation
        return render(request, 'gafc/singleoperation.html', locals())

class HelloassoPaymentView(GafcFormView):
    """
    Helloasso payments generic view
    """
    
    specific_perms = ['gafc.view_helloassopayment',]
    """
    Need gafc.view_helloassopayment permission
    """
    
    def setup_forms(self, request):
        """
        Setup the form handler
        """
        
        if request.user.has_perm('gafc.delete_helloassopayment'):
            self.add_form(DeleteHelloassoPaymentForm)
            
        if request.user.has_perm('gafc.change_helloassopayment'):
            self.add_form(GafcDummyForm, what="conver_payments", form_id="convert_payments")
            
            
            
    def get(self, request):
        """
        GET method: display the payment list
        """
        hello_payments = HelloassoPayment.objects.filter(deleted=False, payment=None)
        
        treeval = FinancialCategoryWidget.getFinlist()
        del treeval[0]
        
        return render(request, 'gafc/helloassopayments.html', locals())
    
    def form_convert_payments(self, request):
        """
        Specific handler for convert_payments method
        """
        
        with transaction.atomic():
            for i in request.POST.getlist('payment'):
                p = get_object_or_404(HelloassoPayment, pk=i)
                
                if p.payment is not None:
                    messages.error(request, ' Le paiement %s a déjà été encodé! '%(i))
                    continue
                
                try:
                    person = get_object_or_404(Person, pk=request.POST['person-%s'%i])
                    category = get_object_or_404(Category, pk=request.POST['category-%s'%i])
                    account = get_object_or_404(FinancialCategory, pk=request.POST['account-%s'%i])
                    event = None
                    comment = request.POST['comment-%s'%i]
    
                    if request.POST['event-%s'%i] != '':
                        try:
                            event = Event.objects.get(pk=request.POST['event-%s'%i])
                        except Event.DoesNotExist:
                            pass
                    
                    p.payment = Payment.objects.create(amount=p.amount,
                                        pay_done=p.date,
                                        method='IN',
                                        person=person,
                                        comment=comment,
                                        encoder=request.user,
                                        category=category,
                                        event=event,
                                        financial=account,
                                        season=event.season if event is not None else get_object_or_404(Season,pk=request.session['season_id']))
                    
                    p.save()
                except ValueError as err:
                    raise GafcFormException(err)
    
    
    
class BalanceView(GafcMultiGetView):
    """
    Display the balance
    """
    
    specific_perms = ['balance_financialcategory',]
    """
    Need balance_financialcategory permission
    """
    
    @classmethod
    def _get_fintree_t(cls, p, totaux):
        """
        Return the tree
        
        :param p: Tree root
        :param totaux: Summing array
        """
        ret = []
        
        for t in p:
            lst = cls._get_fintree_t(t.financialcategorytree_set.all(),totaux)
            catl = {}
            
            for l in t.financialcategory_set.all():
                if l.id in totaux:
                    for cid, tot in totaux[l.id].items():
                        if cid not in catl:
                            catl[cid] = {'category': Category.objects.get(pk=cid), 'total': [0.,0.,0.,0.], 'accounts': {}}
                            
                        catl[cid]['accounts'][l.id] = {'account': l, 'total': totaux[l.id][cid]}
            
            elm = {'elm': t, 'total': [0.,0.,0.,0.], 'sub': lst, 'cats': catl}
            ret += [ elm, ]
            
        return ret
    
    @classmethod
    def _sum_tree(cls, tree):
        """
        Sum the tree elements
        
        :param tree: Tree element
        """
        
        for k,c in tree['cats'].items():
            for aid, a in c['accounts'].items():
                for i in range(len(a['total'])):
                    c['total'][i] += a['total'][i]
                
            for i in range(len(c['total'])):
                tree['total'][i] += c['total'][i]
            
        for t in tree['sub']:
            t = cls._sum_tree(t)
            
            for i in range(len(t)):
                tree['total'][i] += t[i]
            
        return tree['total']
    
    @classmethod
    def _tree_to_list(cls, tree, lvl=0):
        """
        Flatten the tree to a list
        
        :param tree: Tree to flatten
        :param lvl: Current depth level
        """
        r = []
        
        for t in tree:
            tt = t.copy()
            del tt['sub']
            tt['level'] = lvl
            r += [tt, *cls._tree_to_list(t['sub'],lvl+1)]
        
        return r
            
    @classmethod
    def _build_tree(cls, totaux):
        """
        Build the tree and return the flatten one
        
        :param totaux: Summing array
        """
        ret = cls._get_fintree_t(FinancialCategoryTree.objects.filter(toplevel=None), totaux)
        
        totaux = [0.,0.,0.,0.]
        for r in ret:
            t = cls._sum_tree(r)
            totaux[0] += t[0]
            totaux[1] += t[1]
            totaux[2] += t[2]
            totaux[3] += t[3]
    
        return (cls._tree_to_list(ret), totaux)
    
    def balance_tmp(self, request):
        return self.balance(request, False)
    
    def balance_done(self, request):
        return self.balance(request, True)
    
    def balance(self, request, done=False):
        
        payments = Payment.objects.filter(season__id=request.session['season_id'], refund_date=None)
        ipayments = InternalPayment.objects.filter(season__id=request.session['season_id'])
        
        if done:
            payments = payments.exclude(cashing=None)
        
        pmt = payments.values('financial', 'category').annotate(total=Sum('amount'),sign=Sign('amount'))
        ipmt= ipayments.values('financial_c', 'category_c','financial_d','category_d').annotate(total=Sum('amount'))
        
        totaux = {}
        
        for p in pmt:
            if p['financial'] not in totaux:
                totaux[p['financial']] = {}
                
            if p['category'] not in totaux[p['financial']]:
                totaux[p['financial']][p['category']] = [0., 0.,0.,0.]
                
            if p['sign'] == 1:
                totaux[p['financial']][p['category']][0] += float(p['total'])
            else:
                totaux[p['financial']][p['category']][1] += float(p['total'])
                
        for p in ipmt:
            if p['financial_c'] not in totaux:
                totaux[p['financial_c']] = {}
                
            if p['category_c'] not in totaux[p['financial_c']]:
                totaux[p['financial_c']][p['category_c']] = [0., 0.,0.,0.]
                
            if p['financial_d'] not in totaux:
                totaux[p['financial_d']] = {}
                
            if p['category_d'] not in totaux[p['financial_d']]:
                totaux[p['financial_d']][p['category_d']] = [0., 0.,0.,0.]
            
            totaux[p['financial_c']][p['category_c']][0] += float(p['total'])
            totaux[p['financial_d']][p['category_d']][0] -= float(p['total'])
            
        for (k0,tf) in totaux.items():
            for (k1,tfc) in tf.items():
                if tfc[0] < 0.:
                    tfc[1] += tfc[0]
                    tfc[0] = 0.
                    
                    
        fbal = ForseenBalance.objects.filter(season__id=request.session['season_id']).exclude(date_valid=None).order_by('-date_valid').first()
        
        for balval in ForseenBalanceValue.objects.filter(balance=fbal):
            if balval.fincat.id not in totaux:
                totaux[balval.fincat.id] = {}
                
            if balval.cat.id not in totaux[balval.fincat.id]:
                totaux[balval.fincat.id][balval.cat.id] = [0,0,0,0]
                
            if balval.income is not None:
                totaux[balval.fincat.id][balval.cat.id][2] += float(balval.income)
                
            if balval.outcome is not None:
                totaux[balval.fincat.id][balval.cat.id][3] += float(balval.outcome)
            
        tree, totaux = self._build_tree(totaux)
        
        balance = totaux[0] + totaux[1]
        
        return render(request, 'gafc/balance.html', locals())
