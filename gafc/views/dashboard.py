# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
'Dashboard' views:
    - Dashboard
    - Graphs
"""


from django.shortcuts import render, redirect, get_object_or_404

from django.utils.decorators import method_decorator

from django.views.decorators.cache import cache_page

from django.http import HttpResponse

from django.db.models import OuterRef, Subquery, Q, Count, F, Sum

from django.contrib import messages

import io
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from ..models import EventState, License, Event, Participation, Category
from .common import GafcMultiGetView, GafcPermView

import gucem.settings as settings


class DashboardView(GafcPermView):
    """
    Dashboard view
    """
    
    def post(self, request, today=None):
        """
        Allow to change dashboard reference date
        
        :param request: request object
        :param today: reference date
        
        :return redirection to dashboard view
        """
        if 'date_today' in request.POST:
            today = dt.datetime.strptime(request.POST['date_today'],'%Y-%m-%d')
            messages.info(request, "Vision du tableau de bord en date du %s"%today.strftime('%d/%m/%Y'))
            return redirect('dashboard', today=today)
        
        messages.error("Action inconnue")
        return self.get(request, today)
    
    def get(self, request, today=None):
        """
        Display dashboard
        
        :param request: request object
        :param today: reference date
        
        :return dashboard view render
        """
        
        # Events
        if today is None:
            today = dt.datetime.now()
        
        states = EventState.objects.filter(Q(event=OuterRef('pk')) & ~Q(state__in=['CO'])).order_by('-date')
        events = Event.objects.select_related('category').prefetch_related('participation_set','eventstate_set').annotate(state=Subquery(states.values('state')[:1])).filter(finish__gte=today, finish__lte=today+dt.timedelta(days=15)).exclude(state='CN').order_by('begin')
        
        # Adherents
        end_date = dt.date(today.year if today.month < 9 else today.year+1,9,30)
        
        club_valid_licences = License.objects.prefetch_related('person').filter(number__regex=settings.GAFC_LICENSESCHEME, date_in__lte=today, date_end=end_date)
        
        males = club_valid_licences.filter(person__gender='M')
        females = club_valid_licences.filter(person__gender='F')
        
        ages = [ (dt.date.today() - l.person.birthdate).days/(365) for l in club_valid_licences ]
        
        avg_ages = np.mean(ages)
        med_ages = np.median(ages)
        std_ages = np.std(ages)
        
        if len(ages) > 0:
            percent_10 = np.percentile(ages, 10)
            percent_90 = np.percentile(ages, 90)
        
        return render(request, 'gafc/dashboard.html', locals())

@method_decorator(cache_page(15*60), name="dispatch")
class DashboardGraphsView(GafcMultiGetView):
    """
    Graphs for dashboard view
    The actual view is selected by "action" parameter of as_view
    """
    
    def setup(self, request, today=None):
        """
        Initialize the male and female db request for the graphs
        
        :param request: request object
        :param today: reference date
        """
        super().setup(request, today=today)
        self.today = today
        if self.today is None:
            self.today = dt.datetime.now()
    
        end_date = dt.date(self.today.year if self.today.month < 9 else self.today.year+1,9,30)
        
        club_valid_licences = License.objects.filter(number__regex=settings.GAFC_LICENSESCHEME, date_in__lte=self.today, date_end=end_date)
        
        self.males = club_valid_licences.filter(person__gender='M')
        self.females = club_valid_licences.filter(person__gender='F')
    
    def adhesions_timeline(self, request, today=None):
        """
        Return a SVG graph showing the evolution of adhesions with time
        
        :param request: request object
        :param today: reference date
        
        :return rendered SVG graph
        """
        
        Males   = {}
        Females = {}
        
        d = dt.date(self.today.year-1 if self.today.month < 9 else self.today.year,9,1)
        
        while d <= self.today.date():
            Males[d] = 0
            Females[d] = 0
            d += dt.timedelta(days=1)
            
        for m in self.males.values('date_in').annotate(c=Count('id')).order_by('date_in'):
            Males[m['date_in']] = m['c']
            
            
        for f in self.females.values('date_in').annotate(c=Count('id')).order_by('date_in'):
            Females[f['date_in']] = f['c']

        f, ax = plt.subplots()
        
        ax.stackplot(Males.keys(), np.cumsum(list(Males.values())), np.cumsum(list(Females.values())), colors=('green','lightgreen'), labels=('Hommes','Femmes'))
        
        ax.set_title('Evolution des adhésions', fontsize=18)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d %b'))
        ax.set_xlabel('Date d\'adhésion', fontsize=14)
        ax.set_ylabel('Nombre', fontsize=14)
        ax.tick_params(axis='x', labelsize=14)
        ax.tick_params(axis='y', labelsize=14)
        for tick in ax.get_xticklabels():
            tick.set_rotation(30)
        ax.legend(fontsize=12, loc='upper left')
        f.tight_layout()
    
        buf = io.BytesIO()
        f.savefig(buf, format='svg')
        plt.close(f)
        
        return HttpResponse(buf.getvalue(),content_type='image/svg+xml')
    
    def members_by_age(self, request, today=None):
        """
        Return a SVG graph showing the repartition of members by age
        
        :param request: request object
        :param today: reference date
        
        :return rendered SVG graph
        """
        
        male_ages = [ (self.today.date() - l.person.birthdate).days//(365) for l in self.males ]
        female_ages = [ (self.today.date() - l.person.birthdate).days//(365) for l in self.females ]
        
        f, ax = plt.subplots()
        
        mm = max(male_ages) if len(male_ages) > 0 else 0
        mf = max(female_ages) if len(female_ages) > 0 else 0
        
        if len(male_ages) > 0:
            ticks =  [v for v in range(1, max(mm, mf)+1,3) ]
            b = [v+0.5 for v in ticks ]
            
            hh = ax.hist((male_ages, female_ages), bins=b, label=('Hommes', 'Femmes'), color=("green","lightgreen"))
            ax.set_xticks([v for v in range(0, max(mm, mf)+1,10) ])
            ax.set_xlim([0,100])
            
            m = max(max(hh[0][0]),max(hh[0][1]))
            m = np.ceil(m/10)*10
            ax.set_ylim([0,m])
            ax.set_yticks([v for v in range(0, int(m)+1,5) ])
        
        ax.set_title('Age des adhérents', fontsize=18)
        ax.set_xlabel('Age', fontsize=14)
        ax.set_ylabel('Nombre de personnes', fontsize=14)
        ax.tick_params(axis='x', labelsize=14)
        ax.tick_params(axis='y', labelsize=14)
        ax.legend(fontsize=12)
    
        buf = io.BytesIO()
        f.savefig(buf, format='svg')
        plt.close(f)
        
        return HttpResponse(buf.getvalue(),content_type='image/svg+xml')
    
    def members_by_firstyear(self, request, today=None):
        """
        Return a SVG graph showing the repartition of members by first year of adhesion
        
        :param request: request object
        :param today: reference date
        
        :return rendered SVG graph
        """
        
        male_ages = [ int(l.number[4:8]) for l in self.males ]
        female_ages = [ int(l.number[4:8]) for l in self.females ]
        
        f, ax = plt.subplots()
        
        Mm = max(male_ages) if len(male_ages) > 0 else 0
        Mf = max(female_ages) if len(female_ages) > 0 else 0
        mm = min(male_ages) if len(male_ages) > 0 else 0
        mf = min(female_ages) if len(female_ages) > 0 else 0
        
        if len(male_ages) > 0:
            ticks =  [v for v in range(min(mm,mf), max(Mm, Mf)+1) ]
            b = [v+0.5 for v in ticks ]
            
            ax.hist((male_ages, female_ages), bins=b, label=('Hommes', 'Femmes'), color=("green","lightgreen"))
            ax.set_xticks(ticks[::3])
        
        ax.set_title('Ancienneté des adhérents', fontsize=18)
        ax.set_xlabel('Année de première adhésion', fontsize=14)
        ax.set_ylabel('Nombre de personnes', fontsize=14)
        ax.tick_params(axis='x', labelsize=14)
        ax.tick_params(axis='y', labelsize=14)
        ax.legend(fontsize=12)
    
        buf = io.BytesIO()
        f.savefig(buf, format='svg')
        plt.close(f)
        
        return HttpResponse(buf.getvalue(),content_type='image/svg+xml')
    
    
@method_decorator(cache_page(15*60), name="dispatch")
class MixityGraphsView(GafcPermView):
    """
    Graphs for dashboard view
    The actual view is selected by "action" parameter of as_view
    """
        
    def get(self, request, sid, catid=None):
        """
        GET handler
        
        :param request: Request object
        """
        
        from math import pi
        import numpy as np
        
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_formation']:
                allowed_types += [k, ]
        
        events = Event.objects.filter(season__id=sid, eventtype__in=allowed_types).order_by('-begin')
        
        c = None
        if catid is not None:
            c = get_object_or_404(Category, pk=catid)
            events = events.filter(category_id=catid)
        
        events_validated_notcancelled = events.filter(eventstate__state='VA').exclude(eventstate__state='CN')
        
        f, ax = plt.subplots(1,2,figsize=(12,6), subplot_kw={'projection':'polar'})            
        
        participants = Participation.objects.filter(event__in=events_validated_notcancelled, role='0PA', event_done=True).order_by('-person__gender').values('person__gender')
        staffs = Participation.objects.filter(event__in=events_validated_notcancelled, event_done=True).exclude( role='0PA').exclude(role='3PR').order_by('-person__gender').values('person__gender')
        
        P = np.zeros((3,2))
        
        if participants.count() > 0:
            participations_counts = participants.annotate(count=Count('person'))
            participants_counts = participants.annotate(count=Count('person', distinct=True))
            participations_days = participants.annotate(sums=Sum(F('event__finish') - F('event__begin')))
            
            
            for i,setp in enumerate((participations_counts, participants_counts, participations_days)):
                for p in setp:
                    if p['person__gender'] == 'M':
                        if 'count' in p:
                            P[i,0] = p['count']
                        elif 'sums' in p:
                            P[i,0] = int(p['sums'].total_seconds()/3600)
                    elif p['person__gender'] == 'F':
                        if 'count' in p:
                            P[i,1] = p['count']
                        elif 'sums' in p:
                            P[i,1] = int(p['sums'].total_seconds()/3600)
            
            pP = P[:,0] / np.sum(P, axis=1)
            
        else:
            pP = [0,0,0]
        
        S = np.zeros((3,2))
            
        if staffs.count() > 0:
            staff_counts = staffs.annotate(count=Count('person'))
            staffes_counts = staffs.annotate(count=Count('person', distinct=True))
            staffs_days = staffs.annotate(sums=Sum(F('event__finish') - F('event__begin')))
            
            
            for i,setp in enumerate((staff_counts, staffes_counts, staffs_days)):
                for p in setp:
                    if p['person__gender'] == 'M':
                        if 'count' in p:
                            S[i,0] = p['count']
                        elif 'sums' in p:
                            S[i,0] = int(p['sums'].total_seconds()/3600)
                    elif p['person__gender'] == 'F':
                        if 'count' in p:
                            S[i,1] = p['count']
                        elif 'sums' in p:
                            S[i,1] = int(p['sums'].total_seconds()/3600)
            
            pS = S[:,0] / np.sum(S, axis=1)
        else:
            pS = [0,0,0]
        
        startangle = -45
        stopangle  = 225
        
        left = (startangle * pi *2)/ 360 #this is to control where the bar start
        right = (stopangle * pi *2)/ 360 #this is to control where the bar start
        
        for i,p in enumerate(pP):
            x1 = p*(2*pi)*(stopangle - startangle)/360
            x2 = (1-p)*(2*pi)*(stopangle - startangle)/360
            
            ax[0].barh(i*1.1, x1, left=left, height=1, color='green', alpha=0.5+0.2*i, label='Hommes' if i == 2 else None)  
            ax[0].barh(i*1.1, x2, left=x1+left, height=1, color='lightgreen', alpha=0.5+0.2*i, label='Femmes' if i == 2 else None)
            
            ax[0].text(left+.1, i*1.1, P[i,0], ha='center', va='center')
            ax[0].text(right-.1, i*1.1, P[i,1], ha='center', va='center')
        
        ax[0].set_ylim(-3, 3)
        ax[0].set_yticks([]) 
        ax[0].set_xticks([]) 
        ax[0].spines.clear()
        # ax[0].set_title('Participant.s.es',fontsize=18)
        ax[0].legend(loc='upper center', bbox_to_anchor=(1.1, 1), fontsize=12)
        
        ax[0].text(pi*3/2, -.5, "Participations", ha='center', va='center', fontsize=16)
        ax[0].text(pi*3/2, .5, "Participant.s.es", ha='center', va='center', fontsize=16)
        ax[0].text(pi*3/2, 1.5, "Heures", ha='center', va='center', fontsize=16)
        
        for i,p in enumerate(pS):
            x1 = p*(2*pi)*(stopangle - startangle)/360
            x2 = (1-p)*(2*pi)*(stopangle - startangle)/360
            
            ax[1].barh(i*1.1, x1, left=left, height=1, color='green', alpha=0.5+0.2*i) 
            ax[1].barh(i*1.1, x2, left=x1+left, height=1, color='lightgreen', alpha=0.5+0.2*i) 
            
            ax[1].text(left+.1, i*1.1, S[i,0], ha='center', va='center')
            ax[1].text(right-.1, i*1.1, S[i,1], ha='center', va='center')
        
        ax[1].set_ylim(-3, 3)
        ax[1].set_yticks([]) 
        ax[1].set_xticks([])
        ax[1].spines.clear()
        # ax[1].set_title('Encadrant.s.es', fontsize=18)
        
        ax[1].text(pi*3/2, -.5, "Encadrements", ha='center', va='center', fontsize=16)
        ax[1].text(pi*3/2, .5, "Encadrant.s.es", ha='center', va='center', fontsize=16)
        ax[1].text(pi*3/2, 1.5, "Heures", ha='center', va='center', fontsize=16)
        
        f.suptitle('Global' if c is None else c.name, fontsize=30)
        
        buf = io.BytesIO()
        f.savefig(buf, format='svg')
        plt.close(f)
        
        return HttpResponse(buf.getvalue(),content_type='image/svg+xml')
