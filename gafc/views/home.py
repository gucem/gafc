# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
'Home' views:
    - Home
    - Set Season
"""


from django.shortcuts import render,redirect,get_object_or_404

from django.utils.decorators import method_decorator
from django.utils.http import url_has_allowed_host_and_scheme

from django.views.decorators.debug import sensitive_variables, sensitive_post_parameters
from django.views.generic import View

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from ..forms import GafcFormException, PersonFormUser, SympaMailingListUserForm, SeasonChoiceForm, ChangePasswordForm
from ..models import Season, License
from ..decorators import is_staff
from .common import GafcFormView


"""

This file contains home page views

"""

@method_decorator(is_staff, name='dispatch')
class SetSeasonView(View):
    """
    View to set the current season.
    Only support POST
    """
    def post(self, request):
        """
        POST handler
        
        :param request: Request object
        :return: Redirection to page given in request.GET['next']
        """
        
        form_s = SeasonChoiceForm(request.POST)
        
        if 'whattodo' in request.POST and request.POST['whattodo'] == "set_season" and form_s.is_valid():
            s = form_s.cleaned_data['season']
            request.session['season_id'] = s.pk
            messages.success(request, "La saison courante est maintenant %s"%(s.name))
            
            return redirect(request.GET['next'] if 'next' in request.GET and url_has_allowed_host_and_scheme(request.GET['next'], allowed_hosts=None) else 'home')
        else:
            return redirect('home')
            

@method_decorator(sensitive_post_parameters('old_password','new_password', 'new_password2'), name='dispatch')
@method_decorator(login_required, name='dispatch')
class HomeView(GafcFormView):    
    """
    Home view, with all information about logged user, licenses, ...
    """
    
    is_staff = False
    """
    Home is available for everyone.
    """
        
    def setup_forms(self, request, *args, **kwargs):
        """
        Setup the view: add forms
        
        :param request: request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        self.add_form(PersonFormUser, instance=request.user)
        self.add_form(SympaMailingListUserForm, request.user, True)
        self.add_form(ChangePasswordForm)
        
    
    def get(self, request):
        """
        Response to GET request: view home page
        
        :param request: request object
        
        :return Home view
        """
        licences = License.objects.filter(person=request.user).order_by('-date_in')
        current_mailings = [ ml.pk for ml in request.user.sympamailinglist_set.all() ]
        
        return render(request, 'gafc/home.html', locals())
    
    @method_decorator(sensitive_variables('o_pwd', 'n_pwd'))
    def form_update_pwd(self, *args, **kwargs):
        """
        Special processing of password update form
        
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        o_pwd = self.forms['update_pwd'].cleaned_data['old_password']
        n_pwd = self.forms['update_pwd'].cleaned_data['new_password']
        
        user = authenticate(username=self.request.user.email, password=o_pwd)
        
        if user is not None and user == self.request.user:
            self.request.user.set_password(n_pwd)
            self.request.user.save()
            
        else:
            self.forms['update_pwd'].add_error('old_password',"Le mot de passe actuel est erroné")
            raise GafcFormException()
    

