# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Hold Person management views
"""


from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.db import models
from django.db.models.functions import Upper, Lower, Substr, Concat
from django.db.models import Sum, Subquery, Q, OuterRef, F, Value
from django.db import transaction
from django.forms import formset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.forms.models import model_to_dict
from django.views.decorators.debug import sensitive_variables, sensitive_post_parameters
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from html.parser import HTMLParser

from ..forms import PersonForm, SetUnusablePasswordForm, SetNewPasswordForm, SympaMailingListUserForm, DeleteLicenseForm, MergePersonForm, LicenseForm, PersonCommentForm, DeletePersonCommentForm, DiscoveryLicenseForm
from ..models import Person, Season, Event, Participation, Category, License, PersonComment

from ..forms import *
from ..models import *
from ..decorators import *
from .. import ffcam_utils
from django.http import Http404
from django.shortcuts import get_object_or_404

from datetime import datetime as dt
from io import BytesIO
from openpyxl import Workbook, load_workbook

import random
import os
import re
import time
import datetime
import json
import pycurl
from urllib.parse import urlencode
import gucem.settings as settings

from .common import GafcFormView, GafcPermView

from django.contrib import messages

class PersonListView(GafcFormView):
    """
    List of persons view
    """
    
    specific_perms = ['gafc.view_person']
    """
    Must have the right to see person
    """
    
    def setup_forms(self, request, **kwargs):
        """
        Setup the forms
        
        :param request: Request object
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        if request.user.has_perm('gafc.add_person'):
            self.add_form(PersonForm)
            
    def get(self, request, page=1, name_first_letter=None):
        """
        Show the list of persons
        
        :param request: Request object
        :param page: page number in pagination
        :param letter: filter by first letter
        """
        
        try:
            page = int(page)
        except:
            page = 1
        
        persons = Person.objects.order_by(Lower('last_name'))
        
        if name_first_letter is not None:
            persons=persons.filter(last_name__istartswith=name_first_letter)
            
        paginate = Paginator(persons, 50)
        
        # Check pagination, redirect if needed
        if page < 1:
            if name_first_letter is not None:
                return redirect('list_persons', page=1, name_first_letter=name_first_letter)
            else:
                return redirect('list_persons', page=1)
        elif page > paginate.num_pages:
            if name_first_letter is not None:
                return redirect('list_persons', page=paginate.num_pages, name_first_letter=name_first_letter)
            else:
                return redirect('list_persons', page=paginate.num_pages)
        
        persons = paginate.page(page);
        avail_letters = Person.objects.values(first_letter=Upper(Substr('last_name',1,1))).order_by('first_letter').distinct()
        letters = [];
        
        for l in avail_letters:
            if re.search('^[A-Z]$',l['first_letter']):
                letters += [l,]
        
        return render(request, 'gafc/list_persons.html', locals())
            
            
    def form_person_save(self, request, **kwargs):
        """
        Special processing of form: set unusable password when new person is added
        
        :param request: Request object
        :param \\*\\*kwargs: Extra arguments ignored
        """
        user = self.forms['person_save'].save(commit=False)
        user.set_unusable_password()
        user.save()
        
        return redirect('person', user.id)
    
class PersonsStaffingView(GafcPermView):
    """
    Display staffing contribution in all categories
    """
    
    specific_perms = ['gafc.view_person']
    """
    Must have the right to see person
    """
    
    def get(self, request):
    
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_formation']:
                allowed_types += [k, ]
                
        season = Season.objects.get(pk=request.session['season_id'])
        events = Event.objects.filter(season__id=request.session['season_id'], eventstate__state='VA', eventtype__in=allowed_types).exclude(eventstate__state='CN').order_by('-begin')
        staff_participations = Participation.objects.select_related('person','event','event__category').filter(event__in = events, event_done=True).exclude(role='0PA')
        
        staffing = dict()
        cid_ok = []
        
        cats = Category.objects.all()
        
        for p in staff_participations:
            if not p.person.id in staffing:
                staffing[p.person.id] = dict()
                staffing[p.person.id]['total'] = {'pr_count': 0, 'pr_time': 0.,
                                        'su_count': 0, 'su_time': 0.,
                                        'co_count': 0, 'co_time': 0., 'person': p.person}
                
                for c in cats:
                    staffing[p.person.id][c.id] = {'pr_count': 0, 'pr_time': 0.,
                                            'su_count': 0, 'su_time': 0.,
                                            'co_count': 0, 'co_time': 0., 'person':p.person, 'category':c}
            
            if p.event.category.id not in cid_ok:
                cid_ok += [p.event.category.id,]
            
            if p.role == '3PR':
                staffing[p.person.id][p.event.category.id]['pr_count'] += 1
                staffing[p.person.id][p.event.category.id]['pr_time']  += p.event.duration
                staffing[p.person.id]['total']['pr_count'] += 1
                staffing[p.person.id]['total']['pr_time']  += p.event.duration
            elif p.role == '2SU':
                staffing[p.person.id][p.event.category.id]['su_count'] += 1
                staffing[p.person.id][p.event.category.id]['su_time'] += p.event.duration
                staffing[p.person.id]['total']['su_count'] += 1
                staffing[p.person.id]['total']['su_time'] += p.event.duration
            elif p.role == '1CO':
                staffing[p.person.id][p.event.category.id]['co_count'] += 1
                staffing[p.person.id][p.event.category.id]['co_time'] += p.event.duration
                staffing[p.person.id]['total']['co_count'] += 1
                staffing[p.person.id]['total']['co_time'] += p.event.duration
                
        staffing = dict(sorted(staffing.items(), key=lambda s: s[1]['total']['pr_time']*50+s[1]['total']['su_time']+s[1]['total']['co_time']*.5, reverse=True))
                
        return render(request, 'gafc/staffing.html', locals())


class PersonView(GafcFormView):
    """
    Detail view of Person
    """
    
    specific_perms = ['gafc.view_person']
    """
    Need to have view_person specific perm
    """
    
    def setup_forms(self, request, person_id):
        """
        Setup the forms 
        """
        
        self.__person = get_object_or_404(Person, pk=person_id)
        
        if request.user.has_perm('gafc.change_person'):
            self.add_form(PersonForm, instance=self.__person)
            self.add_form(SetUnusablePasswordForm, self.__person)
            self.add_form(SetNewPasswordForm, self.__person)
            self.add_form(SympaMailingListUserForm, self.__person, False)
            
        if request.user.has_perm('gafc.delete_license'):
            self.add_form(DeleteLicenseForm, self.__person)
            
        if request.user.has_perm('gafc.merge_person'):
            self.add_form(MergePersonForm, self.__person)
            
        if request.user.has_perm('gafc.add_license'):
            self.add_form(LicenseForm, self.__person)
            self.add_form(DiscoveryLicenseForm, self.__person)
            
        if request.user.has_perm('gafc.add_personcomment'):
            self.add_form(PersonCommentForm, self.__person, request.user)
            self.add_form(DeletePersonCommentForm, self.__person, request.user)
            
    def get(self, request, *args, **kwargs):
        """
        The Person Detail view
        
        :param request: Request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        p = self.__person
        
        
        particip = Participation.objects.select_related('event','event__category').prefetch_related('event__eventstate_set').filter(Q(person=p) & Q(event__season__id=request.session['season_id'])).order_by('-event__begin')
        licenses = License.objects.select_related('federation').filter(person=p).order_by('date_in')
        comments = PersonComment.objects.select_related('who').filter(person=p)
        
        if not request.user.has_perm('gafc.view_personcomment'):
            comments = comments.filter(who=request.user)
        
        is_caci_invalid = p.caci is None or p.caci < datetime.date.today().year - settings.GAFC_CACI_DURATION
        
        return render(request, 'gafc/person.html',locals())
    
    def form_merge_accounts(self, request, *args, **kwargs):
        """
        Redirect to merged account after merging
        
        :param request: Request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        p2 = self.forms['merge_accounts'].save()
        messages.success(request, "Les deux comptes ont été fusionnés avec succès.")
        return redirect('person', person_id=p2.id)
