# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""
Data in json format views:
    - Persons
    - Events
    - Payments
"""


from django.utils.decorators import method_decorator

from django.http import JsonResponse

from django.urls import reverse

from django.db.models.signals import post_save

from django.dispatch import receiver

from django.contrib import messages

from ..models import Person, Payment, EventState, Event
from ..decorators import json_cache_key, json_global_data_cache
from ..templatetags.gafc_tags import normalize
from .common import GafcMultiGetView

   
def invalidate_cache(view, dataname):
    """
    Invalidate cached view
    
    :param view: View url
    :param dataname: name of dataset
    
    :return True if key was in cache, False otherwise
    :rtype bool
    """
    from django.core.cache import cache
    
    key = json_cache_key(view, dataname)
    
    if cache.has_key(key):
        cache.delete(key)
        return True
    
    return False

@receiver(post_save, sender=Person)
def onPersonSaved(*args, **kwargs):
    """
    Person saved slot. Invalidate cache of person data.
    
    :param \\*args: Extra arguments ignored
    :param \\*\\*kwargs: Extra arguments ignored
    """
    invalidate_cache(reverse('persons_json'), 'persons')
    
@receiver(post_save, sender=Event)
def onEventSaved(*args, **kwargs):
    """
    Event saved slot. Invalidate cache of person data.
    
    :param \\*args: Extra arguments ignored
    :param \\*\\*kwargs: Extra arguments ignored
    """
    invalidate_cache(reverse('events_json'), 'events')
    invalidate_cache(reverse('events_json_all'), 'events')
    
@receiver(post_save, sender=Payment)
def onPaymentSaved(*args, **kwargs):
    """
    Payment saved slot. Invalidate cache of person data.
    
    :param \\*args: Extra arguments ignored
    :param \\*\\*kwargs: Extra arguments ignored
    """
    invalidate_cache(reverse('payments_json'), 'payments')


class JsonDataView(GafcMultiGetView):
    
    is_staff = False
    """
    Permission check is done in json_global_data_cache decorator, because of event-based permissions
    """
    
    
    @method_decorator(json_global_data_cache(15*60, 'persons'))
    def persons(self, request):
        """
        Person JSON data array
        
        :param request: request object
        """
        
        persons = Person.objects.all()
        
        arr = [{ 'display': '%s %s'%(p.first_name, p.last_name),
                 'normalized': '%s %s'%(normalize(p.first_name), normalize(p.last_name)),
                 'value': p.pk,} for p in persons]
        
        return JsonResponse(arr, safe=False)
    
    @method_decorator(json_global_data_cache(15*60, 'events'))
    def events(self, request, allevt=False):
        """
        Person JSON data array
        
        :param request: request object
        :param allevt: Display all events regardless of state
        """
        if allevt:
            events = Event.objects.all()
        else:
            events = Event.objects.exclude(pk__in=EventState.objects.filter(state="VA").values('event_id'))
        
        arr = [{ 'display': e.title,
                 'normalized': normalize(e.title),
                 'value': e.pk,
                 'category': e.category_id} for e in events]
        
        return JsonResponse(arr, safe=False)
    
    @method_decorator(json_global_data_cache(15*60, 'payments'))
    def payments(self, request):
        """
        Payment JSON data array
        
        :param request: request object
        """
        payments = Payment.objects.filter(cashing=None)
        
        arr = [{'display': 'P%i'%p.id,
                'normalized': 'p%i %i'%(p.id, p.id),
                'value': p.pk,} for p in payments]
        
        return JsonResponse(arr, safe=False)
