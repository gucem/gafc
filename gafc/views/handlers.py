from django.shortcuts import render
from django.contrib import messages
#from .logins import login_view

def handler404(request, exception):
    """
    404 handler
    
    :param request: Request object
    :param exception: Exception raised
    """
    messages.error(request,"404: Page introuvable.")
    return render(request, 'gafc/error_handler.html', locals(), status=404, using='errorless')

def handler403(request, exception):
    """
    403 handler
    
    :param request: Request object
    :param exception: Exception raised
    """
    messages.error(request,"403: Accès interdit: %s"%str(exception))
    return render(request, 'gafc/error_handler.html', locals(), status=403, using='errorless')
#    return login_view(request, error_txt, 403)

def handler500(request):
    """
    500 handler
    
    :param request: Request object
    """
    messages.error(request,"500: Erreur du serveur. Un email a été envoyé à l'administrateur avec les détails de l'erreur.")
    return render(request, 'gafc/error_handler.html', locals(), status=500, using='errorless')

