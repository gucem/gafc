# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


"""
Views for Category managements
"""


from django.shortcuts import render,get_object_or_404

from django.http import HttpResponse

from django.db.models.functions import TruncMonth
from django.db.models import Count, OuterRef, Subquery, Q

import datetime as dt

import matplotlib, io, csv
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from ..models import Category, Season, Payment, InternalPayment, Participation, Event, Staff, EventState
from ..forms import PaymentForm, InternalPaymentForm, CategoryForm, CategoryStaffForm, StaffValidationForm, DeleteStaffValidationForm, EventsFormset

from .financials import BalanceView
import gucem.settings as settings

from .common import GafcFormView, GafcPermView


class CategoryView(GafcFormView):
    """
    Category summary view
    """
    
    specific_perms = ['gafc.view_category']
    """
    Need gafc.view_category permission
    """
    
    def setup_forms(self, request, category_id):
        """
        Setup the forms
        
        :param request: Request object
        :param category_id: Category id
        """
        
        self.category = get_object_or_404(Category, pk=category_id)
        self.season = get_object_or_404(Season, id=request.session['season_id'])
        
        if request.user.has_perm('gafc.add_payment'):
            self.add_form(PaymentForm, user=request.user, category=self.category, season=self.season)
            
        if request.user.has_perm('gafc.add_internalpayment'):
            self.add_form(InternalPaymentForm, user=request.user, category_d=self.category, season=self.season)
            
        if request.user.has_perm('gafc.change_category'):
            self.add_form(CategoryForm, instance=self.category)
            
        if request.user.has_perm('gafc.add_staff'):
            self.add_form(CategoryStaffForm, self.category, self.season)
            
        if request.user.has_perm('gafc.add_validation'):
            self.add_form(StaffValidationForm, request.user)
            self.add_form(DeleteStaffValidationForm, request.user)
            
    def get(self, request, category_id):
        """
        Setup the forms
        
        :param request: Request object
        :param category_id: Category id
        """
        
        c = self.category
        
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_formation']:
                allowed_types += [k, ]
        
        events = Event.objects.prefetch_related('participation_set','eventstate_set').filter(category=c,season__id=request.session['season_id']).order_by('-begin')
        payments_ext = Payment.objects.select_related('person','financial','category','event').filter(Q(category=c) & Q(season__id=request.session['season_id'])).order_by('-event')
        payments_int = InternalPayment.objects.select_related('category_c','category_d','financial_c','financial_d','event').filter((Q(category_c=c) | Q(category_d=c)) & Q(season__id=request.session['season_id'])).order_by('-event')
        
        events_validated_notcancelled = events.filter(eventstate__state='VA').exclude(eventstate__state='CN')
        
        #staffing = Person.objects.filter(participation__event__in=events_validated_notcancelled, participation__event_done=True).annotate(pr_count=Count('participation',filter=Q(participation__role='3PR'))).annotate(su_count=Count('participation',filter=Q(participation__role='2SU'))).annotate(co_count=Count('participation',filter=Q(participation__role='1CO'))).annotate(en_total=Count('participation',filter=~Q(participation__role='0PA'))).exclude(en_total=0)
        
        staff_participations = Participation.objects.select_related('person','event').prefetch_related('event__eventstate_set').filter(event__in = events_validated_notcancelled, event_done=True, event__eventtype__in=allowed_types).exclude(role='0PA')
        
        staffing = dict()
        
        for p in staff_participations:
            if not p.person.id in staffing:
                staffing[p.person.id] = {'pr_count': 0, 'pr_time': 0.,
                                            'su_count': 0, 'su_time': 0.,
                                            'co_count': 0, 'co_time': 0., 'person':p.person}
                
            if p.role == '3PR':
                staffing[p.person.id]['pr_count'] += 1
                staffing[p.person.id]['pr_time']  += p.event.duration
            elif p.role == '2SU':
                staffing[p.person.id]['su_count'] += 1
                staffing[p.person.id]['su_time'] += p.event.duration
            elif p.role == '1CO':
                staffing[p.person.id]['co_count'] += 1
                staffing[p.person.id]['co_time'] += p.event.duration
        
        events_participations_counts = Participation.objects.filter(event__in=events_validated_notcancelled, role='0PA', event_done=True).values('event', 'event__begin','event__finish', 'event__eventtype').annotate(count=Count('person'))
        
        stats = { t:dict() for t in settings.EVENT_TYPES }
        
        for (k,s) in stats.items():
            
            s['count'] = 0
            s['dur'] = 0.
            s['p_count'] = 0
            s['dp_sum'] = 0.
            s['s_count'] = 0
            s['ds_sum'] = 0.
            
            evts = events_validated_notcancelled.filter(eventtype=k)
            
            for e in evts:
                s['count'] += 1
                s['dur'] += e.duration
        
        for v in events_participations_counts:
            s = stats[v['event__eventtype']]
            s['p_count'] += v['count']
            s['dp_sum'] += v['count']*Event.calc_duration(v['event__begin'], v['event__finish'])
                
        events_participations_counts = Participation.objects.filter(event__in=events_validated_notcancelled, event_done=True ).exclude(role='0PA' ).values('event', 'event__begin','event__finish', 'event__eventtype').annotate(count=Count('person'))
        
        for v in events_participations_counts:
            s = stats[v['event__eventtype']]
            s['s_count'] += v['count']
            s['ds_sum'] += v['count']*Event.calc_duration(v['event__begin'], v['event__finish'])
            
        stats['total'] = dict()
        
        for t in settings.EVENT_TYPES:
            for k in stats[t].keys():
                
                if k not in stats['total']:
                    stats['total'][k] = 0
                
                stats['total'][k] += stats[t][k]
                
        to_delete = []
        
        for k in stats:
            if k != 'total' and stats[k]['count'] == 0:
                to_delete += [k,]
                
        for k in to_delete:
            del stats[k]
                
        payments = []    
        totaux = {}
        
        for p in payments_ext:
            # Payment list
            if p.event:
                payments += [{'reason': p.comment, 'event': p.event, 'date': p.pay_done, 'amount': p.amount, 'payee': p.person,'type': 'ext', 'id': p.id, 'refunded': p.refund_date is not None, 'method': p.method, 'account': p.financial},]
            else:
                payments += [{'reason': p.comment, 'event': None, 'date': p.pay_done, 'amount': p.amount, 'payee': p.person, 'type': 'ext', 'id': p.id, 'refunded': p.refund_date is not None, 'method': p.method, 'account': p.financial},]
                
            #Accounting repartition
            if p.refund_date is None:
                if p.financial.id not in totaux:
                    totaux[p.financial.id] = {}
                    
                if p.category.id not in totaux[p.financial.id]:
                    totaux[p.financial.id][p.category.id] = [0., 0.]
                    
                if p.amount > 0:
                    totaux[p.financial.id][p.category.id][0] += float(p.amount)
                else:
                    totaux[p.financial.id][p.category.id][1] += float(p.amount)
                
        for p in payments_int:
            # Payment list
            p.amount = float(p.amount)
            payee = None
            
            if p.category_d == c:
                payee = p.category_c
                acc = p.financial_d
                acc_p = p.financial_c
                payments += [{'reason': p.comment, 'event': p.event, 'date': p.pay_done, 'amount': -p.amount, 'payee': payee,'type': 'int', 'id': p.id, 'refunded': False, 'method': p.method, 'account': acc, 'account_p': acc_p},]
            
            if p.category_c == c:
                payee = p.category_d
                acc = p.financial_c
                acc_p = p.financial_d
                payments += [{'reason': p.comment, 'event': p.event, 'date': p.pay_done, 'amount': p.amount, 'payee': payee,'type': 'int', 'id': p.id, 'refunded': False, 'method': p.method, 'account': acc, 'account_p': acc_p},]
                
            # Accounting repartition
            
            if c.id == p.category_c.id:
                if p.financial_c.id not in totaux:
                    totaux[p.financial_c.id] = {}
                    
                if c.id not in totaux[p.financial_c.id]:
                    totaux[p.financial_c.id][p.category_c.id] = [0., 0.]
                    
                totaux[p.financial_c.id][p.category_c.id][0] += float(p.amount)
            
            if c.id == p.category_d.id:
                if p.financial_d.id not in totaux:
                    totaux[p.financial_d.id] = {}
                    
                if c.id not in totaux[p.financial_d.id]:
                    totaux[p.financial_d.id][p.category_d.id] = [0., 0.]
                    
                totaux[p.financial_d.id][p.category_d.id][0] -= float(p.amount)
                
                
        for (k0,tf) in totaux.items():
            for (k1,tfc) in tf.items():
                if tfc[0] < 0.:
                    tfc[1] += tfc[0]
                    tfc[0] = 0.
        
                
        g_payments = dict()
        g_payments[None] = dict()
        g_payments[None]['total'] = 0.
        
        g_total = 0.    
        g_total_by_methods = dict()
        
        tree, totaux = BalanceView._build_tree(totaux)
        
        to_be_removed = []
        
        for t in tree:
            if t['total'][0] == t['total'][1] == 0.:
                to_be_removed += [t,]
                
        for t in to_be_removed:
            tree.remove(t)
        
        for p in payments:
            
            key = p['event'].id if p['event'] is not None else None
            
            if not key in g_payments:
                g_payments[key] = dict()
                g_payments[key]['total'] = 0.
                g_payments[key]['event'] = p['event']
                
            g_payments[key]['%s-%s'%(p['type'],p['id'])] = p      
            
            if p['refunded']:
                continue
                
            if not p['method'] in g_total_by_methods:
                g_total_by_methods[p['method']] = 0.
            
            g_total_by_methods[p['method']] += float(p['amount']) 
            g_payments[key]['total'] += float(p['amount'])
            g_total += float(p['amount'])
                
        staffs = Staff.objects.select_related('person').prefetch_related('validation_set').prefetch_related('validation_set__validator').filter(category=c, season__id=request.session['season_id']).order_by('-level')
        
        return render(request, 'gafc/category.html', locals())


class CategoryEventsEdit(GafcFormView):
    """
    Events bulk edit
    """
    
    specific_perms = ['gafc.change_event',]
    """
    Need gafc.change_event permissions
    """
    
    def setup_forms(self, request, category_id, year=None, month=None):
        """
        Setup the forms
        
        :param request: Request object
        :param category_id: Category id
        :param year: year. If None, current year is used
        :param month: month. If None, current month is used
        """
        
        if year is None or month is None or month > 12:
            if year is None:
                year = dt.date.today().year
            if month is None or month > 12:
                month = dt.date.today().month
                
        self.year = year
        self.month = month
        
        self.category = get_object_or_404(Category, pk=category_id)
        self.season = get_object_or_404(Season, pk=request.session['season_id'])
        
        states = EventState.objects.filter(Q(event=OuterRef('pk')) & ~Q(state__in=['CO'])).order_by('-date')
        events = Event.objects.annotate(state=Subquery(states.values('state')[:1])).filter(state='CR', category=self.category, season=self.season).order_by('begin')
        
        self.months = events.annotate(Month=TruncMonth('begin')).values_list('Month').distinct()
        self.events = events.filter(begin__year=year, begin__month=month)
        
        self.add_form(EventsFormset, self.events, self.season, self.category, request.user, form_id='save_events')
        
    def get(self, request, *args, **kwargs):
        """
        Display the view
        
        :param request: Request object
        :param \\*args: Extra arguments ignored
        :param \\*\\*kwargs: Extra arguments ignored
        """
        
        c = self.category
        s = self.season
        months = self.months
        category_id = self.category.id
        
        year = self.year
        month = self.month
        
        return render(request, 'gafc/category_events_edit.html', locals())


#TODO: import events from CSV??
#@is_staff
#@view_auth('gafc.change_event', 'Vous n\'avez pas l\'autorisation d\'éditer les événements')
#def events_edit(request, category_id, year=None, month=None):
#    
#    what = request.POST['whattodo'] if 'whattodo' in request.POST else None
#            
#    c = get_object_or_404(Category, pk=category_id)
#    s = get_object_or_404(Season, pk=request.session['season_id'])
#    
#    states = EventState.objects.filter(Q(event=OuterRef('pk')) & ~Q(state__in=['CO'])).order_by('-date')
#    events = Event.objects.annotate(state=Subquery(states.values('state')[:1])).filter(state='CR', category=c, season=s).order_by('begin')
#    
#    if what == "upload_csv":
#        filename = 'event-list-%i-%s.csv'%(request.user.id, dt.datetime.now().strftime('%Y%m%d_%H%M%S'))
#        file = settings.GAFC_TEMPFOLDER+'/'+filename
#
#        with open(file, 'wb+') as destination:
#            for chunk in request.FILES['file'].chunks():
#                destination.write(chunk)
#                
#        titles = None
#        example = None
#                
#        with open(file, 'r', newline='') as csvfile:
#            csvreader = csv.reader(csvfile)
#            for l in csvreader:
#                if titles is None:
#                    titles = l
#                elif example is None:
#                    example = l
#                else:
#                    break
#            
#        form_fields = EventsFieldAttributionForm(titles, examples=[example, ])
#        return render(request, 'gafc/events_csv.html', locals())
#    elif what == "set_columns" and 'filename' in request.POST:
#        titles = None
#        lines = []
#        file = settings.GAFC_TEMPFOLDER+'/'+request.POST['filename']
#        with open(file, 'r', newline='') as csvfile:
#            csvreader = csv.reader(csvfile)
#            for l in csvreader:
#                if titles is None:
#                    titles = l
#                else:
#                    lines += [l, ]
#                    
#            form_fields = EventsFieldAttributionForm(titles, request.POST)
#            forms = EventsFormset(events, form_fields.process(lines,[ e.id for e in events ]))
#            error_txt = form_fields.errors_str
#    else:
#        if request.method == 'GET':
#            if year is None or month is None or month > 12:
#                if year is None:
#                    year = dt.date.today().year
#                if month is None or month > 12:
#                    month = dt.date.today().month
#                
#                return redirect('category_events_edit', category_id=category_id, year=year, month=month)                
#        
#            months = events.annotate(Month=TruncMonth('begin')).values_list('Month').distinct()
#            events = events.filter(begin__year=year, begin__month=month)
#        
#        forms = EventsFormset(events, request.POST if what == 'save_events' else None)
#    
#    if what == 'save_events':
#        error_txt = forms.save(s,c, request.user)
##        if len(error_txt) == 0:
##            return redirect('category_events_edit', category_id=category_id)
#
#    return render(request, 'gafc/category_events_edit.html', locals())



class CategoryEventsCSVExportView(GafcPermView):
    """
    Export the events of the category
    """
    
    specific_perms = ['gafc.view_event']
    """
    Need gafc.view_event permission
    """

    def get(self, request, category_id):
        """
        Get method
        
        :param request: Request object
        :param category_id: Category id
        """
        
        c = get_object_or_404(Category, pk=category_id)
        s = get_object_or_404(Season, pk=request.session['season_id'])
        
        states = EventState.objects.filter(Q(event=OuterRef('pk')) & ~Q(state__in=['CO'])).order_by('-date')
        events = Event.objects.annotate(state=Subquery(states.values('state')[:1])).filter(state='CR', category=c, season=s).order_by('begin')
        
        buf = io.StringIO()
        writer = csv.DictWriter(buf, ['Subject','Id','Visibility','Type','Start date','Start time','End date','End time','Description','Location','Prix','Public after validation','Form','License'])
        
        writer.writeheader()
        
        for e in events:
            data = dict()
            data['Subject'] = e.title
            data['Id'] = '%i'%e.id
            data['Visibility'] = e.visibility
            data['Type'] = e.eventtype
            data['Start date'] = e.begin.strftime('%Y-%m-%d')
            data['Start time'] = e.begin.strftime('%H:%M')
            data['End date'] = e.finish.strftime('%Y-%m-%d')
            data['End time'] = e.finish.strftime('%H:%M')
            data['Description'] = e.description
            data['Location'] = e.place
            data['Prix'] = e.cost
            data['Public after validation'] = 'TRUE' if e.public_after_va  else 'FALSE'
            data['Form'] = 'TRUE' if e.link  else 'FALSE'
            data['License'] = 'TRUE' if e.onlyvalid  else 'FALSE'
            
            writer.writerow(data)        
        
        return HttpResponse(buf.getvalue(),content_type='text/csv')



class CategoryParticipationGraphView(GafcPermView):
    """
    Display the participation repartition within category
    """
    
    specific_perms = ['gafc.view_category']
    """
    Need gafc.view_category permission
    """
    
    def get(self, request, category_id):
        """
        GET method: return the graph as SVG file
        
        :param request: Request object
        :param category_id: Category id
        """
        
        c = get_object_or_404(Category, pk=category_id)
        
        allowed_types = []
        for k,t in settings.EVENT_TYPES.items():
            if t['valo_formation']:
                allowed_types += [k, ]
        
        events = Event.objects.filter(category=c, season__id=request.session['season_id'], eventtype__in=allowed_types).order_by('-begin')
        events_validated_notcancelled = events.filter(eventstate__state='VA').exclude(eventstate__state='CN')
        
        participations_counts = Participation.objects.filter(event__in=events_validated_notcancelled, role='0PA', event_done=True).values('person').annotate(count=Count('person'))
        
        staff_counts = Participation.objects.filter(event__in=events_validated_notcancelled, event_done=True).exclude( role='0PA').values('person').annotate(count=Count('person')) 
        
        f = plt.figure()
        
        occurs_p = [ v['count'] for v in participations_counts ]
        occurs_s = [ v['count'] for v in staff_counts ]
        
        ms = max(occurs_s) if len(occurs_s) > 0 else 0
        mp = max(occurs_p) if len(occurs_p) > 0 else 0
        
        ticks =  [v for v in range(0, max(mp, ms)+1) ]
        b = [v+0.5 for v in ticks ]
        
        if len(ticks) > 0:
            
            plt.hist((occurs_p, occurs_s), bins=b, label=('Participants', 'Encadrants'))
            plt.xticks(ticks)
        
        plt.title('Récurrence de la participation', fontsize=18)
        plt.xlabel('Nombre de participations', fontsize=14)
        plt.ylabel('Nombre de personnes', fontsize=14)
        plt.setp(plt.gca().get_xticklabels(), fontsize=14)
        plt.setp(plt.gca().get_yticklabels(), fontsize=14)
        plt.legend(fontsize=12)
    
        buf = io.BytesIO()
        f.savefig(buf, format='svg')
        plt.close(f)
        
        return HttpResponse(buf.getvalue(),content_type='image/svg+xml')