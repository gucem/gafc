# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont

"""gucem URL Configuration
"""

from django.urls import include,path, re_path
from django.conf.urls import handler404, handler500, handler403
from .views import redirect_view
from django.conf import settings
from django.conf.urls.static import static

from gafc.views import handlers as gv
import gucem.settings as gset

urlpatterns = [
    path('',redirect_view),
    path('gafc/main/', include('gafc.urls'), name="intranet"),
    path('gafc/minibus/', include('vehicles.urls'), name="minibus"),
    path('gafc/formation/', include('formation.urls'), name='formation'),
    path('gafc/keys/', include('keys.urls'), name='keys'),
    re_path('gafc/[a-z]*$',redirect_view),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # Only for development. Need static file server for production;

handler403 = gv.handler403
handler404 = gv.handler404
handler500 = gv.handler500

if gset.DEBUG:
    try:
        import debug_toolbar
        urlpatterns += [ path('__debug__/', include(debug_toolbar.urls)), ]
    except Exception as e:
        print(e)
