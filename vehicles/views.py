# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont



from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.contrib import messages

from .models import *
from .forms import *
from gafc import models as gm
from gafc.decorators import *
from gafc.views.financials import BalanceView

import numpy as np
import datetime as dt

from .models import Vehicle, VehicleEquipmentLink, VehicleTrackingCheck
from .forms import StatisticForms, VehicleForm, VehicleLinkForm

from gafc.forms.common import GafcDummyForm
from gafc.views.common import GafcFormView, GafcPermView


"""
Here are the controllers, each function might be called from url (as defined in urls.py).
The functions process the request arguments and perform actions.
Then, a view is returned.

TODO: Bug if a payment is encoded in an event, without tracking associated!
"""


class CalcFeeView(GafcFormView):
    """
    Fee calculation helper
    """
    
    login_required = False
    """
    Login not required
    """
    
    def setup_forms(self, request):
        """
        Setup the form
        
        :param request: Request object
        """
        
        self.add_form(GafcDummyForm, what="calc_fee", form_id="calc_fee")

    
    def repartition(self, km, cost_by_km, extra_fee, N, rounding):
        """
        This function calculate the repartition 
        """
        if len(cost_by_km) != len(extra_fee):
            raise ValueError("Cost by km length is not consistent with fee length")
        
        v_cost = np.ceil((np.array(cost_by_km)*km + np.array(extra_fee))/rounding)*rounding
        p_cost = np.ceil(sum(v_cost)/N/rounding)*rounding
        
        # Share remaining according to vehicle cost
        remaining = N*p_cost - sum(v_cost)
        v_cost += np.floor(np.array(cost_by_km)/sum(cost_by_km)*remaining/rounding)*rounding
        
        # Share rest
        remaining = N*p_cost - sum(v_cost)
        i=0
        while remaining > 0:
            v_cost[i] += rounding
            remaining -= rounding
            i += 1
            if i == len(v_cost):
                i = 0
        
        return (p_cost, v_cost)
    
    def check_positive(self, lst):
        if type(lst) == int or type(lst) == float:
            if lst < 0.:
                raise ValueError('Negative value!')                
        else:
            for l in lst:
                if l < 0.:
                    raise ValueError('Negative value!')
                    
                    
    def form_calc_fee(self, request):
        """
        Compute the fees
        
        :param request: Request object
        """
    
        results = False
    
        try:
            # Get variables from POST
            names = request.POST.getlist('vehicle_name')
            costs = [ float(v) for v in request.POST.getlist('vehicle_cost')]
            extra = [ float(v) for v in request.POST.getlist('vehicle_extracost')]
            doipay= [ v == "yes" for v in request.POST.getlist('vehicle_doipay')]
            N = int(request.POST['persons'])
            km = int(request.POST['km'])
            rounding = float(request.POST['rounding'])
            
            # Rebuild the form from POST data
            vehicles = [{'name': n, 'cost': c, 'extra': e, 'doipay': d } for (n,c,e,d) in tuple(zip(names, costs, extra, doipay))]
        
            # Check
            self.check_positive(costs)
            self.check_positive(extra)
            self.check_positive(doipay)
            self.check_positive(N)
            self.check_positive(km)
        
            if len(names) == len(costs) == len(extra) == len(doipay):
                # Actually compute
                (p_cost, v_cost) = self.repartition(km, costs, extra, N, rounding)
                
                results = True
                for i in range(len(v_cost)):
                    vehicles[i]['total'] = v_cost[i]
                    vehicles[i]['total_abs'] = v_cost[i]
                    
                    if vehicles[i]['doipay']:
                        vehicles[i]['total'] -= p_cost
            else:
                raise ValueError('Missing data! %i, %i, %i, %i'%(len(names), len(costs), len(extra), len(doipay)))
                
        except Exception as e:
            messages.error(request, str(e))
            
        a_vehicles = Vehicle.objects.filter(display=True)
        
        return render(request, 'vehicles/calc_fee.html', locals())
    
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
            
        a_vehicles = Vehicle.objects.filter(display=True)
        
        return render(request, 'vehicles/calc_fee.html', locals())


class VehicleTableView(GafcPermView):
    """
    Display the annual table for the vehicle
    """

    specific_perms = ['vehicles.view_vehicle',]
    """
    Need vehicle.view_vehicle permission
    """

    @classmethod
    def get_data(cls, date_from, date_to):
        """
        Return the aggretated data from all vehicles
        """
        c_id = []
            
        vehicles = VehicleEquipmentLink.objects.prefetch_related('equipment').all()
        
        data = []   
        totaux = {}
        
        for v in vehicles:
            if v.equipment.category_id not in c_id:
                c_id += [v.equipment.category_id, ]
                
                for p in gm.Payment.objects.select_related('category','encoder','person','financial').filter(category__id=v.equipment.category_id, event=None, refund_date=None, pay_done__gte=date_from, pay_done__lte=date_to):
                    data += [{
                        'vehicle': None,
                        'tracking': None,
                        'category': p.category,
                        'date': p.pay_done.date(),
                        'payments': [{'id': p.id, 'amount': p.amount, 'method': p.method, 'payee': p.person, 'external': True, 'comment': p.comment, 'encoder': p.encoder},],
                        'km_in': None,
                        'km_out': None,
                        'km': None,
                        'th_cost': None,
                        'diff': None,
                        'payed' : float(p.amount) if p.amount > 0. else 0.,
                        'fee': float(p.amount) if p.amount < 0. else 0.,
                        'eff_payed': float(p.amount)
                        },]
                    
                    #Accounting repartition
                    if p.financial.id not in totaux:
                        totaux[p.financial.id] = {}
                        
                    if p.category.id not in totaux[p.financial.id]:
                        totaux[p.financial.id][p.category.id] = [0., 0.]
                        
                    if p.amount > 0:
                        totaux[p.financial.id][p.category.id][0] += float(p.amount)
                    else:
                        totaux[p.financial.id][p.category.id][1] += float(p.amount)
                
                for p in gm.InternalPayment.objects.select_related('category_c','category_d','person').filter(category_c=v.equipment.category, event=None, pay_done__gte=date_from,  pay_done__lte=date_to):
                    data += [{
                        'vehicle': None,
                        'tracking': None,
                        'category': p.category_c,
                        'date': p.pay_done.date(),
                        'payments': [{'id': p.id, 'amount': p.amount, 'method': p.method, 'payee': p.category_d, 'external': False, 'comment': p.comment, 'encoder': p.person},],
                        'km_in': None,
                        'km_out': None,
                        'km': None,
                        'th_cost': None,
                        'diff': None,
                        'payed' : float(p.amount),
                        'fee': 0.,
                        'eff_payed': float(p.amount)
                        },]
                    
                    # Accounting repartition
                    if p.financial_c.id not in totaux:
                        totaux[p.financial_c.id] = {}
                        
                    if p.category_c.id not in totaux[p.financial_c.id]:
                        totaux[p.financial_c.id][p.category_c.id] = [0., 0.]
                        
                    totaux[p.financial_c.id][p.category_c.id][0] += float(p.amount)
                    
                
                for p in gm.InternalPayment.objects.select_related('category_c','category_d','person').filter(category_d=v.equipment.category, event=None, pay_done__gte=date_from, pay_done__lte=date_to):
                    data += [{
                        'vehicle': None,
                        'tracking': None,
                        'category': p.category_d,
                        'date': p.pay_done.date(),
                        'payments': [{'id': p.id, 'amount': -p.amount, 'method': p.method, 'payee': p.category_c, 'external': False, 'comment': p.comment, 'encoder': p.person},],
                        'km_in': None,
                        'km_out': None,
                        'km': None,
                        'th_cost': None,
                        'diff': None,
                        'payed' : 0.,
                        'fee': float(-p.amount),
                        'eff_payed': float(-p.amount)
                        },]
                    
                    # Accounting repartition
                    if p.financial_d.id not in totaux:
                        totaux[p.financial_d.id] = {}
                        
                    if p.category_d.id not in totaux[p.financial_d.id]:
                        totaux[p.financial_d.id][p.category_d.id] = [0., 0.]
                        
                    totaux[p.financial_d.id][p.category_d.id][0] -= float(p.amount)
                
            track = gm.EquipmentTracking.objects.select_related('person','equipment','event').prefetch_related('equipmentmetadata_set','vehicletrackingcheck_set').filter(equipment=v.equipment, date__gte=date_from, date__lte=date_to)
            
            for t in track:
                datat = {
                        'vehicle': v,
                        'tracking': t,
                        'category': t.equipment.category,
                        'date': t.date,
                        'payments': [],
                        'km_in': None,
                        'km_out': None,
                        'km': None,
                        'th_cost': None,
                        'diff': None,
                        'payed' : 0.,
                        'fee': 0.,
                        'eff_payed':0.
                        }
                
                try:
                    #datat['km_in'] = int(gm.EquipmentMetaData.objects.get(tracking=t, meta=v.km_start).data)
                    datat['km_in'] = int(t.equipmentmetadata_set.get(meta=v.km_start).data)
                except gm.EquipmentMetaData.DoesNotExist:
                    pass
                except TypeError:
                    pass
                
                try:
                    #datat['km_out'] = int(gm.EquipmentMetaData.objects.get(tracking=t, meta=v.km_end).data)
                    datat['km_out'] = int(t.equipmentmetadata_set.get(meta=v.km_end).data)
                except gm.EquipmentMetaData.DoesNotExist:
                    pass
                except TypeError:
                    pass
                
    
                if t.event is not None:
                    for p in gm.Payment.objects.select_related('category','encoder','person','event','cashing').filter(category=v.equipment.category, event=t.event, refund_date=None):
                        datat['payments'] += [ {'id': p.id, 'amount': float(p.amount), 'method': p.method, 'payee': p.person, 'external': True, 'comment': p.comment, 'encoder': p.encoder, 'cashing': p.cashing} ]
                        
                        #Accounting repartition
                        if p.financial.id not in totaux:
                            totaux[p.financial.id] = {}
                            
                        if p.category.id not in totaux[p.financial.id]:
                            totaux[p.financial.id][p.category.id] = [0., 0.]
                            
                        if p.amount > 0:
                            totaux[p.financial.id][p.category.id][0] += float(p.amount)
                        else:
                            totaux[p.financial.id][p.category.id][1] += float(p.amount)
                        
                    for p in gm.InternalPayment.objects.select_related('category_d','category_c','financial_d','financial_d','person','event').filter(category_c=v.equipment.category, event=t.event):
                        datat['payments'] += [ {'id': p.id, 'amount': float(p.amount), 'method': p.method, 'payee': p.category_d, 'external': False, 'comment': p.comment, 'encoder': p.person, 'cashing': None } ]
                        
                        # Accounting repartition
                        if p.financial_c.id not in totaux:
                            totaux[p.financial_c.id] = {}
                            
                        if p.category_c.id not in totaux[p.financial_c.id]:
                            totaux[p.financial_c.id][p.category_c.id] = [0., 0.]
                            
                        totaux[p.financial_c.id][p.category_c.id][0] += float(p.amount)
                        
                    for p in gm.InternalPayment.objects.select_related('category_d','category_c','financial_d','financial_d','person','event').filter(category_d=v.equipment.category, event=t.event):
                        datat['payments'] += [ {'id': p.id, 'amount': float(-p.amount), 'method': p.method, 'payee': p.category_c, 'external': False, 'comment': p.comment, 'encoder': p.person, 'cashing': None } ]
                        
                        # Accounting repartition
                        if p.financial_d.id not in totaux:
                            totaux[p.financial_d.id] = {}
                            
                        if p.category_d.id not in totaux[p.financial_d.id]:
                            totaux[p.financial_d.id][p.category_d.id] = [0., 0.]
                            
                        totaux[p.financial_d.id][p.category_d.id][0] -= float(p.amount)
                        
                    datat['date'] = t.event.begin.date()
                    
                for p in datat['payments']:
                    if p['amount'] > 0.:
                        datat['payed'] += p['amount']
                    else:
                        datat['fee'] += p['amount']
                        
                if datat['km_in'] is not None and datat['km_out'] is not None:
                    datat['km'] = datat['km_out'] - datat['km_in']
                    datat['th_cost'] = float(datat['km']) * float(v.vehicle_type.cost_per_km) 
                    datat['diff'] = datat['payed'] - datat['th_cost']
                    datat['eff_payed'] = datat['payed'] + datat['fee']
                
                    
                data += [datat, ]
                
            data = sorted(data, key=lambda d: d['date'])
            
            last_km = None
            
            for d in data:
                    
                if d['km_in'] is not None and last_km is not None:
                    d['km_diff'] = d['km_in'] - last_km
                else:
                    d['km_diff'] = 0
                    
                if d['km_out'] is not None:
                    last_km = d['km_out']
                    
        return (data, totaux)

    def get(self, request, tid=None, action=None, year=None):
        """
        GET method
        
        :param request: Request object
        :param tid: Tracking id
        :param action: Action to perform
        :param year: Year to display
        """
        
        if tid is not None and action is not None:
            tr = get_object_or_404(gm.EquipmentTracking, pk=tid)
            
            if action == "addcheck":
                VehicleTrackingCheck.objects.update_or_create(person=request.user, tracking=tr, defaults={'date': dt.datetime.now()})
                return redirect('vehicle_view')
                
            if action == "delcheck":
                get_object_or_404(VehicleTrackingCheck, person=request.user, tracking=tr).delete()
                return redirect('vehicle_view')
            
        if year is None:
            return redirect('vehicle_view_year', year=dt.date.today().year)
        
        try:
            year = int(year)
        except:
            return redirect('vehicle_view_year', year=dt.date.today().year)  
        
        try:
            year_min = gm.EquipmentTracking.objects.order_by('date')[0].date.year
            year_max = gm.EquipmentTracking.objects.order_by('-date')[0].date.year
        except Exception as e:
            print(e)
            year_min = dt.date.today().year
            year_max = dt.date.today().year
            
        years = range(year_min,year_max+1)              
            
        data,totaux = self.get_data(dt.date(year,1,1), dt.date(year+1,1,1))
        if request.user.has_perm('vehicles.add_vehicletrackingcheck'):
            for d in data:
                d['checked'] = None
                if d['tracking'] is not None:
                    d['checked'] = True
                    try:
                        d['check'] = VehicleTrackingCheck.objects.select_related('person').get(person=request.user, tracking=d['tracking'])
                    except VehicleTrackingCheck.DoesNotExist:
                        d['checked'] = False
        
        return render(request, 'vehicles/view.html', locals())


class VehicleFinancialView(GafcPermView):
    """
    Display the pending payments for the vehicle
    """

    specific_perms = ['vehicles.financial', ]

    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
        
        c_id = []
        
        repart = {}
        repart['TOTAL'] = (None, {'CA': 0., 'CH': 0., 'IN': 0., 'VM': 0., 'BC': 0., 'BO': 0., 'TOTAL': 0.})
        
        vehicles = VehicleEquipmentLink.objects.all()
        
        for v in vehicles:
            if v.equipment.category.id not in c_id:
                c_id += [v.equipment.category.id, ]
                repart[v.equipment.category.id] = (v.equipment.category, {'CA': 0., 'CH': 0., 'IN': 0., 'VM': 0., 'BC': 0., 'BO':0., 'TOTAL': 0.})
                
        payments = gm.Payment.objects.filter(cashing=None, category__in=c_id, refund_date=None).order_by('-pay_done')
        
        for p in payments:            
            repart[p.category.id][1][p.method] += float(p.amount)
            
        for (k,c) in repart.items():
            if k == 'TOTAL':
                continue
            
            for (m, a) in c[1].items():
                if m == 'TOTAL':
                    continue
                
                repart['TOTAL'][1][m] += a
                repart[k][1]['TOTAL'] += a
                repart['TOTAL'][1]['TOTAL'] += a
                
        return render(request, 'vehicles/financial.html', locals())


class VehicleStatisticView(GafcFormView):
    """
    Display the statistics
    """
    
    specific_perms = ['vehicles.financial',]
    
    def setup_forms(self, request):
        """
        Setup the stat form
        
        :param request: Request object
        """
        
        self.add_form(StatisticForms)
        
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
        
        return self.statistics(request,
                               self.forms['statistics_calc'].fields['date_from'].initial,
                               self.forms['statistics_calc'].fields['date_to'].initial)
        
    def form_statistics_calc(self, request):
        """
        Data choosen by the form
        
        :param request: Request object        
        """
        
        return self.statistics(request,
                               self.forms['statistics_calc'].cleaned_data['date_from'],
                               self.forms['statistics_calc'].cleaned_data['date_to'])
        
        
        
    def statistics(self, request, from_d, to_d):
        """
        The statistics view
        
        :param request: Request object
        :param from_d: Period start date
        :param to_d: Period stop date
        """
        
        print(from_d, to_d)
            
        data,totaux = VehicleTableView.get_data(from_d, to_d)
        
        km_diff = 0
        by_eventcat = {}
        start = None
        finish = None
        maxdiff = 5.
        fee = 0.
        paf = 0.
        
        for d in data:            
            if d['km_in'] is not None:
                if start is None:
                    start = (d['km_in'], d['date'])
                    
                if d['km_out'] is not None:
                    finish = (d['km_out'], d['date'])
                else:
                    finish = (d['km_in'], d['date'])
                    
                
            km_diff += d['km_diff'] if d['km_diff'] is not None else 0
            fee += d['fee'] if d['fee'] is not None else 0.
            paf += d['payed'] if d['payed'] is not None else 0.
    
            
            if d['tracking'] is not None and d['tracking'].event is not None:
                cid = d['tracking'].event.category.id
                if cid not in by_eventcat:
                    by_eventcat[cid] = { 'category': d['tracking'].event.category, 'km': 0, 'diff': 0., 'N': 0}
                    
                if d['km'] is not None: by_eventcat[cid]['km'] += d['km']
                if d['diff'] is not None: by_eventcat[cid]['diff'] += d['diff']
                by_eventcat[cid]['N'] += 1
                
        by_eventcat['total'] = { 'category': None, 'km': 0, 'diff': 0., 'N': 0 }
                
        for k,evc in by_eventcat.items():
            if k == 'total':
                continue
            
            maxdiff = max(maxdiff, abs(evc['diff']))
            by_eventcat['total']['km'] += evc['km']
            by_eventcat['total']['diff'] += evc['diff']
            by_eventcat['total']['N'] += evc['N']
            
        km = finish[0] - start[0]
        
        for (k0,tf) in totaux.items():
            for (k1,tfc) in tf.items():
                if tfc[0] < 0.:
                    tfc[1] += tfc[0]
                    tfc[0] = 0.
        
        tree, totaux = BalanceView._build_tree(totaux)
        
        to_be_removed = []
        
        for t in tree:
            if t['total'][0] == t['total'][1] == 0.:
                to_be_removed += [t,]
                
        for t in to_be_removed:
            tree.remove(t)
        
        return render(request, 'vehicles/statistics.html', locals())

class VehicleSettingView(GafcFormView):
    """
    Vehicle settings view
    """
    
    specific_perms = ['vehicles.change_vehicle']
    """
    Need vehicles.change_vehicle
    """
    
    def setup_forms(self, request):
        """
        Setup the forms
        
        :param request: Request object
        """
        
        self.add_form(VehicleForm)
        self.add_form(VehicleLinkForm)
    
    def get(self, request):
        """
        GET method
        
        :param request: Request object
        """
            
        vehicles = Vehicle.objects.all()
        links = VehicleEquipmentLink.objects.all()
        
        return render(request, 'vehicles/settings.html', locals())
