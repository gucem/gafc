# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


from django import forms

from gafc.forms.common import  MyDateInput, GafcModelForm, GafcForm
import datetime as dt

from .models import Vehicle, VehicleEquipmentLink


class VehicleForm(GafcModelForm):
    """
    Add a vehicle form
    """
    
    what = "vehicle_save"
        
    class Meta:
        model = Vehicle
        fields = ('name', 'cost_per_km', 'default_pay', 'display')
        
class VehicleLinkForm(GafcModelForm):
    """
    Link Vehicle and EquipmentMeta objects
    """

    what = "vehiclelink_save"

    class Meta:
        model=VehicleEquipmentLink
        fields = ('equipment','vehicle_type','km_start','km_end')
        
    def clean_km_start(self):
        """
        Check that EquipmentMeta belong to the vehicle
        """
        if self.cleaned_data['equipment'] != self.cleaned_data['km_start'].equipment:
            raise forms.ValidationError('Le champ méta n\'appartient pas à l\'équipement')
        
        return self.cleaned_data['km_start']
        
    def clean_km_end(self):
        """
        Check that EquipmentMeta belong to the vehicle
        """
        if self.cleaned_data['equipment'] != self.cleaned_data['km_end'].equipment:
            raise forms.ValidationError('Le champ méta n\'appartient pas à l\'équipement')
        
        return self.cleaned_data['km_end']
    
class StatisticForms(GafcForm):
    """
    Generate statistics between 2 dates
    """
     
    what = 'statistics_calc'
    date_from = forms.DateField(widget=MyDateInput(), label="Depuis")
    date_to = forms.DateField(widget=MyDateInput(), label="Jusqu'à")
    
    def __init__(self, *args, **kwargs):
        """
        Initialize the form
        
        :param \\*args: Extra arguments passed to parent
        :param \\*\\*kwargs: Extra arguments passed to parent
        """
        super().__init__(*args, **kwargs)
        
        td = dt.date.today()
        self.fields['date_to'].initial = td
        self.fields['date_from'].initial   = dt.date(td.year if td.month > 9 else td.year-1, 9, 1)
        
    def clean(self):
        """
        Check date from/to consistency
        """
        if self.cleaned_data['date_from'] > self.cleaned_data['date_to']:
            self.add_error('date_to', 'La date de fin doit être supérieure à la date de début')
        
