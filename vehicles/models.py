# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont


from django.db import models
from gafc import models as gm
from django.utils import timezone

# Create your models here.

class Vehicle(models.Model):
    """
    This model represent a vehicle, with a name and a cost per kilometer.
    """
    
    # Fields
    name            = models.CharField(max_length=250, verbose_name="Nom")
    cost_per_km     = models.DecimalField(decimal_places=2, max_digits=10, verbose_name="Coût kilométrique")
    default_pay     = models.BooleanField(default=False, verbose_name="Le conducteur paie")
    display         = models.BooleanField(default=True, verbose_name="Afficher dans le formulaire de calcul")
    
    def __str__(self):
        return '%s'%self.name
    
    class Meta:
        permissions = [('financial','Can view vehicle financial resume'),]
    
    
class VehicleEquipmentLink(models.Model):
    """
    This model make a link between vehicle and equipment. This allow to check the payment for each vehicles on associated category
    """
    
    vehicle_type    = models.ForeignKey(Vehicle, on_delete=models.PROTECT, verbose_name="Type")
    equipment       = models.ForeignKey(gm.Equipment, on_delete=models.CASCADE, verbose_name="Équipement")
    km_start        = models.ForeignKey(gm.EquipmentMeta, on_delete=models.PROTECT, related_name="link_km_start", verbose_name="Km départ")
    km_end          = models.ForeignKey(gm.EquipmentMeta, on_delete=models.PROTECT, related_name="link_km_end", verbose_name="Km arrivée")
    
    def save(self, *args, **kwargs):
        
        if self.km_start.equipment != self.equipment or self.km_end.equipment != self.equipment:
            raise ValueError("Related meta do not belong to equipement!")

        super().save(*args, **kwargs)
        
class VehicleTrackingCheck(models.Model):
    """
    This is a check of tracking by the responsible
    """
    
    person          = models.ForeignKey(gm.Person, on_delete=models.CASCADE, verbose_name="Vérificateur")
    tracking        = models.ForeignKey(gm.EquipmentTracking, on_delete=models.CASCADE, verbose_name="Tracking")
    date            = models.DateField(default=timezone.now, verbose_name="Date")
        
    class Meta:
        unique_together = (('person','tracking'))
