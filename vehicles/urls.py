# -*- coding: utf-8 -*-
#
# This file is part of GAFC.
#
# GAFC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# GAFC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with GAFC. If not, see <https://www.gnu.org/licenses/>. 
#
# 2022 - William Chèvremont 


from django.urls import path
from . import views

urlpatterns = [
    path('', views.CalcFeeView.as_view(), name='vehicle_home'),
    
    path('view', views.VehicleTableView.as_view(), name='vehicle_view'),
    path('view/<str:action>/<int:tid>', views.VehicleTableView.as_view(), name="vehicle_view_action"),
    path('view/<int:year>', views.VehicleTableView.as_view(), name="vehicle_view_year"),
    
    path('financial', views.VehicleFinancialView.as_view(), name="vehicle_financial"),
    path('settings', views.VehicleSettingView.as_view() , name="vehicle_settings"),
    path('statistics', views.VehicleStatisticView.as_view(), name="vehicle_statistics"),
]
